<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tender extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    protected $uk_parent = '';

    public function __construct() {
        parent::__construct();
        $this->load->model('M_hris', 'dbhris');
        $this->load->model("erf/M_request", "req");
        $this->load->model("eat/M_assign", "assig");
        $this->load->model("dok_eng/M_dok_eng", "dok_eng");
        $this->load->model("dok_eng/M_tender", "tender");
        $this->load->model("bast/M_bast", "bast");
        $this->load->model("others/M_approval", "app");
    }

    public function index() {
        $session = $this->session->userdata('logged_in');
        $info = (array) $session;
        if (empty($info['username'])) {
            redirect('login');
        }

        $header["tittle"] = "Tender Documents";
        $header["short_tittle"] = "tnd";
        // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        // view roles
        $this->db->distinct();
        $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        $header['roles'] = json_decode(json_encode($header['roles']), true);
        $state = '';
        foreach ($header['roles'] as $i => $val) {
            $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
            $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
            if ($val['code'] == $header["short_tittle"]) {
                // code...
                $state = $val['stat'];
            }
            // get count pending status
            if (isset($val['tabel'])) {
                $npending = 0;
                if ($info['special_access'] == 'true') {
                    $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                } else {
                    if (strpos($val['stat'], "-child") !== false) {
                        if (strpos($val['stat'], "{$val['code']}-child=1") !== false) {
                            $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
                            $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                        }
                    } else {
                        $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans'], $info['uk_kode']);
                    }
                }
                $header['roles'][$i]['count'] = $npending;
            }
        }
        // echo "<pre>";
        // print_r($header['roles']);
        // echo "</pre>";
        // exit;
        // set auto increment trans no
        // $where = "A.ID=(SELECT MAX(ID) FROM MPE_DOK_ENG) AND NO_DOK_ENG LIKE '%DE-%'";
        // $trans = $this->tender->get_record($where);;
        // echo "<pre>";
        // print_r($info);
        // echo "</pre>";
        // exit;
        // set list approval hris
        // $where['GEN_VAL']='1';
        // $where['GEN_CODE']='EAT_PENGKAJI_UK';
        // $where['GEN_TYPE']='General';
        // $global = $this->general_model->get_data($where);
        // $uk_parent = $global->GEN_PAR5;

        $strc = "
            k.muk_kode = '{$info['uk_kode']}' AND (k.mk_eselon_code='30' OR k.mk_eselon_code='40')
            ";
        $str = "
            k.mdept_kode = '{$info['dept_code']}' AND (k.mk_eselon_code='30' OR k.mk_eselon_code='20')
            ";
        $str2 = "
            k.mdept_kode = '{$info['dept_code']}' AND k.mk_eselon_code='20'
            ";
        $str3 = "
            k.mdept_kode = '{$info['dept_code']}' AND k.mk_eselon_code<'20'
            ";
        $yyyy = date("Y");
        $xxxx = $this->tender->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data['no'] = "///TD/{$kodemax}//{$yyyy}";
        $data['LIST_CRT'] = $this->dbhris->get_hris_where($strc);
        // mgr & sm
        $data['LIST_MNG'] = $this->dbhris->get_hris_where($str);
        $data['LIST_APP'] = $this->dbhris->get_level_from_array($info['no_badge'], 'DEPT');
        // $data['LIST_GM'] = $this->dbhris->get_level_from_dept($data['LIST_APP'][0]->NOBADGE);
        if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
            $this->db->where("PE.UK_CODE = '{$info['uk_kode']}'");
        }
        $data['foreigns'] = $this->tender->get_foreign($info['uk_kode']);
        // echo "<pre>";
        // print_r($data['LIST_APP']);
        // echo "</pre>";
        // exit;
        // $data['DEPT'] = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
        // $data['BIRO'] = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
        // $data['FOREIGN'] = $this->tender->get_foreign();
        $header['page_state'] = $state;

        $sHdr = '';
        if (isset($info['themes'])) {
            if ($info['themes'] != 'default')
                $sHdr = "_" . $info['themes'];
        }else {
            unset($where);
            $where['GEN_CODE'] = 'themes-app';
            $where['GEN_TYPE'] = 'General';
            $global = (array) $this->general_model->get_data($where);
            if (isset($global) && $global['GEN_VAL'] != '0') {
                $i_par = $global['GEN_VAL'];
                $sHdr = "_" . $global['GEN_PAR' . $i_par];
            }
        }
        if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('dok_eng/dok_tender', $data);
        } else {
            $header["tittle"] = "Forbidden";
            $header["short_tittle"] = "403";

            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('forbidden');
        }
        // $this->load->view('general/header', $header);
        // $this->load->view('dok_eng/dok_engineer', $data);
        $this->load->view('general/footer');
    }

    public function data_list() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if (isset($info)) {
            $search = $this->input->post('search');
            $order = $this->input->post('order');

            $key = array(
                'search' => $search['value'],
                'ordCol' => $order[0]['column'],
                'ordDir' => $order[0]['dir'],
                'length' => $this->input->post('length'),
                'start' => $this->input->post('start')
            );

            if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
                $key['name'] = $info['name'];
                $key['username'] = $info['username'];
                $key['dept_code'] = $info['dept_code'];
                $key['uk_code'] = $info['uk_kode'];
            }

            $data = $this->tender->get_data($key);
            // echo $this->db->last_query();

            $return = array(
                'draw' => $this->input->post('draw'),
                'data' => $data,
                'recordsFiltered' => $this->tender->recFil($key),
                'recordsTotal' => $this->tender->recTot($key)
            );

            echo json_encode($return);
        }
    }

    public function foreign() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if (isset($info['uk_kode'])) {
            if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
                $this->db->where("PE.UK_CODE = '{$info['uk_kode']}'");
            }
            $result = $this->tender->get_foreign();

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function foreign_dtl($f_id) {
        if (isset($f_id)) {
            $result = $this->tender->get_dtl_foreign($f_id);

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function atasan($level) {
        $this->load->model('M_hris', 'dbhris');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $result = $this->dbhris->get_level_from($info['no_badge'], $level);
        // $this->db->last_query();
        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function send_mail($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');

        // $info = $this->session->userdata;
        // $info = $info['logged_in'];

        list($badge, $nama) = split(' - ', $atas);
        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$badge}'");
        $peg = (array) $peg[0];

        $e = str_replace('/', '6s4', $e);
        $e = str_replace('+', '1p4', $e);
        $e = str_replace('=', 'sM9', $e);
        $param['short'] = 'DT';
        $param['link'] = 'tender';
        $param['title'] = 'Document Tender';
        if (isset($param['CREATE_BY'])) {
            $param['sender'] = "{$param['CREATE_BY']}";
        } else {
            $param['sender'] = "{$param['UPDATE_BY']}";
        }
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['title']} :</b> {$param['NO_DOK_TEND']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Paket :</b> {$param['PACKET_TEXT']}";
        // $param['TRN']['LOKASI']="<b>Progress :</b> {$param['PROGRESS']}%";
        $mailto = str_replace("SIG.ID", "SEMENINDONESIA.COM", $to);
        $tomail = str_replace("sig.id", "SEMENINDONESIA.COM", $mailto);

        $this->email($this->template->set_app($param), $subject, '', $tomail, $cc);


        // $message="Dengan Hormat $atas,<br /><br />
        // Mohon untuk ditindak lanjuti approval project berikut :<br />
        // No Notifikasi    : " . $param['NOTIFIKASI'] . "<br/>
        // No Dokumen       : " . $param['NO_DOK_ENG'] . "<br/>
        // Nama Paket       : " . $param['PACKET_TEXT'] . "<br/> <br/> <br/>
        //
    //
    // Click link berikut untuk lebih lanjut <a href='" . base_url('action/dok/  '. $e) . "'> APPROVAL DOCUMENTS </a> <br/><br/><br/>
        //
    // Demikian, terima kasih";
        // $this->email($message, $subject, '', $to, $cc);
    }

    public function send_trm($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        list($badge, $nama) = split(' - ', $atas);
        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$badge}'");
        $peg = (array) $peg[0];
        // print_r($badge);

        $e = str_replace('/', '6s4', $e);
        $e = str_replace('+', '1p4', $e);
        $e = str_replace('=', 'sM9', $e);
        $param['short'] = 'DE';
        $param['link'] = 'eng/tends';
        $param['title'] = 'Document Tender';
        if (isset($param['CREATE_BY'])) {
            $param['sender'] = "{$param['CREATE_BY']}";
        } else {
            $param['sender'] = "{$param['UPDATE_BY']}";
        }
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['title']} :</b> {$param['NO_DOK_ENG']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Paket :</b> {$param['PACKET_TEXT']}";
        // $param['TRN']['LOKASI']="<b>Progress :</b> {$param['PROGRESS']}%";
        // echo '<pre>';
        // print_r($param);
        // exit;
        $this->email($this->template->conf_trm($param), $subject, '', $to, $cc);


        // $message="Dengan Hormat $atas,<br /><br />
        // Mohon untuk ditindak lanjuti approval project berikut :<br />
        // No Notifikasi    : " . $param['NOTIFIKASI'] . "<br/>
        // No Dokumen       : " . $param['NO_DOK_ENG'] . "<br/>
        // Nama Paket       : " . $param['PACKET_TEXT'] . "<br/> <br/> <br/>
        //
    //
    // Click link berikut untuk lebih lanjut <a href='" . base_url('action/dok/  '. $e) . "'> APPROVAL DOCUMENTS </a> <br/><br/><br/>
        //
    // Demikian, terima kasih";
        // $this->email($message, $subject, '', $to, $cc);
    }

    public function resend() {
        if (isset($_POST['ID'])) {
            error_reporting(1);

            $param = $_POST;
            // $sap_result = $this->create_notif($param);
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data = $this->tender->get_record("A.ID='{$param['ID']}'");
            $data = $data[0];
            // echo $this->db->last_query();
            // exit;
            // print_r($data);

            if ($data) {
                $dtApp = $this->app->get_row("REF_ID = '{$param['ID']}' AND REF_TABEL = 'DOK_TEND' AND STATE = 'New' AND DELETE_AT IS NULL");
                $to = array($dtApp['EMAIL']);
                $subject = "Resend : DOCUMENT TENDER APPROVAL REQUEST";
                $n_form = $this->my_crypt("{$dtApp['REF_ID']};{$dtApp['LVL']};{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};Paraf;{$dtApp['APP_BADGE']} - {$dtApp['APP_NAME']}", 'e');
                $this->send_mail($subject, $data, "{$dtApp['APP_BADGE']} - {$dtApp['APP_NAME']}", $n_form, $to, $cc);

                $res_message = 'Email has been sent' . '-';
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function create() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if (isset($_POST) && isset($_POST['NO_DOK_TEND']) && isset($_POST['ID_DOK_ENG']) && isset($_POST['PACKET_TEXT']) && isset($info)) {
            $param = $_POST;
            $arApp = array();
            $arApp[] = $_POST['LIST_CRT'];
            unset($param['LIST_CRT']);
            $arApp[] = $_POST['LIST_PARAF'];
            unset($param['LIST_PARAF']);
            $arApp[] = $_POST['LIST_APP'];
            unset($param['LIST_APP']);
            if (isset($_POST['LIST_GM'])) {
                $arApp[] = $_POST['LIST_GM'];
                unset($param['LIST_GM']);
            }

            $param['NO_DOK_TEND'] = $this->generateNo('NO_DOK_TEND', 'MPE_DOK_TEND', 'TD', $param['NO_DOK_TEND']);
            $param['CREATE_BY'] = $info['name'];
            $param['COMPANY'] = $info['company'];
            $param['DEPT_CODE'] = $info['dept_code'];
            $param['DEPT_TEXT'] = $info['dept_text'];
            $param['UK_CODE'] = $info['uk_kode'];
            $param['UK_TEXT'] = $info['unit_kerja'];
            $cdata = explode(' - ', $_POST['LIST_CRT']);
            $param['APPROVE1_BY'] = $cdata[1];
            $param['APPROVE1_JAB'] = $cdata[3];
            $cdata = explode(' - ', $_POST['LIST_PARAF']);
            $param['APPROVE2_BY'] = $cdata[1];
            $param['APPROVE2_JAB'] = $cdata[3];
            $cdata = explode(' - ', $_POST['LIST_APP']);
            $param['APPROVE3_BY'] = $cdata[1];
            $param['APPROVE3_JAB'] = $cdata[3];
            if (isset($_POST['LIST_GM'])) {
                $cdata = explode(' - ', $_POST['LIST_GM']);
                $param['APPROVE4_BY'] = $cdata[1];
                $param['APPROVE4_JAB'] = $cdata[3];
            }
            $param['STATUS'] = 'In Approval';
            if ($param['RKS_FILE']) {
                $this->db->set('RKS_AT', "CURRENT_DATE", false);
            }
            if ($param['BQ_FILE']) {
                $this->db->set('BQ_AT', "CURRENT_DATE", false);
            }
            if ($param['DRAW_FILE']) {
                $this->db->set('DRAW_AT', "CURRENT_DATE", false);
            }
            if ($param['ECE_FILE']) {
                $this->db->set('ECE_AT', "CURRENT_DATE", false);
            }

            $result = $this->tender->save($param);

            if ($result['REF_ID']) {
                if (count($arApp) > 0) {
                    for ($i = 0; $i < count($arApp); $i++) {
                        $adata = explode(' - ', $arApp[$i]);
                        $tmpdata['REF_ID'] = $result['REF_ID'];
                        $tmpdata['REF_TABEL'] = 'DOK_TEND';
                        if ($i == 0) {
                            $tmpdata['TIPE'] = 'CREATED';
                        } elseif ($i == 1) {
                            $tmpdata['TIPE'] = 'SM';
                        } elseif ($i == 2) {
                            $tmpdata['TIPE'] = 'GM';
                        } elseif ($i == 3) {
                            $tmpdata['TIPE'] = 'GM';
                        }
                        $tmpdata['LVL'] = $i + 1;
                        $tmpdata['STATE'] = 'New';
                        $tmpdata['APP_BADGE'] = $adata[0];
                        $tmpdata['APP_NAME'] = $adata[1];
                        $tmpdata['APP_JAB'] = $adata[3];
                        $tmpdata['APP_EMAIL'] = $adata[2];
                        $tmpdata['NOTE'] = '';
                        $tmpdata['COMPANY'] = $info['company'];
                        $tmpdata['CREATE_BY'] = $info['name'];

                        $this->app->save($tmpdata);

                        if ($i == 0) {
                            // send email
                            $n_form = $this->my_crypt("{$tmpdata['REF_ID']};{$tmpdata['LVL']};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$tmpdata['TIPE']};{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", 'e');

                            $to = array($tmpdata['APP_EMAIL']);
                            $cc = array();
                            $subject = "TENDER APPROVAL REQUEST";
                            $this->send_mail($subject, $param, "{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", $n_form, $to, $cc);
                        }

                        unset($adata);
                        unset($tmpdata);
                    }
                }

                $res_message = 'Data has been saved' . '-';
                if (!$result['REF_ID']) {
                    // if($result != 1){
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function update() {
        if (isset($_POST) && isset($_POST['ID']) && isset($_POST['NO_DOK_TEND']) && isset($_POST['ID_DOK_ENG']) && isset($_POST['PACKET_TEXT'])) {
            $param = $_POST;

            $arApp = array();
            $arApp[] = $_POST['LIST_CRT'];
            unset($param['LIST_CRT']);
            $arApp[] = $_POST['LIST_PARAF'];
            unset($param['LIST_PARAF']);
            $arApp[] = $_POST['LIST_APP'];
            unset($param['LIST_APP']);
            if (isset($_POST['LIST_GM'])) {
                $arApp[] = $_POST['LIST_GM'];
                unset($param['LIST_GM']);
            }
            // echo "<pre>";
            // print_r($param);
            // print_r($arApp);
            // echo "</pre>";
            // exit;

            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $param['UPDATE_BY'] = $info['username'];
            $param['COMPANY'] = $info['company'];

            $result = $this->tender->updateData('update', $param);
            // echo $this->db->last_query();

            if (count($arApp) > 0) {
                // delete data
                unset($delParam);
                $delParam['REF_ID'] = $_POST['ID'];
                $delParam['REF_TABEL'] = 'DOK_TEND';
                $delParam['DELETE_BY'] = $info['name'];
                $this->app->deleteData($delParam);

                for ($i = 0; $i < count($arApp); $i++) {
                    $adata = explode(' - ', $arApp[$i]);
                    $tmpdata['REF_ID'] = $_POST['ID'];
                    $tmpdata['REF_TABEL'] = 'DOK_TEND';
                    if ($i == 0) {
                        $tmpdata['TIPE'] = 'CREATED';
                    } elseif ($i == 1) {
                        $tmpdata['TIPE'] = 'SM';
                    } elseif ($i == 2) {
                        $tmpdata['TIPE'] = 'GM';
                    } elseif ($i == 3) {
                        $tmpdata['TIPE'] = 'GM';
                    }
                    $tmpdata['LVL'] = $i + 1;
                    $tmpdata['STATE'] = 'New';
                    $tmpdata['APP_BADGE'] = $adata[0];
                    $tmpdata['APP_NAME'] = $adata[1];
                    $tmpdata['APP_JAB'] = $adata[3];
                    $tmpdata['APP_EMAIL'] = $adata[2];
                    $tmpdata['NOTE'] = '';
                    $tmpdata['COMPANY'] = $info['company'];
                    $tmpdata['CREATE_BY'] = $info['name'];

                    $this->app->save($tmpdata);

                    if ($i == 0) {
                        // send email
                        $n_form = $this->my_crypt("{$tmpdata['REF_ID']};{$tmpdata['LVL']};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$tmpdata['TIPE']};{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", 'e');

                        $to = array($tmpdata['APP_EMAIL']);
                        $cc = array();
                        $subject = "TENDER APPROVAL REQUEST";
                        $this->send_mail($subject, $param, "{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", $n_form, $to, $cc);
                    }

                    unset($adata);
                    unset($tmpdata);
                }
            }

            // if ($LIST_CRT) {
            //   // delete data
            //   unset($delParam);
            //   $delParam['REF_ID'] = $_POST['ID'];
            //   $delParam['TIPE'] = 'CRT';
            //   $delParam['DELETE_BY'] = $info['name'];
            //   $this->app->deleteData($delParam);
            //
      //   $tmparr = explode(':', $LIST_CRT);
            //   for ($i=0; $i<count($tmparr) ; $i++) {
            //     $adata=explode(' - ',$tmparr[$i]);
            //     $tmpdata['REF_ID']=$_POST['ID'];
            //     $tmpdata['REF_TABEL']='DOK_TEND';
            //     $tmpdata['TIPE']='CRT';
            //     $tmpdata['LVL']=$i+1;
            //     $tmpdata['STATE']='New';
            //     $tmpdata['APP_BADGE']=$adata[0];
            //     $tmpdata['APP_NAME']=$adata[1];
            //     $tmpdata['APP_JAB']=$adata[3];
            //     $tmpdata['APP_EMAIL']=$adata[2];
            //     $tmpdata['STATUS']='In Approval';
            //     $tmpdata['NOTE']='';
            //     $tmpdata['COMPANY']=$info['company'];
            //     $tmpdata['CREATE_BY']=$info['name'];
            //
      //     $this->app->save($tmpdata);
            //
      //     // send email
            //     $n_form=$this->my_crypt("{$tmpdata['REF_ID']};{$tmpdata['LVL']};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$tmpdata['TIPE']};{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}",'e');
            //     // $n_form = str_replace('/', '6s4', $n_form);
            //     // $n_form =  str_replace('+', '1p4', $n_form);
            //     // $n_form =  str_replace('=', 'sM9', $n_form);
            //
      //     $to=array($tmpdata['APP_EMAIL']);
            //     $cc=array();
            //     $subject="CREATE APPROVAL REQUEST";
            //     $this->send_mail($subject, $param, "{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", $n_form, $to, $cc);
            //
      //     unset($adata);
            //     unset($tmpdata);
            //   }
            // }
            // if ($LIST_PARAF) {
            //   // delete data
            //   unset($delParam);
            //   $delParam['REF_ID'] = $_POST['ID'];
            //   $delParam['TIPE'] = 'Paraf';
            //   $delParam['DELETE_BY'] = $info['name'];
            //   $this->app->deleteData($delParam);
            //
      //   $tmparr = explode(':', $LIST_PARAF);
            //   for ($i=0; $i<count($tmparr) ; $i++) {
            //     $adata=explode(' - ',$tmparr[$i]);
            //     $tmpdata['REF_ID']=$_POST['ID'];
            //     $tmpdata['REF_TABEL']='DOK_TEND';
            //     $tmpdata['TIPE']='Paraf';
            //     $tmpdata['LVL']=$i+1;
            //     $tmpdata['STATE']='New';
            //     $tmpdata['APP_BADGE']=$adata[0];
            //     $tmpdata['APP_NAME']=$adata[1];
            //     $tmpdata['APP_JAB']=$adata[3];
            //     $tmpdata['APP_EMAIL']=$adata[2];
            //     $tmpdata['STATUS']='In Approval';
            //     $tmpdata['NOTE']='';
            //     $tmpdata['COMPANY']=$info['company'];
            //     $tmpdata['CREATE_BY']=$info['name'];
            //
      //     $this->app->save($tmpdata);
            //
      //     unset($adata);
            //     unset($tmpdata);
            //   }
            // }
            // if ($LIST_APP) {
            //   // delete data
            //   unset($delParam);
            //   $delParam['REF_ID'] = $_POST['ID'];
            //   $delParam['TIPE'] = 'TTD';
            //   $delParam['DELETE_BY'] = $info['name'];
            //   $this->app->deleteData($delParam);
            //
      //   $tmparr = explode(':', $LIST_APP);
            //   for ($i=0; $i<count($tmparr) ; $i++) {
            //     $adata=explode(' - ',$tmparr[$i]);
            //     $tmpdata['REF_ID']=$_POST['ID'];
            //     $tmpdata['REF_TABEL']='DOK_TEND';
            //     $tmpdata['TIPE']='TTD';
            //     $tmpdata['LVL']=$i+1;
            //     $tmpdata['STATE']='New';
            //     $tmpdata['APP_BADGE']=$adata[0];
            //     $tmpdata['APP_NAME']=$adata[1];
            //     $tmpdata['APP_JAB']=$adata[3];
            //     $tmpdata['APP_EMAIL']=$adata[2];
            //     $tmpdata['STATUS']='In Approval';
            //     $tmpdata['NOTE']='';
            //     $tmpdata['COMPANY']=$info['company'];
            //     $tmpdata['CREATE_BY']=$info['name'];
            //
      //     $this->app->save($tmpdata);
            //     unset($adata);
            //     unset($tmpdata);
            //   }
            // }


            if ($result) {

                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function approve() {
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];
            // $e =  str_replace(' ', '+', $e);
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');
            list($id, $lvl, $notifikasi, $paket, $tipe, $nama) = split(";", $no_form);
            $pgw = explode(' - ', $nama);

            $list_data = $this->tender->get_record("A.ID='{$id}'");
            $list_data = $list_data[0];

            $param['NOTIFIKASI'] = $notifikasi;
            $param['PACKET_TEXT'] = $paket;
            $param['NO_DOK_TEND'] = $list_data['NO_DOK_TEND'];
            // $param['PROGRESS'] = $list_data['PROGRESS'];
            $param['CREATE_BY'] = $list_data['CREATE_BY'];

            // echo "<pre>";
            // print_r($id);
            // print_r($list_data);
            // echo "</pre>";
            // exit;
            //$data = $this->tender->get_record("A.ID = ".$akey[0]);

            $reason = $_POST['reason'];

            // update approval
            unset($data); // clear array
            $data['REF_ID'] = $id;
            $data['REF_TABEL'] = 'DOK_TEND';
            $data['TIPE'] = $tipe;
            $data['LVL'] = $lvl;
            $data['APP_NAME'] = $pgw[1];
            $data['STATE'] = 'Approved';
            $data['STATUS'] = 'Approved';
            $data['UPDATE_BY'] = $pgw[1];
            $data['NOTE'] = $reason;
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $result = $this->app->updateData('updatestatus', $data);

            // update transaction (tender)
            $dtTrans['ID'] = $id;
            $dtTrans['STATUS'] = $tipe . ' Approved';
            $dtTrans['APPROVE' . $lvl . '_BY'] = $pgw[1];
            $this->db->set('APPROVE' . $lvl . '_AT', "CURRENT_DATE", false);
            $result = $this->tender->updateData('approve', $dtTrans);

            // get next approval
            unset($where);
            $where['REF_ID'] = $id;
            $where['REF_TABEL'] = 'DOK_TEND';
            $where['STATE'] = 'New';
            $where['DELETE_AT'] = NULL;
            $nApp = $this->app->get_row($where);

            if (count($nApp)) {
                // set status in approval
                $tmpdata['REF_ID'] = $nApp['REF_ID'];
                $tmpdata['REF_TABEL'] = 'DOK_TEND';
                $tmpdata['TIPE'] = $nApp['TIPE'];
                $tmpdata['LVL'] = $nApp['LVL'];
                $tmpdata['STATE'] = 'New';
                $tmpdata['STATUS'] = 'In Approval';
                $update = $this->app->updateData('updatestatus', $tmpdata);

                //send mail (next approval)
                $n_form = $this->my_crypt("{$nApp['REF_ID']};{$nApp['LVL']};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$nApp['TIPE']};{$nApp['APP_BADGE']} - {$nApp['APP_NAME']}", 'e');
                $to = array($nApp['APP_EMAIL']);
                $cc = array();
                $subject = $nApp['TIPE'] . " DOCUMENTS TENDER - APPROVAL REQUEST";
                $this->send_mail($subject, $param, "{$nApp['APP_BADGE']} - {$nApp['APP_NAME']}", $n_form, $to, $cc);
            }

            if ($result) {
                $result2['message'] = "Documents Paket '{$paket}' was approved succesfully" . $pgw[1];
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function reject() {
        $e = $_POST['e'];
        // $e =  str_replace(' ', '+', $e);
        $e = str_replace('6s4', '/', $e);
        $e = str_replace('1p4', '+', $e);
        $e = str_replace('sM9', '=', $e);
        $no_form = $this->my_crypt($e, 'd');
        list($id, $lvl, $notifikasi, $paket, $tipe, $nama) = split(";", $no_form);
        $pgw = explode(' - ', $nama);

        $list_data = $this->tender->get_record("A.ID='{$id}'");
        $list_data = $list_data[0];

        // echo "<pre>";
        // print_r($list_data['NO_DOK_ENG']);
        // echo "</pre>";
        //$data = $this->tender->get_record("A.ID = ".$akey[0]);

        $reason = $_POST['reason'];

        // update approval
        unset($data); // clear array


        if ($tipe == 'Paraf' || $tipe == 'TTD') {
            $data['ID'] = $id;
            $data['TIPE'] = $tipe;
            $data['APP_NAME'] = $pgw[1];
            $data['STATE'] = 'Rejected';
            $data['STATUS'] = 'Rejected';
            $data['UPDATE_BY'] = $pgw[1];
            $data['NOTE'] = $reason;
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $result = $this->app->updateData('approve', $data);
        } else {
            // code...
            $data['ID'] = $id;
            $data['STATUS'] = 'Rejected';
            $this->db->set('REJECT_REASON', $reason);
            $this->db->set('REJECT_BY', $pgw[0]);
            $this->db->set('REJECT_DATE', "CURRENT_DATE", false);
            $result = $this->tender->updateData('approve', $data);
        }
        // $data['APPROVE_BY']=$nama;


        if ($result) {
            // $ar_form = explode(" - ",$no_form);
            //$param = array(
            //    'STATUS' => 'REL',
            //    'NOTIF_NO' => $notifikasi
            //);
            //$del_sap = $this->sap_del->deletenotif($param);
            // echo $this->response('success', 200, $result);
            // echo "ERF '{$no_form}' was approved succesfully";
            // echo "ERF '{$no_form}' ({$this->my_crypt('letmein','e')}) was approved succesfully";
            $result2['message'] = 'success';
            $result2['status'] = '200';
            echo json_encode($result2);
        } else {
            $result2['message'] = 'fail';
            $result2['status'] = '400';
            echo json_encode($result2);
        }
    }

    public function delete($id) {
        if ($id) {
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data['ID'] = $id;
            $data['DELETE_BY'] = $info['name'];
            $result = $this->tender->updateData('delete', $data);

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function export_pdf($id, $tipe = 'All') {
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if ($id && isset($info)) {
            // error_reporting(0); // -1
            // ini_set('display_errors', 0); //1
            // $list_data = $_POST;
            $list_data = $this->tender->get_record("A.ID='{$id}'");
            $list_data = $list_data[0];
            // get approval log
            $this->db->order_by('LVL', 'ASC');
            $dtApp = $this->app->get_data("REF_ID = '{$id}' AND REF_TABEL='DOK_TEND'");
            // get creator info
            // $dtCre = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['CREATE_BY']}' AND k.company = '{$list_data['COMPANY']}' AND (uk.muk_parent LIKE '".$global->GEN_PAR5."' OR k.muk_kode = '".$global->GEN_PAR5."')");
            // if ($dtCre) {
            //   $dtCre = $dtCre[0];
            // }
            // echo "<pre>";
            // print_r($dtCrt);
            // print_r($dtPar);
            // print_r($dtCrt);
            // print_r($global);
            // echo "</pre>";
            // exit;

            $uri = base_url();
            $this->load->library('Fpdf_gen');
            $this->fpdf->SetFont('Arial', 'B', 13);
            $this->fpdf->SetLeftMargin(20);

            // $this->fpdf->Cell(12,4,'',0,0);
            $this->fpdf->Ln(15);
            $this->fpdf->Cell(35, 6, $this->fpdf->Image($uri . 'media/logo/Logo_SI.png', $this->fpdf->GetX() + 7, $this->fpdf->GetY(), 0, 15, 'PNG'), 'LT', 0, 'C');
            $this->fpdf->Cell(100, 6, "PT. SEMEN INDONESIA (PERSERO)Tbk.", 'T', 0, 'C');
            $this->fpdf->Cell(35, 6, $this->fpdf->Image($uri . 'media/logo/si_group.jpg', $this->fpdf->GetX() + 3, $this->fpdf->GetY() + 4, 0, 10, 'JPG'), 'TR', 1, 'C');
            $this->fpdf->SetFont('Arial', '', 12);
            $this->fpdf->Cell(170, 6, "{$list_data['DEPT_TEXT']}", 'LR', 1, 'C');
            if ($list_data['DEPT_CODE'] != $list_data['UK_CODE']) {
                $this->fpdf->Cell(170, 6, "{$list_data['UK_TEXT']}", 'LBR', 1, 'C');
            } else {
                $this->fpdf->Cell(170, 6, "", 'LBR', 1, 'C');
            }

            $this->fpdf->Cell(170, 6, "", 'LBR', 1);
            if ($list_data['COLOR'] == 'Merah') {
                $this->fpdf->SetFillColor(204, 255, 204);
            } elseif ($list_data['COLOR'] == 'Kuning') {
                $this->fpdf->SetFillColor(230, 230, 0);
            } elseif ($list_data['COLOR'] == 'Biru') {
                $this->fpdf->SetFillColor(200, 220, 255);
            } elseif ($list_data['COLOR'] == 'Hijau') {
                $this->fpdf->SetFillColor(66, 244, 143);
            } else {
                $this->fpdf->SetFillColor(239, 239, 245);
            }
            $this->fpdf->Cell(50, 6, " No. Dokumen", 'LT', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 'T', 0, 'C', 1);
            $this->fpdf->Cell(110, 6, "{$list_data['NO_DOK_TEND']}", 'TR', 1, 'L', 1);
            $this->fpdf->Cell(50, 6, " Notifikasi", 'L', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 0, 0, 'C', 1);
            $this->fpdf->Cell(110, 6, "{$list_data['NOTIFIKASI']}", 'R', 1, 'L', 1);
            $this->fpdf->Cell(50, 6, " Tahun", 'BL', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 'B', 0, 'C', 1);
            $time = strtotime($list_data['CREATE_AT']);
            $year = date('Y', $time);
            $this->fpdf->Cell(110, 6, "{$year}", 'BR', 1, 'L', 1);
            // $this->fpdf->Cell(110,6,"{$list_data['CREATE_AT']}",'R',1,'L',1);
            // $this->fpdf->SetFont('Arial','B',7);
            // $this->fpdf->Cell(170,1,'R/538/012',0,1,'R');
            // $this->fpdf->Cell(0,9,'',0,1);
            // $this->fpdf->SetFont('Arial','B',7);
            // $this->fpdf->Cell(64,1,'PT. SEMEN INDONESIA (PERSERO)Tbk.',0,0);
            $this->fpdf->Cell(170, 6, "", 'LBR', 1);

            $this->fpdf->SetFont('Arial', 'BU', 18);
            $this->fpdf->Cell(12, 4, '', 0, 2);
            $this->fpdf->Ln(5);
            $this->fpdf->Cell(170, 6, "DOKUMEN TENDER", 0, 1, 'C');
            $this->fpdf->Ln(25);
            $this->fpdf->SetFont('Arial', 'B', 18);
            $this->fpdf->Ln(10);
            $sJudul = '';
            $sTitle = '';
            if (($tipe == 'TR')) {
                $sJudul = 'Term of Refference (TOR)';
                $sTitle = 'Tor';
            } else if (($tipe == 'BQ')) {
                $sJudul = 'Bill of Quantity (BQ)';
                $sTitle = 'BQ';
            } else if (($tipe == 'DRW')) {
                $sJudul = 'Drawing';
                $sTitle = 'Drawing';
            } else if (($tipe == 'EC')) {
                $sJudul = 'Engineering Cost Estimate (ECE)';
                $sTitle = 'ECE';
            } else {
                $sJudul = "Dokumen Tender\n(TOR,BQ dan Drawing)";
            }
            $this->fpdf->MultiCell(170, 8, strtoupper($sJudul), 0, 'C');
            $this->fpdf->Ln(10);
            $this->fpdf->MultiCell(170, 8, "{$list_data['PACKET_TEXT']}", 0, 'C');

            $this->fpdf->SetFont('Arial', 'B', 8);
            if (strlen($list_data['PACKET_TEXT']) < 45) {
                $this->fpdf->Ln(35);
            } elseif (strlen($list_data['PACKET_TEXT']) < 90) {
                $this->fpdf->Ln(25);
            } else {
                $this->fpdf->Ln(5);
            }
            $this->fpdf->Cell(12, 4, '', 0, 2);
            $this->fpdf->Cell(170, 5, "Pengesahan", 1, 0, 'L');

            include(APPPATH . 'libraries/phpqrcode/qrlib.php');

            // approval info
            $this->fpdf->SetFont('Arial', 'B', 8);
            $jmlApp = 3;
            if (isset($list_data['APPROVE4_BY'])) {
                $jmlApp = 4;
            }
            $width = (170 / ($jmlApp));
            $this->fpdf->Ln(5);
            $this->fpdf->Cell($width, 5, "Dibuat Oleh,", 'LTR', 0, 'C');
            $this->fpdf->Cell($width, 5, "Diperiksa Oleh,", 'LTR', 0, 'C');
            $this->fpdf->Cell($width, 5, "Disetujui Oleh,", 'LTR', 0, 'C');
            if ($jmlApp == 4) {
                $this->fpdf->Cell($width, 5, "Disetujui Oleh,", 'LTR', 0, 'C');
            }
            // jabatan
            $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'I', 8);
            $y1 = $this->fpdf->GetY();
            $x1 = $this->fpdf->GetX();
            $lnmax = 28;
            if ($jmlApp == 4) {
                $lnmax = 18;
            }
            $h = 8;
            if (strlen($list_data['APPROVE1_JAB']) > $lnmax)
                $h = 4;
            else
                $h = 8;
            $this->fpdf->MultiCell($width, $h, "{$list_data['APPROVE1_JAB']}", 'LR', 'C');
            $pos_x = 20 + $width;
            if (strlen($list_data['APPROVE2_JAB']) > $lnmax)
                $h = 4;
            else
                $h = 8;
            $this->fpdf->SetXY($pos_x, $y1);
            $this->fpdf->MultiCell($width, $h, "{$list_data['APPROVE2_JAB']}", 'LR', 'C');
            $pos_x += $width;
            $this->fpdf->SetXY($pos_x, $y1);
            if (strlen($list_data['APPROVE3_JAB']) > $lnmax)
                $h = 4;
            else
                $h = 8;
            $this->fpdf->MultiCell($width, $h, "{$list_data['APPROVE3_JAB']}", 'LR', 'C');
            if ($jmlApp == 4) {
                $pos_x += $width;
                $this->fpdf->SetXY($pos_x, $y1);
                if (strlen($list_data['APPROVE4_JAB']) > $lnmax)
                    $h = 4;
                else
                    $h = 8;
                $this->fpdf->MultiCell($width, $h, "{$list_data['APPROVE4_JAB']}", 'LR', 'C');
            }

            // QR code
            $this->fpdf->Ln(0);
            // $this->fpdf->Cell(170,1,"",'LR',1);
            // created by (approval 1)
            if ($list_data['APPROVE1_AT']) {
                $tmpCre = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-1-CREATED") . ".png";
                $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE1_BY']};{$list_data['APPROVE1_AT']};", 'e');
                $str = str_replace('/', '6s4', $str);
                $str = str_replace('+', '1p4', $str);
                $str = str_replace('=', 'sM9', $str);
                $str = base_url() . "info/tender/" . $str;
                QRcode::png($str, $tmpCre);
                $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
            } else {
                $this->fpdf->Cell($width, 25, "", "LTR", 0, 'C');
            }
            // approval 2 & 3 (Mgr & SM)
            if ($list_data['APPROVE2_AT']) {
                $tmpCre = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-2-VERF") . ".png";
                $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE2_BY']};{$list_data['APPROVE2_AT']};", 'e');
                $str = str_replace('/', '6s4', $str);
                $str = str_replace('+', '1p4', $str);
                $str = str_replace('=', 'sM9', $str);
                $str = base_url() . "info/tender/" . $str;
                QRcode::png($str, $tmpCre);
                $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
            } else {
                $this->fpdf->Cell($width, 25, "", "LTR", 0, 'C');
            }
            if ($list_data['APPROVE3_AT']) {
                $tmpCre = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-3-SM") . ".png";
                $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE3_BY']};{$list_data['APPROVE3_AT']};", 'e');
                $str = str_replace('/', '6s4', $str);
                $str = str_replace('+', '1p4', $str);
                $str = str_replace('=', 'sM9', $str);
                $str = base_url() . "info/tender/" . $str;
                QRcode::png($str, $tmpCre);
                $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
            } else {
                $this->fpdf->Cell($width, 25, "", "LTR", 0, 'C');
            }
            // approval 4 (GM)
            if ($jmlApp == 4) {
                if ($list_data['APPROVE4_AT']) {
                    $tmpCre = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-4-GM") . ".png";
                    $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE4_BY']};{$list_data['APPROVE4_AT']};", 'e');
                    $str = str_replace('/', '6s4', $str);
                    $str = str_replace('+', '1p4', $str);
                    $str = str_replace('=', 'sM9', $str);
                    $str = base_url() . "info/tender/" . $str;
                    QRcode::png($str, $tmpCre);
                    $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
                } else {
                    $this->fpdf->Cell($width, 25, "", "LTR", 0, 'C');
                }
            }
            // name
            $this->fpdf->Ln(25);
            $this->fpdf->SetFont('Arial', 'B', 7.5);
            $this->fpdf->Cell($width, 5, "({$list_data['APPROVE1_BY']})", 'LBR', 0, 'C');
            $this->fpdf->Cell($width, 5, "({$list_data['APPROVE2_BY']})", 'LBR', 0, 'C');
            $this->fpdf->Cell($width, 5, "({$list_data['APPROVE3_BY']})", 'LBR', 0, 'C');
            if ($jmlApp == 4) {
                $this->fpdf->Cell($width, 5, "({$list_data['APPROVE4_BY']})", 'LBR', 0, 'C');
            }

            // // By Approval Log
            // $width = (170/(count($dtApp)));
            // $this->fpdf->Ln(5);
            // foreach ($dtApp as $key) {
            //   $this->fpdf->Cell($width,5,"Disetujui Oleh,",'LTR',0,'C');
            // }
            // // jabatan
            // $this->fpdf->Ln(5);
            // $this->fpdf->SetFont('Arial','I',8);
            // $y1 = $this->fpdf->GetY();
            // $x1 = $this->fpdf->GetX();
            // $lnmax = 26;
            // if (count($dtApp)>4) {
            //   $lnmax = 20;
            // }elseif (count($dtApp)<4) {
            //   $lnmax = 17;
            // }
            // $y2 = $this->fpdf->GetY();
            // $pos_x = 20;
            // $inum=0;
            // foreach ($dtApp as $key) {
            //   if (strlen($key['APP_JAB'])>$lnmax) {
            //     $h = 4;
            //   }else {
            //     $i++;
            //     $h = 8;
            //     if ($i==(count($dtapp)-2)) {
            //       $h = 4;
            //     }
            //   }
            //   $this->fpdf->SetXY($pos_x,$y1);
            //   $this->fpdf->MultiCell($width,$h,"{$key['APP_JAB']}",'LR','C');
            //   $pos_x += $width;
            // }
            // // QR code
            // $hQRapp=20;
            // foreach ($dtApp as $key) {
            //   if ($key['STATE']=='Approved') {
            //     $tmpApp = FCPATH."media/upload/tmp/".md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APPROVE{$key['LVL']}-TTD").".png";
            //     $strApp = $this->my_crypt("{$list_data['ID']};{$key['APP_NAME']};{$key['UPDATE_AT']};",'e');
            //     $strApp = str_replace('/', '6s4', $strApp);
            //     $strApp = str_replace('+', '1p4', $strApp);
            //     $strApp = str_replace('=', 'sM9', $strApp);
            //     $strApp = base_url()."info/tender/". $strApp;
            //     QRcode::png($strApp, $tmpApp);
            //     $this->fpdf->Cell($width,$hQRapp,$this->fpdf->Image($tmpApp,$this->fpdf->GetX()+($width/2)-($hQRapp/2),$this->fpdf->GetY(),0,$hQRapp,'PNG'),'LTR',0,'C');
            //   }else {
            //     $this->fpdf->Cell($width,$hQRapp,"","LTR",0,'C');
            //   }
            // }
            // // name
            // $this->fpdf->SetFont('Arial','BI',7);
            // $this->fpdf->Ln($hQRapp);
            // foreach ($dtApp as $key) {
            //   $this->fpdf->Cell($width,5,"({$key['APP_NAME']})",'LBR',0,'C');
            // }
            // line
            $this->fpdf->Ln(28);
            $y = $this->fpdf->GetY();
            $x = $this->fpdf->GetX();
            $this->fpdf->SetLineWidth(0.5);
            $this->fpdf->Line($x, $y, ($x + 170), $y);
            $this->fpdf->SetFont('Arial', '', 7);
            $this->fpdf->Ln(2);
            $this->fpdf->Cell(85, 2, "* dokumen ini di approve oleh sistem e-DEMS.", 0, 0);
            $this->fpdf->Cell(85, 2, "diunduh oleh : [ {$info['no_badge']} ] " . ucwords(strtolower($info['name'])), 0, 1, 'R');

            // temporary download
            $tmpPath = FCPATH . "media/upload/tmp/" . md5("{$list_data['NO_DOK_TEND']} {$list_data['PACKET_TEXT']}") . ".pdf";
            $this->fpdf->Output($tmpPath, 'F');

            $arfile = array($tmpPath);
            // RKS
            if ($list_data['RKS_FILE'] && ($tipe == 'All' || $tipe == 'TR')) {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/TD" . md5("{$list_data['NO_DOK_TEND']}-{$list_data['PACKET_TEXT']}-TR") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['RKS_FILE'], $list_data, 'TR', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
                // array_push($arfile, FCPATH.$list_data['RKS_FILE']);
            }
            // BQ
            if ($list_data['BQ_FILE'] && ($tipe == 'All' || $tipe == 'BQ')) {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/TD" . md5("{$list_data['NO_DOK_TEND']}-{$list_data['PACKET_TEXT']}-BQ") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['BQ_FILE'], $list_data, 'BQ', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
                // array_push($arfile, FCPATH.$list_data['BQ_FILE']);
            }
            // Drawwing
            if ($list_data['DRAW_FILE'] && ($tipe == 'All' || $tipe == 'DRW')) {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/TD" . md5("{$list_data['NO_DOK_TEND']}-{$list_data['PACKET_TEXT']}-DW") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['DRAW_FILE'], $list_data, 'DW', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
                // array_push($arfile, FCPATH.$list_data['DRAW_FILE']);
            }
            // ECE
            if ($list_data['ECE_FILE'] && ($tipe == 'EC')) {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/TD" . md5("{$list_data['NO_DOK_TEND']}-{$list_data['PACKET_TEXT']}-EC") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['ECE_FILE'], $list_data, 'EC', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
                // array_push($arfile, $list_data['ECE_FILE']);
            }
            // Kajian
            if ($list_data['KAJIAN_FILE']) {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/DE" . md5("{$list_data['NO_DOK_TEND']}-{$list_data['PACKET_TEXT']}-KJ") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['KAJIAN_FILE'], $list_data, 'KJ', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
                // array_push($arfile, FCPATH.$list_data['KAJIAN_FILE']);
            }

            $this->load->library('PDFMerger');
            $pdf = new PDFMerger;

            foreach ($arfile as $key => $value) {
                $pdf->addPDF($value, 'all');
            }
            // $pdf->addPDF('samplepdfs/three.pdf', 'all'); //  'all' or '1,3'->page

            $pdf->merge('download', "{$list_data['NO_DOK_TEND']} {$list_data['PACKET_TEXT']} {$sTitle}.pdf");
            // REPLACE 'file' WITH 'browser', 'download', 'string', or 'file' for output options
        } else {
            echo "File Not Found!";
        }
    }

    public function setFtrCode($src, $dtList = array(), $jenis = 'TR', $output = 'test.pdf', $tipe = 'D') {
        if (!class_exists("FPDF")) {
            require_once APPPATH . 'libraries/fpdf/fpdf.php';
        }
        if (!class_exists("FPDI")) {
            require_once APPPATH . 'libraries/fpdi/fpdi.php';
        }

        $pdf = new FPDI();
        $pageCount = $pdf->setSourceFile($src);

        // generate qrcode
        $lembar = $pageCount;
        $tmpFtr = FCPATH . "media/upload/tmp/ftr" . md5("TENDER-{$dtList['ID']}-{$dtList['NOTIFIKASI']}") . ".png";
        $strFtr = $this->my_crypt("{$dtList['ID']};{$jenis};{$lembar};", 'e');
        $strFtr = str_replace('/', '6s4', $strFtr);
        $strFtr = str_replace('+', '1p4', $strFtr);
        $strFtr = str_replace('=', 'sM9', $strFtr);
        $strFtr = base_url() . "info/tendering/" . $strFtr;
        QRcode::png($strFtr, $tmpFtr);

        // looping all pages
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            $templateId = $pdf->importPage($pageNo);
            $size = $pdf->getTemplateSize($templateId);
            if ($size['w'] > $size['h']) {
                $pdf->AddPage('L', array($size['w'], $size['h']));
            } else {
                $pdf->AddPage('P', array($size['w'], $size['h']));
            }
            if ($pageNo == $pageCount) {
                // $img = $_SERVER['DOCUMENT_ROOT'] . get_save_url() . D('user')->where('id = ' . get_user_id())->getField('pic_yin');
            }

            // line
            // $this->fpdf->SetFont('Arial','',7);
            // $this->fpdf->SetLineWidth(0.5);
            // $pdf->SetY(($size['h']-30));
            // $pdf->Write(5,'* dokumen ini di approve oleh sistem. ');
            // first add an imported page
            $pdf->useTemplate($templateId);

            //overlay a image on top of template
            $pdf->SetFont('Arial', '', 7);
            $pdf->Image($tmpFtr, 30, ($size['h'] - 30), 0, 20);
            $pdf->Text(52, ($size['h'] - 27), "erf : {$dtList['NO_PENGAJUAN']}");
            $pdf->Text(52, ($size['h'] - 24), "eat : {$dtList['NO_PENUGASAN']}");
            $pdf->Text(52, ($size['h'] - 18), "dibuat : {$dtList['APPROVE1_BY']}");
            $pdf->Text(52, ($size['h'] - 15), "diverifikasi : {$dtList['APPROVE2_BY']}");
            $pdf->Text(52, ($size['h'] - 12), "disetujui : {$dtList['APPROVE3_BY']}");
            $pdf->Text(32, ($size['h'] - 8), '* dokumen ini di approve oleh sistem e-DEMS. ');
            $pdf->SetFont('Helvetica');
            $pdf->SetXY(-5, -5);
        }
        $pdf->Output($tipe, $output);

        if ($tipe == 'F') {
            return $output;
        } else {
            return 'true';
        }
    }

    public function get_app() {
        $id = $this->input->post('id');
        $tipe = $this->input->post('tipe');
        $this->db->order_by('LVL', 'ASC');
        $dtApp = $this->app->get_data("REF_ID = '{$id}' AND REF_TABEL = 'DOK_TEND'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_paraf() {
        $id = $this->input->post('id');
        $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'Paraf'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_ttd() {
        $id = $this->input->post('id');
        $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'TTD'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_ttd_smgm() {
        $id = $this->input->post('id');
        $dtApp = $data = $this->tender->get_record("A.ID = '{$id}'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_prog_dok() {
        $id = $this->input->post('id');
        $dtApp = $data = $this->tender->getIDDOK("$id");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function generateNoDoc() {
        $yyyy = date("Y");
        $xxxx = $this->tender->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data = array("tahun" => $yyyy, "nomor" => $kodemax);
        echo json_encode($data);
    }

}
