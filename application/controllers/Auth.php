<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// class Auth extends APIs {
class Auth extends CI_Controller {

    function __construct()
	{
		parent::__construct();
		// $this->load->model('M_role', 'role');

		// only allow users with 'admin' role to access all methods in this controller
		// $this->acl_auth->restrict_access('admin');
	}
	public function index()
	{
		// if ( ! $this->acl->has_access())
		// {
		// 	show_error('You do not have access to this section');
		// }

		$this->load->view($this->login());

		// $this->output->enable_profiler(TRUE);
	}

	public function role($name=null){
		if ($name==null){
			print_r($this->acl->role());
		}else{
			$role = $this->acl_auth->has_role($name);
			echo $this->db->last_query();

			echo "$name <br>-----------$role------------------";
		}
	}
	public function access(){
		print_r($this->acl->has_access());
	}
	public function admin(){
		$this->load->library('acl_auth');

		echo $this->acl_auth->has_role('Admin');

		// print_r($this->session->userdata());

		if ($this->acl_auth->has_role('Admin')) {

			echo "you're admin";
		}else{
			echo "you're isn't admin";
		}
	}

	public function login(){
    // $param['username'] = $_POST['username'];
    // $param['password'] = $_POST['password'];
    // $param['remember'] = $_POST['remember'];
    // $param['themes'] = $_POST['themes'];
    // $browser = get_browser(null, true);
    // echo "<pre>";
    // print_r($browser);
    // echo "</pre>";
    // exit;

    										// echo "<pre>";
    										// print_r((array)($param));
    										// echo "</pre>";
    										// exit;
		if (isset($_POST['username'])) {

			$credentials['username'] = strtoupper($_POST['username']);
			$credentials['password'] = $_POST['password'];
			$credentials['remember'] = $_POST['remember'];
			$credentials['themes'] = $_POST['themes'];

			$response = $this->attempt($credentials);

			echo json_encode($response);
		}else{
			echo $this->response('username cannot be null', 422);
		}

	}

	public function logged_in(){
		if (isset($_POST['rem_me'])) {

			$response = $this->remember_me($_POST['rem_me']);

			echo json_encode($response);
		}else{
			echo $this->response('username cannot be null', 422);
		}

	}

	public function user($roles = NULL){
		$token = $_GET['token'];

		if ($this->validasi($token)) {

			$payload = $this->payload($token, FALSE);
			unset($payload->iat);
			unset($payload->exp);

			$data = $payload;
			$data->role = $this->_my_role($data->data->role_id);

			echo $this->response('success', 200, $data);
		}else{
			echo $this->response('Invalid_Token_Request', 401);
		}


	}

	public function myrole($role_id){
		echo json_encode($this->_my_role2($role_id));
	}


	public function loggedin(){
		$date = new DateTime();
		// $user = $this->user_model->get_user();
		echo "<br> ".$date->format('U');
	}

	public function logout(){
		$this->acl_auth->logout();
    // echo '<script type="text/javascript">',
    // 'localStorage.clear();location.href = "../welcome";',
    // '</script>';
    echo '<script type="text/javascript">',
    'if(localStorage.dems_me){ localStorage.clear();}',
    'location.href = "../welcome";',
    '</script>';

    // redirect('login');
	}


}
