<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// class Auth extends APIs {
class User extends CI_Controller {

    function __construct()
	{
		parent::__construct();
		$this->load->model('M_hris', 'hris');
		$this->load->model('M_user', 'user');

	}

	public function test(){
		print_r($this->test);
	}
	public function index()
	{


	}

	public function search(){
    $q = '';
    if (isset($_GET['q'])) {
      $q = $_GET['q'];
      $q = strtoupper($q);
    } else {
      $q = strtoupper($_POST['search']);
    }
    $sEsel = '';
    if (isset($_POST['additional'])) {
      $sEsel = $_POST['additional'];
    }

    $result 	= $this->hris->get_user_hris($q, $sEsel);

		echo json_encode($result);
	}

	public function search_by_comp_text(){

		$badge 		= (isset($_GET['nobadge'])) ? $_GET['nobadge'] : (isset($_GET['q']) ? $_GET['q'] : '');
		$badge 		= strtolower($badge);

		$company 	= (isset($_GET['company'])) ? $_GET['company'] : '';
		$uk 		= (isset($_GET['uk'])) ? $_GET['uk'] : '';

    $tmpcomp = $this->user->get_plant_by_comptext($company);
    $acomp = '';
    for($i=0; $i<count($tmpcomp); $i++){
      if($i>0) $acomp .= ', ';
      $acomp.=$tmpcomp[$i]['COMPANY'];
    }

		if(isset($_GET['nobadge']))
		{
			$result 	= $this->hris->get_user_hris_comptext($badge, $acomp, $uk);
		}else
		{
			$result 	= $this->hris->get_user_hris_comptextV2($badge, $acomp, $uk);
      // echo $this->db->last_query();
		}

    // print_r($result);

		echo json_encode($result);
	}


  public function search_atas()
  {
    $token = $_POST['token'];
    if($this->validasi($token)){
      $payload = $this->payload($token)->data;
      $result = $this->hris->get_pic_atas($payload->no_badge);
      if($result){
        echo $this->response2('success', 200, $result);
      }else{
        echo $this->response2('error', 400);
      }

      return $result;

    }
  }

	public function search_uk(){
    $q = '';
    if(isset($_GET['q']))
    {
		    $q 		= $_GET['q'];
        $q 		= strtolower($q);
    }

    $result 	= $this->hris->get_uk_hris_v2($q);
		echo json_encode($result);
	}

	public function view(){

		// $token = $_GET['token'];
		// if ($this->validasi($token)) {

		// }
		$data['plant'] = (isset($_GET['plant']))? $_GET['plant']:'';
		$data['company'] = (isset($_GET['company']))? $_GET['company']:'';
		// $result = $this->user_model->get_by(array('company' => $data['company'], 'plant' => $data['plant']));
		$result = $this->user->get_user_all($data);

		echo json_encode($result);
	}

	public function view_by_comptext(){

		// $token = $_GET['token'];
		// if ($this->validasi($token)) {

		// }
    $company = (isset($_GET['company']))? $_GET['company']:'';
    $tmpcomp = $this->user->get_plant_by_comptext($company);
    $acomp = '';
    for($i=0; $i<count($tmpcomp); $i++){
      if($i>0) $acomp .= ', ';
      $acomp.=$tmpcomp[$i]['COMPANY'];
    }

		$data['plant'] = (isset($_GET['plant']))? $_GET['plant']:'';
		$data['company'] = $acomp;
		// $result = $this->user_model->get_by(array('company' => $data['company'], 'plant' => $data['plant']));
		$result = $this->user->get_user_all_comptext($data);

		echo json_encode($result);
	}

	public function roles(){

	}

	public function update($no_badge=NULL){

		$token = $_POST['token'];

		$contain = array('role_id', 'active', 'company', 'plant', 'ad');

		if ($this->validasi($token)) {
			$payload = $this->payload($token)->data;
			$param = array();

			foreach ($_POST as $key => $value) {
				if (in_array($key, $contain)) {
					$param[$key] = $value;
				}
			}

			$param['no_badge'] = $payload->no_badge;
			if ($no_badge) {
				$param['no_badge'] = $no_badge;
			}

			$param['update_at'] = date('Y-m-d H:m:s');
			$param['update_by'] = $payload->username;


			$result = $this->user->update_user($param);

			if ($result) {

				echo $this->response('success', 200);
			}else{
    			echo $this->response('error', 422);
    		}

		}
	}

}
