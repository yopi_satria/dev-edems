<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notifikasi extends CI_Controller {

    public function index() {
        echo 'tes';
    }

    function get_data() {
        $this->load->model('mnotifikasi', '', true);
        $result = $this->mnotifikasi->mget_data(array("IS_ABNORMAL" => 1));
        $dataFinal = array();
        if ($result) {
            foreach ($result as $value) {
                $priority = ($value->PRIORITY == NULL) ? "0" : $value->PRIORITY;
                $sapequip = ($value->SAPEQUIPNUM_TRANS == NULL || $value->SAPEQUIPNUM_TRANS == "" || empty($value->SAPEQUIPNUM_TRANS) ? "0" : $value->SAPEQUIPNUM_TRANS);
                $idtrans = ($value->IDAMTRANS == NULL || $value->IDAMTRANS == "" || empty($value->IDAMTRANS) ? "0" : $value->IDAMTRANS);
                $keterangan = ($value->KETERANGAN == NULL) ? "." : $value->KETERANGAN;
                $color = "";
                if ($value->PRIORITY == null) {
                    $color = "btn btn-warning";
                } else {
                    $color = "btn btn-success";
                }

                $dataFinal[] = array(
                    ($value->NOTIFNUMBER != NULL ? $value->NOTIFNUMBER : "<button type=\"button\" onClick=\"create_notif({$idtrans},{$sapequip},{$priority},'{$keterangan}')\" class=\"btn {$color}\"><span class=\"glyphicon glyphicon-share\" aria-hidden=\"true\"></span></button>" ),
                    $value->EQUPTNAME,
                    $this->_get_icon($value->IS_BERSIH, 1),
                    $this->_get_icon($value->IS_BERSIH, 0),
                    $this->_get_icon($value->IS_BOCOR, 1),
                    $this->_get_icon($value->IS_BOCOR, 0),
                    $this->_get_icon($value->IS_AUS, 1),
                    $this->_get_icon($value->IS_AUS, 0),
                    $this->_get_icon($value->IS_PANAS, 1),
                    $this->_get_icon($value->IS_PANAS, 0),
                    $this->_get_icon($value->IS_KELBUNYI, 1),
                    $this->_get_icon($value->IS_KELBUNYI, 0),
                    $this->_get_icon($value->IS_VIBRASI, 1),
                    $this->_get_icon($value->IS_VIBRASI, 0),
                    $this->_get_icon($value->IS_KONDISI, 1),
                    $this->_get_icon($value->IS_KONDISI, 0),
                    $this->_get_icon($value->IS_ABNORMAL, 1),
                    $this->_get_icon($value->IS_ABNORMAL, 0),
                    ($value->PICT_BEFORE == NULL ? "Tidak Ada" : "  "),
                    ($value->PICT_AFTER == NULL ? "Tidak Ada" : "  "),
                    $value->KETERANGAN
                );
            }
        }

        echo json_encode(array("data" => $dataFinal));
    }

    function _get_icon($data, $par) {
        /*
          jika kolom tidak dan data 0 maka centang atau jika kolom tidak dan data 1 makan tidak centang
         *  jika kolom iya dan data 1 maka centang atau jika kolom iya dan data 0 maka tidak centang
         */
        $ret = "";
        if ($par == 1) {
            if ($data == 1) {
                $ret = " <span class=\"glyphicon glyphicon-saved\" style=\"color:red\" aria-hidden=\"true\"></span> ";
            }
        } elseif ($par == 0) {
            if ($data == 0) {
                $ret = "<span class=\"glyphicon glyphicon-saved\" style=\"color:green\" aria-hidden=\"true\"></span> ";
            }
        }

        return $ret;
    }

    public function create_notif() {
        // echo json_encode(array("success"=>true,"msg"=>"sukses"));exit;
        // $session = $this->session->userdata('ses_log_id');
        // $username = $session["username"];

        $info = $this->session->userdata;
  			$info = $info['logged_in'];
        error_reporting(E_ALL);
        // $this->load->model('m_request', '', true);
        $this->load->library('sap');
        $this->load->model('erf/M_request', 'req');

        $resul = $this->req->get_seq_id();

        //
        // echo "<pre>";
        // print_r($this->session);
        // print_r($info);
        // print_r($_POST);
        // print_r($resul);
        // echo "</pre>";
        // exit;
        //$user = array("USERNAME"=>$session["username"]);
        // $updat = $this->mdaily_check->mupd_act_convert(array("IDAMTRANS"=>$_POST['IDAMTRANS']),'1');
        // print_r($resul);exit;
        $sap = new SAPConnection();
        $sap->Connect();
        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            $sap->PrintStatus();
            exit;
        }

        $fce = $sap->NewFunction("Z_ZCPM_RFC_NOTIFCREATE");
        if ($fce == false) {
            $sap->PrintStatus();
            exit;
        }
        if (isset($_POST['NOTIF_TYPE'])) {
            $fce->I_NOTIF_TYPE = $_POST['NOTIF_TYPE'];
        } else {
            $fce->I_NOTIF_TYPE = "06";
        }

        $fce->I_LOGINNAME = $info['username'];
        $fce->I_MSO_ID = $resul['NEXTVAL'];
        $fce->I_TYPETRANS = 'MPE'; // "testng amin";
        $fce->I_NOTIFHEADER["SHORT_TEXT"] = substr($_POST['SHORT_TEXT'],0,40); // "testng amin";
        $fce->I_NOTIFHEADER["PRIORITY"] = $_POST['PRIORITY']; // "1";
        $fce->I_NOTIFHEADER["FUNCT_LOC"] = $_POST['FUNCT_LOC'];
        $fce->I_NOTIFHEADER["PLANPLANT"] = $_POST['PLANPLANT']; // "3302";
        $fce->I_NOTIFHEADER["PLANGROUP"] = $_POST['PLANGROUP']; // "B02";
        $fce->I_NOTIFHEADER["REPORTEDBY"] = $info["username"];
        // $fce->I_NOTIFHEADER["EQUIPMENT"] = sprintf('%018d',$_POST["SAPEQUIPNUM"]);
        $fce->I_NOTIFHEADER["NOTIF_DATE"] = date("Ymd"); //"20150101";
        $fce->I_NOTIFHEADER["DESSTDATE"] = $_POST['START_DATE']; //$this->dateTosap($_POST['START_DATE']);//start date;
        $fce->I_NOTIFHEADER["DESENDDATE"] = $_POST['END_DATE']; //$this->dateTosap($_POST['END_DATE']);//end date;
        $fce->I_BDCCHG["BEBER"] = $_POST['BEBER'];
        $fce->I_BDCCHG["SWERK"] = $_POST['SWERK'];

        // $fce->T_LONGTEXTS->row["TEXT_LINE"] = $_POST['TECH_INFO'];
        $fce->T_LONGTEXTS->row["TEXT_LINE"] = "{$info['no_badge']} {$info['name']} {$info['uk_kode']} [{$info['dept_code']} - {$info['dept_text']}]";
        $fce->T_LONGTEXTS->Append($fce->T_LONGTEXTS->row);
        $fce->T_LONGTEXTS->row["TEXT_LINE"] = "Deskripsi : ".$_POST['SHORT_TEXT'];
        $fce->T_LONGTEXTS->Append($fce->T_LONGTEXTS->row);
        // $fce->T_LONGTEXTS->row["TEXT_LINE"] = $resul->AKTIFITAS;
        // $fce->T_LONGTEXTS->Append($fce->T_LONGTEXTS->row);
        // $fce->T_LONGTEXTS->row["TEXT_LINE"] = $resul->KETERANGAN;
        // $fce->T_LONGTEXTS->Append($fce->T_LONGTEXTS->row);

        $fce->call();
        if ($fce->GetStatus() == SAPRFC_OK) {
            $fce->T_RETURN->Reset();
            $msg = "";
            $res_sap = array();
            //$hasil = array();
            while ($fce->T_RETURN->Next()) {
                $msg = $fce->T_RETURN->row["MESSAGE"];
                $NOTIF_NO = $fce->O_NOTIFHEADER_EXPORT["NOTIF_NO"];
                $ID_SAP = $resul['NEXTVAL'];

                $hasil['CONTENT'] = ($fce->T_RETURN->row);
                //$hasil = $fce->O_NOTIFHEADER_EXPORT;
            }
            // var_dump($fce->T_RETURN);
            // var_dump($fce->O_NOTIFHEADER_EXPORT);
            // exit;
            $res_sap['NOTIF_NO'] = $fce->O_NOTIFHEADER_EXPORT["NOTIF_NO"];
            $res_sap['COSTCENTER'] = $fce->O_NOTIFHEADER_EXPORT["COSTCENTER"];
            $res_sap['ID_SAP'] = $resul['NEXTVAL'];
            $res_sap['MSG'] = $msg;
//         $ressss = array("NOTIFNUMBER"=>$fce->O_NOTIFHEADER_EXPORT["NOTIF_NO"],
//                          "SAP_STATUS"=>"OSNO",
//                          "NOTIFCREATEBY"=>$session['username'],
//                           "PGROUP"=>$fce->O_NOTIFHEADER_EXPORT['PLANGROUP'],
//                           "PSECTION"=>$pecahpsection[0],
//                           "IDAMTRANS"=>$_POST["IDAMTRANS"]);

            // echo $msg;
            echo $this->response('success', 200, $res_sap);

        }

        //echo json_encode($hasil);
    }

    function dateTosap($date) {
        $exp = explode("/", $date); // m/d/Y
        $baru = $exp[2] . $exp[0] . $exp[1]; // Y/m/d
        return $baru;
    }

}
