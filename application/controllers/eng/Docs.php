<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Docs extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    protected $uk_parent = '';

    public function __construct() {
        parent::__construct();
        $this->load->model('M_hris', 'dbhris');
        $this->load->model("eng/M_user_docs", "docs");
        $this->load->model("others/M_approval", "app");
    }

    public function index() {
        $session = $this->session->userdata('logged_in');
        $info = (array) $session;
        if (empty($info['username'])) {
            redirect('login');
        }
        // echo "<pre>";
        // print_r($trans);
        // echo "</pre>";
        // exit;

        $header["tittle"] = "Engineering Document Download";
        $header["short_tittle"] = "udc";
        // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        // view roles
        $this->db->distinct();
        $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        $header['roles'] = json_decode(json_encode($header['roles']), true);
        $state = '';
        foreach ($header['roles'] as $i => $val) {
            $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
            $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
            if ($val['code'] == $header["short_tittle"]) {
                // code...
                $state = $val['stat'];
            }
            // get count pending status
            if (isset($val['tabel'])) {
                $npending = 0;
                if ($info['special_access'] == 'true') {
                    $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                } else {
                    if (strpos($val['stat'], "-child") !== false) {
                        if (strpos($val['stat'], "{$val['code']}-child=1") !== false) {
                            $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
                            $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                        }
                    } else {
                        $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans'], $info['uk_kode']);
                    }
                }
                $header['roles'][$i]['count'] = $npending;
            }
        }

        // set auto increment trans no
        // $where = "A.ID=(SELECT MAX(ID) FROM MPE_DOK_ENG) AND NO_DOK_ENG LIKE '%DE-%'";
        // $trans = $this->docs->get_record($where);;
        // echo "<pre>";
        // print_r($trans);
        // echo "</pre>";
        // exit;
        // set list approval hris
        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'EAT_PENGKAJI_UK';
        $where['GEN_TYPE'] = 'General';
        $global = $this->general_model->get_data($where);
        $str = "
            (uk.muk_parent LIKE '{$global->GEN_PAR5}'
          	OR k.muk_kode = '{$global->GEN_PAR5}') AND k.mk_emp_subgroup='30'
          ";
        $str2 = "
            (uk.muk_parent LIKE '{$global->GEN_PAR5}'
          	OR k.muk_kode = '{$global->GEN_PAR5}') AND k.mk_emp_subgroup='20'
          ";
        $uk_parent = $global->GEN_PAR5;

        $yyyy = date("Y");
        $xxxx = $this->docs->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data['no'] = "{$yyyy}///TR/{$kodemax}";
        $data['LIST_MNG'] = $this->dbhris->get_hris_where($str);
        $data['LIST_APP'] = $this->dbhris->get_hris_where($str2);
        $data['foreigns'] = $this->docs->get_foreign();
        // echo "<pre>";
        // print_r($data['LIST_APP']);
        // echo "</pre>";
        // exit;
        // $data['DEPT'] = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
        // $data['BIRO'] = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
        // $data['FOREIGN'] = $this->docs->get_foreign();
        $header['page_state'] = $state;

        $sHdr = '';
        if (isset($info['themes'])) {
            if ($info['themes'] != 'default')
                $sHdr = "_" . $info['themes'];
        }else {
            unset($where);
            $where['GEN_CODE'] = 'themes-app';
            $where['GEN_TYPE'] = 'General';
            $global = (array) $this->general_model->get_data($where);
            if (isset($global) && $global['GEN_VAL'] != '0') {
                $i_par = $global['GEN_VAL'];
                $sHdr = "_" . $global['GEN_PAR' . $i_par];
            }
        }
        if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('eng/user_docs', $data);
        } else {
            $header["tittle"] = "Forbidden";
            $header["short_tittle"] = "403";

            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('forbidden');
        }
        // $this->load->view('general/header', $header);
        // $this->load->view('eng/filing', $data);
        $this->load->view('general/footer');
    }

    public function data_list() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $search = $this->input->post('search');
        $order = $this->input->post('order');

        $key = array(
            'search' => $search['value'],
            'ordCol' => $order[0]['column'],
            'ordDir' => $order[0]['dir'],
            'length' => $this->input->post('length'),
            'start' => $this->input->post('start')
        );

        if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
            $key['name'] = $info['name'];
            $key['username'] = $info['username'];
            // $key['create_by'] = $info['name'];
            $key['uk_kode'] = $info['uk_kode'];
        } else {
            $key['dept_code'] = $info['dept_code'];
        }

        $data = $this->docs->get_data($key);
        // echo $this->db->last_query();

        $return = array(
            'draw' => $this->input->post('draw'),
            'data' => $data,
            'recordsFiltered' => $this->docs->recFil($key),
            'recordsTotal' => $this->docs->recTot($key)
        );

        echo json_encode($return);
    }

    public function foreign() {
        $result = $this->docs->get_foreign();

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function foreign_dtl($f_id) {
        if (isset($f_id)) {
            $result = $this->docs->get_dtl_foreign($f_id);

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function export_pdf($id, $tipe = 'All') {
        if ($id) {
            error_reporting(1);

            $info = $this->session->userdata;
            $info = $info['logged_in'];
            // $list_data = $_POST;
            $list_data = $this->docs->get_record("A.ID='{$id}'");
            $list_data = $list_data[0];
            $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'TTD'");
            $dtPar = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'Paraf'");
            // set list approval hris
            $where['GEN_VAL'] = '1';
            $where['GEN_CODE'] = 'EAT_PENGKAJI_UK';
            $where['GEN_TYPE'] = 'General';
            $global = $this->general_model->get_data($where);
            $dtCre = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['CREATE_BY']}' AND k.company = '{$list_data['COMPANY']}' AND (uk.muk_parent LIKE '" . $global->GEN_PAR5 . "' OR k.muk_kode = '" . $global->GEN_PAR5 . "')");
            if ($dtCre) {
                $dtCre = $dtCre[0];
            }
            // echo "<pre>";
            // print_r($dtCre);
            // print_r($global);
            // echo "</pre>";
            // exit;

            $uri = base_url();
            $this->load->library('Fpdf_gen');
            $this->fpdf->SetFont('Arial', 'B', 13);
            $this->fpdf->SetLeftMargin(20);

            // $this->fpdf->Cell(12,4,'',0,0);
            $this->fpdf->Cell(35, 6, $this->fpdf->Image($uri . 'media/logo/Logo_SI.png', $this->fpdf->GetX() + 7, $this->fpdf->GetY(), 0, 15, 'PNG'), 'LT', 0, 'C');
            $this->fpdf->Cell(100, 6, "PT. SEMEN INDONESIA (PERSERO)Tbk.", 'T', 0, 'C');
            $this->fpdf->Cell(35, 6, $this->fpdf->Image($uri . 'media/logo/si_group.jpg', $this->fpdf->GetX() + 3, $this->fpdf->GetY() + 4, 0, 10, 'JPG'), 'TR', 1, 'C');
            $this->fpdf->SetFont('Arial', '', 12);
            $this->fpdf->Cell(170, 6, "{$dtCre->DEPT_TEXT}", 'LR', 1, 'C');
            $this->fpdf->Cell(170, 6, "{$dtCre->CC_TEXT}", 'LBR', 1, 'C');

            $this->fpdf->SetFont('Arial', 'BU', 12);
            $this->fpdf->Cell(170, 6, "DOKUMEN ENGINEERING", 'LTR', 1, 'C', 1);
            $this->fpdf->SetFont('Arial', '', 12);
            $this->fpdf->Cell(50, 6, " No. Dokumen", 'L', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", '', 0, 'C', 1);
            $this->fpdf->Cell(110, 6, "{$list_data['NO_DOK_ENG']}", 'R', 1, 'L', 1);
            // $sNo=$list_data['NO_DOK_ENG'];
            // if ($tipe=='All') {
            //   // code...
            //   $this->fpdf->Cell(110,6,"{$list_data['NO_DOK_ENG']}",'TR',1,'L',1);
            // }else {
            //   list($thn, $area, $loc, $jns, $urut) =  split('/',$list_data['NO_DOK_ENG']);
            //   $this->fpdf->Cell(110,6,"$thn/$area/$loc/$tipe/$urut",'TR',1,'L',1);
            //   $sNo = "$thn/$area/$loc/$tipe/$urut";
            //   // echo "masuk";
            // }
            // echo "$tipe";
            // exit;
            $this->fpdf->Cell(50, 6, " Notifikasi", 'L', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 0, 0, 'C', 1);
            $this->fpdf->Cell(110, 6, "{$list_data['NOTIFIKASI']}", 'R', 1, 'L', 1);
            $this->fpdf->Cell(50, 6, " Tahun", 'BL', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 'B', 0, 'C', 1);
            $time = strtotime($list_data['CREATE_AT']);
            $year = date('Y', $time);
            $this->fpdf->Cell(110, 6, "{$year}", 'BR', 1, 'L', 1);
            // $this->fpdf->Cell(110,6,"{$list_data['CREATE_AT']}",'R',1,'L',1);
            // $this->fpdf->SetFont('Arial','B',7);
            // $this->fpdf->Cell(170,1,'R/538/012',0,1,'R');
            // $this->fpdf->Cell(0,9,'',0,1);
            // $this->fpdf->SetFont('Arial','B',7);
            // $this->fpdf->Cell(64,1,'PT. SEMEN INDONESIA (PERSERO)Tbk.',0,0);
            $this->fpdf->Cell(170, 6, "", 'LBR', 1);

            $this->fpdf->SetFont('Arial', 'B', 18);
            $this->fpdf->Cell(12, 4, '', 0, 2);
            $this->fpdf->Ln(40);
            $this->fpdf->MultiCell(170, 1, "{$list_data['PACKET_TEXT']}", 0, 'C');

            $this->fpdf->SetFont('Arial', 'B', 8);
            $this->fpdf->Ln(40);
            $this->fpdf->Cell(12, 4, '', 0, 2);
            $this->fpdf->Cell(170, 5, "Pengesahan", 1, 0, 'L');

            include(APPPATH . 'libraries/phpqrcode/qrlib.php');
            // SM Pengkaji Approval
            $width = (170 / (count($dtApp)));
            $this->fpdf->Ln(5);
            foreach ($dtApp as $key) {
                $this->fpdf->Cell($width, 5, "Disetujui Oleh,", 'LTR', 0, 'C');
            }
            // jabatan
            $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'I', 8);
            $y1 = $this->fpdf->GetY();
            $x1 = $this->fpdf->GetX();
            $lnmax = 26;
            if (count($dtApp) > 4) {
                $lnmax = 20;
            } elseif (count($dtApp) < 4) {
                $lnmax = 17;
            }
            $y2 = $this->fpdf->GetY();
            $pos_x = 20;
            $inum = 0;
            foreach ($dtApp as $key) {
                if (strlen($key['APP_JAB']) > $lnmax) {
                    $h = 4;
                } else {
                    $i++;
                    $h = 8;
                    if ($i == (count($dtapp) - 2)) {
                        $h = 4;
                    }
                }
                $this->fpdf->SetXY($pos_x, $y1);
                $this->fpdf->MultiCell($width, $h, "{$key['APP_JAB']}", 'LBR', 'C');
                $pos_x += $width;
            }
            // QR code
            $hQRapp = 20;
            foreach ($dtApp as $key) {
                if ($key['STATE'] == 'Approved') {
                    $tmpApp = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APPROVE{$key['LVL']}-TTD") . ".png";
                    $strApp = $this->my_crypt("{$list_data['ID']};{$key['APP_NAME']};{$key['UPDATE_AT']};", 'e');
                    $strApp = str_replace('/', '6s4', $strApp);
                    $strApp = str_replace('+', '1p4', $strApp);
                    $strApp = str_replace('=', 'sM9', $strApp);
                    $strApp = base_url() . "info/dok_eng/" . $strApp;
                    QRcode::png($strApp, $tmpApp);
                    $this->fpdf->Cell($width, $hQRapp, $this->fpdf->Image($tmpApp, $this->fpdf->GetX() + ($width / 2) - ($hQRapp / 2), $this->fpdf->GetY(), 0, $hQRapp, 'PNG'), 'LTR', 0, 'C');
                } else {
                    $this->fpdf->Cell($width, $hQRapp, "", "LTR", 0, 'C');
                }
            }
            // name
            $this->fpdf->SetFont('Arial', 'BI', 7.8);
            $this->fpdf->Ln($hQRapp);
            foreach ($dtApp as $key) {
                $this->fpdf->Cell($width, 5, "({$key['APP_NAME']})", 'LBR', 0, 'C');
            }

            // approval info
            $this->fpdf->SetFont('Arial', 'B', 8);
            $width = (170 / (4));
            $this->fpdf->Ln(5);
            $this->fpdf->Cell($width, 5, "Dibuat Oleh,", 'LTR', 0, 'C');
            $this->fpdf->Cell($width, 5, "Disetujui Oleh,", 'LTR', 0, 'C');
            $this->fpdf->Cell($width, 5, "Disetujui Oleh,", 'LTR', 0, 'C');
            $this->fpdf->Cell($width, 5, "Disetujui Oleh,", 'LTR', 0, 'C');
            // jabatan
            $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'I', 8);
            $y1 = $this->fpdf->GetY();
            $x1 = $this->fpdf->GetX();
            $lnmax = 26;
            $h = 8;
            if (strlen($dtCre->JAB_TEXT) > $lnmax)
                $h = 4;
            else
                $h = 8;
            $this->fpdf->MultiCell($width, $h, "{$dtCre->JAB_TEXT}", 'LBR', 'C');
            $pos_x = 20 + $width;
            if (strlen($list_data['APPROVE1_JAB']) > $lnmax)
                $h = 4;
            else
                $h = 8;
            $this->fpdf->SetXY($pos_x, $y1);
            $this->fpdf->MultiCell($width, $h, "{$list_data['APPROVE1_JAB']}", 'LBR', 'C');
            $pos_x += $width;
            $this->fpdf->SetXY($pos_x, $y1);
            if (strlen($list_data['APPROVE2_JAB']) > $lnmax)
                $h = 4;
            else
                $h = 8;
            $this->fpdf->MultiCell($width, $h, "{$list_data['APPROVE2_JAB']}", 'LBR', 'C');
            $pos_x += $width;
            $this->fpdf->SetXY($pos_x, $y1);
            if (strlen($list_data['APPROVE3_JAB']) > $lnmax)
                $h = 4;
            else
                $h = 8;
            $this->fpdf->MultiCell($width, $h, "{$list_data['APPROVE3_JAB']}", 'LBR', 'C');

            // QR code
            $this->fpdf->Ln(0);
            $tmpCre = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-CREATE") . ".png";
            $str = $this->my_crypt("{$list_data['ID']};{$list_data['CREATE_BY']};{$list_data['CREATE_AT']};", 'e');
            $str = str_replace('/', '6s4', $str);
            $str = str_replace('+', '1p4', $str);
            $str = str_replace('=', 'sM9', $str);
            $str = base_url() . "info/dok_eng/" . $str;
            QRcode::png($str, $tmpCre);
            $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
            if ($list_data['APPROVE1_AT']) {
                $tmpCre = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-1-MGR") . ".png";
                $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE1_BY']};{$list_data['APPROVE1_AT']};", 'e');
                $str = str_replace('/', '6s4', $str);
                $str = str_replace('+', '1p4', $str);
                $str = str_replace('=', 'sM9', $str);
                $str = base_url() . "info/dok_eng/" . $str;
                QRcode::png($str, $tmpCre);
                $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
            } else {
                $this->fpdf->Cell($width, 25, "", "LTR", 0, 'C');
            }
            if ($list_data['APPROVE2_AT']) {
                $tmpCre = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-2-SM") . ".png";
                $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE2_BY']};{$list_data['APPROVE2_AT']};", 'e');
                $str = str_replace('/', '6s4', $str);
                $str = str_replace('+', '1p4', $str);
                $str = str_replace('=', 'sM9', $str);
                $str = base_url() . "info/dok_eng/" . $str;
                QRcode::png($str, $tmpCre);
                $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
            } else {
                $this->fpdf->Cell($width, 25, "", "LTR", 0, 'C');
            }
            if ($list_data['APPROVE3_AT']) {
                $tmpCre = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-3-GM") . ".png";
                $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE3_BY']};{$list_data['APPROVE3_AT']};", 'e');
                $str = str_replace('/', '6s4', $str);
                $str = str_replace('+', '1p4', $str);
                $str = str_replace('=', 'sM9', $str);
                $str = base_url() . "info/dok_eng/" . $str;
                QRcode::png($str, $tmpCre);
                $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
            } else {
                $this->fpdf->Cell($width, 25, "", "LTR", 0, 'C');
            }
            // name
            $this->fpdf->Ln(25);
            $this->fpdf->SetFont('Arial', 'B', 7.8);
            $this->fpdf->Cell($width, 5, "({$list_data['CREATE_BY']})", 'LBR', 0, 'C');
            $this->fpdf->Cell($width, 5, "({$list_data['APPROVE1_BY']})", 'LBR', 0, 'C');
            $this->fpdf->Cell($width, 5, "({$list_data['APPROVE2_BY']})", 'LBR', 0, 'C');
            $this->fpdf->Cell($width, 5, "({$list_data['APPROVE3_BY']})", 'LBR', 0, 'C');

            $this->fpdf->Ln(7);
            $this->fpdf->SetFont('Arial', '', 7);
            $this->fpdf->Cell(170, 2, "* dokumen ini di approve oleh sistem e-DEMS.", 0, 1);
            $this->fpdf->Ln(15);
            // $width = floor(170/(count($dtPar)));
            $width = 15;
            $this->fpdf->SetFont('Arial', '', 8);
            $this->fpdf->Cell(170 - ($width * count($dtPar)), 10, "paraf : ", 0, 0, 'R');
            foreach ($dtPar as $key) {
                if ($key['STATE'] == 'Approved') {
                    $tmpPar = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APPROVE{$key['LVL']}-Paraf") . ".png";
                    $strPar = $this->my_crypt("{$list_data['ID']};{$key['APP_NAME']};{$key['UPDATE_AT']};", 'e');
                    $strPar = str_replace('/', '6s4', $strPar);
                    $strPar = str_replace('+', '1p4', $strPar);
                    $strPar = str_replace('=', 'sM9', $strPar);
                    $strPar = base_url() . "info/dok_eng/" . $strPar;
                    QRcode::png($strPar, $tmpPar);
                    $this->fpdf->Cell($width, 10, $this->fpdf->Image($tmpPar, $this->fpdf->GetX() + ($width / 2) - 5, $this->fpdf->GetY(), 0, 10, 'PNG'), 1, 0, 'C');
                } else {
                    $this->fpdf->Cell($width, 10, "", 1, 0, 'C');
                }
            }

            $this->fpdf->Cell(12, 4, '', 0, 0);

            // temporary download
            $tmpPath = FCPATH . "media/upload/tmp/" . md5("{$list_data['NO_DOK_ENG']} {$list_data['PACKET_TEXT']}") . ".pdf";
            $this->fpdf->Output($tmpPath, 'F');

            $arfile = array($tmpPath);
            // RKS
            if ($list_data['RKS_FILE'] && ($tipe == 'All' || $tipe == 'TR')) {
                array_push($arfile, FCPATH . $list_data['RKS_FILE']);
            }
            // BQ
            if ($list_data['BQ_FILE'] && ($tipe == 'All' || $tipe == 'BQ')) {
                array_push($arfile, FCPATH . $list_data['BQ_FILE']);
            }
            // Drawwing
            if ($list_data['DRAW_FILE'] && ($tipe == 'All' || $tipe == 'DRW')) {
                array_push($arfile, FCPATH . $list_data['DRAW_FILE']);
            }
            // ECE
            if ($list_data['ECE_FILE'] && ($tipe == 'All' || $tipe == 'EC')) {
                array_push($arfile, $list_data['ECE_FILE']);
            }
            // Kajian
            if ($list_data['KAJIAN_FILE']) {
                array_push($arfile, FCPATH . $list_data['KAJIAN_FILE']);
            }

            $this->load->library('PDFMerger');
            $pdf = new PDFMerger;

            foreach ($arfile as $key => $value) {
                $pdf->addPDF($value, 'all');
            }
            // $pdf->addPDF('samplepdfs/three.pdf', 'all'); //  'all' or '1,3'->page
            $sAdd = '';
            if ($tipe != 'All') {
                // code...
                $sAdd = '-' . $tipe;
            }
            $pdf->merge('download', "{$list_data['NO_DOK_ENG']} {$list_data['PACKET_TEXT']}{$sAdd}.pdf");
            // REPLACE 'file' WITH 'browser', 'download', 'string', or 'file' for output options
        } else {
            echo "File Not Found!";
        }
    }

    public function get_paraf() {
        $id = $this->input->post('id');
        $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'Paraf'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_ttd() {
        $id = $this->input->post('id');
        $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'TTD'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_ttd_smgm() {
        $id = $this->input->post('id');
        $dtApp = $data = $this->docs->get_record("A.ID = '{$id}'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_prog_dok() {
        $id = $this->input->post('id');
        $dtApp = $data = $this->docs->getIDDOK("$id");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function generateNoDoc() {
        $yyyy = date("Y");
        $xxxx = $this->docs->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data = array("tahun" => $yyyy, "nomor" => $kodemax);
        echo json_encode($data);
    }

}
