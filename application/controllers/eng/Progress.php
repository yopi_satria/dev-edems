<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Progress extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    protected $uk_parent = '';

    public function __construct() {
        parent::__construct();
        $this->load->model('M_hris', 'dbhris');
        $this->load->model("eng/M_progress", "prog");
        $this->load->model("others/M_approval", "app");
    }

    public function index() {
        $session = $this->session->userdata('logged_in');
        $info = (array) $session;
        if (empty($info['username'])) {
            redirect('login');
        }

        $header["tittle"] = "Progress Engineering";
        $header["short_tittle"] = "eng";
        // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        // view roles
        $this->db->distinct();
        $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        $header['roles'] = json_decode(json_encode($header['roles']), true);
        $state = '';
        foreach ($header['roles'] as $i => $val) {
            $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
            $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
            if ($val['code'] == $header["short_tittle"]) {
                // code...
                $state = $val['stat'];
            }
            // get count pending status
            if (isset($val['tabel'])) {
                $npending = 0;
                if ($info['special_access'] == 'true') {
                    $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                } else {
                    if (strpos($val['stat'], "-child") !== false) {
                        if (strpos($val['stat'], "{$val['code']}-child=1") !== false) {
                            $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
                            $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                        }
                    } else {
                        $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans'], $info['uk_kode']);
                    }
                }
                $header['roles'][$i]['count'] = $npending;
            }
        }

        $header['page_state'] = $state;

        // data
        unset($where);
        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'PROG_GUIDE';
        $where['GEN_PAR1'] = 'Kajian Teknis';
        $where['GEN_TYPE'] = 'General';
        $data['guide_kajian'] = $this->general_model->get_data_result($where);
        unset($where);
        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'PROG_GUIDE';
        $where['GEN_PAR1'] = 'Detail Design';
        $where['GEN_TYPE'] = 'General';
        $data['guide_detail'] = $this->general_model->get_data_result($where);

        $sHdr = '';
        if (isset($info['themes'])) {
            if ($info['themes'] != 'default')
                $sHdr = "_" . $info['themes'];
        }else {
            unset($where);
            $where['GEN_CODE'] = 'themes-app';
            $where['GEN_TYPE'] = 'General';
            $global = (array) $this->general_model->get_data($where);
            if (isset($global) && $global['GEN_VAL'] != '0') {
                $i_par = $global['GEN_VAL'];
                $sHdr = "_" . $global['GEN_PAR' . $i_par];
            }
        }
        if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('eng/packet', $data);
        } else {
            $header["tittle"] = "Forbidden";
            $header["short_tittle"] = "403";

            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('forbidden');
        }
        // $this->load->view('general/header', $header);
        // $this->load->view('eng/packet');
        $this->load->view('general/footer');
    }

    public function data_list($tipe = '') {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $search = $this->input->post('search');
        $order = $this->input->post('order');

        $key = array(
            'search' => $search['value'],
            'ordCol' => $order[0]['column'],
            'ordDir' => $order[0]['dir'],
            'length' => $this->input->post('length'),
            'start' => $this->input->post('start')
        );

        if (($info['special_access'] == 'false' || $info['special_access'] == '0')) {
            $key['name'] = $info['name'];
            $key['username'] = $info['username'];
            $key['dept_code'] = $info['dept_code'];
            $key['uk_code'] = $info['uk_kode'];
        }
        if ($tipe != '') {
            $key['tipe'] = $tipe;
        }

        $data = json_decode(json_encode($this->prog->get_data($key)), true);
        // $data	= $this->prog->get_data($key);
        foreach ($data as $i => $val) {
            $where['DOC_ID'] = $val['ID'];
            $data[$i]['FILES'] = $this->general_model->file_opening($where);
        }
        // echo $this->db->last_query();

        $return = array(
            'draw' => $this->input->post('draw'),
            'data' => $data,
            'recordsFiltered' => $this->prog->recFil($key),
            'recordsTotal' => $this->prog->recTot($key)
        );

        echo json_encode($return);
    }

    public function foreign() {
        $result = $this->prog->get_foreign();

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function history() {
        $post = $_POST;

        $result = $this->general_model->get_log($post['tbl'], $post['id']);

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function atasan($level) {
        $this->load->model('M_hris', 'dbhris');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $result = $this->dbhris->get_level_from($info['no_badge'], $level);
        // $this->db->last_query();
        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function send_mail($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        list($badge, $nama) = split(' - ', $atas);
        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$badge}'");
        $peg = (array) $peg[0];

        $e = str_replace('/', '6s4', $e);
        $e = str_replace('+', '1p4', $e);
        $e = str_replace('=', 'sM9', $e);
        $param['short'] = 'DE';
        $param['link'] = 'dok_eng';
        $param['title'] = 'Document Engineering';
        $param['sender'] = "{$info['no_badge']} - {$info['name']}";
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$param['NO_DOK_ENG']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Paket :</b> {$param['PACKET_TEXT']}";
        $param['TRN']['LOKASI'] = "<b>Progress :</b> {$param['PROGRESS']}%";
        $mailto = str_replace("SIG.ID", "SEMENINDONESIA.COM", $to);
        $tomail = str_replace("sig.id", "SEMENINDONESIA.COM", $mailto);

        $this->email($this->template->set_app($param), $subject, '', $tomail, $cc);


        // $message="Dengan Hormat $atas,<br /><br />
        // Mohon untuk ditindak lanjuti approval project berikut :<br />
        // No Notifikasi    : " . $param['NOTIFIKASI'] . "<br/>
        // No Dokumen       : " . $param['NO_DOK_ENG'] . "<br/>
        // Nama Paket       : " . $param['PACKET_TEXT'] . "<br/> <br/> <br/>
        //
    //
    // Click link berikut untuk lebih lanjut <a href='" . base_url('action/dok/  '. $e) . "'> APPROVAL DOCUMENTS </a> <br/><br/><br/>
        //
    // Demikian, terima kasih";
        // $this->email($message, $subject, '', $to, $cc);
    }

    public function resend() {
        if (isset($_POST['ID'])) {
            error_reporting(0);

            $param = $_POST;
            // $sap_result = $this->create_notif($param);
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data = $this->prog->get_record("A.ID='{$param['ID']}'");
            $data = $data[0];
            // echo $this->db->last_query();
            // exit;
            // print_r($data);

            if ($data) {

                // $to=array($atasan->EMAIL);
                $to = array('adhiq.rachmadhuha@sisi.id');
                // $cc=array();
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                $cc = array('purnawan.yose@gmail.com');
                // $this->send_mail($subject, $data, $atasan, $n_form, $to, $cc);

                $dtApp = $this->app->get_data("REF_ID = '{$param['ID']}' AND TIPE = 'TTD'");
                $dtPar = $this->app->get_data("REF_ID = '{$param['ID']}' AND TIPE = 'Paraf'");

                if (count($dtApp)) {
                    foreach ($dtApp as $key) {
                        if ($key['STATUS'] == 'In Approval') {
                            $subject = "Resend : DOCUMENT ENGINERING APPROVAL";
                            $n_form = $this->my_crypt("{$key['REF_ID']};{$key['LVL']};{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};TTD;{$key['APP_BADGE']} - {$key['APP_NAME']}", 'e');
                            $this->send_mail($subject, $data, "{$key['APP_BADGE']} - {$key['APP_NAME']}", $n_form, $to, $cc);
                            break;
                        }
                    }
                }
                if (count($dtPar)) {
                    foreach ($dtPar as $key) {
                        if ($key['STATUS'] == 'In Approval') {
                            $subject = "Resend : PACKET APPROVAL";
                            $n_form = $this->my_crypt("{$key['REF_ID']};{$key['LVL']};{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};TTD;{$key['APP_BADGE']} - {$key['APP_NAME']}", 'e');
                            $this->send_mail($subject, $data, "{$key['APP_BADGE']} - {$key['APP_NAME']}", $n_form, $to, $cc);
                        }
                    }
                }

                $res_message = 'Email has been sent' . '-';
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function create() {
        if (isset($_POST) && isset($_POST['ID_PENUGASAN']) && isset($_POST['START_DATE']) && isset($_POST['END_DATE']) && isset($_POST['PROGRESS']) && isset($_POST['REMARKS'])) {
            $param = $_POST;

            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $param['CREATE_BY'] = $info['name'];
            $param['COMPANY'] = $info['company'];
            $param['DEPT_CODE'] = $info['dept_code'];
            $param['DEPT_TEXT'] = $info['dept_text'];
            $UPDATE_DATE = str_replace("-", "/", $data['UPDATE_DATE']);
            unset($param['UPDATE_DATE']);


            $result = $this->prog->save($param);
            // echo $this->db->last_query();

            if ($result) {

                $res_message = 'Data has been saved' . '-';
                if (!$result['ID']) {
                    // if($result != 1){
                    $res_message = 'Failed to save data. Please check input parameter!';
                } else {
                    // create log progress
                    $data['REF_ID'] = $result['ID'];
                    $data['REF_TABLE'] = 'MPE_DTL_PENUGASAN';
                    $data['DESKRIPSI'] = 'Log Progress Engineering';
                    $data['NOTE'] = $_POST['REMARKS'];
                    $data['LOG_RES1'] = $_POST['PROGRESS'];
                    $data['CREATE_BY'] = $info['name'];
                    $this->db->set('UPDATE_AT', "TO_DATE('$UPDATE_DATE','yyyy/mm/dd')", false);
                    $res_hasil = $this->general_model->log_saving($data);

                    if ($res_hasil != 1) {
                        $res_message = 'History log progress cannot inserted!';
                    }
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function update() {
        if (isset($_POST) && isset($_POST['ID']) && isset($_POST['ID_PENUGASAN']) && isset($_POST['START_DATE']) && isset($_POST['END_DATE']) && isset($_POST['PROGRESS']) && isset($_POST['REMARKS'])) {
            $param = $_POST;

            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $param['UPDATE_BY'] = $info['username'];
            $param['COMPANY'] = $info['company'];
            $UPDATE_DATE = str_replace("-", "/", $param['UPDATE_DATE']);
            unset($param['UPDATE_DATE']);

            $result = $this->prog->updateData('update', $param);
            // echo $this->db->last_query();
            // save docs
            $data = array();
            if ($_FILES) {
                for ($i = 0; $i < count($_FILES['FILES']['name']); $i++) {
                    if (isset($_FILES['FILES'])) {
                        if ($_FILES["FILES"]["error"][$i] > 0) {
                            echo "Error: " . $_FILES["FILES"]["error"][$i] . "<br>";
                        } else {
                            $tmpfile = array();
                            $tmpfile['name'] = $_FILES["FILES"]["name"][$i];
                            $tmpfile['type'] = $_FILES["FILES"]["type"][$i];
                            $tmpfile['tmp_name'] = $_FILES["FILES"]["tmp_name"][$i];
                            $tmpfile['error'] = $_FILES["FILES"]["error"][$i];
                            $tmpfile['size'] = $_FILES["FILES"]["size"][$i];
                            $url_files = $this->upload($tmpfile, (object) $info, $param['ID_PENUGASAN'], 'progress', "{$param['ID']}-{$tmpfile['name']}-$i");
                            $sname = explode("/", $url_files);

                            $tmp = array();
                            $tmp['DOC_ID'] = $param['ID'];
                            $tmp['TIPE'] = 'PROGRESS';
                            $tmp['DESCRIPTION'] = $sname[count($sname) - 1];
                            $tmp['PATHS'] = $url_files;
                            $tmp['CREATE_BY'] = $param['UPDATE_BY'];
                            $data[] = $tmp;
                            // echo $url_files . "<br>";
                        }
                    }
                }
                // echo "<pre>";
                // print_r($_FILES);
                // print_r($data);
                // echo "</pre>";
                // exit;

                $result = $this->general_model->file_saving($data);
            }

            if ($result) {

                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                } else {
                    // create log progress
                    unset($data);
                    $data['REF_ID'] = $_POST['ID'];
                    $data['REF_TABLE'] = 'MPE_DTL_PENUGASAN';
                    $data['DESKRIPSI'] = 'Log Progress Engineering';
                    $data['NOTE'] = $_POST['REMARKS'];
                    $data['LOG_RES1'] = $_POST['PROGRESS'];
                    $data['CREATE_BY'] = $info['name'];
                    $this->db->set('UPDATE_AT', "TO_DATE('$UPDATE_DATE','yyyy/mm/dd')", false);
                    $res_hasil = $this->general_model->log_saving($data);

                    if ($res_hasil != 1) {
                        $res_message = 'History log progress cannot inserted!';
                    }
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function approve() {
        if (isset($_GET['e'])) {
            $e = $_GET['e'];
            // $e =  str_replace(' ', '+', $e);
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');
            list($id, $lvl, $notifikasi, $paket, $tipe, $nama) = split(";", $no_form);
            $pgw = explode(' - ', $nama);

            // update approval
            unset($data); // clear array
            $data['ID'] = $id;
            $data['TIPE'] = $tipe;
            $data['APP_NAME'] = $pgw[1];
            $data['STATE'] = 'Approved';
            $data['STATUS'] = 'Approved';
            $data['UPDATE_BY'] = $pgw[1];
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $this->db->where('APP_NAME', $data['APP_NAME']);
            $result = $this->app->updateData('approve', $data);

            // set next approval
            unset($data['APP_NAME']);
            $data['STATE'] = 'New';
            $data['STATUS'] = 'In Approval';
            $this->db->where('LVL', (int) $lvl + 1);
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $result = $this->app->updateData('approve', $data);

            if ($tipe == 'TTD') {
                // update trans
                unset($data);
                $data['ID'] = $id;
                $data['STATUS'] = 'Approved ' . $lvl;
                $data['APPROVE' . $lvl . '_BY'] = $pgw[1];
                $this->db->set('APPROVE' . $lvl . '_AT', "CURRENT_DATE", false);
                $data['UPDATE_BY'] = $pgw[1];
                $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
                $result = $this->prog->updateData('approve', $data);

                // get next approval
                $dtTrn = $this->prog->get_record("A.ID='{$id}'");
                if ($dtTrn) {
                    $dtTrn = $dtTrn[0];
                }
                $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = '{$tipe}' AND LVL = '" . ((int) $lvl + 1) . "' AND STATUS='In Approval'");
                if ($dtApp) {
                    $dtApp = $dtApp[0];
                }
                // echo "<pre>";
                // print_r($dtTrn);
                // print_r($dtApp);
                // echo "</pre>";
                // exit;

                if ($dtApp) {
                    // send email
                    $n_form = $this->my_crypt("{$dtApp['REF_ID']};{$dtApp['LVL']};{$dtTrn['NOTIFIKASI']};{$dtTrn['PACKET_TEXT']};TTD;{$dtApp['APP_BADGE']} - {$dtApp['APP_NAME']}", 'e');
                    $n_form = str_replace('/', '6s4', $n_form);
                    $n_form = str_replace('+', '1p4', $n_form);
                    $n_form = str_replace('=', 'sM9', $n_form);

                    // $to=array($dtApp['APP_EMAIL']);
                    $to = array('adhiq.rachmadhuha@sisi.id');
                    // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                    $cc = array();
                    $subject = "DOCUMENT ENGINERING  APPROVAL";
                    $this->send_mail($subject, $dtTrn, "{$dtApp['APP_BADGE']} - {$dtApp['APP_NAME']}", $n_form, $to, $cc);
                }
            }

            if ($result) {
                echo "Documents Paket '{$paket}' was approved succesfully";
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function delete($id) {
        if ($id) {
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data['ID'] = $id;
            $data['DELETE_BY'] = $info['name'];
            $result = $this->prog->updateData('delete', $data);

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function export_pdf($id) {
        if ($id) {
            error_reporting(1);

            $info = $this->session->userdata;
            $info = $info['logged_in'];
            // $list_data = $_POST;
            $list_data = $this->prog->get_record("A.ID='{$id}'");
            $list_data = $list_data[0];
            $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'TTD'");
            $dtPar = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'Paraf'");
            // set list approval hris
            $where['GEN_VAL'] = '1';
            $where['GEN_CODE'] = 'EAT_PENGKAJI_UK';
            $where['GEN_TYPE'] = 'General';
            $global = $this->general_model->get_data($where);
            $dtCre = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['CREATE_BY']}' AND k.company = '{$list_data['COMPANY']}' AND (uk.muk_parent LIKE '" . $global->GEN_PAR5 . "' OR k.muk_kode = '" . $global->GEN_PAR5 . "')");
            if ($dtCre) {
                $dtCre = $dtCre[0];
            }
            // echo "<pre>";
            // print_r($dtCre);
            // print_r($global);
            // echo "</pre>";
            // exit;

            $uri = base_url();
            $this->load->library('Fpdf_gen');
            $this->fpdf->SetFont('Arial', 'B', 13);
            $this->fpdf->SetLeftMargin(20);

            // $this->fpdf->Cell(12,4,'',0,0);
            $this->fpdf->Cell(35, 6, $this->fpdf->Image($uri . 'media/logo/Logo_SI.png', $this->fpdf->GetX() + 7, $this->fpdf->GetY(), 0, 15, 'PNG'), 'LT', 0, 'C');
            $this->fpdf->Cell(100, 6, "PT. SEMEN INDONESIA (PERSERO)Tbk.", 'T', 0, 'C');
            $this->fpdf->Cell(35, 6, $this->fpdf->Image($uri . 'media/logo/si_group.jpg', $this->fpdf->GetX() + 3, $this->fpdf->GetY() + 4, 0, 10, 'JPG'), 'TR', 1, 'C');
            $this->fpdf->SetFont('Arial', '', 12);
            $this->fpdf->Cell(170, 6, "{$dtCre->DEPT_TEXT}", 'LR', 1, 'C');
            $this->fpdf->Cell(170, 6, "{$dtCre->CC_TEXT}", 'LBR', 1, 'C');

            $this->fpdf->Cell(170, 6, "", 'LBR', 1);
            $this->fpdf->SetFillColor(239, 239, 245);
            $this->fpdf->Cell(50, 6, " No. Dokumen", 'LT', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 'T', 0, 'C', 1);
            $this->fpdf->Cell(110, 6, "{$list_data['NO_DOK_ENG']}", 'TR', 1, 'L', 1);
            $this->fpdf->Cell(50, 6, " Notifikasi", 'L', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 0, 0, 'C', 1);
            $this->fpdf->Cell(110, 6, "{$list_data['NOTIFIKASI']}", 'R', 1, 'L', 1);
            $this->fpdf->Cell(50, 6, " Tahun", 'BL', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 'B', 0, 'C', 1);
            $time = strtotime($list_data['CREATE_AT']);
            $year = date('Y', $time);
            $this->fpdf->Cell(110, 6, "{$year}", 'BR', 1, 'L', 1);
            // $this->fpdf->Cell(110,6,"{$list_data['CREATE_AT']}",'R',1,'L',1);
            // $this->fpdf->SetFont('Arial','B',7);
            // $this->fpdf->Cell(170,1,'R/538/012',0,1,'R');
            // $this->fpdf->Cell(0,9,'',0,1);
            // $this->fpdf->SetFont('Arial','B',7);
            // $this->fpdf->Cell(64,1,'PT. SEMEN INDONESIA (PERSERO)Tbk.',0,0);
            $this->fpdf->Cell(170, 6, "", 'LBR', 1);

            $this->fpdf->SetFont('Arial', 'B', 18);
            $this->fpdf->Cell(12, 4, '', 0, 2);
            $this->fpdf->Ln(60);
            $this->fpdf->MultiCell(170, 1, "{$list_data['PACKET_TEXT']}", 0, 'C');

            $this->fpdf->SetFont('Arial', 'B', 8);
            $this->fpdf->Ln(60);
            $this->fpdf->Cell(12, 4, '', 0, 2);
            $this->fpdf->Cell(170, 5, "Pengesahan", 1, 0, 'L');
            // approval info
            $width = (170 / (count($dtApp) + 1));
            // $width = floor(170/(count($dtApp)+1));
            $this->fpdf->Ln(5);
            $this->fpdf->Cell($width, 5, "Dibuat Oleh,", 'LTR', 0, 'C');
            foreach ($dtApp as $key) {
                $this->fpdf->Cell($width, 5, "Disetujui Oleh,", 'LTR', 0, 'C');
            }
            $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'I', 8);
            $y1 = $this->fpdf->GetY();
            $x1 = $this->fpdf->GetX();
            $lnmax = 30;
            if (count($dtApp) > 2) {
                $lnmax = 20;
            }
            $h = 10;
            if (strlen($dtCre->JAB_TEXT) > $lnmax) {
                $h = 5;
            }
            $this->fpdf->MultiCell($width, $h, "{$dtCre->JAB_TEXT}", 'LBR', 'C');
            $y2 = $this->fpdf->GetY();
            $pos_x = 20 + $width;
            foreach ($dtApp as $key) {
                if (strlen($dtCre->JAB_TEXT) > $lnmax) {
                    $h = 5;
                } else {
                    $h = 10;
                }
                $this->fpdf->SetXY($pos_x, $y1);
                $this->fpdf->MultiCell($width, $h, "{$key['APP_JAB']}", 'LBR', 'C');
                $pos_x += $width;
            }

            // QR code
            $this->fpdf->Ln(0);
            include(APPPATH . 'libraries/phpqrcode/qrlib.php');
            $tmpCre = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-CREATE") . ".png";
            $str = $this->my_crypt("{$list_data['ID']};{$list_data['CREATE_BY']};{$list_data['CREATE_AT']};", 'e');
            $str = str_replace('/', '6s4', $str);
            $str = str_replace('+', '1p4', $str);
            $str = str_replace('=', 'sM9', $str);
            $str = base_url() . "info/dok_eng/" . $str;
            QRcode::png($str, $tmpCre);
            $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
            foreach ($dtApp as $key) {
                if ($key['STATE'] == 'Approved') {
                    $tmpApp = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APPROVE{$key['LVL']}-TTD") . ".png";
                    $strApp = $this->my_crypt("{$list_data['ID']};{$key['APP_NAME']};{$key['UPDATE_AT']};", 'e');
                    $strApp = str_replace('/', '6s4', $strApp);
                    $strApp = str_replace('+', '1p4', $strApp);
                    $strApp = str_replace('=', 'sM9', $strApp);
                    $strApp = base_url() . "info/dok_eng/" . $strApp;
                    QRcode::png($strApp, $tmpApp);
                    $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpApp, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
                } else {
                    $this->fpdf->Cell($width, 25, "", "LTR", 0, 'C');
                }
            }

            $this->fpdf->Ln(25);
            $this->fpdf->Cell($width, 5, "({$list_data['CREATE_BY']})", 'LBR', 0, 'C');
            foreach ($dtApp as $key) {
                $this->fpdf->Cell($width, 5, "({$key['APP_NAME']})", 'LBR', 0, 'C');
            }
            $this->fpdf->Ln(7);
            $this->fpdf->SetFont('Arial', '', 7);
            $this->fpdf->Cell(170, 2, "* dokumen ini di approve oleh sistem e-DEMS.", 0, 1);
            $this->fpdf->Ln(15);
            // $width = floor(170/(count($dtPar)));
            $width = 15;
            $this->fpdf->SetFont('Arial', '', 8);
            $this->fpdf->Cell(170 - ($width * count($dtPar)), 10, "paraf : ", 0, 0, 'R');
            foreach ($dtPar as $key) {
                if ($key['STATE'] == 'Approved') {
                    $tmpPar = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APPROVE{$key['LVL']}-Paraf") . ".png";
                    $strPar = $this->my_crypt("{$list_data['ID']};{$key['APP_NAME']};{$key['UPDATE_AT']};", 'e');
                    $strPar = str_replace('/', '6s4', $strPar);
                    $strPar = str_replace('+', '1p4', $strPar);
                    $strPar = str_replace('=', 'sM9', $strPar);
                    $strPar = base_url() . "info/dok_eng/" . $strPar;
                    QRcode::png($strPar, $tmpPar);
                    $this->fpdf->Cell($width, 10, $this->fpdf->Image($tmpPar, $this->fpdf->GetX() + ($width / 2) - 5, $this->fpdf->GetY(), 0, 10, 'PNG'), 1, 0, 'C');
                } else {
                    $this->fpdf->Cell($width, 10, "", 1, 0, 'C');
                }
            }

            $this->fpdf->Cell(12, 4, '', 0, 0);

            // temporary download
            $tmpPath = FCPATH . "media/upload/tmp/" . md5("{$list_data['NO_DOK_ENG']} {$list_data['PACKET_TEXT']}") . ".pdf";
            $this->fpdf->Output($tmpPath, 'F');

            $arfile = array($tmpPath);
            // RKS
            if ($list_data['RKS_FILE']) {
                array_push($arfile, FCPATH . $list_data['RKS_FILE']);
            }
            // BQ
            if ($list_data['BQ_FILE']) {
                array_push($arfile, FCPATH . $list_data['BQ_FILE']);
            }
            // Drawwing
            if ($list_data['DRAW_FILE']) {
                array_push($arfile, FCPATH . $list_data['DRAW_FILE']);
            }
            // // ECE
            // if ($list_data['ECE_FILE']) {
            //   array_push($arfile, $list_data['ECE_FILE']);
            // }

            $this->load->library('PDFMerger');
            $pdf = new PDFMerger;

            foreach ($arfile as $key => $value) {
                $pdf->addPDF($value, 'all');
            }
            // $pdf->addPDF('samplepdfs/three.pdf', 'all'); //  'all' or '1,3'->page

            $pdf->merge('download', "{$list_data['NO_DOK_ENG']} {$list_data['PACKET_TEXT']}.pdf");
            // REPLACE 'file' WITH 'browser', 'download', 'string', or 'file' for output options
        } else {
            echo "File Not Found!";
        }
    }

}
