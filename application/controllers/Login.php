<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["tittle"] = "Login Page";
		$data["short_tittle"] = "Login";
    unset($where);
    $where['GEN_VAL']='1';
    $where['GEN_TYPE']='Themes';
    $global = $this->general_model->get_data_result($where);
		$data["themes"] = $global;
    $session = $this->session->userdata('logged_in');
    $info = (array)$session;
    if(empty($info['username'])){
		 $this->load->view('login', $data);
		}else {
		 redirect('welcome');
		}
	}
}
