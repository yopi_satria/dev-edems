<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


 	function __construct()
 	{
 	   parent::__construct();

 		 $this->load->model('M_hris', 'dbhris');
		 $this->load->model('erf/M_request', 'req');
     $this->load->model("eat/M_assign", "assig");
     $this->load->model("dok_eng/M_dok_eng", "dok_eng");
  	 $this->load->model("others/M_approval", "app");
  	 $this->load->model("others/M_dashboard", "dash");
  	 $this->load->model("others/M_slide", "slide");
 		 // $session = $this->session->userdata('logged_in');
 		 // $info = (array)$session;
 		 // if(!isset($info['username'])){
 		 // 	redirect('login');
 		 // }
 		 // if(isset($_SESSION)){
 		 // 	redirect('Login');
 		 // }
 	}

	public function index()
	{
    $session = $this->session->userdata('logged_in');
    $info = (array)$session;
    if(empty($info['username'])){
     redirect('login');
    }
		$header["tittle"] = "Dashboard";
		$header["short_tittle"] = "wlc";
    // $header["roles"] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
    // view roles
    $this->db->distinct();
    $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
    $header['roles']	= json_decode(json_encode($header['roles']), true);
    $state='';
    foreach ($header['roles'] as $i => $val) {
      $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
      $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
      if ($val['code']==$header["short_tittle"]) {
        // code...
        $state = $val['stat'];
      }

      // get count pending status
      if (isset($val['tabel'])) {
        $npending = 0;
        if ($info['special_access'] == 'true') {
          $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans']);
        }
        else {
          if (strpos( $val['stat'], "-child" ) !== false) {
            if (strpos( $val['stat'], "{$val['code']}-child=1" ) !== false) {
              $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
              $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans']);
            }
          }
          else {
            $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans'],$info['uk_kode']);
          }
        }
        $header['roles'][$i]['count'] = $npending;
      }

    }
    $header['page_state'] = $state;
    $data['slides'] = $this->slide->get_record();

    $sHdr = '';
    if (isset($info['themes'])) {
      if ($info['themes']!='default') $sHdr = "_".$info['themes'];
    }else {
      unset($where);
      $where['GEN_CODE']='themes-app';
      $where['GEN_TYPE']='General';
      $global = (array)$this->general_model->get_data($where);
      if (isset($global) && $global['GEN_VAL']!='0') {
        // code...
        $i_par = $global['GEN_VAL'];
        $sHdr = "_".$global['GEN_PAR'.$i_par];
      }
    }
    if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
      $this->load->view('general/header'.$sHdr, $header);
      $this->load->view('welcome_message', $data);
    }else {
      $header["tittle"] = "Forbidden";
      $header["short_tittle"] = "403";

      $this->load->view('general/header'.$sHdr, $header);
      $this->load->view('forbidden');
    }
		// $this->load->view('general/header_matcha', $header);
		// $this->load->view('welcome_message');
		$this->load->view('general/footer');
	}

  public function grafik(){
    $result = $this->general_model->grafik_all();
    if(($result)){
      echo $this->response('success', 200, $result);
    }else{
      echo $this->response('error', 400, $result);
    }
  }

  public function grafik_notif(){
    $result = $this->general_model->grafik_notif();
    if(($result)){
      echo $this->response('success', 200, $result);
    }else{
      echo $this->response('error', 400, $result);
    }
  }

  public function grafik_erf(){
    $result = $this->general_model->grafik_erf();
    if(($result)){
      echo $this->response('success', 200, $result);
    }else{
      echo $this->response('error', 400, $result);
    }
  }

  public function grafik_date(){
    $result = $this->general_model->grafik_date();
    if(($result)){
      echo $this->response('success', 200, $result);
    }else{
      echo $this->response('error', 400, $result);
    }
  }

  public function grafik_stat(){
    $result = $this->general_model->grafik_stat();
    if(($result)){
      echo $this->response('success', 200, $result);
    }else{
      echo $this->response('error', 400, $result);
    }
  }

  public function count(){
    $info = $this->session->userdata;
		$info = $info['logged_in'];

    if ($info['special_access']=='false' || $info['special_access']=='0') {
      $this->db->where("DEPT_CODE",$info['dept_code']);
		}

    $from = $_POST['from'];
    $status = $_POST['status'];
    $result = $this->dash->count($status, $from);
    echo $result;
  }

  public function countall(){
    $info = $this->session->userdata;
    $info = $info['logged_in'];

    if ($info['special_access']=='false' || $info['special_access']=='0') {
      $this->db->where("DEPT_CODE",$info['dept_code']);
    }

    $from = $_POST['from'];
    $result = $this->dash->countall($from);
    echo $result;
  }

}
