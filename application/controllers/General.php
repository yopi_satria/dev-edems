<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	private $info=array();

	function __construct()
	{
	   parent::__construct();

		 $this->load->model('M_hris', 'dbhris');
	   $this->load->model('M_general', 'req');
		 // $session = $this->session->userdata('logged_in');
		 // $info = (array)$session;
		 // if(empty($info['username'])){
		 // 	redirect('login');
		 // }
 		 // if(isset($_SESSION)){
 		 // 	redirect('login');
 		 // }
	}

	public function getNotificationPengajuan(){
			$session = $this->session->userdata('logged_in');
	    $info = (array)$session;
	    if(empty($info['username'])){
	     redirect('login');
			 }else{
				 $id = $info['name'];
			 }
  		$data = $this->req->getNotification($id);
  		$return = json_encode($data);
  		echo $return;
  }

	public function getAllLogin(){
		$data = $this->req->getAllLogin();
		$return = json_encode($data);
		echo $return;
	}

	public function deleteSession(){
		$session = $this->session->userdata('logged_in');
		$info = (array)$session;
		if(empty($info['username'])){
		 	redirect('login');
		 }else{
				$id = session_id();
				$this->req->deleteLogin();
		 }
	}

	public function saveSession(){
		$session = $this->session->userdata('logged_in');
		$info = (array)$session;
		$time = time();

		if(empty($info['username'])){
		 	redirect('login');
		 }else{
			 $id = session_id();
			 $data = $this->req->getSession( $id );
		 		$return = json_encode($data);
				if ($return == 0) {
					// code...
					$key	= array(
						'session'	=>	$id,
						'waktu'	=>	$time,
					);
					$this->req->saveLogin($key);
				}else {
					$key	= array(
						'waktu'	=>	$time,
					);
					$this->req->updateLogin($key,$id);
				}
		 }
	}

}
