<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitor extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
 	public function __construct(){
 		parent::__construct();
    $this->load->model('M_hris', 'dbhris');
 		$this->load->model("report/M_monitor", "monitor");
 	}

	public function index()
	{
    $session = $this->session->userdata('logged_in');
    $info = (array)$session;
    if(empty($info['username'])){
     redirect('login');
    }

		$header["tittle"] = "Monitoring";
		$header["short_tittle"] = "mtr";
    // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
    // view roles
    $this->db->distinct();
    $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
    $header['roles']	= json_decode(json_encode($header['roles']), true);
    $state='';
    foreach ($header['roles'] as $i => $val) {
      $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
      $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
      if ($val['code']==$header["short_tittle"]) {
        // code...
        $state = $val['stat'];
      }
      // get count pending status
      if (isset($val['tabel'])) {
        $npending = 0;
        if ($info['special_access'] == 'true') {
          $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans']);
        }
        else {
          if (strpos( $val['stat'], "-child" ) !== false) {
            if (strpos( $val['stat'], "{$val['code']}-child=1" ) !== false) {
              $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
              $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans']);
            }
          }
          else {
            $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans'],$info['uk_kode']);
          }
        }
        $header['roles'][$i]['count'] = $npending;
      }
    }
    $header['page_state'] = $state;

    // $data['DEPT'] = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
    // $data['BIRO'] = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
    // $data['FOREIGN'] = $this->dok_eng->get_foreign();

    $sHdr = '';
    if (isset($info['themes'])) {
      if ($info['themes']!='default') $sHdr = "_".$info['themes'];
    }else {
      unset($where);
      $where['GEN_CODE']='themes-app';
      $where['GEN_TYPE']='General';
      $global = (array)$this->general_model->get_data($where);
      if (isset($global) && $global['GEN_VAL']!='0') {
        $i_par = $global['GEN_VAL'];
        $sHdr = "_".$global['GEN_PAR'.$i_par];
      }
    }
    if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
      $this->load->view('general/header'.$sHdr, $header);
      $this->load->view('report/monitoring');
    }else {
      $header["tittle"] = "Forbidden";
      $header["short_tittle"] = "403";

      $this->load->view('general/header'.$sHdr, $header);
      $this->load->view('forbidden');
    }
		// $this->load->view('general/header', $header);
		// $this->load->view('report/monitoring');
		$this->load->view('general/footer');
	}

	public function data_list(){
		$info = $this->session->userdata;
		$info = $info['logged_in'];

		$search	= $this->input->post('search');
		$order	= $this->input->post('order');

		$key	= array(
			'search'	=>	$search['value'],
			'ordCol'	=>	$order[0]['column'],
			'ordDir'	=>	$order[0]['dir'],
			'length'	=>	$this->input->post('length'),
			'start'		=>	$this->input->post('start')
		);
    // push date range
    if ($_POST['start_date']) {
      $start	= str_replace("-","/",$this->input->post('start_date'));
      $key['start_date'] = $start;
    }
    if ($_POST['end_date']) {
      $end	= str_replace("-","/",$this->input->post('end_date'));
      $key['end_date'] = $end;
    }
		if ($info['special_access']=='false' || $info['special_access']=='0') {
			$key['name'] = $info['name'];
			$key['username'] = $info['username'];
      $key['dept_code'] = $info['dept_code'];
      $key['uk_code'] = $info['uk_kode'];
		}

    $data	= $this->monitor->get_data($key);
		$return	= array(
			'draw'				=>	$this->input->post('draw'),
			'data'				=>	$data,
			'recordsFiltered'	=>	$this->monitor->recFil($key),
			'recordsTotal'		=>	$this->monitor->recTot($key)
		);

		echo json_encode($return);
  }

	public function count_report(){

    $result = $this->monitor->get_count();

    if($result){
      echo $this->response('success', 200, $result);
    }else{
      echo $this->response('error', 400);
    }
  }

  public function excel(){
      $this->load->library('Lixel');
      // error_reporting(1);

      // $id = (int) $this->input->get('id');
      // $tahun = $this->input->get('tahun');
      // $start = $this->input->post('start');
      // $end = $this->input->post('end');
      $tahun = date("Y");
      // if (isset($start)||isset($end)) {
      //   if (isset($start)) {
      //     $start	= str_replace("-","/",$start);
      //     $this->db->where("R.CREATE_AT >= TO_DATE('{$start}','yyyy/mm/dd')");
      //   }else {
      //     $end	= str_replace("-","/",$end);
      //     $this->db->where("R.CREATE_AT <= TO_DATE('{$end}','yyyy/mm/dd')");
      //   }
      // }else {
      //   // code...
      //   $this->db->where("TO_CHAR(R.CREATE_AT, 'YYYY') = '{$tahun}'");
      // }

      if(isset($_POST)){ //&& isset($id_ukur)
        $start = $this->input->post('start');
        $start	= str_replace("-","/",$start);
        $end = $this->input->post('end');
        $end	= str_replace("-","/",$end);
        if ((!$start) && (!$end)) {
          $this->db->where("TO_CHAR(R.CREATE_AT, 'YYYY') = '{$tahun}'");
        }else {
          if (($start)) {
            $this->db->where("R.CREATE_AT >= TO_DATE('{$start}','yyyy/mm/dd')");
          }
          if (($end)) {
            $this->db->where("R.CREATE_AT <= TO_DATE('{$end}','yyyy/mm/dd')");
          }
        }

        $rDetail = $this->monitor->get_record();
        // echo "<pre>";
        // print_r($rDetail);
        // echo "</pre>";
        // exit;

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

        $objset     = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
        $objget     = $objPHPExcel->getActiveSheet();  //inisiasi get object

        $abjad = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S'
        );
        //
        $center_ver = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );
        $top_ver = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::VERTICAL_TOP,
            )
        );
        $style_center = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'style' => PHPExcel_Style_Border::BORDER_MEDIUM
            )
        );
        $style_right = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'style' => PHPExcel_Style_Border::BORDER_MEDIUM
            )
        );
        $border_all = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $border_outline = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $style_bgBlue = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '01A9DB')
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF')
            )
        );
        $style_bgOrange = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => 'DF7401')
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF')
            )
        );
        $style_bgGreenLight = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => '5CD65C')
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF')
            )
        );
        $style_bgPink = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => 'FF33CC')
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF')
            )
        );
        $style_bgGreenOld = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => '86B404')
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF')
            )
        );
        $style_bgPurple= array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => 'CC00FF')
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF')
            )
        );
        $style_bgGreen = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => '9ACD32')
            ),
            'font' => array(
                'color' => array('rgb' => '#000000')
            )
        );
        $font_red = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000')
            )
        );
        $font_black = array(
            'font' => array(
                'color' => array('rgb' => '000000')
            )
        );
        $styleArial = array(
            'font'  => array(
            'bold'  => true,
            'color' => array('rgb' => 'FF0000'),
            'size'  => 15,
            'name'  => 'Arial'
        ));

        $objget->getColumnDimension('A')->setWidth('5');
        $objget->getColumnDimension('B')->setWidth('5');
        $objget->getColumnDimension('C')->setWidth('25');
        $objget->getColumnDimension('D')->setWidth('20');
        $objget->getColumnDimension('E')->setWidth('13');
        $objget->getColumnDimension('F')->setWidth('15');
        $objget->getColumnDimension('G')->setWidth('40');
        $objget->getColumnDimension('H')->setWidth('45');
        $objget->getColumnDimension('I')->setWidth('16');
        $objget->getColumnDimension('J')->setWidth('25');
        $objget->getColumnDimension('K')->setWidth('25');
        $objget->getColumnDimension('L')->setWidth('25');
        $objget->getColumnDimension('M')->setWidth('13');
        $objget->getColumnDimension('N')->setWidth('25');
        $objget->getColumnDimension('O')->setWidth('25');
        $objget->getColumnDimension('P')->setWidth('16');
        $objget->getColumnDimension('Q')->setWidth('50');
        $objget->getColumnDimension('R')->setWidth('25');
        $objget->getColumnDimension('S')->setWidth('20');
        $objget->getColumnDimension('T')->setWidth('25');
        $objget->getColumnDimension('U')->setWidth('20');
        $objget->getColumnDimension('V')->setWidth('20');
        $objget->getColumnDimension('W')->setWidth('50');
        // $objget->getColumnDimension('R')->setWidth('50');
        //HEADER
        $gdImage = imagecreatefromjpeg('media/logo/logo_si.jpg');
        $objDrawing_bef = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing_bef->setName('Before image');
        $objDrawing_bef->setDescription('Before image');
        $objDrawing_bef->setImageResource($gdImage);
        $objDrawing_bef->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
        $objDrawing_bef->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing_bef->setHeight(70);
        $objDrawing_bef->setWidth(100);
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:C3');
        $objDrawing_bef->setCoordinates('B2');
        $objDrawing_bef->setWorksheet($objPHPExcel->getActiveSheet());
        $objset->setCellValue('C2', "                       DEPARTMENT OF DESIGN ENGINEERING");
        $objset->setCellValue('C3', "                       Bureau of Design Engineering Integration");

        $objget->getStyle('B5:B6')->getFont()->setSize(12)->setBold(true);
        $objget->getStyle('B5:B6')->applyFromArray($style_center);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:R5');
        $objset->setCellValue('B5', "CONTROL & REPORT PENGELOLAAN RENCANA KERJA CAPEX DAN KAJIAN DEPARTMENT OF DESIGN & ENGINEERING");
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B6:R6');
        $objset->setCellValue('B6', "TAHUN {$tahun}");
        //END HEADER

        //TABLE
          // -- thead
          $objget->getRowDimension('9')->setRowHeight(25);
          $objget->getStyle('B9:W9')->getFont()->setBold(true);
          $objget->getStyle('B9:W9')->applyFromArray($border_all);
          $objget->getDefaultStyle()->applyFromArray($top_ver);
          $objget->getStyle('B9:W9')->applyFromArray($style_center);
          $objget->getStyle('B9:K9')->applyFromArray($style_bgBlue);
          $objset->setCellValue('B9', "No.");
          $objset->setCellValue('C9', "Deskripsi");
          $objset->setCellValue('D9', "No. ERF");
          $objset->setCellValue('E9', "No. Notifikasi");
          $objset->setCellValue('F9', "Tanggal ERF");
          $objset->setCellValue('G9', "UK Peminta");
          $objset->setCellValue('H9', "UK Tujuan");
          $objset->setCellValue('I9', "Jenis");
          $objset->setCellValue('J9', "No. Proyek");
          $objset->setCellValue('K9', "Status Approval");
          $objget->getStyle('L9:Q9')->applyFromArray($style_bgOrange);
          $objset->setCellValue('L9', "No. EAT");
          $objset->setCellValue('M9', "Tanggal EAT");
          $objset->setCellValue('N9', "Status Approval");
          $objset->setCellValue('O9', "Nama Pekerjaan");
          $objget->getStyle('P9:R9')->applyFromArray($style_bgGreenLight);
          $objset->setCellValue('P9', "No. DE");
          $objset->setCellValue('Q9', "Tanggal DE");
          $objset->setCellValue('R9', "Status Approval");
          $objget->getStyle('S9:T9')->applyFromArray($style_bgPurple);
          $objset->setCellValue('S9', "(Σ) Progress");
          $objset->setCellValue('T9', "Transmital");
          $objget->getStyle('U9:W9')->applyFromArray($style_bgPink);
          $objset->setCellValue('U9', "No. Tender");
          $objset->setCellValue('V9', "Tanggal TD");
          $objset->setCellValue('W9', "Status Approval");
          // -- end thead
          // tbody
          $no = 10;
          $sTipe = '';
          $sLoc = '';
          $sArea = '';
          $no_row = 1;
          $index = 1;
          foreach ($rDetail as $i => $val) {
            $tmpno=$no;
            $objset->setCellValue('B'.$no, "{$no_row}");
            $objset->setCellValue('C'.$no, "{$val['NAMA_PEKERJAAN']}");
            $objset->setCellValue('D'.$no, "{$val['NO_PENGAJUAN']}");
            $objset->setCellValue('E'.$no, "{$val['NOTIFIKASI']}");
            $objset->setCellValue('F'.$no, "{$val['R_DATE']}");
            $objset->setCellValue('G'.$no, "{$val['DESCPSECTION']}");
            $objset->setCellValue('H'.$no, "{$val['DESCPGROUP']}");
            $objset->setCellValue('I'.$no, "{$val['TIPE']}");
            $objset->setCellValue('J'.$no, "{$val['WBS_CODE']}");
            $objset->setCellValue('K'.$no, "{$val['R_APPROVE']}");
            $objset->setCellValue('L'.$no, "{$val['NO_PENUGASAN']}");
            $objset->setCellValue('M'.$no, "{$val['A_DATE']}");
            $objset->setCellValue('N'.$no, "{$val['A_APPROVE']}");
            $objset->setCellValue('O'.$no, "{$val['A_PACKET']}");
            $objset->setCellValue('P'.$no, "{$val['NO_DOK_ENG']}");
            $objset->setCellValue('Q'.$no, "{$val['D_DATE']}");
            $objset->setCellValue('R'.$no, "{$val['D_APPROVE']}");
            $objset->setCellValue('S'.$no, "{$val['TOT_PROGRESS']}");
            $objset->setCellValue('T'.$no, "{$val['TRANSMITAL']}");
            $objset->setCellValue('U'.$no, "{$val['NO_DOK_TEND']}");
            $objset->setCellValue('V'.$no, "{$val['T_DATE']}");
            $objset->setCellValue('W'.$no, "{$val['T_APPROVE']}");
            // $arPacket = explode('<br />', rtrim($val['D_REMARKS'],"<br />"));
            // if (count($arPacket)>0) {
            //   foreach ($arPacket as $key => $value) {
            //     $objset->setCellValue('Q'.$no, $value);
            //     $no++;
            //   }
            //   $no--;
            // }
            // $objset->setCellValue('R'.$no, str_replace('<br />','\n',$val['D_REMARKS']));

            // merge cell
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("B{$tmpno}:B{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("C{$tmpno}:C{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("D{$tmpno}:D{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("E{$tmpno}:E{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("F{$tmpno}:F{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("G{$tmpno}:G{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("H{$tmpno}:H{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("I{$tmpno}:I{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("J{$tmpno}:J{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("K{$tmpno}:K{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("L{$tmpno}:L{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("M{$tmpno}:M{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("N{$tmpno}:N{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("O{$tmpno}:O{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("P{$tmpno}:P{$no}");
            // $objPHPExcel->setActiveSheetIndex(0)->mergeCells("Q{$tmpno}:Q{$no}");
            // border
            $objget->getStyle('B'.$tmpno.':B'.$no)->applyFromArray($border_outline);
            $objget->getStyle('C'.$tmpno.':C'.$no)->applyFromArray($border_outline);
            $objget->getStyle('D'.$tmpno.':D'.$no)->applyFromArray($border_outline);
            $objget->getStyle('E'.$tmpno.':E'.$no)->applyFromArray($border_outline);
            $objget->getStyle('F'.$tmpno.':F'.$no)->applyFromArray($border_outline);
            $objget->getStyle('G'.$tmpno.':G'.$no)->applyFromArray($border_outline);
            $objget->getStyle('H'.$tmpno.':H'.$no)->applyFromArray($border_outline);
            $objget->getStyle('I'.$tmpno.':I'.$no)->applyFromArray($border_outline);
            $objget->getStyle('J'.$tmpno.':J'.$no)->applyFromArray($border_outline);
            $objget->getStyle('K'.$tmpno.':K'.$no)->applyFromArray($border_outline);
            $objget->getStyle('L'.$tmpno.':L'.$no)->applyFromArray($border_outline);
            $objget->getStyle('M'.$tmpno.':M'.$no)->applyFromArray($border_outline);
            $objget->getStyle('N'.$tmpno.':N'.$no)->applyFromArray($border_outline);
            $objget->getStyle('O'.$tmpno.':O'.$no)->applyFromArray($border_outline);
            $objget->getStyle('P'.$tmpno.':P'.$no)->applyFromArray($border_outline);
            $objget->getStyle('Q'.$tmpno.':Q'.$no)->applyFromArray($border_outline);
            $objget->getStyle('R'.$tmpno.':R'.$no)->applyFromArray($border_outline);
            $objget->getStyle('S'.$tmpno.':S'.$no)->applyFromArray($border_outline);
            $objget->getStyle('T'.$tmpno.':T'.$no)->applyFromArray($border_outline);
            $objget->getStyle('U'.$tmpno.':U'.$no)->applyFromArray($border_outline);
            $objget->getStyle('V'.$tmpno.':V'.$no)->applyFromArray($border_outline);
            $objget->getStyle('W'.$tmpno.':W'.$no)->applyFromArray($border_outline);
            // $objget->getStyle('Q'.$no)->applyFromArray($style_right);
            // $objget->getStyle('R'.$tmpno.':R'.$no)->applyFromArray($border_outline);


            $no++;
            $no_row++;
          }

          // end tbody
        //END TABLE

        // FOOTER
        // if ($sSK_No != '-'){
        //   $objset->setCellValue('C'.($no + 2), '* Baku Mutu '. $sSK_No);
        // }
        // if ($sPIC != '-'){
        //   $objset->setCellValue('H'.($no + 2), 'Kasi. Pemantauan Lingkungan');
        //   $objset->setCellValue('H'.($no + 5), $sPIC);
        // }
        // END FOOTER


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');


        $filename = date('Ymd').' CONTROL & REPORT PENGELOLAHAN CAPEX '.$tahun.'.xls';
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        $objWriter->save('php://output');
    }
  // }
  }

  public function pdf(){
    // print_r($_POST);
    $tahun = date('Y');
    $info = $this->session->userdata;
    $info = $info['logged_in'];
    if (isset($_POST) && isset($info))
    {

      // $list_data = $_POST;
      $start = $this->input->post('start');
      $start	= str_replace("-","/",$start);
      $end = $this->input->post('end');
      $end	= str_replace("-","/",$end);
      if ((!$start) && (!$end)) {
        $this->db->where("TO_CHAR(R.CREATE_AT, 'YYYY') = '{$tahun}'");
      }else {
        if (($start)) {
          $this->db->where("R.CREATE_AT >= TO_DATE('{$start}','yyyy/mm/dd')");
        }
        if (($end)) {
          $this->db->where("R.CREATE_AT <= TO_DATE('{$end}','yyyy/mm/dd')");
        }
      }

      $list_data = $this->monitor->get_record();

      // $list_data = $list_data[0];

      // echo "<pre>";;
      // print_r($list_data);
      // print_r($dtApp1);
      // print_r($dtApp2);
      // echo "</pre>";
      // exit;

      $uri = base_url();
      $this->load->library('Fpdf_gen');
      $this->fpdf->SetFont('Arial','B',7);
      $this->fpdf->SetLeftMargin(20);

      // header
      $this->fpdf->Cell(30,11,$this->fpdf->Image($uri.'media/logo/Logo_SI.png',$this->fpdf->GetX(),$this->fpdf->GetY(),0,13,'PNG'),0,1,'C');
      $this->fpdf->Cell(19,4,'',0,0,'R');
      $this->fpdf->SetFont('Arial','',9);
      $this->fpdf->Cell(145,1,'PT. SEMEN INDONESIA (PERSERO)Tbk.',0,1);

      $this->fpdf->Ln(8);
      $this->fpdf->SetFont('Arial','BU',16);
      $sDate = '';
      if ((!$start) && (!$end)) {
        $sDate = $tahun;
      }else {
        if (($start)) {
          $sDate = "dari {$start}";
        }
        if (($end)) {
          $sDate = "- {$end}";
        }
      }
      $this->fpdf->Cell(170,7,"Logbook Engineering {$sDate}",0,1,'C');

      $this->fpdf->Ln(10);
      $this->fpdf->SetFont('Arial','B',10);
      $this->fpdf->Cell(10,6," NO",1,0,'R');
      $this->fpdf->Cell(135,6," Pekerjaan",1,0,'L');
      $this->fpdf->Cell(25,6," Progress",1,1,'C');

      $this->fpdf->SetFont('Arial','',10);
      foreach ($list_data as $key => $value) {
        $this->fpdf->Cell(10,6, ($key+1),1,0,'R');
        $this->fpdf->Cell(135,6, " {$value['NAMA_PEKERJAAN']}",1,0,'L');
        $this->fpdf->Cell(25,6, " ".number_format((float)$value['TOT_PROGRESS'], 2, '.', ''),1,1,'R');
        // code...
      }

      // line
      $this->fpdf->Ln(15);
      // $y = $this->fpdf->GetY();
      // $x = $this->fpdf->GetX();
      // $this->fpdf->SetLineWidth(0.5);
      // $this->fpdf->Line($x,$y,($x+170),$y);

      $this->fpdf->SetFont('Arial','',7);
      $this->fpdf->MultiCell(170,5," Print at ".date('d-m-Y H:i:s')." by {$info['name']}");

      $this->fpdf->Output("Logbook {$sDate}.pdf",'D');

    }
    // else {
    //   echo "File Not Found!";
    // }
  }



}
