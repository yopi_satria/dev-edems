<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Revenue extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
 	public function __construct(){
 		parent::__construct();
    $this->load->model('M_hris', 'dbhris');
 		$this->load->model("report/M_revenue", "revenue");
 	}

	public function index()
	{
    $session = $this->session->userdata('logged_in');
    $info = (array)$session;
    if(empty($info['username'])){
     redirect('login');
    }

		$header["tittle"] = "KA Revenue";
		$header["short_tittle"] = "rvn";
    // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
    // view roles
    $this->db->distinct();
    $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
    $header['roles']	= json_decode(json_encode($header['roles']), true);
    $state='';
    foreach ($header['roles'] as $i => $val) {
      $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
      $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
      if ($val['code']==$header["short_tittle"]) {
        // code...
        $state = $val['stat'];
      }
      // get count pending status
      if (isset($val['tabel'])) {
        $npending = 0;
        if ($info['special_access'] == 'true') {
          $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans']);
        }
        else {
          if (strpos( $val['stat'], "-child" ) !== false) {
            if (strpos( $val['stat'], "{$val['code']}-child=1" ) !== false) {
              $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
              $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans']);
            }
          }
          else {
            $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans'],$info['uk_kode']);
          }
        }
        $header['roles'][$i]['count'] = $npending;
      }
    }
    $header['page_state'] = $state;

    // $data['DEPT'] = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
    // $data['BIRO'] = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
    // $data['FOREIGN'] = $this->dok_eng->get_foreign();

    $sHdr = '';
    if (isset($info['themes'])) {
      if ($info['themes']!='default') $sHdr = "_".$info['themes'];
    }else {
      unset($where);
      $where['GEN_CODE']='themes-app';
      $where['GEN_TYPE']='General';
      $global = (array)$this->general_model->get_data($where);
      if (isset($global) && $global['GEN_VAL']!='0') {
        $i_par = $global['GEN_VAL'];
        $sHdr = "_".$global['GEN_PAR'.$i_par];
      }
    }
    if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
      $this->load->view('general/header'.$sHdr, $header);
      $this->load->view('report/revenue');
    }else {
      $header["tittle"] = "Forbidden";
      $header["short_tittle"] = "403";

      $this->load->view('general/header'.$sHdr, $header);
      $this->load->view('forbidden');
    }
		// $this->load->view('general/header', $header);
		// $this->load->view('report/monitoring');
		$this->load->view('general/footer');
	}

	public function data_list(){
		$info = $this->session->userdata;
		$info = $info['logged_in'];

		$search	= $this->input->post('search');
		$order	= $this->input->post('order');

		$key	= array(
			'search'	=>	$search['value'],
			'ordCol'	=>	$order[0]['column'],
			'ordDir'	=>	$order[0]['dir'],
			'length'	=>	$this->input->post('length'),
			'start'		=>	$this->input->post('start')
		);
    // push date range
    if ($_POST['start_date']) {
      $start	= str_replace("-","/",$this->input->post('start_date'));
      $key['start_date'] = $start;
    }
    if ($_POST['end_date']) {
      $end	= str_replace("-","/",$this->input->post('end_date'));
      $key['end_date'] = $end;
    }
		if ($info['special_access']=='false' || $info['special_access']=='0') {
			$key['name'] = $info['name'];
			$key['username'] = $info['username'];
      $key['dept_code'] = $info['dept_code'];
      $key['uk_code'] = $info['uk_kode'];
		}

    $data	= $this->revenue->get_data($key);
		$return	= array(
			'draw'				=>	$this->input->post('draw'),
			'data'				=>	$data,
			'recordsFiltered'	=>	$this->revenue->recFil($key),
			'recordsTotal'		=>	$this->revenue->recTot($key)
		);

		echo json_encode($return);
  }

	public function count_report(){

    $result = $this->revenue->get_count();

    if($result){
      echo $this->response('success', 200, $result);
    }else{
      echo $this->response('error', 400);
    }
  }

  public function excel(){
      $this->load->library('Lixel');
      // error_reporting(1);

      // $id = (int) $this->input->get('id');
      // $tahun = $this->input->get('tahun');
      // $start = $this->input->post('start');
      // $end = $this->input->post('end');
      $tahun = date("Y");
      // if (isset($start)||isset($end)) {
      //   if (isset($start)) {
      //     $start	= str_replace("-","/",$start);
      //     $this->db->where("R.CREATE_AT >= TO_DATE('{$start}','yyyy/mm/dd')");
      //   }else {
      //     $end	= str_replace("-","/",$end);
      //     $this->db->where("R.CREATE_AT <= TO_DATE('{$end}','yyyy/mm/dd')");
      //   }
      // }else {
      //   // code...
      //   $this->db->where("TO_CHAR(R.CREATE_AT, 'YYYY') = '{$tahun}'");
      // }

      if(isset($_POST)){ //&& isset($id_ukur)
        $start = $this->input->post('start');
        $start	= str_replace("-","/",$start);
        $end = $this->input->post('end');
        $end	= str_replace("-","/",$end);
        if ((!$start) && (!$end)) {
          $this->db->where("TO_CHAR(R.CREATE_AT, 'YYYY') = '{$tahun}'");
        }else {
          if (($start)) {
            $this->db->where("R.CREATE_AT >= TO_DATE('{$start}','yyyy/mm/dd')");
          }
          if (($end)) {
            $this->db->where("R.CREATE_AT <= TO_DATE('{$end}','yyyy/mm/dd')");
          }
        }

        $rDetail = $this->revenue->get_record();
        // echo "<pre>";
        // print_r($rDetail);
        // echo "</pre>";
        // exit;

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

        $objset     = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
        $objget     = $objPHPExcel->getActiveSheet();  //inisiasi get object

        $abjad = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S'
        );
        //
        $center_ver = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );
        $top_ver = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::VERTICAL_TOP,
            )
        );
        $style_center = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'style' => PHPExcel_Style_Border::BORDER_MEDIUM
            )
        );
        $style_right = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'style' => PHPExcel_Style_Border::BORDER_MEDIUM
            )
        );
        $border_all = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $border_outline = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $style_bgBlue = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '01A9DB')
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF')
            )
        );
        $style_bgOrange = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => 'DF7401')
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF')
            )
        );
        $style_bgGreenOld = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => '86B404')
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF')
            )
        );
        $style_bgGreen = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => '9ACD32')
            ),
            'font' => array(
                'color' => array('rgb' => '#000000')
            )
        );
        $font_red = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000')
            )
        );
        $font_black = array(
            'font' => array(
                'color' => array('rgb' => '000000')
            )
        );
        $styleArial = array(
            'font'  => array(
            'bold'  => true,
            'color' => array('rgb' => 'FF0000'),
            'size'  => 15,
            'name'  => 'Arial'
        ));

        $objget->getColumnDimension('A')->setWidth('5');
        $objget->getColumnDimension('B')->setWidth('5');
        $objget->getColumnDimension('C')->setWidth('25');
        $objget->getColumnDimension('D')->setWidth('20');
        $objget->getColumnDimension('E')->setWidth('13');
        $objget->getColumnDimension('F')->setWidth('15');
        $objget->getColumnDimension('G')->setWidth('40');
        $objget->getColumnDimension('H')->setWidth('45');
        $objget->getColumnDimension('I')->setWidth('16');
        $objget->getColumnDimension('J')->setWidth('25');
        $objget->getColumnDimension('K')->setWidth('25');
        $objget->getColumnDimension('L')->setWidth('30');
        $objget->getColumnDimension('M')->setWidth('13');
        $objget->getColumnDimension('N')->setWidth('25');
        $objget->getColumnDimension('O')->setWidth('25');
        $objget->getColumnDimension('P')->setWidth('16');
        $objget->getColumnDimension('Q')->setWidth('50');
        // $objget->getColumnDimension('R')->setWidth('50');
        //HEADER
        $gdImage = imagecreatefromjpeg('media/logo/logo_si.jpg');
        $objDrawing_bef = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing_bef->setName('Before image');
        $objDrawing_bef->setDescription('Before image');
        $objDrawing_bef->setImageResource($gdImage);
        $objDrawing_bef->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
        $objDrawing_bef->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing_bef->setHeight(70);
        $objDrawing_bef->setWidth(100);
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:C3');
        $objDrawing_bef->setCoordinates('B2');
        $objDrawing_bef->setWorksheet($objPHPExcel->getActiveSheet());
        $objset->setCellValue('C2', "                       DEPARTMENT OF DESIGN ENGINEERING");
        $objset->setCellValue('C3', "                       Bureau of Design Engineering Integration");

        $objget->getStyle('B5:B6')->getFont()->setSize(12)->setBold(true);
        $objget->getStyle('B5:B6')->applyFromArray($style_center);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:R5');
        $objset->setCellValue('B5', "CONTROL & REPORT PENGELOLAAN RENCANA KERJA CAPEX DAN KAJIAN DEPARTMENT OF DESIGN & ENGINEERING");
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B6:R6');
        $objset->setCellValue('B6', "TAHUN {$tahun}");
        //END HEADER

        //TABLE
          // -- thead
          $objget->getRowDimension('9')->setRowHeight(25);
          $objget->getStyle('B9:Q9')->getFont()->setBold(true);
          $objget->getStyle('B9:Q9')->applyFromArray($border_all);
          $objget->getDefaultStyle()->applyFromArray($top_ver);
          $objget->getStyle('B9:Q9')->applyFromArray($style_center);
          $objget->getStyle('B9:J9')->applyFromArray($style_bgBlue);
          $objset->setCellValue('B9', "No.");
          $objset->setCellValue('C9', "No. ERF");
          $objset->setCellValue('D9', "No. Notifikasi");
          $objset->setCellValue('E9', "Tanggal ERF");
          $objset->setCellValue('F9', "No. WBS");
          $objset->setCellValue('G9', "Unit Kerja");
          $objset->setCellValue('H9', "Deskripsi");
          $objset->setCellValue('I9', "Status ERF");
          $objset->setCellValue('J9', "Approve SM");
          $objget->getStyle('K9:O9')->applyFromArray($style_bgOrange);
          $objset->setCellValue('K9', "No. EAT");
          $objset->setCellValue('L9', "Lead Technical Assist");
          $objset->setCellValue('M9', "Tanggal EAT");
          $objset->setCellValue('N9', "Approve SM");
          $objset->setCellValue('O9', "Approve GM");
          $objget->getStyle('P9:Q9')->applyFromArray($style_bgGreenOld);
          // $objset->setCellValue('P9', "Status Eng.");
          $objset->setCellValue('P9', "Progress Eng.");
          $objset->setCellValue('Q9', "Remarks");
          // -- end thead
          // tbody
          $no = 10;
          $sTipe = '';
          $sLoc = '';
          $sArea = '';
          $no_row = 1;
          $index = 1;
          foreach ($rDetail as $i => $val) {
            $tmpno=$no;
            $objset->setCellValue('B'.$no, "{$no_row}");
            $objset->setCellValue('C'.$no, "{$val['NO_PENGAJUAN']}");
            $objset->setCellValue('D'.$no, "{$val['NOTIFIKASI']}");
            $objset->setCellValue('E'.$no, "{$val['R_DATE']}");
            $objset->setCellValue('F'.$no, "{$val['WBS_CODE']}");
            $objset->setCellValue('G'.$no, "{$val['UK_TEXT']}");
            $objset->setCellValue('H'.$no, "{$val['NAMA_PEKERJAAN']}");
            $objset->setCellValue('I'.$no, "{$val['R_STATUS']}");
            $objset->setCellValue('J'.$no, "{$val['R_APPROVE']}");
            $objset->setCellValue('K'.$no, "{$val['NO_PENUGASAN']}");
            $objset->setCellValue('L'.$no, "{$val['LEAD_ASS']}");
            $objset->setCellValue('M'.$no, "{$val['A_DATE']}");
            $objset->setCellValue('N'.$no, "{$val['A_APPROVE_SM']}");
            $objset->setCellValue('O'.$no, "{$val['A_APPROVE_GM']}");
            // $objset->setCellValue('P'.$no, "{$val['D_STATUS']}");
            $objset->setCellValue('P'.$no, "{$val['D_PROGRESS']}");
            $arPacket = explode('<br />', rtrim($val['D_REMARKS'],"<br />"));
            if (count($arPacket)>0) {
              foreach ($arPacket as $key => $value) {
                $objset->setCellValue('Q'.$no, $value);
                $no++;
              }
              $no--;
            }
            // $objset->setCellValue('R'.$no, str_replace('<br />','\n',$val['D_REMARKS']));

            // merge cell
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("B{$tmpno}:B{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("C{$tmpno}:C{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("D{$tmpno}:D{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("E{$tmpno}:E{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("F{$tmpno}:F{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("G{$tmpno}:G{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("H{$tmpno}:H{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("I{$tmpno}:I{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("J{$tmpno}:J{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("K{$tmpno}:K{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("L{$tmpno}:L{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("M{$tmpno}:M{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("N{$tmpno}:N{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("O{$tmpno}:O{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("P{$tmpno}:P{$no}");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("Q{$tmpno}:Q{$no}");
            // border
            $objget->getStyle('B'.$tmpno.':B'.$no)->applyFromArray($border_outline);
            $objget->getStyle('C'.$tmpno.':C'.$no)->applyFromArray($border_outline);
            $objget->getStyle('D'.$tmpno.':D'.$no)->applyFromArray($border_outline);
            $objget->getStyle('E'.$tmpno.':E'.$no)->applyFromArray($border_outline);
            $objget->getStyle('F'.$tmpno.':F'.$no)->applyFromArray($border_outline);
            $objget->getStyle('G'.$tmpno.':G'.$no)->applyFromArray($border_outline);
            $objget->getStyle('H'.$tmpno.':H'.$no)->applyFromArray($border_outline);
            $objget->getStyle('I'.$tmpno.':I'.$no)->applyFromArray($border_outline);
            $objget->getStyle('J'.$tmpno.':J'.$no)->applyFromArray($border_outline);
            $objget->getStyle('K'.$tmpno.':K'.$no)->applyFromArray($border_outline);
            $objget->getStyle('L'.$tmpno.':L'.$no)->applyFromArray($border_outline);
            $objget->getStyle('M'.$tmpno.':M'.$no)->applyFromArray($border_outline);
            $objget->getStyle('N'.$tmpno.':N'.$no)->applyFromArray($border_outline);
            $objget->getStyle('O'.$tmpno.':O'.$no)->applyFromArray($border_outline);
            $objget->getStyle('P'.$tmpno.':P'.$no)->applyFromArray($border_outline);
            $objget->getStyle('Q'.$tmpno.':Q'.$no)->applyFromArray($border_outline);
            $objget->getStyle('Q'.$no)->applyFromArray($style_right);
            // $objget->getStyle('R'.$tmpno.':R'.$no)->applyFromArray($border_outline);


            $no++;
            $no_row++;
          }

          // end tbody
        //END TABLE

        // FOOTER
        // if ($sSK_No != '-'){
        //   $objset->setCellValue('C'.($no + 2), '* Baku Mutu '. $sSK_No);
        // }
        // if ($sPIC != '-'){
        //   $objset->setCellValue('H'.($no + 2), 'Kasi. Pemantauan Lingkungan');
        //   $objset->setCellValue('H'.($no + 5), $sPIC);
        // }
        // END FOOTER


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');


        $filename = date('Ymd').' CONTROL & REPORT PENGELOLAHAN CAPEX '.$tahun.'.xls';
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        $objWriter->save('php://output');
    }
  // }
}



}
