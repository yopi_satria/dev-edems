<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('M_hris', 'hris');
    }

    public function index() {
        $data["tittle"] = "Approval Info";
        $data["short_tittle"] = "Login";
        // echo base_url();
        // $this->load->view('login', $data);
    }

    public function erf($key) {
        $this->load->model("erf/M_request", "req");
        $this->load->library('Template');

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->req->get_datarequest($akey[0]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[1]}' OR k.mk_email like '%{$akey[1]}@%')");
        $peg = (array) $peg[0];

        $param['short'] = 'ERF';
        $param['title'] = 'Engineering Request Form';

        $param['APP']['DATE'] = $akey[2];
        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$data['NO_PENGAJUAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Deskripsi :</b> {$data['NAMA_PEKERJAAN']}";
        $param['TRN']['LOKASI'] = "<b>Lokasi :</b> {$data['LOKASI']}";

        echo $this->template->view_app($param);
    }

    public function eat($key) {
        $this->load->model("eat/M_assign", "assig");
        $this->load->library('Template');

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->assig->get_record($akey[0]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[1]}' OR k.mk_email like '%{$akey[1]}@%')");
        $peg = (array) $peg[0];

        $param['short'] = 'EAT';
        $param['title'] = 'Engineering Assign Task';

        $param['APP']['DATE'] = $akey[2];
        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$data['NO_PENUGASAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Deskripsi :</b> {$data['SHORT_TEXT']}";
        $param['TRN']['LOKASI'] = "<b>Lokasi :</b> {$data['LOKASI']}";

        echo $this->template->view_app($param);
    }

    public function dok_eng($key) {
        $this->load->model("dok_eng/M_dok_eng", "dok_eng");
        $this->load->library('Template');

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->dok_eng->get_record("A.ID = '{$akey[0]}'");
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[1]}' OR k.mk_email like '%{$akey[1]}@%')");
        $peg = (array) $peg[0];

        $param['short'] = 'DE';
        $param['title'] = 'Document Engineering';

        $param['APP']['DATE'] = $akey[2];
        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$data['NO_DOK_ENG']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Paket :</b> {$data['PACKET_TEXT']}";
        // $param['TRN']['LOKASI']="<b>Progress :</b> {$data['PROGRESS']}%";

        echo $this->template->view_app($param);
    }

    public function tender($key) {
        $this->load->model("dok_eng/M_tender", "tender");
        $this->load->library('Template');

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->tender->get_record("A.ID = '{$akey[0]}'");
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[1]}' OR k.mk_email like '%{$akey[1]}@%')");
        $peg = (array) $peg[0];

        $param['short'] = 'Tender';
        $param['title'] = 'Document Tender';

        $param['APP']['DATE'] = $akey[2];
        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$data['NO_DOK_TEND']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Paket :</b> {$data['PACKET_TEXT']}";

        echo $this->template->view_app($param);
    }

    public function bast($key) {
        $this->load->model("bast/M_bast", "bast");
        $this->load->library('Template');

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->bast->get_record($akey[0]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[1]}' OR k.mk_email like '%{$akey[1]}@%')");
        $peg = (array) $peg[0];

        $param['short'] = 'BAST';
        $param['title'] = 'Berita Acara Serah Terima';

        $param['APP']['DATE'] = $akey[2];
        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$data['NO_BAST']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Packet :</b> {$data['PACKET_TEXT']}";
        $param['TRN']['LOKASI'] = "<b>Lokasi :</b> {$data['LOKASI']}";

        echo $this->template->view_app($param);
    }

    public function awr($key) {
        $this->load->model("assist/M_req_aanwidjing", "req_aan");
        $this->load->library('Template');

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->req_aan->get_record($akey[0]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[1]}' OR k.mk_email like '%{$akey[1]}@%')");
        $peg = (array) $peg[0];

        $param['short'] = 'Permintaan Aanwidjing';
        $param['title'] = 'Permintaan Aanwidjing';

        $param['APP']['DATE'] = $akey[2];
        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$data['NO_PENDAMPINGAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Deskripsi :</b> {$data['SHORT_TEXT']}";
        $param['TRN']['PACKET'] = "<b>Paket Pekerjaan :</b> {$data['PACKET_TEXT']}";
        $param['TRN']['LOKASI'] = "<b>Unit Kerja :</b> {$data['UNIT_KERJA']}";

        echo $this->template->view_app($param);
    }

    public function aws($key) {
        $this->load->model("assist/M_aanwidjing", "aan");
        $this->load->library('Template');

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->aan->get_record($akey[0]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[1]}' OR k.mk_email like '%{$akey[1]}@%')");
        $peg = (array) $peg[0];

        $param['short'] = 'Aanwidjing';
        $param['title'] = 'Aanwidjing';

        $param['APP']['DATE'] = $akey[2];
        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$data['NO_PENDAMPINGAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Deskripsi :</b> {$data['SHORT_TEXT']}";
        $param['TRN']['PACKET'] = "<b>Paket Pekerjaan :</b> {$data['PACKET_TEXT']}";
        $param['TRN']['LOKASI'] = "<b>Unit Kerja :</b> {$data['UNIT_KERJA']}";

        echo $this->template->view_app($param);
    }

    public function tvr($key) {
        $this->load->model("assist/M_req_evaluation", "req_eva");
        $this->load->library('Template');

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->req_eva->get_record($akey[0]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[1]}' OR k.mk_email like '%{$akey[1]}@%')");
        $peg = (array) $peg[0];

        $param['short'] = 'Permintaan Evaluasi Teknis';
        $param['title'] = 'Permintaan Evaluasi Teknis';

        $param['APP']['DATE'] = $akey[2];
        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$data['NO_PENDAMPINGAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Deskripsi :</b> {$data['SHORT_TEXT']}";
        $param['TRN']['PACKET'] = "<b>Paket Pekerjaan :</b> {$data['PACKET_TEXT']}";
        $param['TRN']['LOKASI'] = "<b>Unit Kerja :</b> {$data['UNIT_KERJA']}";

        echo $this->template->view_app($param);
    }

    public function tva($key) {
        $this->load->model("assist/M_evatech", "evatech");
        $this->load->library('Template');

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->evatech->get_record($akey[0]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[1]}' OR k.mk_email like '%{$akey[1]}@%')");
        $peg = (array) $peg[0];

        $param['short'] = 'Evaluasi Teknis';
        $param['title'] = 'Evaluasi Teknis';

        $param['APP']['DATE'] = $akey[2];
        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$data['NO_PENDAMPINGAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Deskripsi :</b> {$data['SHORT_TEXT']}";
        $param['TRN']['PACKET'] = "<b>Paket Pekerjaan :</b> {$data['PACKET_TEXT']}";
        $param['TRN']['LOKASI'] = "<b>Unit Kerja :</b> {$data['UNIT_KERJA']}";

        echo $this->template->view_app($param);
    }

    public function dokumen($key) {
        $this->load->model("dok_eng/M_dok_eng", "dok_eng");
        $this->load->library('Template');

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->dok_eng->get_record("A.ID = '{$akey[0]}'");
        $data = $data[0];

        $param['short'] = 'DE';
        $param['title'] = 'Document Engineering';

        if (isset($data['COMP_TEXT']) && !empty($data['COMP_TEXT'])) {
            $param['HDR']['COMP_TEXT'] = "<b>Company :</b> " . $data['COMP_TEXT'];
        }
        if (isset($data['DEPT_TEXT']) && !empty($data['DEPT_TEXT'])) {
            $param['HDR']['DEPT_TEXT'] = "<b>Departemen :</b> " . $data['DEPT_TEXT'];
        }
        if ($data['DEPT_TEXT'] != $data['UK_TEXT']) {
            $param['HDR']['UK_TEXT'] = "<b>Unit Kerja :</b> " . $data['UK_TEXT'];
        }
//        $param['HDR']['CRT'] = "<b>Pembuat :</b> {$data['NAME_CRT']}";//Edited by Agung.Tarmudi as requested by  Indra.Yudhi
        $param['HDR']['CRT'] = "<b>Dibuat :</b> {$data['NAME_CRT']}";
//        $param['HDR']['PARAF'] = "<b>Pemverifikasi :</b> {$data['NAME_PARAF']}";//Edited by Agung.Tarmudi as requested by  Indra.Yudhi
        $param['HDR']['PARAF'] = "<b>Diverifikasi :</b> {$data['NAME_PARAF']}";
//        $param['HDR']['APP'] = "<b>Approval :</b> {$data['NAME_APP']}";//Edited by Agung.Tarmudi as requested by  Indra.Yudhi
        if ($dtList['APPROVE3_AT']) {
            $headde = ", " . $dtList['APPROVE3_BY'];
        } else {
            $headde = '';
        }
        $param['HDR']['APP'] = "<b>Disetujui :</b> {$data['NAME_APP']} $headde";
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> " . str_replace("TR", $akey[1], $data['NO_DOK_ENG']);
        $param['TRN']['PENUGASAN'] = "<b>No. EAT :</b> {$data['NO_PENUGASAN']}";
        $param['TRN']['PENGAJUAN'] = "<b>No. ERF :</b> {$data['NO_PENGAJUAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Paket :</b> {$data['PACKET_TEXT']}";
        $param['TRN']['JML_ISI'] = "<b>Jumlah isi :</b> {$akey[2]} lembar";
        // $param['TRN']['LOKASI']="<b>Progress :</b> {$data['PROGRESS']}%";

        echo $this->template->view_docs($param);
    }

    public function tendering($key) {
        $this->load->model("dok_eng/M_tender", "tender");
        $this->load->library('Template');

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->tender->get_record("A.ID = '{$akey[0]}'");
        $data = $data[0];

        $param['short'] = 'TD';
        $param['title'] = 'Document Tender';

        if (isset($data['COMP_TEXT']) && !empty($data['COMP_TEXT'])) {
            $param['HDR']['COMP_TEXT'] = "<b>Company :</b> " . $data['COMP_TEXT'];
        }
        if (isset($data['DEPT_TEXT']) && !empty($data['DEPT_TEXT'])) {
            $param['HDR']['DEPT_TEXT'] = "<b>Departemen :</b> " . $data['DEPT_TEXT'];
        }
        if ($data['DEPT_TEXT'] != $data['UK_TEXT']) {
            $param['HDR']['UK_TEXT'] = "<b>Unit Kerja :</b> " . $data['UK_TEXT'];
        }
        $param['HDR']['CRT'] = "<b>Pembuat :</b> {$data['APPROVE1_BY']}";
        $param['HDR']['PARAF'] = "<b>Pemverifikasi :</b> {$data['APPROVE2_BY']}";
        $param['HDR']['APP'] = "<b>Approval :</b> {$data['APPROVE3_BY']}";
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> " . str_replace("TD", $akey[1], $data['NO_DOK_TEND']);
        $param['TRN']['PENUGASAN'] = "<b>No. EAT :</b> {$data['NO_PENUGASAN']}";
        $param['TRN']['PENGAJUAN'] = "<b>No. ERF :</b> {$data['NO_PENGAJUAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Paket :</b> {$data['PACKET_TEXT']}";
        $param['TRN']['JML_ISI'] = "<b>Jumlah isi :</b> {$akey[2]} lembar";
        // $param['TRN']['LOKASI']="<b>Progress :</b> {$data['PROGRESS']}%";

        echo $this->template->view_docs($param);
    }

    public function view($mParam = array()) {
        $this->load->library('Template');

        return $this->template->view_app($mParam);
    }

}
