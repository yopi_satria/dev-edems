<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 // require_once APPPATH . '/libraries/REST_Controller.php';

class Sap_apd extends CI_Controller {

    function __construct(){
        parent::__construct();

        $this->load->model('sap/M_sapapd', 'sapapd');

    }

    protected $nosapapd = array("605-203846", "605-203845", "605-201291", "605-201277", "605-200909", "605-201274", "605-203973", "605-203966", "605-203965", "605-201280", "605-200893", "605-201292", "605-200915", "605-203847", "605-203863", "605-201232", "604-200724", "605-203861", "605-201276", "605-203933", "605-203864", "605-201598", "605-200260", "605-201447", "605-201310", "605-203189", "605-203862", "605-203200", "605-201557", "605-201580", "605-201740", "605-203952", "605-201293", "605-201354", "605-203188", "605-203932", "605-201374", "605-203841", "605-201538", "605-201404", "604-200583", "605-201490");

    // protected $tmpnosapapd = array();

    public function index(){

        $result = $this->sapapd->get_codemat_apd();

        $company = $_GET['company'];

        // echo "$result";
        // print_r($result);

        $tmpnosapapd = array();
        for ($i=0; $i < count($result); $i++) {
            array_push($tmpnosapapd,$result[$i]['MATERIAL_CODE']);
            # code...
        }

        // print_r($tmpnosapapd);
        $datanosap = $this->nosapapd;

        // echo "$company";
        // print_r($datanosap);

        $data = $this->sapapd->get_stock_in_warehouse($company,$tmpnosapapd);

        // echo "$data";
        // print_r($data);

        $datastock["data"] = $data;
        echo json_encode($datastock);


    }

}
