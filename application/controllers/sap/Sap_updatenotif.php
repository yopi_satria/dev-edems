<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 // require_once APPPATH . '/libraries/REST_Controller.php';

class Sap_updatenotif extends CI_Controller {

    function __construct(){
        parent::__construct();

        $this->load->model('sap/M_updatenotif', 'sapnotif');

    }

    public function index(){
        $param = array(
            'loginsap' => $_POST['loginsap'],
            'notiftype' => $_POST['notiftype'],
            'codefuncloc' => $_POST['codefuncloc'],
            'header' => $_POST['header'],
            'reporter' => $_POST['reporter'],
            'priority' => $_POST['priority'],
            'datestart' => $_POST['datestart'],
            'timestart' => $_POST['timestart'],
            'dateend' => $_POST['dateend'],
            'description' => $_POST['description'],
            'planplant' => $_POST['planplant'],
            'plangroup' => $_POST['plangroup'],
            'timeend' => $_POST['timeend']
        );

        # code...

        // print_r($param);

        $data = $this->sapnotif->createnotif($param);

        $datanotif["data"] = $data;
        echo json_encode($datanotif);

    }

    public function schafolding($actiontxt,$nonotifsap){

        // $notiftype = $_GET['company'];
        // $codefuncloc = $_GET['company'];
        // $header = $_GET['company'];
        // $reporter = $_GET['company'];
        // $priority = $_GET['company'];
        // $datestart = $_GET['company'];
        // $timestart = $_GET['company'];
        // $dateend = $_GET['company'];
        // $timeend = $_GET['company'];

        $param = array(
            'NO_NOTIF_SAP' => $nonotifsap,
            'FUNCTIONAL_LOC' => $_POST['FUNCTIONAL_LOC'],
            'TITTLE' => $_POST['TITTLE'],
            'REPORTER' => $_POST['REPORTER'],
            'PRIORITY' => $_POST['PRIORITY'],
            'PLANPLANT' => $_POST['PLANPLANT'],
            'PLANGROUP' => $_POST['PLANGROUP'],
            'MPLANT' => $_POST['MPLANT']
        );

        # code...

        if ($actiontxt == 'reject') {
            # code...
            $data = $this->sapnotif->rejectnotif($param);

        } else if ($actiontxt == 'close') {
            # code...
            $data = $this->sapnotif->closenotif($param);

        } else if ($actiontxt == 'update') {
            # code...
            $data = $this->sapnotif->updatenotif($param);

        }

        $datastock["data"] = $data;
        echo json_encode($datastock);
    }

}
