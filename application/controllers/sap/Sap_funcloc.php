<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 // require_once APPPATH . '/libraries/REST_Controller.php';

class Sap_funcloc extends CI_Controller {

    function __construct(){
        parent::__construct();

        $this->load->model('sap/M_funcloc', 'sapfuncloc');

    }

    public function index(){

        $company = $_GET['company'];
        $comptextshort = $_GET['comptextshort'];
        $category = $_GET['category'];

        $data = $this->sapfuncloc->get_funcloclist($company,$comptextshort,$category);

        $datafuncloc["data"] = $data;
        echo json_encode($datafuncloc);


    }

}
