<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 // require_once APPPATH . '/libraries/REST_Controller.php';

class Sap_ceknotif extends CI_Controller {

    function __construct(){
        parent::__construct();

        $this->load->model('sap/M_ceknotif', 'sapnotif');

    }

    public function index(){
        $param = array(
            'loginsap' => $_POST['loginsap'],
            'notiftype' => $_POST['notiftype'],
            'codefuncloc' => $_POST['codefuncloc'],
            'header' => $_POST['header'],
            'reporter' => $_POST['reporter'],
            'priority' => $_POST['priority'],
            'datestart' => $_POST['datestart'],
            'timestart' => $_POST['timestart'],
            'dateend' => $_POST['dateend'],
            'description' => $_POST['description'],
            'planplant' => $_POST['planplant'],
            'plangroup' => $_POST['plangroup'],
            'timeend' => $_POST['timeend']
        );

        # code...

        // print_r($param);

        $data = $this->sapnotif->createnotif($param);

        $datanotif["data"] = $data;
        echo json_encode($datanotif);

    }

    public function schafolding(){

        $param = array(
            'no_notif' => $_POST['NO_NOTIF_SAP']
        );

        # code...

        // echo "$param";
        $data = $this->sapnotif->searchnotif($param);

        $datastock["data"] = $data;
        echo json_encode($datastock);
    }

}
