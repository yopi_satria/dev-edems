<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 // require_once APPPATH . '/libraries/REST_Controller.php';

class Sap_createnotif extends CI_Controller {

    function __construct(){
        parent::__construct();

        $this->load->model('sap/M_createnotif', 'sapnotif');

    }

    public function index(){
      // echo "<pre>";
      // print_r($_POST);
      // echo "</pre>";
      // exit;

        $mplant = '';
        $pgroup = '';
        if ($_POST['PLANTGROUP']) {
          # code...
          $pgroup = $_POST['PLANTGROUP'];
        }

        if ($_POST['SWERK']) {
          # code...
          $mplant = $_POST['SWERK'];
        }

        $param = array(
            'loginsap' => $_POST['LOGINSAP'],
            'notiftype' => $_POST['NOTIFTYPE'],
            'codefuncloc' => $_POST['FUNCT_LOC'],
            'header' => $_POST['header'],
            'reporter' => $_POST['reporter'],
            'priority' => $_POST['priority'],
            'datestart' => $_POST['START_DATE'],
            // 'timestart' => $_POST['timestart'],
            'dateend' => $_POST['END_DATE'],
            'description' => $_POST['description'],
            'planplant' => $_POST['PLANPLANT'],
            'BEBER' => $_POST['BEBER'],
            'plangroup' => $pgroup,
            'maintplant' => $mplant,
            'latar_belakang' => $_POST['latar_belakang'],
            'cust_req' => $_POST['cust_req'],
            'tech_info' => $_POST['tech_info'],
            'res_mit' => $_POST['res_mit'],
            // 'timeend' => $_POST['timeend']
        );

        # code...

        // print_r($param);

        $data = $this->sapnotif->createnotif($param);

        $datanotif["data"] = $data;
        echo json_encode($datanotif);

    }

    public function schafolding(){

        // $notiftype = $_GET['company'];
        // $codefuncloc = $_GET['company'];
        // $header = $_GET['company'];
        // $reporter = $_GET['company'];
        // $priority = $_GET['company'];
        // $datestart = $_GET['company'];
        // $timestart = $_GET['company'];
        // $dateend = $_GET['company'];
        // $timeend = $_GET['company'];

        $param = array(
            'notiftype' => $_POST['notiftype'],
            'codefuncloc' => $_POST['codefuncloc'],
            'header' => $_POST['header'],
            'reporter' => $_POST['reporter'],
            'priority' => $_POST['priority'],
            'datestart' => $_POST['datestart'],
            'timestart' => $_POST['timestart'],
            'dateend' => $_POST['dateend'],
            'timeend' => $_POST['timeend']
        );

        # code...

        $data = $this->sapnotif->createnotif($company,$tmpnosapapd);

        $datastock["data"] = $data;
        echo json_encode($datastock);
    }

}
