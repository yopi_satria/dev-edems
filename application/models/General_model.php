<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class General_model extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        // $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

    public function grafik_all($thn = null) {
        $atable = array("MPE_PENGAJUAN", "MPE_PENUGASAN", "MPE_DTL_PENUGASAN", "MPE_DOK_ENG", "MPE_BAST");
        $alias = array("ERF", "EAT", "Paket", "Docs", "Trans");
        if (!isset($thn)) {
            $thn = date("Y");
        }
        $awhere = array(
            "DELETE_AT IS NULL AND STATE = 'Active' AND STATUS='Approved' AND APPROVE_AT IS NOT NULL AND to_char(APPROVE_AT,'YYYY') = '{$thn}' AND EXTRACT(month FROM APPROVE_AT)",
            "DELETE_AT IS NULL AND (CASE WHEN APPROVE1_AT IS NOT NULL THEN APPROVE1_AT WHEN APPROVE0_AT IS NOT NULL THEN APPROVE0_AT END) IS NOT NULL AND to_char((CASE WHEN APPROVE1_AT IS NOT NULL THEN APPROVE1_AT WHEN APPROVE0_AT IS NOT NULL THEN APPROVE0_AT END),'YYYY') = '{$thn}' AND EXTRACT(month FROM (CASE WHEN APPROVE1_AT IS NOT NULL THEN APPROVE1_AT WHEN APPROVE0_AT IS NOT NULL THEN APPROVE0_AT END))",
            "DELETE_AT IS NULL AND ID_PENUGASAN IN (SELECT ID FROM MPE_PENUGASAN WHERE ID=MPE_DTL_PENUGASAN.ID_PENUGASAN AND DELETE_AT IS NULL AND (CASE WHEN APPROVE1_AT IS NOT NULL THEN APPROVE1_AT WHEN APPROVE0_AT IS NOT NULL THEN APPROVE0_AT END) IS NOT NULL AND to_char((CASE WHEN APPROVE1_AT IS NOT NULL THEN APPROVE1_AT WHEN APPROVE0_AT IS NOT NULL THEN APPROVE0_AT END),'YYYY') = '{$thn}') AND EXTRACT(month FROM (SELECT DISTINCT (CASE WHEN APPROVE1_AT IS NOT NULL THEN APPROVE1_AT WHEN APPROVE0_AT IS NOT NULL THEN APPROVE0_AT END) FROM MPE_PENUGASAN WHERE ID=MPE_DTL_PENUGASAN.ID_PENUGASAN AND (CASE WHEN APPROVE1_AT IS NOT NULL THEN APPROVE1_AT WHEN APPROVE0_AT IS NOT NULL THEN APPROVE0_AT END) IS NOT NULL AND to_char((CASE WHEN APPROVE1_AT IS NOT NULL THEN APPROVE1_AT WHEN APPROVE0_AT IS NOT NULL THEN APPROVE0_AT END),'YYYY') = '{$thn}'))",
            "DELETE_AT IS NULL AND APPROVE3_AT IS NOT NULL AND to_char(APPROVE3_AT,'YYYY') = '{$thn}' AND EXTRACT(month FROM APPROVE3_AT)",
            "DELETE_AT IS NULL AND to_char(CREATE_AT,'YYYY') = '{$thn}' AND EXTRACT(month FROM CREATE_AT)"
        );

        $data = array();
        for ($i = 0; $i < count($atable); $i++) {
            $str = "";
            for ($b = 1; $b < 13; $b++) {
                $bln = $b;
                $str .= "(SELECT COUNT(*) FROM {$atable[$i]} WHERE {$awhere[$i]}={$bln}) AS " . date('F', mktime(0, 0, 0, $bln, 10));
                if ($b < 12) {
                    $str .= ", ";
                }
            }

            $sql = "SELECT {$str} FROM dual";
            // echo $sql.'<br /><br />';
            $query = $this->db->query($sql);
            $query = $query->result_array();
            $query = $query[0];
            // echo $this->db->last_query();
            $data[$alias[$i]] = array_values($query);
        }

        return $data;
    }

    public function grafik_notif($thn = null) {
        $atable = array("MPE_PENGAJUAN");
        $alias = array("Notifikasi");
        if (!isset($thn)) {
            $thn = date("Y");
        }
        $awhere = array(
            "DELETE_AT IS NULL AND to_char(APPROVE_AT,'YYYY') = '{$thn}' AND STATUS='Approved' AND APPROVE_AT IS NOT NULL AND EXTRACT(month FROM CREATE_AT)"
        );

        $data = array();
        for ($i = 0; $i < count($atable); $i++) {
            $str = "";
            for ($b = 1; $b < 13; $b++) {
                $bln = $b;
                $str .= "(SELECT COUNT(*) FROM {$atable[$i]} WHERE {$awhere[$i]}={$bln}) AS " . date('F', mktime(0, 0, 0, $bln, 10));
                if ($b < 12) {
                    $str .= ", ";
                }
            }

            $sql = "SELECT {$str} FROM dual";
            // echo $sql.'<br /><br />';
            $query = $this->db->query($sql);
            $query = $query->result_array();
            $query = $query[0];
            // echo $this->db->last_query();
            $data[$alias[$i]] = array_values($query);
        }

        return $data;
    }

    public function grafik_date($thn = null) {
        if (!isset($thn)) {
            $thn = date("Y");
        }
        $this->db->select("A.ID,
                        	A.NOTIFIKASI,
                        	A.NO_PENUGASAN,
                          (SELECT DISTINCT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE=A.ID_PENGAJUAN) ERF_TEXT,
                        	A.START_DATE,
                        	A.END_DATE,
                        	AD.ID ID_DTL,
                        	(SELECT ROWNUM FROM MPE_DTL_PENUGASAN WHERE ID=AD.ID) RN,
                        	AD.DESKRIPSI,
                        	(
                            SELECT
                            	(CASE WHEN APPROVE2_AT > APPROVE1_AT THEN APPROVE2_AT ELSE APPROVE1_AT END)
                            FROM
                            	MPE_BAST
                            WHERE
                            	STATUS = 'Closed'
                            	AND ID_DOK_ENG = ( SELECT ID FROM MPE_DOK_ENG WHERE NOTIFIKASI = A.NOTIFIKASI AND PACKET_TEXT = AD.DESKRIPSI AND DELETE_AT IS NULL AND ROWNUM = 1 )
                          ) BAST_DATE ");
        $this->db->from('MPE_PENUGASAN A');
        $this->db->join("MPE_DTL_PENUGASAN AD", "AD.ID_PENUGASAN = A.ID ", 'left');
        // when showing all data, comment two after join
        $this->db->join("MPE_DOK_ENG D", "D.ID_PENUGASAN = AD.ID_PENUGASAN AND D.PACKET_TEXT = AD.DESKRIPSI ", 'left');
        $this->db->join("MPE_BAST B", "B.ID_DOK_ENG = D.ID ", 'left');
        $this->db->where("A.DELETE_AT IS NULL
                           AND AD.DELETE_AT IS NULL
                           AND D.DELETE_AT IS NULL
                           AND B.DELETE_AT IS NULL
                           AND to_char(A.END_DATE, 'YYYY') = '{$thn}'
                           /*AND to_char(A.END_DATECREATE_AT, 'YYYY') = '{$thn}'*/
                          "); //AND A.NO_PENGAJUAN LIKE '{$thn}%'*/
        $this->db->order_by('A.ID ASC, AD.ID ASC');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function grafik_erf($thn = null) {
        if (!isset($thn)) {
            $thn = date("Y");
        }
        $this->db->select("COMPANY,
                        	GEN_PAR4,
                        	GEN_PAR5,
                        	UK_CODE,
                        	UK_TEXT,
                        	DEPT_CODE,
                        	DEPT_TEXT,
                        	COUNT(UK_CODE) JML,
                          (SELECT COUNT(COMPANY) FROM MPE_PENGAJUAN WHERE DELETE_AT IS NULL
                          	/*AND NO_PENGAJUAN LIKE '{$thn}%'*/
                            AND to_char(APPROVE_AT,'YYYY') = '{$thn}'
                            AND APPROVE_AT IS NOT NULL AND STATUS='Approved'
                          	AND ( STATE = 'Active' OR STATE IS NULL )
                          	AND STATUS != 'Rejected' AND COMPANY=P.COMPANY
                          ) JML_CMP,
                          (SELECT COUNT(COMPANY) FROM MPE_PENGAJUAN WHERE DELETE_AT IS NULL
                          	/*AND NO_PENGAJUAN LIKE '{$thn}%'*/
                            AND to_char(APPROVE_AT,'YYYY') = '{$thn}'
                            AND APPROVE_AT IS NOT NULL AND STATUS='Approved'
                          	AND ( STATE = 'Active' OR STATE IS NULL )
                          	AND STATUS != 'Rejected'
                          ) TOT_CMP");
        $this->db->from('MPE_PENGAJUAN P');
        $this->db->join("MPE_GENERAL G", "G.GEN_TYPE = 'DEF_PGROUP' AND G.GEN_PAR2 = P.COMPANY AND G.GEN_VAL = '1' ");
        $this->db->where("/*to_char(P.CREATE_AT,'YYYY') = '{$thn}'
                          AND*/ DELETE_AT IS NULL AND P.APPROVE_AT IS NOT NULL AND P.STATUS='Approved'
                          AND to_char(APPROVE_AT,'YYYY') = '{$thn}'
                          /*AND NO_PENGAJUAN LIKE '{$thn}%'*/
                        	AND ( STATE = 'Active' OR STATE IS NULL )
                        	AND P.STATUS != 'Rejected'");
        $this->db->group_by(array("COMPANY", "GEN_PAR4", "GEN_PAR5", "UK_CODE", "UK_TEXT", "DEPT_CODE", "DEPT_TEXT"));
        $this->db->order_by('COMPANY');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function grafik_stat($thn = null) {
        $atable = array("MPE_PENGAJUAN", "MPE_PENGAJUAN");
        $atable2 = array("MPE_PENGAJUAN", "MPE_PENGAJUAN");
        $alias = array("ERF", "Terkirim");
        $alias2 = array("Tot. ERF", "Tot. Terkirim");
        if (!isset($thn)) {
            $thn = date("Y");
        }
        $awhere = array(
            "WHERE DELETE_AT IS NULL AND STATE = 'Active' AND STATUS='Approved' AND APPROVE_AT IS NOT NULL AND to_char(APPROVE_AT,'YYYY') = '{$thn}' AND EXTRACT(month FROM APPROVE_AT) = #bln",
            "INNER JOIN MPE_PENUGASAN ON MPE_PENUGASAN.ID_PENGAJUAN=MPE_PENGAJUAN.ID_MPE WHERE MPE_PENGAJUAN.DELETE_AT IS NULL AND MPE_PENUGASAN.DELETE_AT IS NULL AND MPE_PENGAJUAN.STATE = 'Active' AND MPE_PENGAJUAN.STATUS='Approved' AND MPE_PENUGASAN.STATUS<>'Rejected' AND MPE_PENGAJUAN.APPROVE_AT IS NOT NULL AND to_char(MPE_PENGAJUAN.APPROVE_AT,'YYYY') = '{$thn}' AND (SELECT COUNT(ID) FROM MPE_DTL_PENUGASAN WHERE ID_PENUGASAN=MPE_PENUGASAN.ID AND DELETE_AT IS NULL) = (SELECT COUNT(ID) FROM MPE_BAST WHERE NOTIFIKASI=MPE_PENGAJUAN.NOTIFIKASI AND NOTIFIKASI=MPE_PENUGASAN.NOTIFIKASI AND DELETE_AT IS NULL AND EXTRACT( month FROM CREATE_AT ) = #bln) AND EXTRACT(month FROM MPE_PENGAJUAN.APPROVE_AT) <= #bln"
        );
        $awhere2 = array(
            "WHERE DELETE_AT IS NULL AND STATE = 'Active' AND STATUS='Approved' AND APPROVE_AT IS NOT NULL AND to_char(APPROVE_AT,'YYYY') = '{$thn}' AND EXTRACT(month FROM APPROVE_AT) <= #bln",
            "INNER JOIN MPE_PENUGASAN ON MPE_PENUGASAN.ID_PENGAJUAN=MPE_PENGAJUAN.ID_MPE WHERE MPE_PENGAJUAN.DELETE_AT IS NULL AND MPE_PENUGASAN.DELETE_AT IS NULL AND MPE_PENGAJUAN.STATE = 'Active' AND MPE_PENGAJUAN.STATUS='Approved' AND MPE_PENUGASAN.STATUS<>'Rejected' AND MPE_PENGAJUAN.APPROVE_AT IS NOT NULL AND to_char(MPE_PENGAJUAN.APPROVE_AT,'YYYY') = '{$thn}' AND (SELECT COUNT(ID) FROM MPE_DTL_PENUGASAN WHERE ID_PENUGASAN=MPE_PENUGASAN.ID AND DELETE_AT IS NULL) = (SELECT COUNT(ID) FROM MPE_BAST WHERE NOTIFIKASI=MPE_PENGAJUAN.NOTIFIKASI AND NOTIFIKASI=MPE_PENUGASAN.NOTIFIKASI AND DELETE_AT IS NULL AND EXTRACT( month FROM CREATE_AT ) <= #bln) AND EXTRACT(month FROM MPE_PENGAJUAN.APPROVE_AT) <= #bln"
        );

        $data = array();
        for ($i = 0; $i < count($atable); $i++) {
            $str = "";
            for ($b = 1; $b < 13; $b++) {
                $bln = $b;
                $str .= "(SELECT COUNT(*) FROM {$atable[$i]} /*WHERE*/ " . str_replace('#bln', $bln, $awhere[$i]) . ") AS " . date('F', mktime(0, 0, 0, $bln, 10));
                if ($b < 12) {
                    $str .= ", ";
                }
            }

            $sql = "SELECT {$str} FROM dual";
            // echo $sql.'<br /><br />';
            $query = $this->db->query($sql);
            $query = $query->result_array();
            $query = $query[0];
            // echo $this->db->last_query();
            $data['STS'][$alias[$i]] = array_values($query);
        }
        for ($i = 0; $i < count($atable2); $i++) {
            $str = "";
            for ($b = 1; $b < 13; $b++) {
                $bln = $b;
                $str .= "(SELECT COUNT(*) FROM {$atable2[$i]} /*WHERE*/ " . str_replace('#bln', $bln, $awhere2[$i]) . ") AS " . date('F', mktime(0, 0, 0, $bln, 10));
                if ($b < 12) {
                    $str .= ", ";
                }
            }

            $sql = "SELECT {$str} FROM dual";
            // echo $sql.'<br /><br />';
            $query = $this->db->query($sql);
            $query = $query->result_array();
            $query = $query[0];
            // echo $this->db->last_query();
            $data['TOT'][$alias2[$i]] = array_values($query);
        }

        return $data;
    }

    public function get_data($where) {
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get('MPE_GENERAL');

        return $query->row();
    }

    public function get_data_result($where) {
        $this->db->select('*');
        $this->db->where($where);
        $this->db->order_by('ID');
        $query = $this->db->get('MPE_GENERAL');

        return $query->result();
    }

    public function get_role_result($where) {
        $this->db->select("
                            RD.ROLE_ID AS role,
                            G.ID AS form_id,
                          	G.GEN_TYPE AS type,
                          	G.GEN_CODE AS code,
                          	G.GEN_DESC AS desc,
                          	G.GEN_VAL AS val,
                          	G.GEN_PAR1 AS group,
                          	G.GEN_PAR2 AS uri,
                          	G.GEN_PAR3 AS icon,
                          	G.GEN_PAR4 AS seq,
                          	G.GEN_PAR5 AS spancol,
                          	G.GEN_PAR6 AS tabel,
                          	G.GEN_PAR7 AS ref_tabel,
                          	G.STATUS AS ststrans,
                          	(
                            SELECT DISTINCT
                            	listagg ( GEN_CODE || '=' || (SELECT COUNT(*) FROM MPE_ROLES_DTL WHERE ROLE_ID=RD.ROLE_ID AND FORM_ID=RD.FORM_ID AND PERMISSION_ID=G2.ID) , ';' ) WITHIN GROUP ( ORDER BY ID ) csv
                            FROM
                            	MPE_GENERAL G2
                            WHERE
                            	GEN_PAR2 = G.GEN_CODE AND GEN_VAL='1'
                          	) AS \"stat\"
                          ");
        $this->db->from('MPE_GENERAL G');
        $this->db->join('MPE_ROLES_DTL RD', 'G.ID=RD.FORM_ID', 'left');
        $this->db->where($where);
        $this->db->order_by('TO_NUMBER(G.GEN_PAR4) asc');
        $query = $this->db->get();
//        echo $this->db->last_query();
        return $query->result();
    }

    public function get_action_result($sjoin, $where) {
        $this->db->select("
                          	RD.ROLE_ID AS role,
                          	RD.FORM_ID AS form_id,
                          	G.ID AS permission_id,
                          	G.GEN_TYPE AS type,
                          	G.GEN_CODE AS code,
                          	G.GEN_DESC AS desc,
                          	G.GEN_VAL AS val,
                          	G.GEN_PAR1 AS group,
                          	G.GEN_PAR2 AS menu_code,
                          	G.GEN_PAR3 AS seq,
                          	G.GEN_PAR4 AS ref_id,
                          	G.GEN_PAR5 AS note,
                            (CASE RD.FORM_ID WHEN NULL THEN '0' ELSE '1' END) AS \"state\"
                          ");
        $this->db->from('MPE_GENERAL G');
        $this->db->join('MPE_ROLES_DTL RD', "G.ID=RD.PERMISSION_ID {$sjoin}", 'left');
        $this->db->where($where);
        $this->db->order_by('CAST(G.GEN_PAR3 AS INTEGER) asc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_count_pending($tabel, $ref_tabel, $status = '', $uk_kode = '') {
        $sAdd = '';
        if ($uk_kode != '') {
            // code...
            $sAdd = " AND UK_CODE='{$uk_kode}'";
        }
        $this->db->select('*');
        if ($status != '') {
            $this->db->where("STATUS LIKE '%{$status}%'");
        }
        if ($tabel == 'MPE_PENGAJUAN') {
            if (isset($ref_tabel)) {
                $this->db->where("ID_MPE NOT IN (SELECT ID_PENGAJUAN FROM {$ref_tabel} WHERE (DELETE_AT IS NULL OR STATUS!='Rejected')) {$sAdd} AND DELETE_AT IS NULL AND STATUS!='Rejected'", '', FALSE);
            } else {
                $this->db->where(" DELETE_AT IS NULL AND STATUS!='Rejected' {$sAdd}", '', FALSE);
            }
        } else if ($tabel == 'MPE_PENUGASAN') {
            if (isset($ref_tabel)) {
                $this->db->where("ID NOT IN (SELECT ID_PENUGASAN FROM {$ref_tabel} WHERE (DELETE_AT IS NULL OR STATUS!='Rejected') {$sAdd}) {$sAdd} AND DELETE_AT IS NULL AND STATUS!='Rejected'", '', FALSE);
            } else {
                $this->db->where(" DELETE_AT IS NULL AND STATUS!='Rejected' {$sAdd}", '', FALSE);
            }
        } else if ($tabel == 'MPE_DOK_ENG') {
            if (isset($ref_tabel)) {
                $this->db->where("ID NOT IN (SELECT ID_DOK_ENG FROM {$ref_tabel} WHERE (DELETE_AT IS NULL OR STATUS!='Rejected') AND NOTIFIKASI IN (SELECT NOTIFIKASI FROM MPE_PENGAJUAN WHERE (DELETE_AT IS NULL OR STATUS != 'Rejected') AND TIPE='Detail Design' {$sAdd}) AND DELETE_AT IS NULL)  AND NOTIFIKASI IN (SELECT NOTIFIKASI FROM MPE_PENGAJUAN WHERE (DELETE_AT IS NULL OR STATUS != 'Rejected') AND TIPE='Detail Design' {$sAdd}) AND DELETE_AT IS NULL AND STATUS!='Rejected'", '', FALSE);
            } else {
                $this->db->where(" DELETE_AT IS NULL AND STATUS!='Rejected' AND ID NOT IN (SELECT ID_DOK_ENG FROM MPE_BAST WHERE STATUS='Closed' AND DELETE_AT IS NULL) {$sAdd}", '', FALSE);
            }
        } else if ($tabel == 'MPE_PENDAMPINGAN') {
            $this->db->where(" DELETE_AT IS NULL AND STATUS!='Rejected' AND TIPE = '{$ref_tabel}' {$sAdd}", '', FALSE);
        } else if ($tabel == 'MPE_DOK_TEND') {
            if (!isset($ref_tabel)) {
                $this->db->where(" DELETE_AT IS NULL AND STATUS!='Rejected' {$sAdd}", '', FALSE);
            }
        }
        $query = $this->db->get("$tabel tbl");

        return $query->num_rows();
        ;
    }

    public function get_no_dokumen($sfield, $stable, $sdokumen) {
        $this->db->select("NVL(MAX(CAST(SUBSTR({$sfield},15,3) AS INTEGER))+1,1) AS URUT", FALSE);
        $this->db->where("SUBSTR({$sfield},-4)=TO_CHAR(CURRENT_DATE, 'YYYY') AND SUBSTR({$sfield}, 12, 2)='$sdokumen'");
        $this->db->order_by("CREATE_AT", "DESC");
        $query = $this->db->get($stable);
        // echo $this->db->last_query();

        return $query->row();
    }

    function get_log($tbl, $id) {
        $this->db->select("ID,
                      	REF_ID,
                      	REF_TABLE,
                      	DESKRIPSI,
                      	NOTE,
                      	STATUS,
                      	LOG_RES1,
                      	LOG_RES2,
                      	LOG_RES3,
                      	LOG_RES4,
                      	LOG_RES5,
                      	LOG_RES6,
                      	LOG_RES7,
                      	CREATE_BY,
                      	CREATE_AT,
                      	UPDATE_BY,
                      	UPDATE_AT,
                      	DELETE_BY,
                      	DELETE_AT", FALSE);
        $this->db->where('REF_ID', $id);
        $this->db->where('REF_TABLE', $tbl);
        $this->db->where('DELETE_AT IS NULL');
        $this->db->order_by("CREATE_AT", "DESC");
        $query = $this->db->get('MPE_LOG');
        // echo $this->db->last_query();

        return $query->result_array();
    }

    function log_saving($data) {
        $query = $this->db->insert('MPE_LOG', $data);
        // $result = false;
        // if($query)
        // {
        //   $this->db->select_max('ID','ID');
        //   $result = $this->db->get('MPE_LOG')->row_array();
        // }
        // return $result;
        return $this->db->affected_rows();
    }

    function file_saving($data) {
        $query = $this->db->insert_batch('MPE_FILES', $data);
        // echo $this->db->last_query();

        return $this->db->affected_rows();
    }

    public function file_opening($where) {
        $this->db->select("*", FALSE);
        $this->db->from("MPE_FILES");
        $this->db->where($where);
        $this->db->where("DELETE_AT IS NULL");
        $this->db->order_by("CREATE_AT", "DESC");
        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

}
