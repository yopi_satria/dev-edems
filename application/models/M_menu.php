<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_menu extends CI_Model
{
    private $roles_tb = 'SHE_ROLES';
    private $roles_users_tb = 'SHE_USERS_ROLES';
    private $menu_tb = 'SHE_MENU';
    private $role_permission_tb = 'SHE_ROLE_PERMISSIONS';
    private $permissions_tb = 'SHE_PERMISSIONS';

    function __construct(){
        parent::__construct();


    }

    function insert_menu($param){
        $sql = $this->sql_insert($this->menu_tb, $param);

        $query = $this->db->query($sql);

        return (bool) $query;
    }
    function update_menu($param){
        $sql = $this->sql_update($this->menu_tb, $param, '"ID"');

        $query = $this->db->query($sql);

        return (bool) $query;
    }

    function view_menu(){
        $sql = "SELECT * FROM {$this->menu_tb}";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function delete_menu($id){
        $sql = "DELETE {$this->menu_tb} WHERE \"ID\" = {$id}";

        $query = $this->db->query($sql);

         return (bool) $query;
    }



}
