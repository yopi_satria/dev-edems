<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_support extends CI_Model
{
    // private $tb_m_plant      = 'SHE_MASTER_PLANT';

    function __construct(){
        parent::__construct();

    }

    ///====================================================================================================
    ///PLANT ==========================================================================================
    ///====================================================================================================

    public function getData_plant($where = array()){
      $this->db->distinct();
      $this->db->select('PLANT, PLANT_TEXT');
      $this->db->where($where);
      $this->db->where('PLANT IS NOT NULL');

      $query = $this->db->get($this->tb_m_plant);
      return $query->result_array();
    }

    public function insertData_plant($param = array()){
      $sql = $this->sql_insert2($this->tb_m_plant, $param, array('CREATE_AT'), array('DATE_ACTIVITY'));
      $query = $this->db->query($sql);
      return $query;
    }

    public function updateData_plant($param = array(), $where = array()){
      $sql = $this->sql_update2($this->tb_m_plant, $param, $where, array('UPDATE_AT'), array('DATE_ACTIVITY'));
      $query = $this->db->query($sql);
      return $query;
    }

    public function deleteData_plant($param = array(), $where = array()){
      $sql = $this->sql_update2($this->tb_m_plant, $param, $where, array('DELETE_AT'));
      $query = $this->db->query($sql);
      return $query;
    }
}
