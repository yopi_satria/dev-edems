<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_general extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

    function _qryNotifPengajuan($user){
      $this->db->select('"NOTIFIKASI", "CREATE_AT", "NAMA_PEKERJAAN","APPROVE_AT"');
      $this->db->from('MPE_PENGAJUAN');
      $this->db->where('APPROVE_BY',$user);
      $this->db->where('APPROVE_AT',NULL);
      $this->db->where('MPE_PENGAJUAN.DELETE_AT IS NULL');
    }

    function getNotification($user){
      $this->_qryNotifPengajuan($user);
      $query    = $this->db->get();$this->db->last_query();
      $data     = $query->result_array();
      return $data;
    }

    function getAllLogin(){
      $query			= $this->db->get('MPE_SESSION');
  		$num_rows		= $query->num_rows();
  		return $num_rows;
    }

    function _qrySession($id){
      $this->db->select('"session","waktu"');
      $this->db->from('MPE_SESSION');
      $this->db->where('session',$id);
    }

    function getSession($id){
      $this->_qrySession($id);
      $query    = $this->db->get();$this->db->last_query();
      $data     = $query->num_rows();
      return $data;
    }

    function saveLogin($data){
      $this->db->set($data);
      $this->db->insert('MPE_SESSION');
      return $this->db->affected_rows();
    }

    function updateLogin($data,$session ){
      $this->db->set($data);
      $this->db->where('session', $session);
      $this->db->update('MPE_SESSION');
      return $this->db->affected_rows();
    }

    function deleteLogin(){
      $time = time();
      $timeout = $time - 600;
      $this->db->where('waktu <', $timeout);
      $this->db->delete('MPE_SESSION');
      return $this->db->affected_rows();
    }

}
