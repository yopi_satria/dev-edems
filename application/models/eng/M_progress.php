<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_progress extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

    var $column = array(
        'NO_PENUGASAN', 'ID_PENUGASAN', 'DESKRIPSI', 'START_DATE', 'END_DATE', 'REMARKS', 'PROGRESS', 'STATUS', 'CREATE_BY', 'CREATE_AT', 'ID'
    );

    function _qry($key) {
        $this->db->select('
                            A."ID",
                            A.ID_PENUGASAN,
                            (B.NO_PENUGASAN || \' \' || (SELECT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE=B.ID_PENGAJUAN AND STATUS=\'Approved\') || \' (\' || B.NOTIFIKASI || \')\' || \' ; \' || (SELECT AVG(PROGRESS) FROM "MPE_DTL_PENUGASAN" WHERE DELETE_AT IS NULL AND PROGRESS IS NOT NULL AND ID_PENUGASAN = B.ID)) GROUP_TEXT,
                            B.NO_PENUGASAN,
                            B.NOTIFIKASI,
                            (SELECT TIPE FROM MPE_PENGAJUAN WHERE DELETE_AT IS NULL AND ID_MPE = B.ID_PENGAJUAN AND ROWNUM=1) TIPE,
                            B.OBJECTIVE,
                          	A.DESKRIPSI,
                          	A.REMARKS,
                            A.PROGRESS PROGRESS,
                          	A.START_DATE,
                            A.END_DATE,
                            TO_CHAR(A."START_DATE", \'YYYY-MM-DD\') AS "S_DATE",
                            TO_CHAR(A."END_DATE", \'YYYY-MM-DD\') AS "E_DATE",
                          	A.STATUS,
                          	A.NOTE,
                            (SELECT AVG(PROGRESS) FROM "MPE_DTL_PENUGASAN" WHERE DELETE_AT IS NULL AND PROGRESS IS NOT NULL AND ID_PENUGASAN = B.ID) TOT_PROG,
                            (SELECT COUNT(*) FROM MPE_DOK_ENG WHERE ID_PENUGASAN=A.ID_PENUGASAN AND PACKET_TEXT=A.DESKRIPSI AND ID IN (SELECT ID_DOK_ENG FROM MPE_BAST WHERE ID_DOK_ENG=MPE_DOK_ENG.ID AND DELETE_AT IS NULL)) STATE,
                          	A.COMPANY,
                          	A.CREATE_AT,
                          	A.CREATE_BY,
                          	A.UPDATE_AT,
                          	A.UPDATE_BY,
                          	A.DELETE_AT,
                          	A.DELETE_BY
                        ');
        $this->db->from('MPE_DTL_PENUGASAN A');
        $this->db->join("MPE_PENUGASAN B", "A.ID_PENUGASAN = B.ID");
        if ($key['search'] !== '') {
            $sAdd = "(
                  LOWER(B.NOTIFIKASI) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(B.NO_PENUGASAN) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.DESKRIPSI) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.REMARKS) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.PROGRESS) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.START_DATE) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.STATUS) LIKE '%" . strtolower($key['search']) . "%'
                  )";
            $this->db->where($sAdd, '', FALSE);
        }
        $this->db->where('A.DELETE_AT IS NULL');
        $this->db->where('B.DELETE_AT IS NULL');
        // $this->db->where('B.APPROVE2_AT IS NOT NULL');
        if (isset($key['dept_code'])) {
            // $this->db->where("(A.CREATE_BY = '{$key['name']}' OR A.CREATE_BY LIKE '{$key['username']}@%')");
            $this->db->where("A.DEPT_CODE = '{$key['dept_code']}'");
        }
        if (isset($key['uk_code'])) {
            $this->db->where("A.UK_CODE = '{$key['uk_code']}'");
        }
        if (isset($key['tipe'])) {
            // $this->db->where('A.DESKRIPSI NOT IN (SELECT PACKET_TEXT FROM MPE_DOK_ENG WHERE ID_PENUGASAN=B.ID AND PACKET_TEXT=A.DESKRIPSI AND APPROVE3_AT IS NOT NULL)');
            $this->db->not_like('LOWER(A.REMARKS)', 'cancel');
            $this->db->where('A.ID_PENUGASAN NOT IN (SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID_PENUGASAN=A.ID_PENUGASAN AND PACKET_TEXT=A.DESKRIPSI AND APPROVE3_AT IS NOT NULL AND ID IN (SELECT ID_DOK_ENG FROM MPE_BAST WHERE ID_DOK_ENG=MPE_DOK_ENG.ID AND DELETE_AT IS NULL))');
        }
        $this->db->where("
                        	((B.APPROVE0_AT IS NOT NULL AND A.UK_CODE IN (SELECT GEN_PAR1 FROM MPE_GENERAL WHERE GEN_TYPE='APP_LIMIT' AND GEN_CODE='EAT_DE' AND GEN_VAL='1'))
                        	OR (B.APPROVE1_AT IS NOT NULL AND A.UK_CODE NOT IN (SELECT GEN_PAR1 FROM MPE_GENERAL WHERE GEN_TYPE='APP_LIMIT' AND GEN_CODE='EAT_DE' AND GEN_VAL='1')))
                        ");
        $order = $this->column[$key['ordCol']];
        $this->db->order_by($order, $key['ordDir']);
    }

    function get($key) {
        $this->_qry($key);
        $this->db->limit($key['length'], $key['start']);
        $query = $this->db->get();
        $data = $query->result();
        return $data;
    }

    function get_data($key) {
        $this->_qry($key);
        $this->db->limit($key['length'], $key['start']);
        $query = $this->db->get();
        $data = $query->result();
//        echo $this->db->last_query();

        return $data;
    }

    function recFil($key) {
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function recTot($key) {
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function get_record($where) {
        $this->db->select('
                            A."ID",
                            A.ID_PENUGASAN,
                            (B.NO_PENUGASAN || \' \' || (SELECT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE=B.ID_PENGAJUAN AND STATUS=\'Approved\') || \' (\' || B.NOTIFIKASI || \')\') GROUP_TEXT,
                            B.NO_PENUGASAN,
                            B.NOTIFIKASI,
                            B.OBJECTIVE,
                          	A.DESKRIPSI,
                          	A.REMARKS,
                            A.PROGRESS PROGRESS,
                          	A.START_DATE,
                            A.END_DATE,
                            TO_CHAR(A."START_DATE", \'YYYY-MM-DD\') AS "S_DATE",
                            TO_CHAR(A."END_DATE", \'YYYY-MM-DD\') AS "E_DATE",
                          	A.STATUS,
                          	A.NOTE,
                            (SELECT COUNT(*) FROM MPE_DOK_ENG WHERE PACKET_TEXT=A.DESKRIPSI AND DELETE_AT IS NULL) STATE,
                          	A.COMPANY,
                          	A.CREATE_AT,
                          	A.CREATE_BY,
                          	A.UPDATE_AT,
                          	A.UPDATE_BY,
                          	A.DELETE_AT,
                          	A.DELETE_BY
                        ');
        $this->db->from('MPE_DTL_PENUGASAN A');
        $this->db->join("MPE_PENUGASAN B", "A.ID_PENUGASAN = B.ID");
        $this->db->where('A.DELETE_AT IS NULL');
        $this->db->where($where);

        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    public function get_foreign() {
        $this->db->select('
                        		PN."ID" ID_PENUGASAN,
                        		PN.NO_PENUGASAN,
                            (SELECT DISTINCT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE = PN.ID_PENGAJUAN AND DELETE_AT IS NULL AND APPROVE_AT IS NOT NULL) PENGAJUAN,
                        		PN.NOTIFIKASI,
                        		PN.OBJECTIVE,
                          	PN.CUST_REQ,
                          	PN.SCOPE_ENG,
                          	PN.KOMITE_PENGARAH,
                          	PN.TIM_PENGKAJI,
                          	PN.TW_KOM_PENGARAH,
                          	PN.TW_TIM_PENGKAJI,
                          	PN.STATUS,
                        		PN.COMPANY,
                        		PN.APPROVE1_AT,
                        		PN.APPROVE1_BY,
                        		PN.APPROVE2_AT,
                        		PN.APPROVE2_BY,
                        		PN.CREATE_AT,
                        		PN.CREATE_BY,
                        		PN.UPDATE_AT,
                        		PN.UPDATE_BY,
                        		PN.DELETE_AT,
                        		PN.DELETE_BY
                          ');
        $this->db->from("MPE_PENUGASAN PN");
        $this->db->where("
                          PN.DELETE_AT IS NULL
                          AND PN.APPROVE1_AT IS NOT NULL
                          AND PN.APPROVE2_AT IS NOT NULL
                          AND PN.ID_PENGAJUAN IN (SELECT ID_MPE FROM MPE_PENGAJUAN WHERE STATUS='In Progress')
                        ");
        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    function save($data) {
        $START_DATE = str_replace("-", "/", $data['START_DATE']);
        $END_DATE = str_replace("-", "/", $data['START_DATE']);
        $this->db->set('START_DATE', "TO_DATE('$START_DATE','yyyy/mm/dd')", false);
        $this->db->set('END_DATE', "TO_DATE('$START_DATE','yyyy/mm/dd')", false);
        unset($data['START_DATE']);
        unset($data['END_DATE']);
        $query = $this->db->insert('MPE_DTL_PENUGASAN', $data);
        $result = false;
        if ($query) {
            $this->db->select_max('ID', 'ID');
            $result = $this->db->get('MPE_DTL_PENUGASAN')->row_array();
        }
        return $result;
        // return $this->db->affected_rows();
    }

    function updateData($tipe, $param = array()) {
        if ($tipe == 'delete') {
            $id = $param['ID'];
            unset($param['ID']);
            $this->db->set('DELETE_AT', "CURRENT_DATE", false);
            $this->db->where('ID', $id);
            $query = $this->db->update('MPE_DTL_PENUGASAN', $param);

            return (bool) $query;
        } else if ($tipe == 'approve') {
            $id = $param['ID'];
            unset($param['ID']);
            $this->db->where('ID', $id);
            $query = $this->db->update('MPE_DTL_PENUGASAN', $param);

            return (bool) $query;
        } else {
            $id = $param['ID'];
            $START_DATE = str_replace("-", "/", $param['START_DATE']);
            $END_DATE = str_replace("-", "/", $param['END_DATE']);
            $this->db->set('START_DATE', "TO_DATE('$START_DATE','yyyy/mm/dd')", false);
            $this->db->set('END_DATE', "TO_DATE('$END_DATE','yyyy/mm/dd')", false);
            unset($param['START_DATE']);
            unset($param['END_DATE']);
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $this->db->where('ID', $id);
            unset($param['ID']);

            $query = $this->db->update('MPE_DTL_PENUGASAN', $param);
            // echo $this->db->last_query();

            return (bool) $query;
        }
    }

}
