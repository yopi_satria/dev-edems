<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_ceknotif extends CI_Model {

    private $apd_mat_list = 'SHE_APD_SAP_MATERIAL';

    function __construct(){
        parent::__construct();
        require_once APPPATH."/third_party/sapclasses/sap.php";
    }

    public function get_codemat_apd(){
        $sql = "SELECT * FROM {$this->apd_mat_list} ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function coba($post){
        $sap = new SAPConnection();
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();

            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }
        $fce = $sap->NewFunction('Z_ZAPPSD_SELECT_TRUK2');  // ON Dev

            if ($fce == TRUE) {

                     $fce->XPARAM["NOPOLISI"] = $post; //Sales Organization
                     $fce->XDATA_APP["NMORG"] = "7000"; //Unit of measure of delivery unit

                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {
                    $fce->RETURN_DATA->Reset();
                  while ($fce->RETURN_DATA->Next()) {
                     $sapPrint[] = ($fce->RETURN_DATA->row);
                    }
                }
                $fce->Close();
                $sap->Close();
            }
        return $sapPrint[0]['NO_EXPEDITUR'];
    }

    function searchnotif($param){
        // print_r($param['codefuncloc']);
        $sap = new SAPConnection();
        // $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataCloning.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();

            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }

        $fce = $sap->NewFunction('Z_ZCPM_RFC_NOTIFGETDATA');  // ON Dev
            // print_r($param);

            if ($fce == TRUE) {

                $fce->I_NUMBER = $param['no_notif'];

                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {

                    $sapPrint['HEADER'] = $fce->O_NOTIFHEADER_EXPORT;
                    $sapPrint['STATUS'] = $fce->O_STTXT;

                    // print_r($fce);
                    $fce->T_NOTLONGTXT->Reset();
                    while ($fce->T_NOTLONGTXT->Next()) {
                        $sapPrint['CONTENT'] = ($fce->T_NOTLONGTXT->row);
                    }
                }

                $fce->Close();
                $sap->Close();
            }

        return $sapPrint;
    }

}
