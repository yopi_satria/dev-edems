<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sapapd extends CI_Model {

    private $apd_mat_list = 'SHE_APD_SAP_MATERIAL';

    function __construct(){
        parent::__construct();
        require_once APPPATH."/third_party/sapclasses/sap.php";
    }

    public function get_codemat_apd(){
        $sql = "SELECT * FROM {$this->apd_mat_list} ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function coba($post){
        $sap = new SAPConnection();
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();

            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }
        $fce = $sap->NewFunction('Z_ZAPPSD_SELECT_TRUK2');  // ON Dev


            if ($fce == TRUE) {

                     $fce->XPARAM["NOPOLISI"] = $post; //Sales Organization
                     $fce->XDATA_APP["NMORG"] = "7000"; //Unit of measure of delivery unit

                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {
                    $fce->RETURN_DATA->Reset();
                  while ($fce->RETURN_DATA->Next()) {
                     $sapPrint[] = ($fce->RETURN_DATA->row);
                    }
                }
                $fce->Close();
                $sap->Close();
            }
        return $sapPrint[0]['NO_EXPEDITUR'];
    }

    function get_stock_in_warehouse($post,$nosap){
        $sap = new SAPConnection();
        // $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataCloning.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();
            // echo "string";
            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }
        $fce = $sap->NewFunction('ZRMM_STOCK_GIGR');   // ON Dev
        // $fce = $sap->NewFunction('ZRMM_STOCK_GIGR');  // ON Clone
        // print_r($fce);
            if ($fce == TRUE) {

                $fce->P_SOBKZ = '';
                $fce->P_LAYOUT = '/AH';
                $fce->P_VARIANT = '';

                $compcode = $post;

                if ($post == 7000 || $post == 5000 || $post == 2000) {
                    $fce->T_BUKRS->row["SIGN"] = 'I';
                    $fce->T_BUKRS->row["OPTION"] = 'EQ';
                    $fce->T_BUKRS->row["LOW"] = 2000;
                    $fce->T_BUKRS->row["HIGH"] = '';
                    $fce->T_BUKRS->Append($fce->T_BUKRS->row);
                    $fce->T_BUKRS->row["SIGN"] = 'I';
                    $fce->T_BUKRS->row["OPTION"] = 'EQ';
                    $fce->T_BUKRS->row["LOW"] = 5000;
                    $fce->T_BUKRS->row["HIGH"] = '';
                    $fce->T_BUKRS->Append($fce->T_BUKRS->row);
                    $fce->T_BUKRS->row["SIGN"] = 'I';
                    $fce->T_BUKRS->row["OPTION"] = 'EQ';
                    $fce->T_BUKRS->row["LOW"] = 7000;
                    $fce->T_BUKRS->row["HIGH"] = '';
                    $fce->T_BUKRS->Append($fce->T_BUKRS->row);
                }else{
                    $fce->T_BUKRS->row["SIGN"] = 'I';
                    $fce->T_BUKRS->row["OPTION"] = 'EQ';
                    $fce->T_BUKRS->row["LOW"] = $compcode;
                    $fce->T_BUKRS->row["HIGH"] = '';
                    $fce->T_BUKRS->Append($fce->T_BUKRS->row);
                }
                // echo $compcode;

                for($i = 0, $c = count($nosap); $i < $c; $i++){
                    // echo $nosap[$i];
                    $fce->T_MATNR->row["SIGN"] = 'I';
                    $fce->T_MATNR->row["OPTION"] = 'EQ';
                    $fce->T_MATNR->row["LOW"] = $nosap[$i];
                    $fce->T_MATNR->row["HIGH"] = '';
                    $fce->T_MATNR->Append($fce->T_MATNR->row);

                }



                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {
                    $fce->IT_HASIL->Reset();
                    while ($fce->IT_HASIL->Next()) {
                        $sapPrint[] = ($fce->IT_HASIL->row);
                    }
                }
                $fce->Close();
                $sap->Close();
            }else{
              echo "Not Connected";
            }

        return $sapPrint;
    }

}
