<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_deletenotif extends CI_Model {

    function __construct(){
        parent::__construct();
        require_once APPPATH."/third_party/sapclasses/sap.php";
    }

    function deletenotif($param){
      $sapPrint = array();
      $sap = new SAPConnection();
      // $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");
      $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataCloning.conf");

      if ($sap->GetStatus() == SAPRFC_OK) {
        $sap->Open();
      }else{
        echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
        exit;
      }

      $fce = $sap->NewFunction('ZCPM_NOTIF_CHG_SHE');
          if ($fce == TRUE) {
              $fce->P_FL = $param['STATUS'];
              $fce->I_NUMBER = $param['NOTIF_NO'];

              $fce->Call();
              if ($fce->GetStatus() == SAPRFC_OK) {
                // echo "<pre>";
                // echo "Return System Status : ";
                // print_r($fce->SYSTEMSTATUS);
                // echo "<br />Return FCE : <br />";
                // print_r($fce);
                // echo "</pre>";
                $sapPrint['STATUS'] = $fce->SYSTEMSTATUS;
                // $fce->T_RETURN->Reset();
                // while ($fce->T_RETURN->Next()) {
                //   $sapPrint['CONTENT'] = ($fce->T_RETURN->row);
                // }
              }
            $fce->Close();
            $sap->Close();
          }

      return $sapPrint;
    }

}
