<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_roles extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

  	var $column = array(
  		'NAME', 'DESCRIPTION', 'SPECIAL', 'CREATE_BY', 'CREATE_AT', 'ID'
  	);

  	function _qry($key){
    		$this->db->select('
                            A."ID",
                          	A."NAME",
                          	A."DESCRIPTION",
                          	(CASE A."SPECIAL" WHEN \'1\' THEN \'true\' ELSE \'false\' END) SPECIAL,
                          	A."NOTE",
                          	A."STATUS",
                          	A.CREATE_AT,
                          	A.CREATE_BY,
                          	A.UPDATE_AT,
                          	A.UPDATE_BY,
                          	A.DELETE_AT,
                          	A.DELETE_BY
                        ');
    		$this->db->from('MPE_ROLES A');
    		// $this->db->join("MPE_PENUGASAN B", "A.ID_PENUGASAN = B.ID");
    		if($key['search']!==''){
          $sAdd ="(
                  LOWER(A.NAME) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(A.DESCRIPTION) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(A.CREATE_AT) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(A.CREATE_BY) LIKE '%".strtolower($key['search'])."%'
                  )";
          $this->db->where($sAdd, '', FALSE);
    		}
    		$this->db->where('A.DELETE_AT IS NULL');
    		$order = $this->column[$key['ordCol']];
    		$this->db->order_by($order, $key['ordDir']);
  	}

  	function get($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
  		$data			= $query->result();
  		return $data;
  	}

  	function get_data($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
  		$data			= $query->result();

  		return $data;
  	}

  	function recFil($key){
  		$this->_qry($key);
  		$query			= $this->db->get();
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

  	function recTot(){
  		$query			= $this->db->get('MPE_ROLES');
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

  	function get_record($where){
    		$this->db->select('
                            A."ID",
                          	A."NAME",
                          	A."DESCRIPTION",
                          	(CASE A."SPECIAL" WHEN \'1\' THEN \'true\' ELSE \'false\' END) SPECIAL,
                          	A."NOTE",
                          	A."STATUS",
                          	A.CREATE_AT,
                          	A.CREATE_BY,
                          	A.UPDATE_AT,
                          	A.UPDATE_BY,
                          	A.DELETE_AT,
                          	A.DELETE_BY
                        ');
    		$this->db->from('MPE_ROLES A');
    		$this->db->where('A.DELETE_AT IS NULL');
    		$this->db->where($where);

        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
  	}

    public function get_foreign($id = null){
        if ($id) {
          $this->db->where("ROLE_ID = '{$id}'");
        }
        $this->db->select('*');
    		$this->db->from("MPE_ROLES_DTL");
        // $this->db->where("DELETE_AT IS NULL");
        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    function save($data) {
        // $START_DATE = str_replace("-","/",$data['START_DATE']);
        // $END_DATE = str_replace("-","/",$data['START_DATE']);
        // $this->db->set('START_DATE', "TO_DATE('$START_DATE','yyyy/mm/dd')", false);
        // $this->db->set('END_DATE', "TO_DATE('$START_DATE','yyyy/mm/dd')", false);
        // unset($data['START_DATE']);
        // unset($data['END_DATE']);
        $query = $this->db->insert('MPE_ROLES', $data);
        $result = false;
        if($query)
        {
          $this->db->select_max('ID','ID');
          $result = $this->db->get('MPE_ROLES')->row_array();
        }
        return $result;
        // return $this->db->affected_rows();
    }

    function savedtl($data = array()){
      $result = $this->db->insert_batch('MPE_ROLES_DTL', $data);

      return $result;
    }

    function delete_dtl($id){
      $this->db->where('ROLE_ID', $id);
      $this->db->delete('MPE_ROLES_DTL');

      return $this->db->affected_rows();
    }

    function updateData($tipe, $param=array()){
        if($tipe=='delete'){
          $id = $param['ID'];
          unset($param['ID']);
          $this->db->set('DELETE_AT', "CURRENT_DATE", false);
          $this->db->where('ID', $id);
          $query = $this->db->update('MPE_ROLES', $param);

          return (bool) $query;
        }else  if($tipe=='approve'){
          $id = $param['ID'];
          unset($param['ID']);
          $this->db->where('ID', $id);
          $query = $this->db->update('MPE_ROLES', $param);

          return (bool) $query;
        }else {
          $id = $param['ID'];
          $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
          $this->db->where('ID', $id);
          unset($param['ID']);

          $query = $this->db->update('MPE_ROLES', $param);

          $this->delete_dtl($id);
          // echo $this->db->last_query();

          return (bool) $query;
        }
    }

}
