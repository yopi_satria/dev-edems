<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_approval extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        // $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

    public function get_data($where){
        // $this->db->select("
        //                     ID,
        //                     REF_ID,
        //                     REF_TABEL,
        //                     TIPE,
        //                     LVL,
        //                     STATE,
        //                     APP_BADGE,
        //                     APP_NAME,
        //                     APP_JAB,
        //                     APP_EMAIL,
        //                     STATUS,
        //                     NOTE,
        //                     COMPANY,
        //                     CREATE_AT,
        //                     CREATE_BY,
        //                     UPDATE_AT,
        //                     UPDATE_BY,
        //                     DELETE_AT,
        //                     DELETE_BY
        // ");
        $this->db->distinct("
                            ID,
                            REF_ID,
                            REF_TABEL,
                            TIPE,
                            LVL,
                            STATE,
                            APP_BADGE,
                            APP_NAME,
                            APP_JAB,
                            APP_EMAIL,
                            STATUS,
                            NOTE,
                            COMPANY,
                            CREATE_AT,
                            CREATE_BY,
                            UPDATE_AT,
                            UPDATE_BY,
                            DELETE_AT,
                            DELETE_BY
        ");
        $this->db->where($where);
        $this->db->where('DELETE_AT',NULL);
        $query= $this->db->get('MPE_APPROVAL');
        // echo $this->db->last_query();

        return $query->result_array();
    }

    public function get_row($where){
        $this->db->distinct("
                            ID,
                            REF_ID,
                            REF_TABEL,
                            TIPE,
                            LVL,
                            STATE,
                            APP_BADGE,
                            APP_NAME,
                            APP_JAB,
                            APP_EMAIL,
                            STATUS,
                            NOTE,
                            COMPANY,
                            CREATE_AT,
                            CREATE_BY,
                            UPDATE_AT,
                            UPDATE_BY,
                            DELETE_AT,
                            DELETE_BY
        ");
        $this->db->where($where);
        $this->db->where('DELETE_AT',NULL);
        $this->db->order_by('LVL', 'ASC');
        $query= $this->db->get('MPE_APPROVAL');
        // echo $this->db->last_query();

        return $query->row_array();
    }

    function save($data) {
        $this->db->insert('MPE_APPROVAL', $data);
        return $this->db->affected_rows();
    }

    function deleteData($param){
        $this->db->set('DELETE_AT', "CURRENT_DATE", false);
        $this->db->where('REF_ID', $param['REF_ID']);
        unset($param['REF_ID']);
        if (isset($param['TIPE'])) {
          $this->db->where('TIPE', $param['TIPE']);
          unset($param['TIPE']);
        }
        $query = $this->db->update('MPE_APPROVAL', $param);

        return $this->db->affected_rows();
    }

    function updateData($tipe, $param=array()){
        if($tipe=='delete'){
          $id = $param['id'];
          unset($param['id']);
          $this->db->set('DELETE_AT', "CURRENT_DATE", false);
          $this->db->where('ID', $id);
          $query = $this->db->update('MPE_APPROVAL', $param);

          return (bool) $query;
        }else  if($tipe=='approve'){
          // $id = $param['ID'];
          $this->db->where('APP_NAME', $param['APP_NAME']);
          unset($param['APP_NAME']);
          $this->db->where('TIPE', $param['TIPE']);
          unset($param['TIPE']);
          $this->db->where('REF_ID', $param['ID']);
          unset($param['ID']);
          $query = $this->db->update('MPE_APPROVAL', $param);

          return (bool) $query;
        }else  if($tipe=='reject'){
          $this->db->where('REF_ID', $param['ID']);
          $query = $this->db->update('MPE_APPROVAL', $param);
          return (bool) $query;
        }elseif ($tipe=='updatestatus') {
          if (isset($param['ID'])) {
            $this->db->where('ID', $param['ID']);
            unset($param['ID']);
          }
          if (isset($param['REF_ID'])) {
            $this->db->where('REF_ID', $param['REF_ID']);
            unset($param['REF_ID']);
            $this->db->where('REF_TABEL', $param['REF_TABEL']);
            unset($param['REF_TABEL']);
            $this->db->where('TIPE', $param['TIPE']);
            unset($param['TIPE']);
            $this->db->where('LVL', $param['LVL']);
            unset($param['LVL']);
          }
          $query = $this->db->update('MPE_APPROVAL', $param);
        }else {
          $id = $param['ID'];
          $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
          $this->db->where('ID', $id);
          unset($param['ID']);

          $query = $this->db->update('MPE_APPROVAL', $param);
          // echo $this->db->last_query();

          return (bool) $query;
        }
    }

    // function count_app($tipe, $id){
    //   if($tipe=='Paraf'){
    //     $this->db->select("*");
    //     $this->db->from('MPE_APPROVAL');
    //     $this->db->where('TIPE', $tipe);
    //     $this->db->where('REF_ID', $id);
    //     $result = $this->db->get()->row();
    //     return $result;
    // }
    //
    // function check_app($tipe, $id){
    //   if($tipe=='Paraf'){
    //     $this->db->select("*");
    //     $this->db->from('MPE_APPROVAL');
    //     $this->db->where('TIPE', $tipe);
    //     $this->db->where('REF_ID', $id);
    //     $this->db->where('UPDATE_AT', IS NOT NULL);
    //     $result = $this->db->get()->row();
    //     return $result;
    // }

}
