<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_users extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

  	var $column = array(
  		'username', 'email', 'role_name', 'pos_text', 'unit_kerja', 'dept_text', 'company', 'id'
  	);

  	function _qry($key){
    		$this->db->select('
                            A.id,
                          	A.role_id,
                            B.NAME role_name,
                          	A.email,
                          	A.username,
                          	A.password,
                          	A.name,
                          	A.uk_kode,
                          	A.unit_kerja,
                          	A.no_badge,
                          	A.cost_center,
                          	A.cc_text,
                          	A.dept_code,
                          	A.dept_text,
                          	A.position,
                          	A.pos_text,
                          	A.reset_code,
                          	A.reset_time,
                          	A.remember_code,
                          	A.last_login,
                          	A.device_login,
                          	A.active,
                          	A.company,
                          	A.plant,
                          	A.themes,
                          	A.date_created,
                          	A.update_at,
                          	A.update_by
                        ');
    		$this->db->from('MPE_USERS A');
    		$this->db->join("MPE_ROLES B", "A.role_id = B.ID");
    		if($key['search']!==''){
          $sAdd ="(
                  LOWER(\"A\".\"username\") LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(\"A\".\"email\") LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(\"A\".\"name\") LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(\"B\".\"NAME\") LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(\"A\".\"uk_kode\") LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(\"A\".\"unit_kerja\") LIKE '%".strtolower($key['search'])."%'
                  )";
          $this->db->where($sAdd, '', FALSE);
    		}
    		$this->db->where('A.active','1');
    		$order = $this->column[$key['ordCol']];
    		$this->db->order_by($order, $key['ordDir']);
  	}

  	function get($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
  		$data			= $query->result();
  		return $data;
  	}

  	function get_data($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
  		$data			= $query->result();

  		return $data;
  	}

  	function recFil($key){
  		$this->_qry($key);
  		$query			= $this->db->get();
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

  	function recTot(){
  		$query			= $this->db->get('MPE_USERS');
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

  	function get_record($where){
    		$this->db->select('
                            A.id,
                            A.role_id,
                            B.NAME role_name,
                            A.email,
                            A.username,
                            A.password,
                            A.name,
                            A.uk_kode,
                            A.unit_kerja,
                            A.no_badge,
                            A.cost_center,
                            A.cc_text,
                            A.dept_code,
                            A.dept_text,
                            A.position,
                            A.pos_text,
                            A.reset_code,
                            A.reset_time,
                            A.remember_code,
                            A.last_login,
                            A.device_login,
                            A.active,
                            A.company,
                            A.plant,
                            A.themes,
                            A.date_created,
                            A.update_at,
                            A.update_by
                        ');
    		$this->db->from('MPE_USERS A');
        $this->db->join("MPE_ROLES B", "A.role_id = B.ID");
    		$this->db->where('A.active','1');
    		$this->db->where($where);

        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
  	}

    public function get_foreign($id = null){
        $this->db->select('
                            ID,
                            NAME,
                            DESCRIPTION,
                            (CASE SPECIAL WHEN \'1\' THEN \'true\' ELSE \'false\' END) SPECIAL,
                            NOTE,
                            STATUS
        ');
    		$this->db->from("MPE_ROLES");
        $this->db->where("DELETE_AT IS NULL");
        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    function save($data) {
        $query = $this->db->insert('MPE_USERS', $data);
        $result = false;
        if($query)
        {
          $this->db->select_max('id','id');
          $result = $this->db->get('MPE_USERS')->row_array();
        }
        return $result;
        // return $this->db->affected_rows();
    }

    function updateData($tipe, $param=array()){
        if($tipe=='delete'){
          $id = $param['id'];
          unset($param['id']);
          // $this->db->set('DELETE_AT', "CURRENT_DATE", false);
          $this->db->set('"update_at"', "CURRENT_DATE", false);
          $this->db->where('id', $id);
          $query = $this->db->update('MPE_USERS', $param);

          return (bool) $query;
        }else  if($tipe=='approve'){
          $id = $param['id'];
          unset($param['id']);
          $this->db->where('id', $id);
          $query = $this->db->update('MPE_USERS', $param);

          return (bool) $query;
        }else {
          $id = $param['id'];
          $this->db->set('"update_at"', "CURRENT_DATE", false);
          $this->db->where('id', $id);
          unset($param['id']);

          $query = $this->db->update('MPE_USERS', $param);
          // echo $this->db->last_query();


          return (bool) $query;
        }
    }

}
