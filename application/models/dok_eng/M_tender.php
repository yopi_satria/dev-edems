<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_Tender extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

  	var $column = array(
  		'ID', 'CREATE_AT', 'NO_DOK_TEND', 'NOTIFIKASI', 'NO_PENGAJUAN', 'NO_DOK_ENG', 'PACKET_TEXT', 'STATUS', 'ID'
  	);

  	function _qry($key){
    		$this->db->select('
                            A."ID",
                            A.ID_DOK_ENG,
                            B.NO_PENUGASAN,
                            C.NO_PENGAJUAN,
                            A.NOTIFIKASI,
                            (SELECT DISTINCT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE = B.ID_PENGAJUAN) PENGAJUAN,
                            (SELECT DISTINCT TIPE FROM MPE_PENGAJUAN WHERE ID_MPE = B.ID_PENGAJUAN) JENIS,
                        		B.OBJECTIVE,
                            A.NO_DOK_TEND,
                            D.NO_DOK_ENG,
                            A.TIPE,
                            A.ID_PACKET,
                            A.PACKET_TEXT,
                            A.NOMINAL,
                            A.COLOR,
                            A.NOTE,
                            A.STATUS,
                            A.COMPANY,
                            (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=C.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL = \'DOK_TEND\' AND DELETE_AT IS NULL AND TIPE = \'GM\' ) LIST_GM,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL = \'DOK_TEND\' AND DELETE_AT IS NULL AND TIPE = \'SM\' ) LIST_APP,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL = \'DOK_TEND\' AND DELETE_AT IS NULL AND TIPE = \'VERF\' ) LIST_PARAF,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL = \'DOK_TEND\' AND DELETE_AT IS NULL AND TIPE = \'CREATED\' ) LIST_CRT,
                          	(SELECT DISTINCT COUNT(*) FROM MPE_APPROVAL WHERE REF_ID = "A"."ID" AND REF_TABEL = \'DOK_TEND\' AND DELETE_AT IS NULL) JML_APP,
                          	(SELECT DISTINCT COUNT(*) FROM MPE_APPROVAL WHERE REF_ID = "A"."ID" AND REF_TABEL = \'DOK_TEND\' AND STATE=\'Approved\' AND DELETE_AT IS NULL) STT_APP,
                            A.DEPT_CODE,
                            A.DEPT_TEXT,
                            A.UK_CODE,
                            A.UK_TEXT,
                            A.APPROVE1_AT,
                            A.APPROVE1_BY,
                            A.APPROVE1_JAB,
                            A.APPROVE2_AT,
                            A.APPROVE2_BY,
                            A.APPROVE2_JAB,
                            A.APPROVE3_AT,
                            A.APPROVE3_BY,
                            A.APPROVE3_JAB,
                            A.APPROVE4_AT,
                            A.APPROVE4_BY,
                            A.APPROVE4_JAB,
                            A.APPROVE5_AT,
                            A.APPROVE5_BY,
                            A.APPROVE5_JAB,
                            A.APPROVE6_AT,
                            A.APPROVE6_BY,
                            A.APPROVE6_JAB,
                            A.APPROVE7_AT,
                            A.APPROVE7_BY,
                            A.APPROVE7_JAB,
                            A.APPROVE8_AT,
                            A.APPROVE8_BY,
                            A.APPROVE8_JAB,
                            A.CREATE_AT,
                            A.CREATE_BY,
                            A.UPDATE_AT,
                            A.UPDATE_BY,
                            A.DELETE_AT,
                            A.DELETE_BY,
                            A.RKS_FILE,
                            A.RKS_AT,
                            A.RKS_STATE,
                            A.BQ_FILE,
                            A.BQ_AT,
                            A.BQ_STATE,
                            A.DRAW_FILE,
                            A.DRAW_AT,
                            A.DRAW_STATE,
                            A.ECE_FILE,
                            A.ECE_AT,
                            A.ECE_STATE,
                            A.KAJIAN_FILE,
                            A.KAJIAN_AT,
                            A.KAJIAN_STATE,
                            (SELECT DISTINCT ID FROM MPE_BAST WHERE ID_DOK_ENG=A.ID AND STATUS=\'Closed\' AND DELETE_AT IS NULL) ID_BAST
                        ');
    		$this->db->from('MPE_DOK_TEND A');
    		$this->db->join("MPE_DOK_ENG D", "A.ID_DOK_ENG = D.ID");
    		$this->db->join("MPE_PENUGASAN B", "D.ID_PENUGASAN = B.ID AND B.STATUS!='Rejected'");
        $this->db->join("MPE_PENGAJUAN C", "B.ID_PENGAJUAN = C.ID_MPE AND C.STATUS='Approved'");
    		// $this->db->join("MSO_FUNCLOC C", "C.TPLNR = B.FUNCT_LOC", 'left');
    		if($key['search']!==''){
          $sAdd ="(
                  LOWER(A.NO_DOK_TEND) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(A.NOTIFIKASI) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(A.PACKET_TEXT) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(C.NO_PENGAJUAN) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(D.NO_DOK_ENG) LIKE '%".strtolower($key['search'])."%'
                  )";
          $this->db->where($sAdd, '', FALSE);
    			// $this->db->or_like('LOWER(A.NO_DOK_TEND)', strtolower($key['search']));
    			// $this->db->or_like('LOWER(A.PACKET_TEXT)', strtolower($key['search']));
    			// $this->db->or_like('LOWER(B.NO_PENUGASAN)', strtolower($key['search']));
    			// $this->db->or_like('LOWER(A.NOTIFIKASI)', strtolower($key['search']));
    			// $this->db->or_like('LOWER(A.NOTIFIKASI)', strtolower($key['search']));
    		}
        if (isset($key['dept_code'])) {
          // $this->db->where("(A.CREATE_BY = '{$key['name']}' OR A.CREATE_BY LIKE '{$key['username']}@%')");
          $this->db->where("A.DEPT_CODE = '{$key['dept_code']}'");
        }
        if (isset($key['uk_code'])) {
          $this->db->where("A.UK_CODE = '{$key['uk_code']}'");
        }
        if (isset($key['vGroup'])) {
          $this->db->or_where_in("A.UK_CODE", $key['vGroup']);
        }
    		$this->db->where('A.DELETE_AT IS NULL');
    		$this->db->where('B.DELETE_AT IS NULL');
    		$this->db->where('C.DELETE_AT IS NULL');
        // if (isset($key['vGroup'])) {
        //   $this->db->or_where_in("A.UK_CODE", $key['vGroup']);
        // }
    		$order = $this->column[$key['ordCol']];
    		$this->db->order_by($order, $key['ordDir']);
  	}

  	function get($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
  		$data			= $query->result();
  		return $data;
  	}

  	function get_data($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
      // echo "<pre>";
      // print_r($key);
      // echo $this->db->last_query();
  		$data			= $query->result();

  		return $data;
  	}

  	function recFil($key){
  		$this->_qry($key);
  		$query			= $this->db->get();
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

    function recTot($key){
      $key['search']='';
      $this->_qry($key);
  		$query			= $this->db->get();
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

  	function get_record($where){
    		$this->db->select('
                            A."ID",
                            A.ID_DOK_ENG,
                            C.NO_PENGAJUAN,
                            B.NO_PENUGASAN,
                            A.NOTIFIKASI,
                            (SELECT DISTINCT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE = B.ID_PENGAJUAN) PENGAJUAN,
                        		B.OBJECTIVE,
                            A.NO_DOK_TEND,
                            A.TIPE,
                            A.ID_PACKET,
                            A.PACKET_TEXT,
                            A.COLOR,
                            A.NOTE,
                            A.STATUS,
                            A.COMPANY,
                            (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=C.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                            A.DEPT_CODE,
                            A.DEPT_TEXT,
                            A.UK_CODE,
                            A.UK_TEXT,
                            A.APPROVE1_AT,
                            A.APPROVE1_BY,
                            A.APPROVE1_JAB,
                            A.APPROVE2_AT,
                            A.APPROVE2_BY,
                            A.APPROVE2_JAB,
                            A.APPROVE3_AT,
                            A.APPROVE3_BY,
                            A.APPROVE3_JAB,
                            A.APPROVE4_AT,
                            A.APPROVE4_BY,
                            A.APPROVE4_JAB,
                            A.APPROVE5_AT,
                            A.APPROVE5_BY,
                            A.APPROVE5_JAB,
                            A.APPROVE6_AT,
                            A.APPROVE6_BY,
                            A.APPROVE6_JAB,
                            A.APPROVE7_AT,
                            A.APPROVE7_BY,
                            A.APPROVE7_JAB,
                            A.APPROVE8_AT,
                            A.APPROVE8_BY,
                            A.APPROVE8_JAB,
                            A.UK_CODE,
                            A.UK_TEXT,
                            A.DEPT_CODE,
                            A.DEPT_TEXT,
                            A.CREATE_AT,
                            A.CREATE_BY,
                            A.UPDATE_AT,
                            A.UPDATE_BY,
                            A.DELETE_AT,
                            A.DELETE_BY,
                            A.RKS_FILE,
                            A.RKS_AT,
                            A.RKS_STATE,
                            A.BQ_FILE,
                            A.BQ_AT,
                            A.BQ_STATE,
                            A.DRAW_FILE,
                            A.DRAW_AT,
                            A.DRAW_STATE,
                            A.ECE_FILE,
                            A.ECE_AT,
                            A.ECE_STATE,
                            A.KAJIAN_FILE,
                            A.KAJIAN_AT,
                            A.KAJIAN_STATE,
                            A.NOTE1,
                            A.NOTE2,
                            A.NOTE3,
                            A.REJECT_DATE,
                            A.REJECT_BY,
                            A.REJECT_REASON
                        ');
    		$this->db->from('MPE_DOK_TEND A');
    		$this->db->join("MPE_DOK_ENG D", "A.ID_DOK_ENG = D.ID");
    		$this->db->join("MPE_PENUGASAN B", "D.ID_PENUGASAN = B.ID AND B.STATUS!='Rejected'");
        $this->db->join("MPE_PENGAJUAN C", "B.ID_PENGAJUAN = C.ID_MPE AND C.STATUS='Approved'");
    		$this->db->where('A.DELETE_AT IS NULL AND C.DELETE_AT IS NULL');
    		$this->db->where($where);

        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
  	}

    public function get_foreign(){
        $this->db->select('
                            DE.ID,
                            DE.ID_PENUGASAN,
                            DE.NOTIFIKASI,
                            DE.TIPE,
                            DE.NO_DOK_ENG,
                            DE.ID_PACKET,
                            DE.PACKET_TEXT,
                            DE.COLOR,
                            DE.NOTE,
                            DE.STATUS,
                            DE.RKS_FILE,
                            DE.BQ_FILE,
                            DE.DRAW_FILE,
                            DE.ECE_FILE,
                            PE.COMPANY,
                            PE.DEPT_CODE,
                            PE.DEPT_TEXT,
                            PE.UK_CODE,
                            PE.UK_TEXT
                          ');
        $this->db->from("MPE_DOK_ENG DE");
        $this->db->join("MPE_PENUGASAN PN", "PN.ID=DE.ID_PENUGASAN");
        $this->db->join("MPE_PENGAJUAN PE", "PE.ID_MPE=PN.ID_PENGAJUAN AND PE.TIPE='Detail Design'");
        $this->db->where("
                            PN.DELETE_AT IS NULL
                            AND PN.STATUS != 'Rejected'
                            AND PE.STATUS = 'Approved'
                            AND DE.STATUS = 'GM Approved'
                            AND DE.DELETE_AT IS NULL
                          ");
        $this->db->where("
                            DE.ID NOT IN (SELECT ID_DOK_ENG FROM MPE_DOK_TEND WHERE DELETE_AT IS NULL)
                          ");
        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    public function get_dtl_foreign($f_id = null){
        $this->db->select('
                        		DPN."ID",
                          	DPN."ID_PENUGASAN",
                          	DPN."DESKRIPSI",
                          	DPN."REMARKS",
                          	DPN."PROGRESS",
                          	DPN."START_DATE",
                          	DPN."STATUS",
                          	DPN."NOTE",
                          	DPN."COMPANY",
                          	DPN."CREATE_AT",
                          	DPN."CREATE_BY",
                          	DPN."UPDATE_AT",
                          	DPN."UPDATE_BY",
                          	DPN."DELETE_AT",
                          	DPN."DELETE_BY",
                            A.TIPE
                          ');
    		$this->db->from("MPE_DTL_PENUGASAN DPN");
        $this->db->join("MPE_PENUGASAN B", "B.ID = DPN.ID_PENUGASAN", "left");
      	$this->db->join("MPE_PENGAJUAN A", "A.ID_MPE = B.ID_PENGAJUAN", "left");
        $this->db->where("
                          DPN.DELETE_AT IS NULL
                          AND B.STATUS != 'Rejected'
                          AND DPN.ID_PENUGASAN = '{$f_id}'
                          AND DPN.DESKRIPSI NOT IN (SELECT PACKET_TEXT FROM MPE_DOK_ENG WHERE ID_PENUGASAN = '{$f_id}' AND DELETE_AT IS NULL AND STATUS != 'Rejected')
                          AND A.STATE = 'Active'
                        	AND (LOWER( DPN.REMARKS ) NOT LIKE '%cancel%' ESCAPE '!' OR DPN.REMARKS IS NULL)
                        ");
        // $this->db->not_like('LOWER(DPN.REMARKS)', "cancel", 'after');
        $this->db->order_by('DPN.DESKRIPSI', 'ASC');
        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    function save($data) {
        $query = $this->db->insert('MPE_DOK_TEND', $data);
        $result = false;
        if($query)
        {
          $this->db->select_max('ID','REF_ID');
          $result = $this->db->get('MPE_DOK_TEND')->row_array();
        }
        return $result;
        // return $this->db->affected_rows();
    }

    function updateData($tipe, $param=array()){
        if($tipe=='delete'){
          $id = $param['ID'];
          unset($param['ID']);
          $this->db->set('DELETE_AT', "CURRENT_DATE", false);
          $this->db->set('STATUS', "Deleted");
          $this->db->where('ID', $id);
          $query = $this->db->update('MPE_DOK_TEND', $param);

          return (bool) $query;
        }else  if($tipe=='approve'){
          $id = $param['ID'];
          unset($param['ID']);
          $this->db->where('ID', $id);
          $query = $this->db->update('MPE_DOK_TEND', $param);
          return (bool) $query;
        }else  if($tipe=='reject'){
          $id = $param['ID'];
          unset($param['ID']);
          $this->db->where('ID', $id);
          $query = $this->db->update('MPE_DOK_TEND', $param);
          return (bool) $query;
        }else {
          $id = $param['ID'];
          $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
          $this->db->where('ID', $id);
          unset($param['ID']);

          $query = $this->db->update('MPE_DOK_TEND', $param);
          // echo $this->db->last_query();

          return (bool) $query;
        }
    }

    function getMgr($id){
      $this->db->select('GEN_CODE, GEN_DESC,
                         GEN_VAL, GEN_PAR1,
                         GEN_PAR2, GEN_PAR3,
                         GEN_PAR4');
      $this->db->from('MPE_GENERAL');
      $this->db->where('GEN_CODE' , $id);
      $query = $this->db->get();
      return $query->result_array();
    }

    function getIDDOK($id){
      $this->db->select('
                        	MPE_DOK_ENG.ID,
                        	MPE_DOK_ENG.APPROVE1_AT,
                        	MPE_DOK_ENG.APPROVE1_BY,
                        	MPE_DOK_ENG.APPROVE1_JAB,
                        	MPE_DOK_ENG.APPROVE2_AT,
                        	MPE_DOK_ENG.APPROVE2_BY,
                        	MPE_DOK_ENG.APPROVE2_JAB,
                        	MPE_DOK_ENG.APPROVE3_AT,
                        	MPE_DOK_ENG.APPROVE3_BY,
                        	MPE_DOK_ENG.APPROVE3_JAB,
                        	MPE_DOK_ENG.REJECT_DATE,
                        	MPE_DOK_ENG.REJECT_BY,
                        	MPE_DOK_ENG.REJECT_REASON,
                        	MPE_DOK_ENG.CREATE_AT,
                        	MPE_DOK_ENG.CREATE_BY,
                        	MPE_DOK_ENG.PACKET_TEXT,
                        	NVL(MPE_DOK_ENG.NOTE1,'-') NOTE1,
                        	NVL(MPE_DOK_ENG.NOTE2,'-') NOTE2,
                        	NVL(MPE_DOK_ENG.NOTE3,'-') NOTE3,
                        	MPE_BAST.CREATE_AT AS BASTAT,
                        	MPE_BAST.CREATE_BY AS BASTBY,
                        	MPE_BAST.APPROVE1_AT AS BAST1AT,
                        	MPE_BAST.APPROVE1_BY AS BAST1BY,
                        	NVL(MPE_BAST.NOTE1,'-') AS BASTNOTE1,
                        	MPE_BAST.APPROVE1_0_AT,
                        	MPE_BAST.APPROVE1_0_BY,
                        	NVL(MPE_BAST.NOTE1_0,'-') NOTE1_0,
                        	MPE_BAST.APPROVE2_AT AS bast2AT,
                        	MPE_BAST.APPROVE2_BY AS bast2BY,
                        	NVL(MPE_BAST.NOTE2,'-') AS BASTNOTE2,
                        	MPE_BAST.APPROVE2_0_AT,
                        	MPE_BAST.APPROVE2_0_BY,
                        	NVL(MPE_BAST.NOTE2_0,'-') NOTE2_0,
                        	MPE_BAST.NO_BAST,
                        	MPE_BAST.REJECT_BY AS BASTREJECTAT,
                        	MPE_BAST.REJECT_AT AS BASTREJECTBY,
                        	NVL(MPE_BAST.REJECT_NOTE,'-') AS BASTREJECTNOTE'
                        );
      $this->db->from('MPE_DOK_ENG');
      $this->db->join('MPE_PENUGASAN', 'MPE_PENUGASAN.ID = MPE_DOK_ENG.ID_PENUGASAN', 'left');
      $this->db->join('MPE_PENGAJUAN', 'MPE_PENGAJUAN.ID_MPE = MPE_PENUGASAN.ID_PENGAJUAN', 'left');
      $this->db->join('MPE_BAST', 'MPE_BAST.ID_DOK_ENG = MPE_DOK_ENG.ID', 'left');
      $this->db->where("(MPE_PENGAJUAN.ID_MPE = '{$id}' OR MPE_PENGAJUAN.NOTIFIKASI LIKE '{$id}')");
      $this->db->where('(MPE_PENGAJUAN.STATE = \'Active\' OR MPE_PENGAJUAN.STATE IS NULL)');
      $this->db->where('MPE_PENGAJUAN.STATUS != \'Rejected\'');
      $this->db->where('MPE_DOK_ENG.DELETE_AT IS NULL');
      $this->db->where('MPE_PENUGASAN.DELETE_AT IS NULL');
      $this->db->where('MPE_PENGAJUAN.DELETE_AT IS NULL');
      $this->db->where('MPE_BAST.DELETE_AT IS NULL');
      $query = $this->db->get();
      // echo $this->db->last_query();
      return $query->result_array();
    }

    function getCount(){
        $this->db->select("NVL(MAX(CAST(SUBSTR(NO_DOK_TEND,15,3) AS INTEGER))+1,1) AS URUT", FALSE);
        $this->db->from('MPE_DOK_TEND');
        $this->db->where("SUBSTR(NO_DOK_TEND,-4)=TO_CHAR(CURRENT_DATE, 'YYYY') AND SUBSTR(NO_DOK_TEND, 12, 2)='TD'");
        $urut = $this->db->get()->row();

        return $urut->URUT;
    }

}
