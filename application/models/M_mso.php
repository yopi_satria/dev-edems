<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_mso extends CI_Model
{   
    private $conn = NULL;

    function __construct(){
        parent::__construct();

    	$this->conn = $this->load->database('mso', TRUE);
        
    }

    public function get_funcloc(){

        // $this->conn->like('FUNCLOC', '%SG%');
        // $this->conn->where('FUNCLOC', 'IS NOT NULL');


        // $query = $this->conn->get('MSO_MEQUPMENT');
        $sql = "SELECT * FROM MSO_MEQUPMENT WHERE FUNCLOC IS NOT NULL AND FUNCLOC LIKE '%SG%'";

        $query = $this->conn->query($sql);

        // print_r($query->result_array());
                
        return $query->result_array();

    }


}