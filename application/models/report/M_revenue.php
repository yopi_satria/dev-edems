<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_revenue extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

  	var $column = array(
  		'UK_TEXT', 'NOTIFIKASI', 'NO_PENGAJUAN', 'NAMA_PEKERJAAN', 'CONSTRUCT_COST', 'ENG_COST', 'DE_CONSTRUCT_COST', 'DE_ENG_COST'
  	);

  	function _qry($key){
    		$this->db->select('
                            R.ID_MPE,
                            R.NO_PENGAJUAN,
                            R.NOTIFIKASI,
                            /*TO_CHAR( R.CREATE_AT, \'DD-MM-YYYY\' )*/R.CREATE_AT R_DATE,
                            R.WBS_CODE,
                            R.UK_TEXT,
                            R.NAMA_PEKERJAAN,
                            /*P.DESKRIPSI NAMA_PEKERJAAN,*/
                            R.STATUS R_STATUS,
                            (
                            CASE
                            WHEN R.APPROVE_AT IS NOT NULL THEN /*TO_CHAR( R.APPROVE_AT, \'DD-MM-YYYY\' )*/R.APPROVE_AT|| \' (\' || R.APPROVE_BY || \')\'
                            END ) R_APPROVE,
                            NVL(R.CONSTRUCT_COST, 0) CONSTRUCT_COST,
                            NVL(R.ENG_COST, 0) ENG_COST,
                            A.NO_PENUGASAN,
                            A.LEAD_ASS,
                            A.CREATE_AT A_DATE,
                            (
                            CASE
                            WHEN A.APPROVE1_AT IS NOT NULL THEN /*TO_CHAR( A.APPROVE1_AT, \'DD-MM-YYYY\' )*/A.APPROVE1_AT|| \' (\' || A.APPROVE1_BY || \')\'
                            END ) A_APPROVE_SM,
                            (
                            CASE
                            WHEN A.APPROVE2_AT IS NOT NULL THEN /*TO_CHAR( A.APPROVE2_AT, \'DD-MM-YYYY\' )*/A.APPROVE2_AT|| \' (\' || A.APPROVE2_BY || \')\'
                            END ) A_APPROVE_GM,
                            NVL(A.DE_CONSTRUCT_COST, 0) DE_CONSTRUCT_COST,
                            NVL(A.DE_ENG_COST, 0) DE_ENG_COST
                        ', FALSE);
    		$this->db->from('MPE_PENGAJUAN R');
    		$this->db->join("MPE_PENUGASAN A", "A.ID_PENGAJUAN = R.ID_MPE",'left');
    		// $this->db->join("MPE_DTL_PENUGASAN P", "P.ID_PENUGASAN = A.ID",'left');
    		// $this->db->join("MPE_DOK_ENG D", "D.ID_PENUGASAN = A.ID", 'left');
    		// $this->db->join("MSO_FUNCLOC C", "C.TPLNR = A.FUNCT_LOC", 'left');
    		if($key['search']!==''){
          $sAdd ="(
                  LOWER(R.NO_PENGAJUAN) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(R.NOTIFIKASI) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(R.NAMA_PEKERJAAN) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(R.UK_TEXT) LIKE '%".strtolower($key['search'])."%'
                  )";
          $this->db->where($sAdd, '', FALSE);
    		}
        // set range date
        if (isset($key['start_date'])) {
          $this->db->where("R.CREATE_AT >= TO_DATE('{$key['start_date']}','yyyy/mm/dd')");
        }
        if (isset($key['end_date'])) {
          $this->db->where("R.CREATE_AT <= TO_DATE('{$key['end_date']}','yyyy/mm/dd')");
        }
        if (isset($key['dept_code'])) {
          // $this->db->where("(A.CREATE_BY = '{$key['name']}' OR A.CREATE_BY LIKE '{$key['username']}@%')");
          $this->db->where("A.DEPT_CODE = '{$key['dept_code']}'");
        }
        if (isset($key['uk_code'])) {
            $this->db->where("A.UK_CODE = '{$key['uk_code']}'");
        }
        $this->db->where('(R.STATE = \'Active\' OR R.STATE IS NULL)');
    		$this->db->where('R.ERF_DELETE_AT IS NULL AND R.NO_PENGAJUAN IS NOT NULL AND A.DELETE_AT IS NULL');
    		$order = $this->column[$key['ordCol']];
        $this->db->order_by($order, $key['ordDir']);
        $this->db->distinct();
  	}

  	function get($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
      $data			= $query->result();
  		return $data;
  	}

  	function get_data($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
      // echo $this->db->last_query();
  		$data			= $query->result();

  		return $data;
  	}

  	function recFil($key){
  		$this->_qry($key);
  		$query			= $this->db->get();
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

  	function recTot($key){
      $key['search']='';
      $this->_qry($key);
  		$query			= $this->db->get();
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

  	function get_record(){
    		$this->db->select('
                            R.ID_MPE,
                            R.NO_PENGAJUAN,
                            R.NOTIFIKASI,
                            /*TO_CHAR( R.CREATE_AT, \'DD-MM-YYYY\' )*/R.CREATE_AT R_DATE,
                            R.WBS_CODE,
                            R.UK_TEXT,
                            R.NAMA_PEKERJAAN,
                            R.STATUS R_STATUS,
                            (
                            CASE
                            WHEN R.APPROVE_AT IS NOT NULL THEN /*TO_CHAR( R.APPROVE_AT, \'DD-MM-YYYY\' )*/R.APPROVE_AT|| \' (\' || R.APPROVE_BY || \')\'
                            END ) R_APPROVE,
                            A.NO_PENUGASAN,
                            A.LEAD_ASS,
                            A.CREATE_AT A_DATE,
                            (
                            CASE
                            WHEN A.APPROVE1_AT IS NOT NULL THEN /*TO_CHAR( A.APPROVE1_AT, \'DD-MM-YYYY\' )*/A.APPROVE1_AT|| \' (\' || A.APPROVE1_BY || \')\'
                            END ) A_APPROVE_SM,
                            (
                            CASE
                            WHEN A.APPROVE2_AT IS NOT NULL THEN /*TO_CHAR( A.APPROVE2_AT, \'DD-MM-YYYY\' )*/A.APPROVE2_AT|| \' (\' || A.APPROVE2_BY || \')\'
                            END ) A_APPROVE_GM,
                            (NVL(P.PROGRESS,0) || (CASE P.PROGRESS WHEN NULL THEN \'\' ELSE \' %\' END)) D_PROGRESS,
                            /*(
                            CASE
                            D.ID_PENUGASAN
                            WHEN A.ID THEN \'Detail Design\'
                            END ) D_STATUS,
                          	( CASE
                          		A.ID
                          		WHEN D.ID_PENUGASAN THEN (
                          		SELECT
                          			( SUM(PROGRESS)/ COUNT(1))|| \'%\'
                          		FROM
                          			MPE_DTL_PENUGASAN
                          		WHERE
                          			ID_PENUGASAN = P.ID_PENUGASAN AND DELETE_AT IS NULL )
                          	END ) D_PROGRESS,*/
                            P.REMARKS D_REMARKS
                          	/*(
                          	SELECT
                          		listagg( \'P\'|| rownum || \'. \' || PACKET_TEXT || \' (\' || PROGRESS || \'%\' || \')\'||\'<br />\',
                          		CHR( 13 )) WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_DOK_ENG
                          	WHERE
                          		ID_PENUGASAN = D.ID_PENUGASAN AND DELETE_AT IS NULL ) D_REMARKS */
                        ', FALSE);
    		$this->db->from('MPE_PENGAJUAN R');
    		$this->db->join("MPE_PENUGASAN A", "A.ID_PENGAJUAN = R.ID_MPE",'left');
        $this->db->join("MPE_DTL_PENUGASAN P", "P.ID_PENUGASAN = A.ID",'left');
    		$this->db->join("MPE_DOK_ENG D", "D.ID_PENUGASAN = A.ID", 'left');
    		$this->db->where('R.DELETE_AT IS NULL AND A.DELETE_AT IS NULL AND D.DELETE_AT IS NULL');
        $this->db->distinct();

        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
  	}

  	function get_count(){
        $query = $this->db->query("
                                  SELECT
                                  	(
                                  	SELECT
                                  		COUNT(*)
                                  	FROM
                                  		MPE_PENGAJUAN
                                  	WHERE
                                  		DELETE_AT IS NULL AND STATE = 'Active' ) J_ERF,
                                  	(
                                  	SELECT
                                  		COUNT(*)
                                  	FROM
                                  		MPE_PENUGASAN
                                  	WHERE
                                  		DELETE_AT IS NULL ) J_EAT,
                                    (
                                  	SELECT
                                  		COUNT(*)
                                  	FROM
                                  		MPE_DTL_PENUGASAN  A, MPE_PENUGASAN B
                                  	WHERE
                                      to_char(B.CREATE_AT,'YYYY') = '".date("Y")."' AND
                                      A.ID_PENUGASAN = B.ID AND B.REJECT_DATE IS NULL AND
                                      A.DELETE_AT IS NULL AND B.DELETE_AT IS NULL ) J_PROG,
                                  	(
                                  	SELECT
                                  		COUNT(*)
                                  	FROM
                                  		MPE_DOK_ENG
                                  	WHERE
                                  		DELETE_AT IS NULL ) J_DOK,
                                    (
                                  	SELECT
                                  		COUNT(*)
                                  	FROM
                                  		MPE_BAST
                                  	WHERE
                                  		DELETE_AT IS NULL ) J_BAST
                                  FROM
                                  	dual
                                    ");
        // echo $this->db->last_query();

        return $query->result_array();
  	}

}
