<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Name:  FPDF
*
* Author: Jd Fiscus
* 	 	  jdfiscus@gmail.com
*         @iamfiscus
*
*
* Origin API Class: http://www.fpdf.org/
*
* Location: http://github.com/iamfiscus/Codeigniter-FPDF/
*
* Created:  06.22.2010
*
* Description:  This is a Codeigniter library which allows you to generate a PDF with the FPDF library
*
*/

class pdfWatermark_gen {

	public function __construct() {

		require_once APPPATH.'libraries/pdf-watermarker-master/pdfwatermark.php';
		require_once APPPATH.'libraries/pdf-watermarker-master/pdfwatermarker.php';

	}

}
