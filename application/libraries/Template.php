<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Template {

	protected $CI;

	protected $column;

	/**
	 * Constructor
	 *
	 * @param   array   $config
	 */
	public function __construct($config = array())
	{
		 // parent::__construct();
		$this->CI = &get_instance();
	}


	public function view_app($param){
		// $user_hris = $this->CI->dbhris->get_user_hris($param['header']['NO_BADGE']);
		// $user_hris = (array) $user_hris[0];
		// $kebutuhan = str_replace('Permintaan ', '', $param['detail'][0]['KETERANGAN']);
		$strn = "";
		foreach ($param['TRN'] as $key => $value) {
			$strn .="<li>$value</li>";
		}
		$html = "
              <!DOCTYPE html>
              <html>

              <head>
                <meta name='viewport' content='width=device-width'>
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
                <title>Approval Info</title>
                <style type='text/css'>
                  /* -------------------------------------
                              INLINED WITH https://putsmail.com/inliner
                          ------------------------------------- */

                  /* -------------------------------------
                              RESPONSIVE AND MOBILE FRIENDLY STYLES
                          ------------------------------------- */

                  @media only screen and (max-width: 620px) {
                    table[class=body] h1 {
                      font-size: 28px !important;
                      margin-bottom: 10px !important;
                    }
                    table[class=body] p,
                    table[class=body] ul,
                    table[class=body] ol,
                    table[class=body] td,
                    table[class=body] span,
                    table[class=body] a {
                      font-size: 16px !important;
                    }
                    table[class=body] .wrapper,
                    table[class=body] .article {
                      padding: 10px !important;
                    }
                    table[class=body] .content {
                      padding: 0 !important;
                    }
                    table[class=body] .container {
                      padding: 0 !important;
                      width: 100% !important;
                    }
                    table[class=body] .main {
                      border-left-width: 0 !important;
                      border-radius: 0 !important;
                      border-right-width: 0 !important;
                    }
                    table[class=body] .btn table {
                      width: 100% !important;
                    }
                    table[class=body] .btn a {
                      width: 100% !important;
                    }
                    table[class=body] .img-responsive {
                      height: auto !important;
                      max-width: 100% !important;
                      width: auto !important;
                    }
                  }

                  /* -------------------------------------
                              PRESERVE THESE STYLES IN THE HEAD
                          ------------------------------------- */

                  @media all {
                    .ExternalClass {
                      width: 100%;
                    }
                    .ExternalClass,
                    .ExternalClass p,
                    .ExternalClass span,
                    .ExternalClass font,
                    .ExternalClass td,
                    .ExternalClass div {
                      line-height: 100%;
                    }
                    .apple-link a {
                      color: inherit !important;
                      font-family: inherit !important;
                      font-size: inherit !important;
                      font-weight: inherit !important;
                      line-height: inherit !important;
                      text-decoration: none !important;
                    }
                    .btn-primary table td:hover {
                      background-color: #34495e !important;
                    }
                    .btn-primary a:hover {
                      background-color: #34495e !important;
                      border-color: #34495e !important;
                    }
                  }
                </style>
              </head>

              <body class='' style='background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;'>
                <table border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;'>
                  <tr>
                    <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
                    <td class='container' style='font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;'>
                      <div class='content' style='box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;'>
                        <!-- START CENTERED WHITE CONTAINER -->
                        <span class='preheader' style='color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;'>This is preheader text. Some clients will show this text as a preview.</span>
                        <table border='0' cellpadding='0' cellspacing='0' class='body' style='background-color:#f0f0f0; text-align:left; border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                          <tr>
                            <th width='20%' align='center'><img src='".base_url()."media/logo/Logo_SI.png' style='width:50%'></th>
                            <td width='60%' align='center'>
                              <h4>
                                            Approval Info
                                            <br />
                                            e-DEMS
                                          </h4>
                            </td>
                            <th width='20%' align='center'><img src='".base_url()."media/CoE.png' style='width:50%'></th>
                          </tr>
                        </table>
                        <table class='main' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;0px 0px 0px 5px;width:100%;'>
                          <!-- START MAIN CONTENT AREA -->
                          <tr>
                            <td class='wrapper' style='font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;'>
                              <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                                <tr>
                                  <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>
                                    <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
                                      <center>
                                        <h4><u>{$param['title']}</u></h4></center>
                                    </p>

                                    <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Informasi persetujuan aplikasi Electronic Design Engineering Management System (e-DEMS) untuk <u>{$param['short']}</u>, telah diapprove oleh:</p>

                                    <ul>
                                      <li><b>Tanggal Persetujuan :</b> {$param['APP']['DATE']} </li>
                                      <li><b>No. Badge :</b> {$param['APP']['BADGE']} </li>
                                      <li><b>Nama :</b> {$param['APP']['NAMA']} </li>
                                      <li><b>Unit Kerja :</b> {$param['APP']['UK_TEXT']} </li>
                                    </ul>
                                    <!-- <br /> -->

                                    <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Detail dokumen {$param['title']} sebagai berikut, </p>

                                    <ul>
                                      {$strn}
                                    </ul>


                                    <br />

                                    <!-- <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Surat permintaan ini sebagai informasi untuk melakukan persetujuan, klik 'Approve' untuk setuju.</p> -->
                                    <!-- <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Hormat kami.</p> -->
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <!-- END MAIN CONTENT AREA -->
                        </table>
                        <!-- START FOOTER -->
                        <div class='footer' style='clear:both;padding-top:10px;text-align:center;width:100%;'>
                          <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                            <tr>
                              <td class='content-block' style='font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;'>
                                <span class='apple-link' style='color:#999999;font-size:12px;text-align:center;'>
                                            e-DEMS, <a style='color:#999999;font-size:12px;' href='http://e-dems.semenindonesia.com'>Electronic Design Engineering Management System</a>
                                            <br>
                                            Copyright by <a style='color:#999999;font-size:12px;' href='http://sisi.id/'>PT.SISI</a>
                                          </span>
                                </span>
                              </td>
                            </tr>
                          </table>
                        </div>
                        <!-- END FOOTER -->
                        <!-- END CENTERED WHITE CONTAINER -->
                      </div>
                    </td>
                    <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
                  </tr>
                </table>
              </body>

              </html>




		";

		return $html;

	}

	public function view_docs($param){
		// $user_hris = $this->CI->dbhris->get_user_hris($param['header']['NO_BADGE']);
		// $user_hris = (array) $user_hris[0];
		// $kebutuhan = str_replace('Permintaan ', '', $param['detail'][0]['KETERANGAN']);
		$shdr = "";
		foreach ($param['HDR'] as $key => $value) {
			$shdr .="<li>$value</li>";
		}
		$strn = "";
		foreach ($param['TRN'] as $key => $value) {
			$strn .="<li>$value</li>";
		}
		$html = "
              <!DOCTYPE html>
              <html>

              <head>
                <meta name='viewport' content='width=device-width'>
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
                <title>Approval Info</title>
                <style type='text/css'>
                  /* -------------------------------------
                              INLINED WITH https://putsmail.com/inliner
                          ------------------------------------- */

                  /* -------------------------------------
                              RESPONSIVE AND MOBILE FRIENDLY STYLES
                          ------------------------------------- */

                  @media only screen and (max-width: 620px) {
                    table[class=body] h1 {
                      font-size: 28px !important;
                      margin-bottom: 10px !important;
                    }
                    table[class=body] p,
                    table[class=body] ul,
                    table[class=body] ol,
                    table[class=body] td,
                    table[class=body] span,
                    table[class=body] a {
                      font-size: 16px !important;
                    }
                    table[class=body] .wrapper,
                    table[class=body] .article {
                      padding: 10px !important;
                    }
                    table[class=body] .content {
                      padding: 0 !important;
                    }
                    table[class=body] .container {
                      padding: 0 !important;
                      width: 100% !important;
                    }
                    table[class=body] .main {
                      border-left-width: 0 !important;
                      border-radius: 0 !important;
                      border-right-width: 0 !important;
                    }
                    table[class=body] .btn table {
                      width: 100% !important;
                    }
                    table[class=body] .btn a {
                      width: 100% !important;
                    }
                    table[class=body] .img-responsive {
                      height: auto !important;
                      max-width: 100% !important;
                      width: auto !important;
                    }
                  }

                  /* -------------------------------------
                              PRESERVE THESE STYLES IN THE HEAD
                          ------------------------------------- */

                  @media all {
                    .ExternalClass {
                      width: 100%;
                    }
                    .ExternalClass,
                    .ExternalClass p,
                    .ExternalClass span,
                    .ExternalClass font,
                    .ExternalClass td,
                    .ExternalClass div {
                      line-height: 100%;
                    }
                    .apple-link a {
                      color: inherit !important;
                      font-family: inherit !important;
                      font-size: inherit !important;
                      font-weight: inherit !important;
                      line-height: inherit !important;
                      text-decoration: none !important;
                    }
                    .btn-primary table td:hover {
                      background-color: #34495e !important;
                    }
                    .btn-primary a:hover {
                      background-color: #34495e !important;
                      border-color: #34495e !important;
                    }
                  }
                </style>
              </head>

              <body class='' style='background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;'>
                <table border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;'>
                  <tr>
                    <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
                    <td class='container' style='font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;'>
                      <div class='content' style='box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;'>
                        <!-- START CENTERED WHITE CONTAINER -->
                        <span class='preheader' style='color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;'>This is preheader text. Some clients will show this text as a preview.</span>
                        <table border='0' cellpadding='0' cellspacing='0' class='body' style='background-color:#f0f0f0; text-align:left; border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                          <tr>
                            <th width='20%' align='center'><img src='".base_url()."media/logo/Logo_SI.png' style='width:50%'></th>
                            <td width='60%' align='center'>
                              <h4>
                                            Document Info
                                            <br />
                                            e-DEMS
                                          </h4>
                            </td>
                            <th width='20%' align='center'><img src='".base_url()."media/CoE.png' style='width:50%'></th>
                          </tr>
                        </table>
                        <table class='main' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;0px 0px 0px 5px;width:100%;'>
                          <!-- START MAIN CONTENT AREA -->
                          <tr>
                            <td class='wrapper' style='font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;'>
                              <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                                <tr>
                                  <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>
                                    <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
                                      <center>
                                        <h4><u>{$param['title']}</u></h4></center>
                                    </p>

                                    <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Informasi detail dokumen aplikasi Electronic Design Engineering Management System (e-DEMS) untuk <u>{$param['title']}</u>, diterbitkan oleh:</p>

                                    <ul>
                                      {$shdr}
                                    </ul>
                                    <!-- <br /> -->

                                    <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Detail dokumen {$param['title']} sebagai berikut, </p>

                                    <ul>
                                      {$strn}
                                    </ul>


                                    <br />

                                    <!-- <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Surat permintaan ini sebagai informasi untuk melakukan persetujuan, klik 'Approve' untuk setuju.</p> -->
                                    <!-- <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Hormat kami.</p> -->
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <!-- END MAIN CONTENT AREA -->
                        </table>
                        <!-- START FOOTER -->
                        <div class='footer' style='clear:both;padding-top:10px;text-align:center;width:100%;'>
                          <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                            <tr>
                              <td class='content-block' style='font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;'>
                                <span class='apple-link' style='color:#999999;font-size:12px;text-align:center;'>
                                            e-DEMS, <a style='color:#999999;font-size:12px;' href='http://e-dems.semenindonesia.com'>Electronic Design Engineering Management System</a>
                                            <br>
                                            Copyright by <a style='color:#999999;font-size:12px;' href='http://sisi.id/'>PT.SISI</a>
                                          </span>
                                </span>
                              </td>
                            </tr>
                          </table>
                        </div>
                        <!-- END FOOTER -->
                        <!-- END CENTERED WHITE CONTAINER -->
                      </div>
                    </td>
                    <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
                  </tr>
                </table>
              </body>

              </html>




		";

		return $html;

	}

	public function set_app($param){
		// $user_hris = $this->CI->dbhris->get_user_hris($param['header']['NO_BADGE']);
		// $user_hris = (array) $user_hris[0];
		// $kebutuhan = str_replace('Permintaan ', '', $param['detail'][0]['KETERANGAN']);
		$strn = "";
		foreach ($param['TRN'] as $key => $value) {
			$strn .="<li>$value</li>";
		}
		$html = "
							<!DOCTYPE html>
							<html>

							<head>
							  <meta name='viewport' content='width=device-width'>
							  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
							  <title>Simple Transactional Email</title>
							  <style type='text/css'>
							    /* -------------------------------------
							                INLINED WITH https://putsmail.com/inliner
							            ------------------------------------- */

							    /* -------------------------------------
							                RESPONSIVE AND MOBILE FRIENDLY STYLES
							            ------------------------------------- */

							    @media only screen and (max-width: 620px) {
							      table[class=body] h1 {
							        font-size: 28px !important;
							        margin-bottom: 10px !important;
							      }
							      table[class=body] p,
							      table[class=body] ul,
							      table[class=body] ol,
							      table[class=body] td,
							      table[class=body] span,
							      table[class=body] a {
							        font-size: 16px !important;
							      }
							      table[class=body] .wrapper,
							      table[class=body] .article {
							        padding: 10px !important;
							      }
							      table[class=body] .content {
							        padding: 0 !important;
							      }
							      table[class=body] .container {
							        padding: 0 !important;
							        width: 100% !important;
							      }
							      table[class=body] .main {
							        border-left-width: 0 !important;
							        border-radius: 0 !important;
							        border-right-width: 0 !important;
							      }
							      table[class=body] .btn table {
							        width: 100% !important;
							      }
							      table[class=body] .btn a {
							        width: 100% !important;
							      }
							      table[class=body] .img-responsive {
							        height: auto !important;
							        max-width: 100% !important;
							        width: auto !important;
							      }
							    }

							    /* -------------------------------------
							                PRESERVE THESE STYLES IN THE HEAD
							            ------------------------------------- */

							    @media all {
							      .ExternalClass {
							        width: 100%;
							      }
							      .ExternalClass,
							      .ExternalClass p,
							      .ExternalClass span,
							      .ExternalClass font,
							      .ExternalClass td,
							      .ExternalClass div {
							        line-height: 100%;
							      }
							      .apple-link a {
							        color: inherit !important;
							        font-family: inherit !important;
							        font-size: inherit !important;
							        font-weight: inherit !important;
							        line-height: inherit !important;
							        text-decoration: none !important;
							      }
							      .btn-primary table td:hover {
							        background-color: #34495e !important;
							      }
							      .btn-primary a:hover {
							        background-color: #34495e !important;
							        border-color: #34495e !important;
							      }
							    }
							  </style>
							</head>

							<body class='' style='background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;'>
							  <table border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;'>
							    <tr>
							      <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
							      <td class='container' style='font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;'>
							        <div class='content' style='box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;'>
							          <!-- START CENTERED WHITE CONTAINER -->
							          <span class='preheader' style='color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;'>This is preheader text. Some clients will show this text as a preview.</span>
							          <table border='0' cellpadding='0' cellspacing='0' class='body' style='background-color:#f0f0f0; text-align:left; border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
							            <tr>
							              <th width='20%' align='center'><img src='".base_url()."media/logo/Logo_SI.png' style='width:50%'></th>
							              <td width='60%' align='center'>
							                <h4>
							                              e-DEMS
							                              <br />
							                              Electronic Design Engineering Management System
							                            </h4>
							              </td>
							              <th width='20%' align='center'><img src='".base_url()."media/CoE.png' style='width:50%'></th>
							              <!-- <th width='20%' align='center'><img src='../../img/k3.png' style='width:50%'></th> -->
							            </tr>
							          </table>
							          <table class='main' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;0px 0px 0px 5px;width:100%;'>
							            <!-- START MAIN CONTENT AREA -->
							            <tr>
							              <td class='wrapper' style='font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;'>
							                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
							                  <tr>
							                    <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>
							                      <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
							                        <center>
							                          <h4><u>APPROVAL REQUEST</u><br />{$param['title']}</h4></center>
							                      </p>

							                      <!-- <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
							                                  <center><h4 style='text-decoration:underline'>Kode ORDER</h4></center>
							                                </p> -->
							                      <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
							                        Kepada Yth,<br> {$param['APP']['UK_TEXT']}
							                        <br> {$param['APP']['BADGE']} - {$param['APP']['NAMA']}<br> Di tempat<br>
							                      </p>
							                      <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Terdapat permintaan approval untuk pengajuan <u>{$param['title']}</u>, detail rincian sebagai berikut</p>

							                      <!-- <table style='width:100%'>
							                                  <tr>
							                                    <td align='center' style='text-decoration: underline; font-weight: bold'>Foto ketika temuan</td>
							                                    <td align='center' style='text-decoration: underline; font-weight: bold'>Foto setelah tindak lanjut</td>
							                                  </tr>
							                                  <tr>
							                                    <td align='center'><img src='../../img/preview-icon.png' style='width:50%'></td>
							                                    <td align='center'><img src='../../img/preview-icon.png' style='width:50%'></td>
							                                  </tr>
							                                </table> -->

                                    <ul>
                                      {$strn}
                                    </ul>

							                      <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Surat permintaan ini sebagai informasi, klik tombol untuk melihat dan melakukan tindak lanjut.</p>
							                      <table border='0' cellpadding='0' cellspacing='0' class='btn btn-primary' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;box-sizing:border-box;width:100%;'>
							                        <tbody>
							                          <tr>
							                            <td align='center' style='font-family:sans-serif;font-size:14px;vertical-align:top;padding-bottom:15px;'>

							                              <!-- ================================== BUTTON ======================================= -->
							                              <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;width:auto;'>
							                                <tbody>
							                                  <tr>
							                                    <td style='font-family:sans-serif;font-size:14px;vertical-align:top;background-color:#ffffff;border-radius:5px;text-align:center;background-color:#3498db;'>
							                                      <center> <a href='".base_url()."action/{$param['link']}/{$param['code']}' target='_blank' style='text-decoration:underline;background-color:#ffffff;border:solid 1px #3498db;border-radius:5px;box-sizing:border-box;color:#3498db;cursor:pointer;display:inline-block;font-size:14px;font-weight:bold;margin:0;padding:12px 25px;text-decoration:none;text-transform:capitalize;background-color:#3498db;border-color:#3498db;color:#ffffff;'>Tindak Lanjut</a></center>
							                                    </td>
							                                  </tr>
							                                </tbody>
							                              </table>
							                              <!-- ================================== BUTTON ======================================= -->

							                            </td>
							                          </tr>
							                        </tbody>
							                      </table>
							                      <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Hormat saya,<br />{$param['sender']}</p>
							                    </td>
							                  </tr>
							                </table>
							              </td>
							            </tr>
							            <!-- END MAIN CONTENT AREA -->
							          </table>
							          <!-- START FOOTER -->
							          <div class='footer' style='clear:both;padding-top:10px;text-align:center;width:100%;'>
							            <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
							              <tr>
							                <td class='content-block' style='font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;'>
							                  <span class='apple-link' style='color:#999999;font-size:12px;text-align:center;'>
							                              e-DEMS, <a style='color:#999999;font-size:12px;' href='http://e-dems.semenindonesia.com'>Safety Hygiene Environment System</a>
							                              <br>
							                              Copyright by <a style='color:#999999;font-size:12px;' href='http://sisi.id/'>PT.SISI</a>
							                            </span>
							                  </span>
							                </td>
							              </tr>
							            </table>
							          </div>
							          <!-- END FOOTER -->
							          <!-- END CENTERED WHITE CONTAINER -->
							        </div>
							      </td>
							      <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
							    </tr>
							  </table>
							</body>

							</html>

		";

		return $html;

	}

	public function set_app_scm($param){
		// $user_hris = $this->CI->dbhris->get_user_hris($param['header']['NO_BADGE']);
		// $user_hris = (array) $user_hris[0];
		// $kebutuhan = str_replace('Permintaan ', '', $param['detail'][0]['KETERANGAN']);
		$strn = "";
		foreach ($param['TRN'] as $key => $value) {
			$strn .="<li>$value</li>";
		}
		$html = "
							<!DOCTYPE html>
							<html>

							<head>
							  <meta name='viewport' content='width=device-width'>
							  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
							  <title>Simple Transactional Email</title>
							  <style type='text/css'>
							    /* -------------------------------------
							                INLINED WITH https://putsmail.com/inliner
							            ------------------------------------- */

							    /* -------------------------------------
							                RESPONSIVE AND MOBILE FRIENDLY STYLES
							            ------------------------------------- */

							    @media only screen and (max-width: 620px) {
							      table[class=body] h1 {
							        font-size: 28px !important;
							        margin-bottom: 10px !important;
							      }
							      table[class=body] p,
							      table[class=body] ul,
							      table[class=body] ol,
							      table[class=body] td,
							      table[class=body] span,
							      table[class=body] a {
							        font-size: 16px !important;
							      }
							      table[class=body] .wrapper,
							      table[class=body] .article {
							        padding: 10px !important;
							      }
							      table[class=body] .content {
							        padding: 0 !important;
							      }
							      table[class=body] .container {
							        padding: 0 !important;
							        width: 100% !important;
							      }
							      table[class=body] .main {
							        border-left-width: 0 !important;
							        border-radius: 0 !important;
							        border-right-width: 0 !important;
							      }
							      table[class=body] .btn table {
							        width: 100% !important;
							      }
							      table[class=body] .btn a {
							        width: 100% !important;
							      }
							      table[class=body] .img-responsive {
							        height: auto !important;
							        max-width: 100% !important;
							        width: auto !important;
							      }
							    }

							    /* -------------------------------------
							                PRESERVE THESE STYLES IN THE HEAD
							            ------------------------------------- */

							    @media all {
							      .ExternalClass {
							        width: 100%;
							      }
							      .ExternalClass,
							      .ExternalClass p,
							      .ExternalClass span,
							      .ExternalClass font,
							      .ExternalClass td,
							      .ExternalClass div {
							        line-height: 100%;
							      }
							      .apple-link a {
							        color: inherit !important;
							        font-family: inherit !important;
							        font-size: inherit !important;
							        font-weight: inherit !important;
							        line-height: inherit !important;
							        text-decoration: none !important;
							      }
							      .btn-primary table td:hover {
							        background-color: #34495e !important;
							      }
							      .btn-primary a:hover {
							        background-color: #34495e !important;
							        border-color: #34495e !important;
							      }
							    }
							  </style>
							</head>

							<body class='' style='background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;'>
							  <table border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;'>
							    <tr>
							      <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
							      <td class='container' style='font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;'>
							        <div class='content' style='box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;'>
							          <!-- START CENTERED WHITE CONTAINER -->
							          <span class='preheader' style='color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;'>This is preheader text. Some clients will show this text as a preview.</span>
							          <table border='0' cellpadding='0' cellspacing='0' class='body' style='background-color:#f0f0f0; text-align:left; border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
							            <tr>
							              <th width='20%' align='center'><img src='".base_url()."media/logo/Logo_SI.png' style='width:50%'></th>
							              <td width='60%' align='center'>
							                <h4>
							                              e-DEMS
							                              <br />
							                              Electronic Design Engineering Management System
							                            </h4>
							              </td>
							              <th width='20%' align='center'><img src='".base_url()."media/CoE.png' style='width:50%'></th>
							              <!-- <th width='20%' align='center'><img src='../../img/k3.png' style='width:50%'></th> -->
							            </tr>
							          </table>
							          <table class='main' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;0px 0px 0px 5px;width:100%;'>
							            <!-- START MAIN CONTENT AREA -->
							            <tr>
							              <td class='wrapper' style='font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;'>
							                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
							                  <tr>
							                    <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>
							                      <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
							                        <center>
							                          <h4><u>APPROVAL REQUEST</u><br />{$param['title']}</h4></center>
							                      </p>

							                      <!-- <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
							                                  <center><h4 style='text-decoration:underline'>Kode ORDER</h4></center>
							                                </p> -->
							                      <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
							                        Kepada Yth,<br> {$param['APP']['UK_TEXT']}
							                        <br> {$param['APP']['BADGE']} - {$param['APP']['NAMA']}<br> Di tempat<br>
							                      </p>
							                      <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Terdapat permintaan approval untuk pengajuan <u>{$param['title']}</u>, detail rincian sebagai berikut</p>

							                      <!-- <table style='width:100%'>
							                                  <tr>
							                                    <td align='center' style='text-decoration: underline; font-weight: bold'>Foto ketika temuan</td>
							                                    <td align='center' style='text-decoration: underline; font-weight: bold'>Foto setelah tindak lanjut</td>
							                                  </tr>
							                                  <tr>
							                                    <td align='center'><img src='../../img/preview-icon.png' style='width:50%'></td>
							                                    <td align='center'><img src='../../img/preview-icon.png' style='width:50%'></td>
							                                  </tr>
							                                </table> -->

                                    <ul>
                                      {$strn}
                                    </ul>

							                      <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Surat permintaan ini sebagai informasi, klik tombol untuk melihat dan melakukan tindak lanjut.</p>
							                      <table border='0' cellpadding='0' cellspacing='0' class='btn btn-primary' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;box-sizing:border-box;width:100%;'>
							                        <tbody>
							                          <tr>
							                            <td align='center' style='font-family:sans-serif;font-size:14px;vertical-align:top;padding-bottom:15px;'>

							                              <!-- ================================== BUTTON ======================================= -->
							                              <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;width:auto;'>
							                                <tbody>
							                                  <tr>
							                                    <td style='font-family:sans-serif;font-size:14px;vertical-align:top;background-color:#ffffff;border-radius:5px;text-align:center;background-color:#3498db;'>
							                                      <center> <a href='".base_url()."action/{$param['link']}/{$param['code']}' target='_blank' style='text-decoration:underline;background-color:#ffffff;border:solid 1px #3498db;border-radius:5px;box-sizing:border-box;color:#3498db;cursor:pointer;display:inline-block;font-size:14px;font-weight:bold;margin:0;padding:12px 25px;text-decoration:none;text-transform:capitalize;background-color:#3498db;border-color:#3498db;color:#ffffff;'>Tindak Lanjut</a></center>
							                                    </td>
							                                  </tr>
							                                </tbody>
							                              </table>
							                              <!-- ================================== BUTTON ======================================= -->

							                            </td>
							                          </tr>
							                        </tbody>
							                      </table>
							                      <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Hormat saya,<br />{$param['sender']}</p>
							                    </td>
							                  </tr>
							                </table>
							              </td>
							            </tr>
							            <!-- END MAIN CONTENT AREA -->
							          </table>
							          <!-- START FOOTER -->
							          <div class='footer' style='clear:both;padding-top:10px;text-align:center;width:100%;'>
							            <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
							              <tr>
							                <td class='content-block' style='font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;'>
							                  <span class='apple-link' style='color:#999999;font-size:12px;text-align:center;'>
							                              e-DEMS, <a style='color:#999999;font-size:12px;' href='http://e-dems.semenindonesia.com'>Safety Hygiene Environment System</a>
							                              <br>
							                              Copyright by <a style='color:#999999;font-size:12px;' href='http://sisi.id/'>PT.SISI</a>
							                            </span>
							                  </span>
							                </td>
							              </tr>
							            </table>
							          </div>
							          <!-- END FOOTER -->
							          <!-- END CENTERED WHITE CONTAINER -->
							        </div>
							      </td>
							      <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
							    </tr>
							  </table>
							</body>

							</html>

		";

		return $html;

	}

	public function conf_trm($param){
		// $user_hris = $this->CI->dbhris->get_user_hris($param['header']['NO_BADGE']);
		// $user_hris = (array) $user_hris[0];
		// $kebutuhan = str_replace('Permintaan ', '', $param['detail'][0]['KETERANGAN']);
		$strn = "";
		foreach ($param['TRN'] as $key => $value) {
			$strn .="<li>$value</li>";
		}
		$html = "
		          <!DOCTYPE html>
		          <html>

		          <head>
		            <meta name='viewport' content='width=device-width'>
		            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
		            <title>Simple Transactional Email</title>
		            <style type='text/css'>
		              /* -------------------------------------
		                          INLINED WITH https://putsmail.com/inliner
		                      ------------------------------------- */

		              /* -------------------------------------
		                          RESPONSIVE AND MOBILE FRIENDLY STYLES
		                      ------------------------------------- */

		              @media only screen and (max-width: 620px) {
		                table[class=body] h1 {
		                  font-size: 28px !important;
		                  margin-bottom: 10px !important;
		                }
		                table[class=body] p,
		                table[class=body] ul,
		                table[class=body] ol,
		                table[class=body] td,
		                table[class=body] span,
		                table[class=body] a {
		                  font-size: 16px !important;
		                }
		                table[class=body] .wrapper,
		                table[class=body] .article {
		                  padding: 10px !important;
		                }
		                table[class=body] .content {
		                  padding: 0 !important;
		                }
		                table[class=body] .container {
		                  padding: 0 !important;
		                  width: 100% !important;
		                }
		                table[class=body] .main {
		                  border-left-width: 0 !important;
		                  border-radius: 0 !important;
		                  border-right-width: 0 !important;
		                }
		                table[class=body] .btn table {
		                  width: 100% !important;
		                }
		                table[class=body] .btn a {
		                  width: 100% !important;
		                }
		                table[class=body] .img-responsive {
		                  height: auto !important;
		                  max-width: 100% !important;
		                  width: auto !important;
		                }
		              }

		              /* -------------------------------------
		                          PRESERVE THESE STYLES IN THE HEAD
		                      ------------------------------------- */

		              @media all {
		                .ExternalClass {
		                  width: 100%;
		                }
		                .ExternalClass,
		                .ExternalClass p,
		                .ExternalClass span,
		                .ExternalClass font,
		                .ExternalClass td,
		                .ExternalClass div {
		                  line-height: 100%;
		                }
		                .apple-link a {
		                  color: inherit !important;
		                  font-family: inherit !important;
		                  font-size: inherit !important;
		                  font-weight: inherit !important;
		                  line-height: inherit !important;
		                  text-decoration: none !important;
		                }
		                .btn-primary table td:hover {
		                  background-color: #34495e !important;
		                }
		                .btn-primary a:hover {
		                  background-color: #34495e !important;
		                  border-color: #34495e !important;
		                }
		              }
		            </style>
		          </head>

		          <body class='' style='background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;'>
		            <table border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;'>
		              <tr>
		                <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
		                <td class='container' style='font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;'>
		                  <div class='content' style='box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;'>
		                    <!-- START CENTERED WHITE CONTAINER -->
		                    <span class='preheader' style='color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;'>This is preheader text. Some clients will show this text as a preview.</span>
		                    <table border='0' cellpadding='0' cellspacing='0' class='body' style='background-color:#f0f0f0; text-align:left; border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
		                      <tr>
		                        <th width='20%' align='center'><img src='".base_url()."media/logo/Logo_SI.png' style='width:50%'></th>
		                        <td width='60%' align='center'>
		                          <h4>
		                                        e-DEMS
		                                        <br />
		                                        Electronic Design Engineering Management System
		                                      </h4>
		                        </td>
		                        <th width='20%' align='center'><img src='".base_url()."media/CoE.png' style='width:50%'></th>
		                        <!-- <th width='20%' align='center'><img src='../../img/k3.png' style='width:50%'></th> -->
		                      </tr>
		                    </table>
		                    <table class='main' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;0px 0px 0px 5px;width:100%;'>
		                      <!-- START MAIN CONTENT AREA -->
		                      <tr>
		                        <td class='wrapper' style='font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;'>
		                          <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
		                            <tr>
		                              <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>
		                                <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
		                                  <center>
		                                    <h4><u>KONFIRMASI TRANSMITAL </u><br />{$param['title']}</h4></center>
		                                </p>

		                                <!-- <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
		                                            <center><h4 style='text-decoration:underline'>Kode ORDER</h4></center>
		                                          </p> -->
		                                <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
		                                  Kepada Yth,<br> {$param['APP']['UK_TEXT']}
		                                  <br> {$param['APP']['BADGE']} - {$param['APP']['NAMA']}<br> Di tempat<br>
		                                </p>
		                                <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Sebagai informasi penyelesaian <u>{$param['title']}</u>, dengan detail rincian sebagai berikut</p>

		                                <!-- <table style='width:100%'>
		                                            <tr>
		                                              <td align='center' style='text-decoration: underline; font-weight: bold'>Foto ketika temuan</td>
		                                              <td align='center' style='text-decoration: underline; font-weight: bold'>Foto setelah tindak lanjut</td>
		                                            </tr>
		                                            <tr>
		                                              <td align='center'><img src='../../img/preview-icon.png' style='width:50%'></td>
		                                              <td align='center'><img src='../../img/preview-icon.png' style='width:50%'></td>
		                                            </tr>
		                                          </table> -->

		                                <ul>
		                                  {$strn}
		                                </ul>

		                                <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Berdasar email ini, maka bukti Transmital Produk Engineering sudah dapat dicetak dan digunakan untuk keperluan lebih lanjut.</p>
		                                <table border='0' cellpadding='0' cellspacing='0' class='btn btn-primary' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;box-sizing:border-box;width:100%;'>
		                                  <tbody>
		                                    <tr>
		                                      <td align='center' style='font-family:sans-serif;font-size:14px;vertical-align:top;padding-bottom:15px;'>

		                                        <!-- ================================== BUTTON ======================================= -->
		                                        <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;width:auto;'>
		                                          <tbody>
		                                            <tr>
		                                              <td style='font-family:sans-serif;font-size:14px;vertical-align:top;background-color:#ffffff;border-radius:5px;text-align:center;background-color:#3498db;'>
		                                                <!-- <center> <a href='".base_url()."{$param['link']}/{$param['code']}' target='_blank' style='text-decoration:underline;background-color:#ffffff;border:solid 1px #3498db;border-radius:5px;box-sizing:border-box;color:#3498db;cursor:pointer;display:inline-block;font-size:14px;font-weight:bold;margin:0;padding:12px 25px;text-decoration:none;text-transform:capitalize;background-color:#3498db;border-color:#3498db;color:#ffffff;'>Tindak Lanjut</a></center>  -->
		                                                <center> <a href='".base_url()."{$param['link']}' target='_blank' style='text-decoration:underline;background-color:#ffffff;border:solid 1px #3498db;border-radius:5px;box-sizing:border-box;color:#3498db;cursor:pointer;display:inline-block;font-size:14px;font-weight:bold;margin:0;padding:12px 25px;text-decoration:none;text-transform:capitalize;background-color:#3498db;border-color:#3498db;color:#ffffff;'>Buka Link e-DEMS</a></center>
		                                              </td>
		                                            </tr>
		                                          </tbody>
		                                        </table>
		                                        <!-- ================================== BUTTON ======================================= -->

		                                      </td>
		                                    </tr>
		                                  </tbody>
		                                </table>
		                                <!-- <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Hormat saya,<br />{$param['sender']}</p> -->
		                              </td>
		                            </tr>
		                          </table>
		                        </td>
		                      </tr>
		                      <!-- END MAIN CONTENT AREA -->
		                    </table>
		                    <!-- START FOOTER -->
		                    <div class='footer' style='clear:both;padding-top:10px;text-align:center;width:100%;'>
		                      <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
		                        <tr>
		                          <td class='content-block' style='font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;'>
		                            <span class='apple-link' style='color:#999999;font-size:12px;text-align:center;'>
		                                        e-DEMS, <a style='color:#999999;font-size:12px;' href='http://e-dems.semenindonesia.com'>Safety Hygiene Environment System</a>
		                                        <br>
		                                        Copyright by <a style='color:#999999;font-size:12px;' href='http://sisi.id/'>PT.SISI</a>
		                                      </span>
		                            </span>
		                          </td>
		                        </tr>
		                      </table>
		                    </div>
		                    <!-- END FOOTER -->
		                    <!-- END CENTERED WHITE CONTAINER -->
		                  </div>
		                </td>
		                <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
		              </tr>
		            </table>
		          </body>

		          </html>


		";

		return $html;

	}

	public function _test(){

        $body = '<!DOCTYPE html>
                <html>
                  <head>
                    <meta name="viewport" content="width=device-width">
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <title>Simple Transactional Email</title>
                    <style type="text/css">
                    /* -------------------------------------
                        INLINED WITH https://putsmail.com/inliner
                    ------------------------------------- */
                    /* -------------------------------------
                        RESPONSIVE AND MOBILE FRIENDLY STYLES
                    ------------------------------------- */
                    @media only screen and (max-width: 620px) {
                      table[class=body] h1 {
                        font-size: 28px !important;
                        margin-bottom: 10px !important; }
                      table[class=body] p,
                      table[class=body] ul,
                      table[class=body] ol,
                      table[class=body] td,
                      table[class=body] span,
                      table[class=body] a {
                        font-size: 16px !important; }
                      table[class=body] .wrapper,
                      table[class=body] .article {
                        padding: 10px !important; }
                      table[class=body] .content {
                        padding: 0 !important; }
                      table[class=body] .container {
                        padding: 0 !important;
                        width: 100% !important; }
                      table[class=body] .main {
                        border-left-width: 0 !important;
                        border-radius: 0 !important;
                        border-right-width: 0 !important; }
                      table[class=body] .btn table {
                        width: 100% !important; }
                      table[class=body] .btn a {
                        width: 100% !important; }
                      table[class=body] .img-responsive {
                        height: auto !important;
                        max-width: 100% !important;
                        width: auto !important; }}
                    /* -------------------------------------
                        PRESERVE THESE STYLES IN THE HEAD
                    ------------------------------------- */
                    @media all {
                      .ExternalClass {
                        width: 100%; }
                      .ExternalClass,
                      .ExternalClass p,
                      .ExternalClass span,
                      .ExternalClass font,
                      .ExternalClass td,
                      .ExternalClass div {
                        line-height: 100%; }
                      .apple-link a {
                        color: inherit !important;
                        font-family: inherit !important;
                        font-size: inherit !important;
                        font-weight: inherit !important;
                        line-height: inherit !important;
                        text-decoration: none !important; }
                      .btn-primary table td:hover {
                        background-color: #34495e !important; }
                      .btn-primary a:hover {
                        background-color: #34495e !important;
                        border-color: #34495e !important; } }
                    </style>
                  </head>
                  <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
                      <tr>
                        <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                        <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
                          <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                            <!-- START CENTERED WHITE CONTAINER -->
                            <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
                            <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                              <!-- START MAIN CONTENT AREA -->
                              <tr>
                                <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">

                                  <a href="http://10.15.5.150/dev/she/safety_harian">Link</a>
                                 <!--  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                    <tr>
                                      <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi there,</p>
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Sometimes you just want to send a simple HTML email with a simple design and clear call to action. This is it.</p>
                                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;box-sizing:border-box;width:100%;">
                                          <tbody>
                                            <tr>
                                              <td align="left" style="font-family:sans-serif;font-size:14px;vertical-align:top;padding-bottom:15px;">
                                                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;width:auto;">
                                                  <tbody>
                                                    <tr>
                                                      <td style="font-family:sans-serif;font-size:14px;vertical-align:top;background-color:#ffffff;border-radius:5px;text-align:center;background-color:#3498db;"> <a href="http://htmlemail.io" target="_blank" style="text-decoration:underline;background-color:#ffffff;border:solid 1px #3498db;border-radius:5px;box-sizing:border-box;color:#3498db;cursor:pointer;display:inline-block;font-size:14px;font-weight:bold;margin:0;padding:12px 25px;text-decoration:none;text-transform:capitalize;background-color:#3498db;border-color:#3498db;color:#ffffff;">Call To Action</a> </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.</p>
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Good luck! Hope it works.</p>
                                      </td>
                                    </tr>
                                  </table> -->
                                </td>
                              </tr>
                              <!-- END MAIN CONTENT AREA -->
                            </table>
                            <!-- START FOOTER -->
                            <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                <tr>
                                  <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                    <span class="apple-link" style="color:#999999;font-size:12px;text-align:center;">Company Inc, 3 Abbey Road, San Francisco CA 94102</span>
                                    <br>
                                     Don\'t like these emails? <a href="http://i.imgur.com/CScmqnj.gif" style="color:#3498db;text-decoration:underline;color:#999999;font-size:12px;text-align:center;">Unsubscribe</a>.
                                  </td>
                                </tr>
                                <tr>
                                  <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                    Powered by <a href="http://htmlemail.io" style="color:#3498db;text-decoration:underline;color:#999999;font-size:12px;text-align:center;text-decoration:none;">HTMLemail</a>.
                                  </td>
                                </tr>
                              </table>
                            </div>
                            <!-- END FOOTER -->
                            <!-- END CENTERED WHITE CONTAINER -->
                          </div>
                        </td>
                        <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                      </tr>
                    </table>
                  </body>
                </html>
                ';
        return $body;
    }


}
