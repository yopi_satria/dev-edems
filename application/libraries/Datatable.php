<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Datatable {

	protected $CI;

	protected $column;

	/**
	 * Constructor
	 *
	 * @param   array   $config
	 */
	public function __construct($config = array())
	{	
		 // parent::__construct();
		$this->CI = &get_instance();
	}

	public function out(){
		echo "DataTable Library";
	}
	public function encode($columns, $modelName, $input){

		$model_alias = str_replace('/', '_', $modelName);

		$this->CI->load->model($modelName, $model_alias);

		$limit = $input['length'];
        $start = $input['start'];
        $order = $columns[$input['order'][0]['column']];
        $dir = $input['order'][0]['dir'];
  
        $totalData = $this->CI->{$model_alias}->_all_count();
            
        $totalFiltered = $totalData; 
            
        if(empty($input['search']['value']))
        {            
            $posts = $this->CI->{$model_alias}->_all($limit,$start,$order,$dir);
        }
        else {
            $search = $input['search']['value']; 

            $posts =  $this->CI->{$model_alias}->_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->CI->{$model_alias}->_search_count($search);
        }

        $data = array();
        if(!empty($posts))
        {
            // foreach ($posts as $post)
            // {

            //     $nestedData['id'] = $post->id;
            //     $nestedData['title'] = $post->title;
            //     $nestedData['body'] = substr(strip_tags($post->body),0,50)."...";
            //     $nestedData['created_at'] = date('j M Y h:i a',strtotime($post->created_at));
                
            //     $data[] = $nestedData;

            // }
            $data =(array) $posts;
        }
          
        $json_data = array(
                    "draw"            => intval($input['draw']),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        return $json_data; 
	}

	
}
