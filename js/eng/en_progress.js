var oTable;

$(document).ready(function() {
  // getForeign();
  reset_form();
  tb_progress();
  // getFiles();
  $('.multifile').multifile();

  //   $('.multifile').multifile({
  //     container: "#upload-container",
  //     template: function (file) {
  //         var fileName = file.name;
  //         var fileExtension = file.name.split('.').pop();
  //         // <li class="pekerjaan">test<i style="float:right; cursor:pointer" class="todo-item-delete fa fa-times"></i></li>
  //         var result =
  //             '<p class="uploaded_image alert alert-info">' +
  //             '<a href="#" class="multifile_remove_input todo-list fa fa-times" style="float:left; cursor:pointer" >Close me</a>' +
  //             '<span class="filename">$fileName ($fileExtension)</span>' +
  //             '</p>';
  //
  //         result = result.replace('$fileExtension', fileExtension).replace('$fileName', fileName)
  //
  //         return $(result);
  //     }
  // })

  // pTable = $('#tb_progress').dataTable({
  //   scrollY: "300px",
  //   scrollX: true,
  //   scrollCollapse: true,
  //   paging: false,
  //   fixedColumns: {
  //     leftColumns: 2
  //   }
  // });
  // new FixedColumns( pTable , {
  //     "iLeftColumns":2,
  //     "sHeightMatch" : "auto",
  //     "iRightColumns": 0
  // });
  // $('.select2').select2();
  // setAtasan();
  // reloadtabel();
});

function getFiles() {
  //get the input and UL list
  var input = document.getElementById('FILES');
  var list = document.getElementById('filelist');

  //empty list for now...
  while (list.hasChildNodes()) {
    list.removeChild(ul.firstChild);
  }

  //for every file...
  for (var x = 0; x < input.files.length; x++) {
    //add to list
    var li = document.createElement('li');
    li.innerHTML = 'File ' + (x + 1) + ':  ' + input.files[x].name;
    list.append(li);
  }
}

function setActiveTab(ul_id = null, content_id = null) {
  $(".nav-link").removeClass("active show");
  $('#' + ul_id).addClass('active show');
  $(".tab-pane").removeClass("active show");
  $('#' + content_id).addClass('active show');
}

function tb_progress() {
  oTable = $('#tb_progress').DataTable({
    destroy: true,
    processing: true,
    serverSide: true,
    // scrollY: "300px",
    // scrollX: true,
    // scrollCollapse: true,
    // paging: false,
    // fixedColumns: {
    //   leftColumns: 2
    // },
    ajax: {
      url: 'eng/progress/data_list',
      type: "POST"
    },
    columns: [{
        "data": "GROUP_TEXT",
        "mRender": function(row, data, index) {
          state = '0 %';
          if (index['TOT_AVG']) {
            state = index['GROUP_TEXT'] + index['TOT_AVG'] + ' %';
          }
          return state;
        }
      },
      {
        "data": "ID_PENUGASAN"
      },
      // {
      //   "data": "NOTIFIKASI"
      // },
      {
        "data": "DESKRIPSI",
        "width": 120
      },
      {
        "data": "START_DATE",
        "width": 50
      },
      {
        "data": "END_DATE",
        "width": 50
      },
      {
        "data": "REMARKS",
        "width": 140
      },
      {
        "data": "PROGRESS",
        "width": 50,
        "className": "text-right",
        "mRender": function(row, data, index) {
          state = '0 %';
          if (index['PROGRESS']) {
            state = index['PROGRESS'] + ' %';
          }
          return state;
        }
      },
      // {
      //   "data": "STATUS"
      // },
      // {
      //   "data": "CREATE_BY",
      //   "width": 70
      // },
      // {
      //   "data": "CREATE_AT",
      //   "width": 75
      // },
      {
        "data": "ID",
        "width": 40,
        "mRender": function(row, data, index) {
          state = '';
          // if (index['STATE']) {
          //   state = 'disabled';
          // }
          return '<center><div style="float: none;" class="btn-group"><button class="btnEdit btn btn-xs btn-primary ' + state + '" data-toggle="tooltip" title="Update" ' + state + ' ><i class="fa fa-pencil-square-o"></i></button></div></center>';
        }
      },
    ],
    "order": [
      [1, "desc"],
      [2, "asc"]
    ],
    columnDefs: [{
        "visible": false,
        "targets": 0
      },
      {
        "visible": false,
        "targets": 1
      }
    ],
    drawCallback: function(settings) {
      var api = this.api();
      var rows = api.rows({
        page: 'current'
      }).nodes();
      var last = null;

      api.column(0, {
        page: 'current'
      }).data().each(function(group, i) {

        if (last !== group) {
          dtGrp = group.split(' ; ');
          prg = ' <button type="button" class="btn btn-dark btn-outline btn-rounded btn-sm" style="cursor:default;">' + Math.round(dtGrp[1]) + ' %</button>';

          $(rows).eq(i).before(
            '<tr class="group"><td colspan="9" style="/*background-color:rgba(116, 96, 238,0.2);*/font-weight:700;color:#006232;text-align:left;">' + ' ' + dtGrp[0] + prg + '</td></tr>'
          );

          last = group;
        }
      });
    }
  });
}

function tb_log(id, tbl = 'MPE_DTL_PENUGASAN') {
  no = 0;

  table = $("#tb_log").DataTable({
    destroy: true,
    processing: true,
    serverSide: false,
    ajax: {
      url: 'eng/progress/history',
      dataType: "json",
      type: "POST",
      data: {
        "id": id,
        "tbl": tbl
      },
      dataSrc: function(json) {
        var return_data = new Array();
        if (json['status'] == 200) {
          $.each(json['data'], function(i, item) {

            id = (item['ID'] == null ? '' : item['ID']);
            progress = (item['LOG_RES1'] == null ? '' : item['LOG_RES1'] + ' %');
            // create_at = (item['CREATE_AT'] == null ? '' : item['CREATE_AT']);
            create_at = (item['UPDATE_AT'] == null ? '' : item['UPDATE_AT']);
            note = (item['NOTE'] == null ? '' : item['NOTE']);

            return_data.push({
              'no': '<center class="text-success">' + (no + 1) + '</center>',
              'progress': '<p class="text-danger" align="right">' + progress + '</p>',
              'create_at': '<center class="text-success">' + create_at + '</center>',
              'note': '<p class="text-success">' + note + '</p>'
            });
            no++;
          });
        }
        return return_data;
      },
    },
    columns: [{
        data: 'no'
      },
      {
        data: 'progress'
      },
      {
        data: 'create_at'
      },
      {
        data: 'note'
      }
    ],
    columnDefs: [{
      targets: -1,
      className: 'dt-body-left'
    }]
  });
}

// function getForeign() {
//   $.ajax({
//     type: 'POST',
//     url: 'eng/progress/foreign',
//     success: function(json) {
//       json = $.parseJSON(json);
//       $('#ID_PENUGASAN').html('');
//       foreigns = '';
//       if (json) {
//         if (json['status'] == 200) {
//           foreigns = ('<option value >Please select...</option>');
//           $.each(json['data'], function(i, data) {
//             foreigns += ('<option value="' + data['ID_PENUGASAN'] + '" title="' + data['NO_PENUGASAN'] + ' - ' + data['OBJECTIVE'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-no="' + data['NO_PENUGASAN'] + '" data-notif="' + data['NOTIFIKASI'] + '" data-pengajuan="' + data['PENGAJUAN'] + '" data-object="' + data['OBJECTIVE'] + '" >' + data['NO_PENUGASAN'] + ' - ' + data['PENGAJUAN'] + '</option>');
//           });
//         } else {
//           foreigns = ('<option value >Empty...</option>');
//         }
//         $('#ID_PENUGASAN').html(foreigns);
//         onForeign();
//       }
//     },
//     error: function(xhr, status, error) {
//       alert('Server Error ...');
//     }
//   });
// }

// function onForeign() {
//   $('#NO_PENUGASAN').val($('#ID_PENUGASAN :selected').attr('data-no'));
//   $('#NOTIFIKASI').val($('#ID_PENUGASAN :selected').attr('data-notif'));
//   $('#PENGAJUAN').val($('#ID_PENUGASAN :selected').attr('data-pengajuan'));
//   $('#OBJECTIVE').val($('#ID_PENUGASAN :selected').attr('data-object'));
// }

// $('#ID_PENUGASAN').on('change', function() {
//   onForeign();
// })

function deleteTodoItem(e, item) {
  console.log('deleted');
  // e.preventDefault();
  $(item).parent().fadeOut('slow', function() {
    $(item).parent().remove();
  });
}

function reset_form(reset = null) {
  // $('#ID').val('');
  // $('#ID_PENUGASAN').html('');
  // $('#NO_PENUGASAN').val('');
  // $('#NOTIFIKASI').val('');
  // $('#PENGAJUAN').val('');
  // $('#OBJECTIVE').val('');
  // $('#DESKRIPSI').val('');
  // $('#START_DATE').val('');
  // $('#REMARKS').val('');
  // $('#PROGRESS').val('');

  // getForeign();
  $("#fm-new")[0].reset();
  $(".multifile_container").html('');

  $("#btn-save").removeClass("disabled");
  $("#btn-save").html(' <i class="fa fa-check"></i> Save');
  $('.selectpicker').selectpicker('refresh');
  $('#ul-new').text('Edit Progress Engineering');
  $('#ul-new').hide();
  $('#btn-save').hide();
  if (reset) {
    setActiveTab('ul-list', 'list-form');
  }

}

$("#btn-cancel").on("click", function() {
  reset_form('yes');
});

// export
// $(document).on('click', ".btnExport", function() {
//   var data = oTable.row($(this).parents('tr')).data();
//
//   window.location.assign('eng/progress/export_pdf/' + data['ID']);
// });

// event resend mail
// $(document).on('click', ".btnResend", function() {
//   $('.title_ukur').text('Resend Email');
//   var data = oTable.row($(this).parents('tr')).data();
//
//
//   var form_data = new FormData();
//   form_data.append('ID', data['ID']);
//
//   $.ajax({
//     url: 'eng/progress/resend', // point to server-side PHP script
//     dataType: 'json', // what to expect back from the PHP script, if anything
//     cache: false,
//     contentType: false,
//     processData: false,
//     data: form_data,
//     type: 'post',
//     error: function(xhr, status, error) {
//       swal({
//         title: "Resend Email : NOT Success! " + error,
//         type: "error"
//       });
//       // $("#btn-save").removeClass("disabled");
//       // $("#btn-save").val("Save");
//     },
//     success: function(json) {
//       if (json['status'] == 200) {
//         // tb_request();
//         // tb_erf2();
//         swal({
//           title: "Email was sent.",
//           type: "success"
//         });
//         reset_form('yes');
//       }
//       $("#btn-save").removeClass("disabled");
//       // $("#btn-save").val("Save");
//     }
//   });
// });

// view
$(document).on('click', ".btnEdit", function() {
  var data = oTable.row($(this).parents('tr')).data();
  $('#ID').val(data['ID']);
  // $('#NO_DOK_ENG').val(data['NO_DOK_ENG']);
  // if (data['ID_PENUGASAN']) {
  //   $('#ID_PENUGASAN').html('<option value="' + data['ID_PENUGASAN'] + '" title="' + data['NO_PENUGASAN'] + ' - ' + data['OBJECTIVE'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-no="' + data['NO_PENUGASAN'] + '" data-notif="' + data['NOTIFIKASI'] + '" data-pengajuan="' + data['PENGAJUAN'] + '" data-object="' + data['OBJECTIVE'] + '" >' + data['NO_PENUGASAN'] + '</option>');
  // }

  // console.log(data);
  $('#ID_PENUGASAN').val(data['ID_PENUGASAN']);
  $('#NO_PENUGASAN').val(data['NO_PENUGASAN']);
  $('#NOTIFIKASI').val(data['NOTIFIKASI']);
  $('#PENGAJUAN').val(data['PENGAJUAN']);
  $('#OBJECTIVE').val(data['OBJECTIVE']);
  $('#DESKRIPSI').val(data['DESKRIPSI']);
  $('#START_DATE').val(data['S_DATE']);
  $('#END_DATE').val(data['E_DATE']);
  $('#REMARKS').val(data['REMARKS']);
  $('#PROGRESS').val(data['PROGRESS']);

  if (/detail/i.test(data['TIPE'])) {
    $('.guide-ded').show();
    $('.guide-kaj').hide();
  } else {
    $('.guide-ded').hide();
    $('.guide-kaj').show();
  }

  var sli = '';
  $.each(data['FILES'], function(index, value) {
    sli += "<li class='afile'><a id='file_uri rel=noopener noreferrer' class='linkfile' href='" + value['PATHS'] + "' target='_blank'><i id=file_name class='fa fa-file-pdf-o m-r-10'></i>" + value['DESCRIPTION'] + "</a> </li>";
    // sli += "<li class='afile'><a id='file_uri rel=noopener noreferrer' class='linkfile' href='"+value['PATHS']+"' target='_blank'><i id=file_name class='fa fa-file-pdf-o m-r-10'></i>"+value['DESCRIPTION']+"</a><i style='float:right; cursor:pointer' class='todo-item-delete fa fa-times m-l-20' onClick='deleteTodoItem()' > </i> </li>";
  });
  $('#filelist').html(sli);

  // if (data['STATUS']) {
  //   $('#btn-save').hide();
  // }

  tb_log(data['ID']);

  // $('.select2').select2();
  // $('.selectpicker').selectpicker('refresh');
  $('#btn-save').show();
  $('#ul-new').text('Edit Progress Engineering');
  $('#ul-new').show();
  setActiveTab('ul-new', 'new-form');
  $('#btn-save').html(' <i class="fa fa-check"></i> Save');
  if (page_state.indexOf(page_short + '-modify=1') == -1 || data['STATE'] != '0') {
    $('.' + page_short + '-modify').hide();
  }
});

// save
$("#fm-new").submit(function(e) {
  e.preventDefault();
  $("#btn-save").addClass("disabled");

  id = $('#ID').val();
  id_penugasan = $('#ID_PENUGASAN').val();
  deskripsi = $('#DESKRIPSI').val();
  start_date = $('#START_DATE').val();
  end_date = $('#END_DATE').val();
  update_date = $('#UPDATE_DATE').val();
  remarks = $('#REMARKS').val();
  progress = $('#PROGRESS').val();


  var formURL = "";

  var form_data = new FormData();
  form_data.append('ID', id);
  form_data.append('ID_PENUGASAN', id_penugasan);
  form_data.append('START_DATE', start_date);
  form_data.append('END_DATE', end_date);
  form_data.append('UPDATE_DATE', update_date);
  form_data.append('REMARKS', remarks);
  form_data.append('PROGRESS', progress);
  // form_data.append('FILES[]', files);
  files = [];
  // files = $("input[name^='FILES']");
  $("input[name^='FILES']").each(function(index) {
    if ($("input[name^='FILES']")[index].files[0]) {
      // console.log($("input[name^='FILES']")[index].files[0]);
      files.push($("input[name^='FILES']")[index].files[0]);
      form_data.append('FILES[]', $("input[name^='FILES']")[index].files[0]);
    }
  });
  // console.log(files);
  // console.log(form_data.get('FILES[]'));
  // return;

  var uri = 'create';
  if (id) {
    uri = 'update';
  }

  $.ajax({
    url: 'eng/progress/' + uri, // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Data Save : NOT Success! " + error,
        type: "error"
      });
      $("#btn-save").removeClass("disabled");
    },
    success: function(json) {
      if (json['status'] == 200) {
        tb_progress();
        swal({
          title: "Data Saved!\n" + deskripsi,
          type: "success"
        });
        reset_form('yes');
      }
      $("#btn-save").removeClass("disabled");
    }
  });
  return false;
});

// edit

// delete
$(document).on('click', ".btnDel", function() {
  var data = oTable.row($(this).parents('tr')).data();
  swal({
      title: "Are you sure?",
      text: "Data (" + data['DESKRIPSI'] + ") will be deleted!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Delete",
      confirmButtonClass: "btn-danger",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        url = 'eng/progress/delete/' + data['ID'];
        $.ajax({
          url: url, // point to server-side PHP script
          dataType: 'json', // what to expect back from the PHP script, if anything
          cache: false,
          contentType: false,
          processData: false,
          type: 'get',
          error: function(xhr, status, error) {
            swal("Error", "Your data failed to delete!", "error");
          },
          success: function(json) {
            if (json['status'] == 200) {
              tb_progress();
              swal("Deleted!", "Your data has been deleted.", "success");
            }
          }
        });
      } else {
        swal("Cancelled", "Deletion of data canceled", "error");
      }
    });
});
