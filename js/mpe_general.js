var page_state = '';
var page_short = '';

$(document).ready(function() {
  page_short = $('#page_short_title').val();
  page_state = $('#page_state').val();
  // console.log(page_short);
  // console.log(page_state);
  if (page_state.indexOf(page_short + '-write=1') == -1) {
    //   $('.'+page_short+'-write').show();
    // }else {
    $('.' + page_short + '-write').hide();
  }
});

$(document).ajaxStart(function() {
  $("#loading").show();
  // console.log('loading show');
});

$(document).ajaxStop(function() {
  $("#loading").hide();
  // console.log('loading hide');
});

function clearStorage() {
  if (localStorage.dems_me) {
    localStorage.removeItem('dems_me');
    location.href = "login";
  }
}

function toTitleCase(str) {
  return str.replace(/(?:^|\s)\w/g, function(match) {
    return match.toUpperCase();
  });
}

function toCurrency(nStr) {
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}

function toNumber(nStr) {
  return nStr.replace(/,/gi,'');
}

function hideShowModal(idModal){
  if ($('body').hasClass('modal-open')) {
    $('#'+idModal).hide();
    $('.modal-backdrop').hide();
    $('body').removeClass('modal-open');
  }else {
    $('#'+idModal).show();
    $('.modal-backdrop').show();
    $('body').addClass('modal-open');
  }
}

function GetDateName(weekNumber) {
  var weeks = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
  return weeks[weekNumber];
}

function GetMonthName(monthNumber) {
  var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
  return months[monthNumber - 1];
}

function terbilang(n) {

  var string = n.toString(),
    units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words;

  // var and = custom_join_character || 'and';

  /* Is number zero? */
  if (parseInt(string) === 0) {
    return 'nol';
  }

  /* Array of units as words */
  units = ['', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sempilan', 'sepuluh', 'sebelas', 'dua belas', 'tiga belas', 'empat belas', 'lima belas', 'enam belas', 'tujuh belas', 'delapan belas', 'sembilan belas'];

  /* Array of tens as words */
  tens = ['', '', 'dua puluh', 'tiga puluh', 'empat puluh', 'lima puluh', 'enam puluh', 'tujuh puluh', 'delapan puluh', 'sembilan puluh'];

  /* Array of scales as words */
  scales = ['', 'ribu', 'juta', 'milyar', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion'];

  /* Split user arguemnt into 3 digit chunks from right to left */
  start = string.length;
  chunks = [];
  while (start > 0) {
    end = start;
    chunks.push(string.slice((start = Math.max(0, start - 3)), end));
  }

  /* Check if function has enough scale words to be able to stringify the user argument */
  chunksLen = chunks.length;
  if (chunksLen > scales.length) {
    return '';
  }

  /* Stringify each integer in each chunk */
  words = [];
  for (i = 0; i < chunksLen; i++) {

    chunk = parseInt(chunks[i]);

    if (chunk) {

      /* Split chunk into array of individual integers */
      ints = chunks[i].split('').reverse().map(parseFloat);

      /* If tens integer is 1, i.e. 10, then add 10 to units integer */
      if (ints[1] === 1) {
        ints[0] += 10;
      }

      /* Add scale word if chunk is not zero and array item exists */
      if ((word = scales[i])) {
        words.push(word);
      }

      /* Add unit word if array item exists */
      if ((word = units[ints[0]])) {
        words.push(word);
      }

      /* Add tens word if array item exists */
      if ((word = tens[ints[1]])) {
        words.push(word);
      }

      /* Add hundreds word if array item exists */
      if ((word = units[ints[2]])) {
        words.push(word + ' ratus');
      }

    }

  }

  kalimat = words.reverse().join(' ');
  kalimat.replace("satu ribu", "seribu").replace("satu ratus", "seratus");

  return kalimat;
}

/**
 * Convert an integer to its words representation
 *
 * @author McShaman (http://stackoverflow.com/users/788657/mcshaman)
 * @source http://stackoverflow.com/questions/14766951/convert-digits-into-words-with-javascript
 */
function numberToEnglish(n, custom_join_character) {

  var string = n.toString(),
    units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words;

  var and = custom_join_character || 'and';

  /* Is number zero? */
  if (parseInt(string) === 0) {
    return 'zero';
  }

  /* Array of units as words */
  units = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

  /* Array of tens as words */
  tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

  /* Array of scales as words */
  scales = ['', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion'];

  /* Split user arguemnt into 3 digit chunks from right to left */
  start = string.length;
  chunks = [];
  while (start > 0) {
    end = start;
    chunks.push(string.slice((start = Math.max(0, start - 3)), end));
  }

  /* Check if function has enough scale words to be able to stringify the user argument */
  chunksLen = chunks.length;
  if (chunksLen > scales.length) {
    return '';
  }

  /* Stringify each integer in each chunk */
  words = [];
  for (i = 0; i < chunksLen; i++) {

    chunk = parseInt(chunks[i]);

    if (chunk) {

      /* Split chunk into array of individual integers */
      ints = chunks[i].split('').reverse().map(parseFloat);

      /* If tens integer is 1, i.e. 10, then add 10 to units integer */
      if (ints[1] === 1) {
        ints[0] += 10;
      }

      /* Add scale word if chunk is not zero and array item exists */
      if ((word = scales[i])) {
        words.push(word);
      }

      /* Add unit word if array item exists */
      if ((word = units[ints[0]])) {
        words.push(word);
      }

      /* Add tens word if array item exists */
      if ((word = tens[ints[1]])) {
        words.push(word);
      }

      /* Add 'and' string after units or tens integer if: */
      if (ints[0] || ints[1]) {

        /* Chunk has a hundreds integer or chunk is the first of multiple chunks */
        if (ints[2] || !i && chunksLen) {
          words.push(and);
        }

      }

      /* Add hundreds word if array item exists */
      if ((word = units[ints[2]])) {
        words.push(word + ' hundred');
      }

    }

  }

  return words.reverse().join(' ');

}

$(document).on("focus", ".form-control", function(e) {
  $('.form-control').removeClass('input-focus');
  $(this).addClass('input-focus');
});

$(document).ready(function() {
  // notification();
  getLoginSession();
  saveSession();
  deleteSession();
});

$('html').on("click", function(e) {
  // if (!$(e.target).parent().hasClass('div-search') && !$(e.target).parent().hasClass('search-glb') && !$(e.target).hasClass('is_active') && !$(e.target).parent().hasClass('container') && !$(e.target).hasClass('progressbar')) {
  if ($(e.target).parents('.div-search').length<1 && $(e.target).parents('.search-glb').length<1 && $(e.target).parents('.container').length<1 && !$(e.target).hasClass('is_active')) {
    $(".div-search").removeClass('show');
    // console.log(e.target);
  }
});

$('.srh-btn').click(function(e) {
  $("#search-glb").val('');
});

// searchfuntion
var searchNotif = function() {
  if ($("#search-glb").val() && $("#search-glb").val().length == 12) {
    // Open this row
    var html = '';
    var notif = $("#search-glb").val();
    var data;
    var approveAt = '';
    var status = '';
    html += '<div class="container">' +
      '<ul class="progressbar">' +
      '<li data-toggle="tooltip" data-placement="top" title="' + status + '">Request Form</li>' +
      '<li>Assign Task</li>' +
      '<li>Documents</li>' +
      '<li>Finish</li>' +
      '</ul>';

    var dokeng;
    $.ajax({
      url: 'erf/request/getApproval',
      data: {
        id: notif
      },
      dataType: 'json',
      type: 'POST',
      success: function(result) {
        data = result;
        statuspenugasan = '';
        statusdok = '';
        statusbast = '';
        $.map(data, function(item, index) {
          fprogress = 0;
          pengajuan = item.STATUS1;
          penugasan = item.STATUS2;
          if (item.STATUS1 === 'Open') {
            status = 'Pending';
          } else if (item.STATUS1 === 'Approved') {
            status = 'active';
            fprogress = 12.5;
          } else {
            status = 'reject';
          }
          if (item.STATUS2 === 'In Approval') {
            statuspenugasan = 'pending';
          } else if (item.STATUS2 === 'Approved Biro.' || item.STATUS2 === 'SM Approved') {
            statuspenugasan = 'pending';
            fprogress = 25;
          } else if (item.STATUS2 === 'Approved Mgr.' || item.STATUS2 === 'MGR Approved') {
            statuspenugasan = 'active';
            fprogress = 25;
          } else if (item.STATUS2 === 'Rejected') {
            statuspenugasan = 'reject';
          } else {
            statuspenugasan = '';
          }
          if (item.DOKSTATUS === 'ada') {
            statusdok = 'active';
            statuspenugasan = 'active';
            fprogress = 25 + 75 * parseFloat(item.STS_PRGS)/100;
          } else {
            statusdok = '';
          }
          if (item.STATUSBAST === 'Closed') {
            statusbast = 'active';
          } else if (item.STATUSBAST === 'Rejected') {
            statuspenugasan = 'reject';
          } else {
            statusbast = '';
          }
        });

        html = '<div class="container">' +
          '<ul class="progressbar">';

        if (status === 'Pending') {
          html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" title="Pending">Request Form</li>';
        } else if (status === 'reject') {
          html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" title="Pending">Request Form</li>';
        } else {
          html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" >Request Form</li>';
        }
        html += '<li class="' + statuspenugasan + '">Assign Task</li>';
        html += '<li class="' + statusdok + '">Engineering Documents</li>';
        html += '<li class="' + statusbast + '">Finish</li>' +

          '</ul>' +
          '<button type="button" class="btn btn-dark btn-outline btn-rounded btn-sm" style="cursor:default;position: absolute;">'+ parseFloat(fprogress).toFixed(2) +' %</button>' +
          '</div>' +
          '<div class="container" style="height:400px;width:1000px;overflow-y:scroll">' +
          '<table class="table display table-bordered table-striped" id="tableprogress">' +
          '<thead>' +
          '<tr>' +
          '<th>No</th>' +
          '<th>Type</th>' +
          '<th>Status</th>' +
          '<th>Date</th>' +
          '<th>By</th>' +
          '<th style="width:100px">Note</th>' +
          '</tr>' +
          '</thead>' +
          '<tbody>';



        $.ajax({
          url: 'erf/request/getLogPengajuan',
          data: {
            id: notif
          },
          dataType: 'json',
          type: 'POST',
          beforeSend: function() {},
          success: function(result) {
            data = result;
            $.map(data, function(item, index) {
              dokeng = item.ID;
              html += '<tr>' +
                '<td>' + (index + 1) + '</td>' +
                '<td>ERF Create</td>' +

                '<td>Created</td>' +
                '<td>' + item.CREATE_AT + '</td>' +
                '<td>' + item.CREATE_BY + '</td>' +

                '<td style="width:280px">-</td>' +
                '</tr>';
              if (item.APPROVE_AT != null && item.APPROVE_BY != null) {
                html += '<tr>' +
                  '<td>' + (index + 2) + '</td>' +
                  '<td>ERF Approval</td>' +

                  '<td>Approved</td>' +
                  '<td>' + item.APPROVE_AT + '</td>' +
                  '<td>' + item.APPROVE_BY + '</td>' +
                  '<td style="width:280px">' + item.NOTE + '</td>' +
                  '</tr>';
              } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                html += '<tr>' +
                  '<td>' + (index + 2) + '</td>' +
                  '<td>ERF Approval</td>' +
                  '<td>Rejected</td>' +
                  '<td>' + item.REJECT_DATE + '</td>' +
                  '<td>' + item.REJECT_BY + '</td>' +
                  '<td style="width:280px">' + item.NOTE + '</td>' +
                  '</tr>';
              } else {
                html += '<tr>' +
                  '<td>' + (index + 2) + '</td>' +
                  '<td>ERF Approval</td>' +
                  '<td>-</td>' +
                  '<td>-</td>' +
                  '<td>-</td>' +
                  '<td style="width:280px">-</td>' +
                  '</tr>';
              }
              if (item.REJECT_FILE != null) {
                index++;
                arFile = item.REJECT_FILE.split('/');
                html += '<tr>' +
                  '<td>' + (index + 2) + '</td>' +
                  '<td>ERF Approval</td>' +
                  '<td>Rejected</td>' +
                  '<td>' + item.REJECT_DATE + '</td>' +
                  '<td>' + item.REJECT_BY + '</td>' +
                  '<td style="width:280px"><a href="'+ item.REJECT_FILE +'" rel="noopener noreferrer" target="_blank" ><i class="fa fa-file-pdf-o"></i> '+ arFile[(arFile.length - 1)] +'</a></td>' +
                  '</tr>';
              }
              if (item.CREATEAT2 != null) {
                html += '<tr>' +
                  '<td>' + (index + 3) + '</td>' +
                  '<td>EAT Create</td>' +
                  '<td>Created</td>' +
                  '<td>' + item.CREATEAT2 + '</td>' +
                  '<td>' + item.CREATEBY2 + '</td>' +
                  '<td style="width:280px">-</td>' +
                  '</tr>';
              } else {
                html += '<tr>' +
                  '<td>' + (index + 3) + '</td>' +
                  '<td>EAT Create</td>' +
                  '<td>-</td>' +
                  '<td>-</td>' +
                  '<td>-</td>' +

                  '<td style="width:280px">-</td>' +
                  '</tr>';
              }
              if (item.APPROVE0_BY != null && item.APPROVE0_AT != null) {
                html += '<tr>' +
                  '<td>' + (index + 4) + '</td>' +
                  '<td>EAT Approval MGR</td>' +
                  '<td>Approved</td>' +
                  '<td>' + item.APPROVE0_AT + '</td>' +
                  '<td>' + item.APPROVE0_BY + '</td>' +
                  '<td style="width:280px">' + item.NOTE0 + '</td>' +
                  '</tr>';
              } else if (item.REJECTAT2 != null && item.REJECTBY2 != null && item.APPROVE1_BY == item.REJECTBY2) {
                html += '<tr>' +
                  '<td>' + (index + 4) + '</td>' +
                  '<td>EAT Approval MGR</td>' +
                  '<td>Approved</td>' +
                  '<td>' + item.REJECTAT2 + '</td>' +
                  '<td>' + item.REJECTBY2 + '</td>' +
                  '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                  '</tr>';
              } else {
                html += '<tr>' +
                  '<td>' + (index + 4) + '</td>' +
                  '<td>EAT Approval MGR</td>' +
                  '<td>-</td>' +
                  '<td>-</td>' +
                  '<td>-</td>' +

                  '<td style="width:280px">-</td>' +
                  '</tr>';
              }
              if (item.APPROVE1_BY != null && item.APPROVE1_AT != null) {
                html += '<tr>' +
                  '<td>' + (index + 5) + '</td>' +
                  '<td>EAT Approval SM</td>' +
                  '<td>Approved</td>' +
                  '<td>' + item.APPROVE1_AT + '</td>' +
                  '<td>' + item.APPROVE1_BY + '</td>' +
                  '<td style="width:280px">' + item.NOTE1 + '</td>' +
                  '</tr>';
              } else if (item.REJECTAT2 != null && item.REJECTBY2 != null && item.APPROVE1_BY == item.REJECTBY2) {
                html += '<tr>' +
                  '<td>' + (index + 5) + '</td>' +
                  '<td>EAT Approval SM</td>' +
                  '<td>Approved</td>' +
                  '<td>' + item.REJECTAT2 + '</td>' +
                  '<td>' + item.REJECTBY2 + '</td>' +
                  '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                  '</tr>';
              } else {
                html += '<tr>' +
                  '<td>' + (index + 5) + '</td>' +
                  '<td>EAT Approval SM</td>' +
                  '<td>-</td>' +
                  '<td>-</td>' +
                  '<td>-</td>' +

                  '<td style="width:280px">-</td>' +
                  '</tr>';
              }
              // if (item.APPROVE2_AT != null && item.APPROVE2_BY != null) {
              //   html += '<tr>' +
              //     '<td>' + (index + 6) + '</td>' +
              //     '<td>EAT APPROVE GM</td>' +
              //     '<td>Approved</td>' +
              //     '<td>' + item.APPROVE2_AT + '</td>' +
              //     '<td>' + item.APPROVE2_BY + '</td>' +
              //
              //     '<td style="width:280px"> ' + item.NOTE2 + '</td>' +
              //     '</tr>';
              // } else if (item.REJECTAT2 != null && item.REJECTBY2 != null && item.APPROVE2_BY == item.REJECTBY2) {
              //   html += '<tr>' +
              //     '<td>' + (index + 6) + '</td>' +
              //     '<td>EAT Approval SM</td>' +
              //     '<td>Approved</td>' +
              //     '<td>' + item.REJECTAT2 + '</td>' +
              //     '<td>' + item.REJECTBY2 + '</td>' +
              //     '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
              //     '</tr>';
              // } else {
              //   html += '<tr>' +
              //     '<td>' + (index + 6) + '</td>' +
              //     '<td>EAT APPROVE GM</td>' +
              //     '<td>-</td>' +
              //     '<td>-</td>' +
              //     '<td>-</td>' +
              //
              //     '<td style="width:280px">-</td>' +
              //     '</tr>';
              // }
            });
            return html;
          },
          error: function() {

          },
          complete: function() {
            var i = 6;
            //paraf
            $.ajax({
              url: 'dok_eng/documents/get_prog_dok',
              data: {
                id: notif
              },
              dataType: 'json',
              type: 'POST',
              beforeSend: function() {},
              success: function(result) {
                data = result;
                $.map(data, function(item, index) {

                  html += '<tr>' +
                    '<td>' + (index + i) + '</td>' +
                    '<td>Document Engineering</td>' +
                    '<td>Created</td>' +
                    '<td>' + item.CREATE_AT + '</td>' +
                    '<td>' + item.CREATE_BY + '</td>' +
                    '<td style="width:280px"> ' + item.PACKET_TEXT + '</td>' +
                    '</tr>';

                  i++;

                  // if (item.APPROVE1_AT != null) {
                  //   html += '<tr>' +
                  //     '<td>' + (index + i) + '</td>' +
                  //     '<td>TTD Manager</td>' +
                  //     '<td>Approved</td>' +
                  //     '<td>' + item.APPROVE1_AT + '</td>' +
                  //     '<td>' + item.APPROVE1_BY + '</td>' +
                  //
                  //     '<td style="width:280px"> ' + item.NOTE1 + '</td>' +
                  //     '</tr>';
                  // } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                  //   html += '<tr>' +
                  //     '<td>' + (index + i) + '</td>' +
                  //     '<td>TTD Manager</td>' +
                  //     '<td>Rejected</td>' +
                  //     '<td>' + item.REJECT_DATE + '</td>' +
                  //     '<td>' + item.REJECT_BY + '</td>' +
                  //     '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                  //     '</tr>';
                  // } else {
                  //   html += '<tr>' +
                  //     '<td>' + (index + i) + '</td>' +
                  //     '<td>TTD Manager</td>' +
                  //     '<td>-</td>' +
                  //     '<td>-</td>' +
                  //     '<td>' + item.APPROVE1_BY + '</td>' +
                  //     '<td style="width:280px">-</td>' +
                  //     '</tr>';
                  // }
                  // i++;
                  //
                  // if (item.APPROVE2_AT != null) {
                  //   html += '<tr>' +
                  //     '<td>' + (index + i) + '</td>' +
                  //     '<td>TTD SM</td>' +
                  //     '<td>Approved</td>' +
                  //     '<td>' + item.APPROVE2_AT + '</td>' +
                  //     '<td>' + item.APPROVE2_BY + '</td>' +
                  //
                  //     '<td style="width:280px"> ' + item.NOTE2 + '</td>' +
                  //     '</tr>';
                  // } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                  //   html += '<tr>' +
                  //     '<td>' + (index + i) + '</td>' +
                  //     '<td>TTD SM</td>' +
                  //     '<td>Rejected</td>' +
                  //     '<td>' + item.REJECT_DATE + '</td>' +
                  //     '<td>' + item.REJECT_BY + '</td>' +
                  //     '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                  //     '</tr>';
                  // } else {
                  //   html += '<tr>' +
                  //     '<td>' + (index + i) + '</td>' +
                  //     '<td>TTD SM</td>' +
                  //     '<td>-</td>' +
                  //     '<td>-</td>' +
                  //     '<td>' + item.APPROVE2_BY + '</td>' +
                  //     '<td style="width:280px">-</td>' +
                  //     '</tr>';
                  // }
                  //
                  // i++

                  if (item.APPROVE3_AT != null) {
                    html += '<tr>' +
                      '<td>' + (index + i) + '</td>' +
                      '<td>TTD GM</td>' +
                      '<td>Approved</td>' +
                      '<td>' + item.APPROVE3_AT + '</td>' +
                      '<td>' + item.APPROVE3_BY + '</td>' +

                      '<td style="width:280px"> ' + item.NOTE3 + '</td>' +
                      '</tr>';
                  } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                    html += '<tr>' +
                      '<td>' + (index + i) + '</td>' +
                      '<td>TTD GM</td>' +
                      '<td>Rejected</td>' +
                      '<td>' + item.REJECT_DATE + '</td>' +
                      '<td>' + item.REJECT_BY + '</td>' +
                      '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                      '</tr>';
                  } else {
                    html += '<tr>' +
                      '<td>' + (index + i) + '</td>' +
                      '<td>TTD GM</td>' +
                      '<td>-</td>' +
                      '<td>-</td>' +
                      '<td>' + item.APPROVE3_BY + '</td>' +
                      '<td style="width:280px">-</td>' +
                      '</tr>';
                  }
                  i++;

                  if (item.BASTAT != null && item.BASTBY != null) {
                    html += '<tr>' +
                      '<td>' + (index + i) + '</td>' +
                      '<td>Transmital</td>' +
                      '<td>Created</td>' +
                      '<td>' + item.BASTAT + '</td>' +
                      '<td>' + item.BASTBY + '</td>' +

                      '<td style="width:280px"> Transmital Created ' + item.NO_BAST + '</td>' +
                      '</tr>';
                    i++;

                    // if (item.APPROVE1_0_AT != null) {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD SM Cust.</td>' +
                    //     '<td>Approved</td>' +
                    //     '<td>' + item.APPROVE1_0_AT + '</td>' +
                    //     '<td>' + item.APPROVE1_0_BY + '</td>' +
                    //     '<td style="width:280px"> ' + item.NOTE1_0 + '</td>' +
                    //     '</tr>';
                    // } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.APPROVE1_0_BY) {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD SM Cust.</td>' +
                    //     '<td>Rejected</td>' +
                    //     '<td>' + item.BASTREJECTAT + '</td>' +
                    //     '<td>' + item.BASTREJECTBY + '</td>' +
                    //     '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
                    //     '</tr>';
                    // } else {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD SM Cust.</td>' +
                    //     '<td>-</td>' +
                    //     '<td>-</td>' +
                    //     '<td>' + item.APPROVE1_0_BY + '</td>' +
                    //     '<td style="width:280px">-</td>' +
                    //     '</tr>';
                    // }
                    // i++;
                    //
                    // if (item.APPROVE2_0_AT != null) {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD GM Cust.</td>' +
                    //     '<td>Approved</td>' +
                    //     '<td>' + item.APPROVE2_0_AT + '</td>' +
                    //     '<td>' + item.APPROVE2_0_BY + '</td>' +
                    //     '<td style="width:280px">  ' + item.NOTE2_0 + '</td>' +
                    //     '</tr>';
                    // } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.APPROVE2_0_BY) {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD GM Cust.</td>' +
                    //     '<td>Rejected</td>' +
                    //     '<td>' + item.BASTREJECTAT + '</td>' +
                    //     '<td>' + item.BASTREJECTBY + '</td>' +
                    //     '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
                    //     '</tr>';
                    // } else {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD GM Cust.</td>' +
                    //     '<td>-</td>' +
                    //     '<td>-</td>' +
                    //     '<td>' + item.APPROVE2_0_BY + '</td>' +
                    //     '<td style="width:280px">-</td>' +
                    //     '</tr>';
                    // }
                    // i++;
                    //
                    // if (item.BAST1AT != null) {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD SM</td>' +
                    //     '<td>Approved</td>' +
                    //     '<td>' + item.BAST1AT + '</td>' +
                    //     '<td>' + item.BAST1BY + '</td>' +
                    //     '<td style="width:280px">  ' + item.BASTNOTE1 + '</td>' +
                    //     '</tr>';
                    // } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.BAST1BY) {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD SM</td>' +
                    //     '<td>Rejected</td>' +
                    //     '<td>' + item.BASTREJECTAT + '</td>' +
                    //     '<td>' + item.BASTREJECTBY + '</td>' +
                    //     '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
                    //     '</tr>';
                    // } else {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD SM</td>' +
                    //     '<td>-</td>' +
                    //     '<td>-</td>' +
                    //     '<td>' + item.BAST1BY + '</td>' +
                    //     '<td style="width:280px">-</td>' +
                    //     '</tr>';
                    // }
                    // i++;
                    //
                    // if (item.bast2AT != null) {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD GM</td>' +
                    //     '<td>Approved</td>' +
                    //     '<td>' + item.bast2AT + '</td>' +
                    //     '<td>' + item.bast2BY + '</td>' +
                    //     '<td style="width:280px">  ' + item.BASTNOTE2 + '</td>' +
                    //     '</tr>';
                    // } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.bast2BY) {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD GM</td>' +
                    //     '<td>Rejected</td>' +
                    //     '<td>' + item.BASTREJECTAT + '</td>' +
                    //     '<td>' + item.BASTREJECTBY + '</td>' +
                    //     '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
                    //     '</tr>';
                    // } else {
                    //   html += '<tr>' +
                    //     '<td>' + (index + i) + '</td>' +
                    //     '<td>TTD GM</td>' +
                    //     '<td>-</td>' +
                    //     '<td>-</td>' +
                    //     '<td>' + item.bast2BY + '</td>' +
                    //     '<td style="width:280px">-</td>' +
                    //     '</tr>';
                    // }

                  } else {

                  }

                });
                return html;
              },
              error: function() {

              },
              complete: function() {
                html +=
                  '</tbody>' +
                  '</table>' +
                  '</div>';
                // console.log(html);
                $('#search-content').html(html);
                $('.div-search').addClass('show');
                return html;
              }
            });





          }
        });

        return html;

      },
      error: function() {
        alert("Error");
      },
      complete: function() {
        return html;
      }
    });
    // $.ajax({
    //   url: 'erf/request/getApproval',
    //   data: {
    //     id: notif
    //   },
    //   dataType: 'json',
    //   type: 'POST',
    //   success: function(result) {
    //     data = result;
    //     statuspenugasan = '';
    //     statusdok = '';
    //     statusbast = '';
    //     $.map(data, function(item, index) {
    //
    //       pengajuan = item.STATUS1;
    //       penugasan = item.STATUS2;
    //       if (item.STATUS1 === 'Open') {
    //         status = 'Pending';
    //       } else if (item.STATUS1 === 'Approved') {
    //         status = 'active';
    //       } else {
    //         status = 'reject';
    //       }
    //       if (item.STATUS2 === 'In Approval') {
    //         statuspenugasan = 'pending';
    //       } else if (item.STATUS2 === 'Approved Biro.' || item.STATUS2 === 'SM Approved') {
    //         statuspenugasan = 'pending';
    //       } else if (item.STATUS2 === 'Approved Dept.' || item.STATUS2 === 'GM Approved') {
    //         statuspenugasan = 'active';
    //       } else if (item.STATUS2 === 'Rejected') {
    //         statuspenugasan = 'reject';
    //       } else {
    //         statuspenugasan = '';
    //       }
    //       if (item.DOKSTATUS === 'ada') {
    //         statusdok = 'active';
    //       } else {
    //         statusdok = '';
    //       }
    //       if (item.STATUSBAST === 'Closed') {
    //         statusbast = 'active';
    //       } else if (item.STATUSBAST === 'Rejected') {
    //         statuspenugasan = 'reject';
    //       } else {
    //         statusbast = '';
    //       }
    //     });
    //
    //     html = '<div class="container" style="margin:40px auto;">' +
    //       '<ul class="progressbar">';
    //
    //     if (status === 'Pending') {
    //       html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" title="Pending">Request Form</li>';
    //     } else if (status === 'reject') {
    //       html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" title="Pending">Request Form</li>';
    //     } else {
    //       html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" >Request Form</li>';
    //     }
    //     html += '<li class="' + statuspenugasan + '">Assign Task</li>';
    //     html += '<li class="' + statusdok + '">Engineering Documents</li>';
    //     html += '<li class="' + statusbast + '">Finish</li>' +
    //
    //       '</ul>' +
    //       '</div>' +
    //       '<div class="container" style="height:400px;width:1000px;overflow-y:scroll">' +
    //       '<table class="table display table-bordered table-striped" id="tableprogress">' +
    //       '<thead>' +
    //       '<tr>' +
    //       '<th>No</th>' +
    //       '<th>Type</th>' +
    //       '<th>Status</th>' +
    //       '<th>Date</th>' +
    //       '<th>By</th>' +
    //       '<th style="width:100px">Note</th>' +
    //       '</tr>' +
    //       '</thead>' +
    //       '<tbody>';
    //
    //
    //
    //     $.ajax({
    //       url: 'erf/request/getLogPengajuan',
    //       data: {
    //         id: notif
    //       },
    //       dataType: 'json',
    //       type: 'POST',
    //       beforeSend: function() {},
    //       success: function(result) {
    //         data = result;
    //         $.map(data, function(item, index) {
    //           dokeng = item.ID;
    //           html += '<tr>' +
    //             '<td>' + (index + 1) + '</td>' +
    //             '<td>ERF Create</td>' +
    //
    //             '<td>Created</td>' +
    //             '<td>' + item.CREATE_AT + '</td>' +
    //             '<td>' + item.CREATE_BY + '</td>' +
    //
    //             '<td style="width:280px">-</td>' +
    //             '</tr>';
    //           if (item.APPROVE_AT != null && item.APPROVE_BY != null) {
    //             html += '<tr>' +
    //               '<td>' + (index + 2) + '</td>' +
    //               '<td>ERF Approval</td>' +
    //
    //               '<td>Approved</td>' +
    //               '<td>' + item.APPROVE_AT + '</td>' +
    //               '<td>' + item.APPROVE_BY + '</td>' +
    //               '<td style="width:280px">' + item.NOTE + '</td>' +
    //               '</tr>';
    //           } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
    //             html += '<tr>' +
    //               '<td>' + (index + 2) + '</td>' +
    //               '<td>ERF Approval</td>' +
    //               '<td>Rejected</td>' +
    //               '<td>' + item.REJECT_DATE + '</td>' +
    //               '<td>' + item.REJECT_BY + '</td>' +
    //               '<td style="width:280px">' + item.NOTE + '</td>' +
    //               '</tr>';
    //           } else {
    //             html += '<tr>' +
    //               '<td>' + (index + 2) + '</td>' +
    //               '<td>ERF Approval</td>' +
    //               '<td>-</td>' +
    //               '<td>-</td>' +
    //               '<td>-</td>' +
    //               '<td style="width:280px">-</td>' +
    //               '</tr>';
    //           }
    //           if (item.CREATEAT2 != null) {
    //             html += '<tr>' +
    //               '<td>' + (index + 3) + '</td>' +
    //               '<td>EAT Create</td>' +
    //               '<td>Created</td>' +
    //               '<td>' + item.CREATEAT2 + '</td>' +
    //               '<td>' + item.CREATEBY2 + '</td>' +
    //               '<td style="width:280px">-</td>' +
    //               '</tr>';
    //           } else {
    //             html += '<tr>' +
    //               '<td>' + (index + 3) + '</td>' +
    //               '<td>EAT Create</td>' +
    //               '<td>-</td>' +
    //               '<td>-</td>' +
    //               '<td>-</td>' +
    //
    //               '<td style="width:280px">-</td>' +
    //               '</tr>';
    //           }
    //           if (item.APPROVE0_BY != null && item.APPROVE0_AT != null) {
    //             html += '<tr>' +
    //               '<td>' + (index + 4) + '</td>' +
    //               '<td>EAT Approval MGR</td>' +
    //               '<td>Approved</td>' +
    //               '<td>' + item.APPROVE0_AT + '</td>' +
    //               '<td>' + item.APPROVE0_BY + '</td>' +
    //               '<td style="width:280px">' + item.NOTE0 + '</td>' +
    //               '</tr>';
    //           } else if (item.REJECTAT2 != null && item.REJECTBY2 != null && item.APPROVE1_BY == item.REJECTBY2) {
    //             html += '<tr>' +
    //               '<td>' + (index + 4) + '</td>' +
    //               '<td>EAT Approval MGR</td>' +
    //               '<td>Approved</td>' +
    //               '<td>' + item.REJECTAT2 + '</td>' +
    //               '<td>' + item.REJECTBY2 + '</td>' +
    //               '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
    //               '</tr>';
    //           } else {
    //             html += '<tr>' +
    //               '<td>' + (index + 4) + '</td>' +
    //               '<td>EAT Approval MGR</td>' +
    //               '<td>-</td>' +
    //               '<td>-</td>' +
    //               '<td>-</td>' +
    //
    //               '<td style="width:280px">-</td>' +
    //               '</tr>';
    //           }
    //           if (item.APPROVE1_BY != null && item.APPROVE1_AT != null) {
    //             html += '<tr>' +
    //               '<td>' + (index + 5) + '</td>' +
    //               '<td>EAT Approval SM</td>' +
    //               '<td>Approved</td>' +
    //               '<td>' + item.APPROVE1_AT + '</td>' +
    //               '<td>' + item.APPROVE1_BY + '</td>' +
    //               '<td style="width:280px">' + item.NOTE1 + '</td>' +
    //               '</tr>';
    //           } else if (item.REJECTAT2 != null && item.REJECTBY2 != null && item.APPROVE1_BY == item.REJECTBY2) {
    //             html += '<tr>' +
    //               '<td>' + (index + 5) + '</td>' +
    //               '<td>EAT Approval SM</td>' +
    //               '<td>Approved</td>' +
    //               '<td>' + item.REJECTAT2 + '</td>' +
    //               '<td>' + item.REJECTBY2 + '</td>' +
    //               '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
    //               '</tr>';
    //           } else {
    //             html += '<tr>' +
    //               '<td>' + (index + 5) + '</td>' +
    //               '<td>EAT Approval SM</td>' +
    //               '<td>-</td>' +
    //               '<td>-</td>' +
    //               '<td>-</td>' +
    //
    //               '<td style="width:280px">-</td>' +
    //               '</tr>';
    //           }
    //           if (item.APPROVE2_AT != null && item.APPROVE2_BY != null) {
    //             html += '<tr>' +
    //               '<td>' + (index + 6) + '</td>' +
    //               '<td>EAT APPROVE GM</td>' +
    //               '<td>Approved</td>' +
    //               '<td>' + item.APPROVE2_AT + '</td>' +
    //               '<td>' + item.APPROVE2_BY + '</td>' +
    //
    //               '<td style="width:280px"> ' + item.NOTE2 + '</td>' +
    //               '</tr>';
    //           } else if (item.REJECTAT2 != null && item.REJECTBY2 != null && item.APPROVE2_BY == item.REJECTBY2) {
    //             html += '<tr>' +
    //               '<td>' + (index + 6) + '</td>' +
    //               '<td>EAT Approval SM</td>' +
    //               '<td>Approved</td>' +
    //               '<td>' + item.REJECTAT2 + '</td>' +
    //               '<td>' + item.REJECTBY2 + '</td>' +
    //               '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
    //               '</tr>';
    //           } else {
    //             html += '<tr>' +
    //               '<td>' + (index + 6) + '</td>' +
    //               '<td>EAT APPROVE GM</td>' +
    //               '<td>-</td>' +
    //               '<td>-</td>' +
    //               '<td>-</td>' +
    //
    //               '<td style="width:280px">-</td>' +
    //               '</tr>';
    //           }
    //         });
    //         return html;
    //       },
    //       error: function() {
    //
    //       },
    //       complete: function() {
    //         var i = 7;
    //         //paraf
    //         $.ajax({
    //           url: 'dok_eng/documents/get_prog_dok',
    //           data: {
    //             id: notif
    //           },
    //           dataType: 'json',
    //           type: 'POST',
    //           beforeSend: function() {},
    //           success: function(result) {
    //             data = result;
    //             $.map(data, function(item, index) {
    //
    //               html += '<tr>' +
    //                 '<td>' + (index + i) + '</td>' +
    //                 '<td>Document Engineering</td>' +
    //                 '<td>Created</td>' +
    //                 '<td>' + item.CREATE_AT + '</td>' +
    //                 '<td>' + item.CREATE_BY + '</td>' +
    //                 '<td style="width:280px"> ' + item.PACKET_TEXT + '</td>' +
    //                 '</tr>';
    //
    //               i++;
    //
    //               if (item.APPROVE1_AT != null) {
    //                 html += '<tr>' +
    //                   '<td>' + (index + i) + '</td>' +
    //                   '<td>TTD Manager</td>' +
    //                   '<td>Approved</td>' +
    //                   '<td>' + item.APPROVE1_AT + '</td>' +
    //                   '<td>' + item.APPROVE1_BY + '</td>' +
    //
    //                   '<td style="width:280px"> ' + item.NOTE1 + '</td>' +
    //                   '</tr>';
    //               } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
    //                 html += '<tr>' +
    //                   '<td>' + (index + i) + '</td>' +
    //                   '<td>TTD Manager</td>' +
    //                   '<td>Rejected</td>' +
    //                   '<td>' + item.REJECT_DATE + '</td>' +
    //                   '<td>' + item.REJECT_BY + '</td>' +
    //                   '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
    //                   '</tr>';
    //               } else {
    //                 html += '<tr>' +
    //                   '<td>' + (index + i) + '</td>' +
    //                   '<td>TTD Manager</td>' +
    //                   '<td>-</td>' +
    //                   '<td>-</td>' +
    //                   '<td>' + item.APPROVE1_BY + '</td>' +
    //                   '<td style="width:280px">-</td>' +
    //                   '</tr>';
    //               }
    //               i++;
    //
    //               if (item.APPROVE2_AT != null) {
    //                 html += '<tr>' +
    //                   '<td>' + (index + i) + '</td>' +
    //                   '<td>TTD SM</td>' +
    //                   '<td>Approved</td>' +
    //                   '<td>' + item.APPROVE2_AT + '</td>' +
    //                   '<td>' + item.APPROVE2_BY + '</td>' +
    //
    //                   '<td style="width:280px"> ' + item.NOTE2 + '</td>' +
    //                   '</tr>';
    //               } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
    //                 html += '<tr>' +
    //                   '<td>' + (index + i) + '</td>' +
    //                   '<td>TTD SM</td>' +
    //                   '<td>Rejected</td>' +
    //                   '<td>' + item.REJECT_DATE + '</td>' +
    //                   '<td>' + item.REJECT_BY + '</td>' +
    //                   '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
    //                   '</tr>';
    //               } else {
    //                 html += '<tr>' +
    //                   '<td>' + (index + i) + '</td>' +
    //                   '<td>TTD SM</td>' +
    //                   '<td>-</td>' +
    //                   '<td>-</td>' +
    //                   '<td>' + item.APPROVE2_BY + '</td>' +
    //                   '<td style="width:280px">-</td>' +
    //                   '</tr>';
    //               }
    //
    //               i++
    //
    //               if (item.APPROVE3_AT != null) {
    //                 html += '<tr>' +
    //                   '<td>' + (index + i) + '</td>' +
    //                   '<td>TTD GM</td>' +
    //                   '<td>Approved</td>' +
    //                   '<td>' + item.APPROVE3_AT + '</td>' +
    //                   '<td>' + item.APPROVE3_BY + '</td>' +
    //
    //                   '<td style="width:280px"> ' + item.NOTE3 + '</td>' +
    //                   '</tr>';
    //               } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
    //                 html += '<tr>' +
    //                   '<td>' + (index + i) + '</td>' +
    //                   '<td>TTD GM</td>' +
    //                   '<td>Rejected</td>' +
    //                   '<td>' + item.REJECT_DATE + '</td>' +
    //                   '<td>' + item.REJECT_BY + '</td>' +
    //                   '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
    //                   '</tr>';
    //               } else {
    //                 html += '<tr>' +
    //                   '<td>' + (index + i) + '</td>' +
    //                   '<td>TTD GM</td>' +
    //                   '<td>-</td>' +
    //                   '<td>-</td>' +
    //                   '<td>' + item.APPROVE3_BY + '</td>' +
    //                   '<td style="width:280px">-</td>' +
    //                   '</tr>';
    //               }
    //               i++;
    //
    //               if (item.BASTAT != null && item.BASTBY != null) {
    //                 html += '<tr>' +
    //                   '<td>' + (index + i) + '</td>' +
    //                   '<td>BAST</td>' +
    //                   '<td>Created</td>' +
    //                   '<td>' + item.BASTAT + '</td>' +
    //                   '<td>' + item.BASTBY + '</td>' +
    //
    //                   '<td style="width:280px"> BAST Created ' + item.NO_BAST + '</td>' +
    //                   '</tr>';
    //                 i++;
    //
    //                 if (item.APPROVE1_0_AT != null) {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD SM Cust.</td>' +
    //                     '<td>Approved</td>' +
    //                     '<td>' + item.APPROVE1_0_AT + '</td>' +
    //                     '<td>' + item.APPROVE1_0_BY + '</td>' +
    //                     '<td style="width:280px"> ' + item.NOTE1_0 + '</td>' +
    //                     '</tr>';
    //                 } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.APPROVE1_0_BY) {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD SM Cust.</td>' +
    //                     '<td>Rejected</td>' +
    //                     '<td>' + item.BASTREJECTAT + '</td>' +
    //                     '<td>' + item.BASTREJECTBY + '</td>' +
    //                     '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
    //                     '</tr>';
    //                 } else {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD SM Cust.</td>' +
    //                     '<td>-</td>' +
    //                     '<td>-</td>' +
    //                     '<td>' + item.APPROVE1_0_BY + '</td>' +
    //                     '<td style="width:280px">-</td>' +
    //                     '</tr>';
    //                 }
    //                 i++;
    //
    //                 if (item.APPROVE2_0_AT != null) {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD GM Cust.</td>' +
    //                     '<td>Approved</td>' +
    //                     '<td>' + item.APPROVE2_0_AT + '</td>' +
    //                     '<td>' + item.APPROVE2_0_BY + '</td>' +
    //                     '<td style="width:280px">  ' + item.NOTE2_0 + '</td>' +
    //                     '</tr>';
    //                 } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.APPROVE2_0_BY) {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD GM Cust.</td>' +
    //                     '<td>Rejected</td>' +
    //                     '<td>' + item.BASTREJECTAT + '</td>' +
    //                     '<td>' + item.BASTREJECTBY + '</td>' +
    //                     '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
    //                     '</tr>';
    //                 } else {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD GM Cust.</td>' +
    //                     '<td>-</td>' +
    //                     '<td>-</td>' +
    //                     '<td>' + item.APPROVE2_0_BY + '</td>' +
    //                     '<td style="width:280px">-</td>' +
    //                     '</tr>';
    //                 }
    //                 i++;
    //
    //                 if (item.BAST1AT != null) {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD SM</td>' +
    //                     '<td>Approved</td>' +
    //                     '<td>' + item.BAST1AT + '</td>' +
    //                     '<td>' + item.BAST1BY + '</td>' +
    //                     '<td style="width:280px">  ' + item.BASTNOTE1 + '</td>' +
    //                     '</tr>';
    //                 } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.BAST1BY) {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD SM</td>' +
    //                     '<td>Rejected</td>' +
    //                     '<td>' + item.BASTREJECTAT + '</td>' +
    //                     '<td>' + item.BASTREJECTBY + '</td>' +
    //                     '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
    //                     '</tr>';
    //                 } else {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD SM</td>' +
    //                     '<td>-</td>' +
    //                     '<td>-</td>' +
    //                     '<td>' + item.BAST1BY + '</td>' +
    //                     '<td style="width:280px">-</td>' +
    //                     '</tr>';
    //                 }
    //                 i++;
    //
    //                 if (item.bast2AT != null) {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD GM</td>' +
    //                     '<td>Approved</td>' +
    //                     '<td>' + item.bast2AT + '</td>' +
    //                     '<td>' + item.bast2BY + '</td>' +
    //                     '<td style="width:280px">  ' + item.BASTNOTE2 + '</td>' +
    //                     '</tr>';
    //                 } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.bast2BY) {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD GM</td>' +
    //                     '<td>Rejected</td>' +
    //                     '<td>' + item.BASTREJECTAT + '</td>' +
    //                     '<td>' + item.BASTREJECTBY + '</td>' +
    //                     '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
    //                     '</tr>';
    //                 } else {
    //                   html += '<tr>' +
    //                     '<td>' + (index + i) + '</td>' +
    //                     '<td>TTD GM</td>' +
    //                     '<td>-</td>' +
    //                     '<td>-</td>' +
    //                     '<td>' + item.bast2BY + '</td>' +
    //                     '<td style="width:280px">-</td>' +
    //                     '</tr>';
    //                 }
    //
    //               } else {
    //
    //               }
    //
    //             });
    //             return html;
    //           },
    //           error: function() {
    //
    //           },
    //           complete: function() {
    //             html +=
    //               '</tbody>' +
    //               '</table>' +
    //               '</div>';
    //             // console.log(html);
    //             $('#search-content').html(html);
    //             $('.div-search').addClass('show');
    //             return html;
    //           }
    //         });
    //
    //       }
    //     });
    //
    //     return html;
    //
    //   },
    //   error: function() {
    //     alert("Error");
    //   },
    //   complete: function() {
    //     return html;
    //   }
    // });


  }
}

$("#search-glb").on('paste', function(e){
  setTimeout(searchNotif, 500);
});

$("#search-glb").keypress(function(event) {
  setTimeout(searchNotif, 500);
});

//event notifikasi
function notification() {
  $.ajax({
    url: 'General/getNotificationPengajuan',
    data: {},
    dataType: 'json',
    type: 'POST',
    success: function(result) {
      data = result;
      $.map(data, function(item, index) {
        html = '<a href="#">' +
          '<div class="btn btn-info btn-circle m-r-10"><i class="ti-settings"></i></div>' +
          '<div class="mail-contnet">' +
          '<h5>' + item.NAMA_PEKERJAAN + '</h5> <span class="mail-desc">Document ERF with ' + item.NOTIFIKASI + ' need to be approved</span>' +
          '<span class="time">' + item.CREATE_AT + '</span>' +
          '</div></a>';
        document.getElementById("notification").innerHTML += html;
      });
      // console.log(html);
      return data;
    },
    error: function() {},
    complete: function() {}
  });
}

function getLoginSession() {
  $.ajax({
    url: 'General/getAllLogin',
    data: {},
    dataType: 'json',
    type: 'POST',
    success: function(result) {
      data = result;
      // console.log(data);
      document.getElementById("online").append(data + " online");
      return data;
    },
    error: function() {},
    complete: function() {}
  });
}

function saveSession() {
  $.ajax({
    url: 'General/saveSession',
    data: {},
    dataType: 'json',
    type: 'POST',
    success: function(result) {
      data = result;
      console.log(data);
      return data;
    },
    error: function() {},
    complete: function() {}
  });
}

function deleteSession() {
  $.ajax({
    url: 'General/deleteSession',
    data: {},
    dataType: 'json',
    type: 'POST',
    success: function(result) {
      data = result;
      console.log(data);
      return data;
    },
    error: function() {;
    },
    complete: function() {}
  });
}
