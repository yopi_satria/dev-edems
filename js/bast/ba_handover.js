var oTable;

$(document).ready(function() {
  // reset_form();
  // getForeign();
  tb_list();
  $('.select2').select2();
  if ($('#ID_DOK_ENG').val()) {
    onForeign();
    onChange();
  }
  setTrnText();
  // generateDoc();
  // setAtasan();
  // reloadtabel();
});

function setActiveTab(ul_id = null, content_id = null) {
  $(".nav-link").removeClass("active show");
  $('#' + ul_id).addClass('active show');
  $(".tab-pane").removeClass("active show");
  $('#' + content_id).addClass('active show');
}

function tb_list() {
  oTable = $('#tb_list').DataTable({
    destroy: true,
    processing: true,
    serverSide: true,
    ajax: {
      url: 'bast/handover/data_list',
      type: "POST"
    },
    columns: [{
        "className": 'details-control',
        "orderable": false,
        "data": null,
        "defaultContent": ''
      },
      {
        "data": "CREATE_AT",
        "width": 75
      },
      {
        "data": "NO_BAST"
      },
      {
        "data": "PACKET_TEXT"
      },
      {
        "data": "DESCPSECTION"
      },
      {
        "data": "FL_TEXT"
      },
      {
        "data": "COMP_TEXT"
      },
      {
        "data": "STATUS",
        "mRender": function(row, data, index) {
          if (index['STATUS'].indexOf('Approved') != -1) {
            return '<center><p style="color: green">'+ index['STATUS'] +' </p></center> ';
          } else if (index['STATUS'].indexOf('Closed') != -1) {
            return '<center><p style="color: orange">'+ index['STATUS'] +' </p></center>';
          } else if (index['STATUS'].indexOf('Rejected') != -1) {
            return '<center><p style="color: red">'+ index['STATUS'] +' </p></center>';
          } else {
            return '<center><p style="color: blue">'+index['STATUS']+' </p></center>';
            // return '';
          }
        }
      },
      // {
      //   "data": "CREATE_BY"
      // },
      {
        "data": "ID",
        "width": 70,
        "mRender": function(row, data, index) {
          sresend = '';
          sresend = '<button class="btnResend btn btn-xs btn-success" data-toggle="tooltip" title="Resend Email"><i class="fa fa-envelope-square"></i></button>';
          // if (!index['APPROVE1_AT'] || !index['APPROVE2_AT']) {
          //   sresend = '<button class="btnResend btn btn-xs btn-success" data-toggle="tooltip" title="Resend Email"><i class="fa fa-envelope-square"></i></button>';
          // }
          sdis = '';
          if (page_state.indexOf(page_short+'-delete=1') == -1 || index['STATUS'].indexOf('For Approval') == -1) {
            sdis = 'disabled';
          }
          sexp = '';
          if (page_state.indexOf(page_short+'-export=1') == -1) {
            sexp = ' style="display:none;"';
          }
          // return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Export"'+sexp+' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-primary" data-toggle="tooltip" title="Update" ><i class="fa fa-pencil"></i></button><button class="btnDel btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></button></div></center>';
          return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Export"'+sexp+' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-primary" data-toggle="tooltip" title="View" ><i class="fa fa-eye"></i></button>' + sresend + '<button class="btnDel btn btn-xs btn-danger '+page_short+'-delete '+sdis+'" data-toggle="tooltip" title="Delete" '+sdis+'><i class="fa fa-trash"></i></button></div></center>';
        }
      },
    ],
    columnDefs: [
        { "visible": false, "targets": 0 }
    ],
    "order": [
      [6, "desc"]
    ]
  });
  $('#tb_list tbody').on('click', 'td.details-control', function() {
    var tr = $(this).closest('tr');
    var row = oTable.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    } else {

      var html = '';
      var d = row.data();
      var data;
      var approveAt = '';
      var status = '';
      var i = 0;

      html += '<div class="container" style="width:1000px;height:250px;overflow-y:scroll">' +
        '<table class="table display table-bordered table-striped" id="tableprogress">' +
        '<thead>' +
        '<tr>' +
        '<th>No</th>' +
        '<th>Type</th>' +
        '<th>Date</th>' +
        '<th>By</th>' +
        '<th style="width:100px">Note</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>';

      //paraf
      $.ajax({
        url: 'bast/handover/get_record',
        data: {
          id: d.ID
        },
        dataType: 'json',
        type: 'POST',
        beforeSend: function() {},
        success: function(result) {
          data = result;
          $.map(data, function(item, index) {

            if (item.APPROVE1_AT == null) {
              item.APPROVE1_AT = '-';
            }
            if (item.APPROVE2_AT == null) {
              item.APPROVE2_AT = '-';
            }
            if (item.APPROVE1_0_AT == null) {
              item.APPROVE1_0_AT = '-';
            }
            if (item.APPROVE2_0_AT == null) {
              item.APPROVE2_0_AT = '-';
            }
            if (item.NOTE1 == null) {
              item.NOTE1 = '-';
            }
            if (item.NOTE2 == null) {
              item.NOTE2 = '-';
            }
            if (item.NOTE1_0 == null) {
              item.NOTE1_0 = '-';
            }
            if (item.NOTE2_0 == null) {
              item.NOTE2_0 = '-';
            }

            html += '<tr>' +
              '<td>' + (index + 1) + '</td>' +
              '<td>' + item.APPROVE1_0_JAB + '</td>' +
              //if(item.UPDATE_AT != null) {
              '<td>' + item.APPROVE1_0_AT + '</td>' +
              // }else {
              //   '<td>-</td>' +
              // }
              '<td>' + item.APPROVE1_0_BY + '</td>' +
              '<td style="width:280px">' + item.NOTE1_0 + '</td>' +
              '</tr>';
            html += '<tr>' +
              '<td>' + (index + 2) + '</td>' +
              '<td>' + item.APPROVE2_0_JAB + '</td>' +
              //if(item.UPDATE_AT != null) {
              '<td>' + item.APPROVE2_0_AT + '</td>' +
              // }else {
              //   '<td>-</td>' +
              // }
              '<td>' + item.APPROVE1_0_BY + '</td>' +
              '<td style="width:280px">' + item.NOTE2_0 + '</td>' +
              '</tr>';
            html += '<tr>' +
              '<td>' + (index + 3) + '</td>' +
              '<td>' + item.APPROVE1_JAB + '</td>' +
              //if(item.UPDATE_AT != null) {
              '<td>' + item.APPROVE1_AT + '</td>' +
              // }else {
              //   '<td>-</td>' +
              // }
              '<td>' + item.APPROVE1_BY + '</td>' +
              '<td style="width:280px">' + item.NOTE1 + '</td>' +
              '</tr>';
            html += '<tr>' +
              '<td>' + (index + 4) + '</td>' +
              '<td>' + item.APPROVE2_JAB + '</td>' +
              //if(item.UPDATE_AT != null) {
              '<td>' + item.APPROVE2_AT + '</td>' +
              // }else {
              //   '<td>-</td>' +
              // }
              '<td>' + item.APPROVE2_BY + '</td>' +
              '<td style="width:280px">' + item.NOTE2 + '</td>' +
              '</tr>';
            i++;
          });
          return html;
        },
        error: function() {

        },
        complete: function() {
          html +=
            '</tbody>' +
            '</table>' +
            '</div>';
          // console.log(html);
          row.child(html).show();
          tr.addClass('shown');
          return html;
        }
      });
    }
  });
}

function getForeign() {
  $.ajax({
    type: 'POST',
    url: 'bast/handover/foreign',
    success: function(json) {
      json = $.parseJSON(json);
      $('#ID_DOK_ENG').html('');
      foreigns = '';
      if (json) {
        if (json['status'] == 200) {
          // foreigns = ('<option value >Please select...</option>');
          $.each(json['data'], function(i, data) {
            sAdd='';
            if (i==0) {
              sAdd='Selected';
            }
            foreigns += ('<option value="' + data['ID_DOK_ENG'] + '" title="' + data['NO_DOK_ENG'] + '" data-subtext="' + data['NO_DOK_ENG'] + '" data-no="' + data['NO_DOK_ENG'] + '" data-notif="' + data['NOTIFIKASI'] + '" data-packet="' + data['PACKET_TEXT'] + '" data-const="' + data['DE_CONSTRUCT_COST'] + '" data-eng="' + data['DE_ENG_COST'] + '" '+sAdd+' >' + data['NO_DOK_ENG'] + ' - ' + data['PACKET_TEXT'] + '</option>');
          });
        } else {
          foreigns = ('<option value >Empty...</option>');
        }
        $('#ID_DOK_ENG').html(foreigns);
        $('.select2').select2();
        // console.log('foreign');
        onForeign();
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });
}

function onForeign() {
  $('#NO_DOK_ENG').val($('#ID_DOK_ENG :selected').attr('data-no'));
  $('#NOTIFIKASI').val($('#ID_DOK_ENG :selected').attr('data-notif'));
  $('#PACKET_TEXT').val($('#ID_DOK_ENG :selected').attr('data-packet'));
  $('#DE_CONSTRUCT_COST').val($('#ID_DOK_ENG :selected').attr('data-const'));
  $('#DE_ENG_COST').val($('#ID_DOK_ENG :selected').attr('data-eng'));
  // console.log($('#ID_DOK_ENG :selected').attr('data-no'));
  onChange();
}

function onChange() {
  var no_penugasan = $("#NO_BAST").val();
  var arr = no_penugasan.split("/");
  var pengajuan = $("#NO_DOK_ENG").val();
  // console.log(pengajuan);
  var arr2 = pengajuan.split("/");
  $("#NO_BAST").val(arr[0] + "/" + arr2[0] + "/" + arr2[1] + "/" + arr[3] + "/" + arr[4]);
}

function setTrnText() {
  // set terbilang for date
  var d = new Date();
  yr = d.getFullYear(),
    month = d.getMonth() < 10 ? '0' + d.getMonth()+1 : d.getMonth()+1,
    day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate(),
    tanggal = yr + '-' + month + '-' + day;
    // console.log(d.getMonth().toString());
  sHari = toTitleCase(GetDateName((d.getDay().toString())));
  sBulan = toTitleCase(GetMonthName((d.getMonth()+1)));
  tglNumber = toTitleCase(terbilang(d.getDate()));
  sTahun = toTitleCase(terbilang(d.getFullYear()));
  hdr = "Pada hari ini " + sHari + ", tanggal " + tglNumber + ", bulan " + sBulan + ", tahun " + sTahun + " (" + tanggal + ") yang bertandatangan di bawah ini :";
  $('#HDR_TEXT').val(hdr);

  mid = "Bahwa kajian engineering untuk Investasi PT Semen Indonesia (Persero) Tbk, berikut ini :";
  $('#MID_TEXT').val(mid);
  ftr = "Dinyatakan selesai 100% dan diserahterimakan kepada unit kerja sesuai dengan rencana.\n\nDemikian Berita Acara Serah Terima ini dibuat dengan sebenar-benarnya untuk dapat dipergunakan sebagaimana mestinya.";
  $('#FTR_TEXT').val(ftr);
}

$('#ID_DOK_ENG').on('change', function() {
  onForeign();
  onChange();
})

function reset_form(reset = null) {
  $('#ID').val('');
  $('#NO_BAST').val('');
  $('#ID_DOK_ENG').html('');
  $('#NO_DOK_ENG').val('');
  $('#NOTIFIKASI').val('');
  $('#PACKET_TEXT').val('');

  generateDoc();
  setTrnText();

  $("#btn-save").removeClass("disabled");
  $("#btn-save").prop("disabled", false);
  $("#btn-save").html(' <i class="fa fa-check"></i> Save');
  $('.selectpicker').selectpicker('refresh');
  $('#ul-new').text('Create BAST');
  $('#btn-save').show();
  // console.log($('#NO_BAST').val());

  if (reset) {
    setActiveTab('ul-list', 'list-form');
  }
  getForeign();

}

$("#btn-cancel").on("click", function() {
  reset_form();
});

// export
$(document).on('click', ".btnExport", function() {
  var data = oTable.row($(this).parents('tr')).data();

  window.location.assign('bast/handover/export_pdf/' + data['ID']);
});

// event resend mail
$(document).on('click', ".btnResend", function() {
  $('.title_ukur').text('Resend Email');
  var data = oTable.row($(this).parents('tr')).data();


  var form_data = new FormData();
  form_data.append('ID', data['ID']);

  $.ajax({
    url: 'bast/handover/resend_trans', // point to server-side PHP script
    // url: 'bast/handover/resend', // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Resend Email : NOT Success! " + error,
        type: "error"
      });
      // $("#btn-save").removeClass("disabled");
      // $("#btn-save").val("Save");
    },
    success: function(json) {
      if (json['status'] == 200) {
        // tb_request();
        // tb_erf2();
        swal({
          title: "Email was sent.",
          type: "success"
        });
        reset_form('yes');
      }
      $("#btn-save").removeClass("disabled");
      // $("#btn-save").val("Save");
    }
  });
});

// view
$(document).on('click', ".btnEdit", function() {
  var data = oTable.row($(this).parents('tr')).data();
  $('#ID').val(data['ID']);
  $('#NO_BAST').val(data['NO_BAST']);
  if (data['ID_DOK_ENG']) {
    $('#ID_DOK_ENG').html('<option value="' + data['ID_DOK_ENG'] + '" title="' + data['NO_DOK_ENG'] + '" data-subtext="' + data['NO_DOK_ENG'] + '" data-no="' + data['NO_DOK_ENG'] + '" data-notif="' + data['NOTIFIKASI'] + '" data-packet="' + data['PACKET_TEXT'] + '" >' + data['NO_DOK_ENG'] + ' - ' + data['PACKET_TEXT'] + '</option>');
  }
  $('#NO_DOK_ENG').val(data['NO_DOK_ENG']);
  $('#NOTIFIKASI').val(data['NOTIFIKASI']);
  $('#PACKET_TEXT').val(data['PACKET_TEXT']);
  $('#HDR_TEXT').val(data['HDR_TEXT']);
  $('#MID_TEXT').val(data['MID_TEXT']);
  $('#FTR_TEXT').val(data['FTR_TEXT']);
  $('#DE_CONSTRUCT_COST').val(toCurrency(data['DE_CONSTRUCT_COST']));
  $('#DE_ENG_COST').val(toCurrency(data['DE_ENG_COST']));

  $('.select2').select2();
  $('.selectpicker').selectpicker('refresh');
  setActiveTab('ul-new', 'new-form');
  $('#btn-save').html(' <i class="fa fa-check"></i> Edit');
  $('#ul-new').text('View BAST');
  if (page_state.indexOf(page_short+'-modify=1') == -1) {
    $('.'+page_short+'-modify').hide();
  }
});

// save
$("#fm-new").submit(function(e) {
  e.preventDefault();
  $("#btn-save").prop("disabled", false);
  $("#btn-save").addClass("disabled");

  id = $('#ID').val();
  no_bast = $('#NO_BAST').val();
  id_dok_eng = $('#ID_DOK_ENG').val();
  notifikasi = $('#NOTIFIKASI').val();
  hdr_text = $('#HDR_TEXT').val();
  mid_text = $('#MID_TEXT').val();
  ftr_text = $('#FTR_TEXT').val();
  packet_text = $('#PACKET_TEXT').val();

  var formURL = "";

  var form_data = new FormData();
  form_data.append('ID', id);
  form_data.append('NO_BAST', no_bast);
  form_data.append('ID_DOK_ENG', id_dok_eng);
  form_data.append('NOTIFIKASI', notifikasi);
  form_data.append('HDR_TEXT', hdr_text);
  form_data.append('MID_TEXT', mid_text);
  form_data.append('FTR_TEXT', ftr_text);
  form_data.append('PACKET_TEXT', packet_text);

  var uri = 'create';
  if (id) {
    uri = 'update';
  }

  $.ajax({
    url: 'bast/handover/' + uri, // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Data Save : NOT Success! " + error,
        type: "error"
      });
      $("#btn-save").removeClass("disabled");
    },
    success: function(json) {
      if (json['status'] == 200) {
        tb_list();
        swal({
          title: "Data Saved!\n" + no_bast,
          type: "success"
        });
        reset_form('yes');
      }
      $("#btn-save").removeClass("disabled");
    }
  });
  return false;
});

// edit

// delete
$(document).on('click', ".btnDel", function() {
  var data = oTable.row($(this).parents('tr')).data();
  swal({
      title: "Are you sure?",
      text: "Data (" + data['NO_BAST'] + ") will be deleted!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Delete",
      confirmButtonClass: "btn-danger",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        url = 'bast/handover/delete/' + data['ID'];
        $.ajax({
          url: url, // point to server-side PHP script
          dataType: 'json', // what to expect back from the PHP script, if anything
          cache: false,
          contentType: false,
          processData: false,
          type: 'get',
          error: function(xhr, status, error) {
            swal("Error", "Your data failed to delete!", "error");
          },
          success: function(json) {
            if (json['status'] == 200) {
              tb_list();
              swal("Deleted!", "Your data has been deleted.", "success");
            }
          }
        });
      } else {
        swal("Cancelled", "Deletion of data canceled", "error");
      }
    });
});

function generateDoc() {
  var tahun;
  var lokasi;
  var Nomor;
  $.ajax({
    type: 'POST',
    url: 'bast/handover/generateNoDoc',
    //data:{'id':id},
    success: function(data) {
      // console.log("test " + data);
      try {
        data = JSON.parse(data);
        Nomor = data.nomor;
        tahun = data.tahun;

        $("#NO_BAST").val(tahun + "/" + "/" + "/" + "BA/" + Nomor);

      } catch (e) {
        alert(e);
      } finally {

      }

    },
    error: function() {
      console.log("error");
    }
  });
}
