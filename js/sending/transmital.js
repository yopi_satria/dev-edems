//AJAX SELECTPICKER CT
$(".selectpicker").selectpicker().filter('#in_mail').ajaxSelectPicker({
  "ajax": {
    "url": 'user/search?company=' + localStorage.company,
    "type": "GET",
    "dataType": "json",
    "data": {
      q: "{{{q}}}",
    }
  },
  "log": false,
  "preprocessData": function(data) {
    var i, l = data.length,
      array = [];
    if (l) {
      for (i = 0; i < l; i++) {
        // a = moment();
        // b = moment(data[i].TGL_LAHIR);
        // years = a.diff(b, 'year');
        // umur = birthday.diff(moment(data[i].TGL_LAHIR), 'years');
        array.push({
          "value": data[i].NOBADGE,
          "text": '[' + data[i].NOBADGE + '] ' + data[i].NAMA,
          "data": {
            "nama": data[i].NAMA,
            // "umur": years,
            "alamat": data[i].ALAMAT,
            "uk_kode": data[i].UNIT_KERJA,
            "uk_nama": data[i].UK_TEXT,
            "jbt_kode": data[i].POSITION,
            "jbt_nama": data[i].POS_TEXT,
            "wkt_msuk": data[i].TGL_MASUK,
            "mail": data[i].EMAIL,
            "upah_skrng": '',
          }
        });
      }
    }
    return array;
  }
});

//=======================================================================================================================
//ADD DATA TO TABLE EMAIL ===============================================================================================
//=======================================================================================================================

// $(document).on('click', "#tampung_email_btn", function() {
//   id_acd = $('#tampung_email_btn').attr('id_acd');
//   type_mail = $('#in_type_mail option:selected').val();
//   // ver_code = generateId();
//   var list_mail = [];
//   var jml_row = $('#tb_mail >tbody >tr').length;
//   $('#in_mail :selected').each(function(i, item) {
//     list_mail.push({
//       'type': type_mail == null ? '' : type_mail,
//       'mail': $(item).attr('data-mail') == null ? '' : $(item).attr('data-mail'),
//       'opsi': '<center><button class="btn btn-danger btn-sm" id="remove_mail"><i class="fa fa-trash"></i></button></center>'
//     });
//   });
//   if (list_mail.length > 0) {
//     $("#tb_mail").DataTable().destroy();
//     table = $("#tb_mail").DataTable({
//       columns: [{
//           data: 'type'
//         },
//         {
//           data: 'mail'
//         },
//         {
//           data: 'opsi'
//         }
//       ]
//     });
//     table.rows.add(list_mail).draw();
//   }
//   // $('.selectpicker').selectpicker('deselectAll');
//   $('.selectpicker').val('default');
//   // $('.selectpicker').selectpicker('val','');
//   list_mail.length = 0;
// });

$("#tampung_email_btn").on("click", function() {
  console.log($('#in_type_mail option:selected').val());
  id_acd = $('#tampung_email_btn').attr('id_acd');
  type_mail = $('#in_type_mail option:selected').val();
  // ver_code = generateId();
  var list_mail = [];
  var jml_row = $('#tb_mail >tbody >tr').length;
  $('#in_mail :selected').each(function(i, item) {
    list_mail.push({
      'type': type_mail == null ? '' : type_mail,
      'mail': $(item).attr('data-mail') == null ? '' : $(item).attr('data-mail'),
      'opsi': '<center><button class="btn btn-danger btn-xs" id="remove_mail"><i class="fa fa-trash"></i></button></center>'
    });
  });
  if (list_mail.length > 0) {
    $("#tb_mail").DataTable().destroy();
    table = $("#tb_mail").DataTable({
      columns: [{
          data: 'type'
        },
        {
          data: 'mail'
        },
        {
          data: 'opsi'
        }
      ]
    });
    table.rows.add(list_mail).draw();
  }
  $('.selectpicker').selectpicker('deselectAll');
  list_mail.length = 0;
});

$(".save_email_btn").on("click", function() {
  id_acd = $('#tampung_email_btn').attr('id_acd');
  tb_row = $('#tb_mail >tbody >tr').length;
  var table = document.getElementById("tb_mail");
  var tableArr = [];
  var isi_tb_mail = '';
  $('#tb_mail tr td').each(function() {
    isi_tb_mail += $(this).html();
  });
  if (isi_tb_mail != 'No data available in table') {
    url_param = window.location.href.split('/');
    for (var i = 1; i < table.rows.length; i++) {
      tableArr.push({
        ID_DOC: id_acd,
        GROUP_MENU: url_param.pop(),
        TIPE: table.rows[i].cells[0].innerHTML,
        NOTIF: 'Kecelakaan Kerja',
        EMAIL: table.rows[i].cells[1].innerHTML,
        COMPANY: localStorage.company,
        PLANT: localStorage.plant
      });
    }
    var mMenu = (window.location.href).split('/');
    var form_data = new FormData();
    form_data.append('token', localStorage.token_user);
    form_data.append('ALL_MAIL', JSON.stringify(tableArr));
    form_data.append('MENU', mMenu[(mMenu.length - 1)]);
    form_data.append('NOTIF', 'Temuan Kondisi Tidak Aman');
    form_data.append('COMPANY', localStorage.company);
    form_data.append('PLANT', localStorage.plant);
    form_data.append('ID_DOC', id_acd);
    $.ajax({
      url: base_url_api + 'safety/recap/sendemail', //MASIH KOSOONG
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      error: function(xhr, status, error) {
        swal({
          title: "Data Save : NOT Success! ",
          type: "error"
        });
      },
      success: function(json) {
        if (json['status'] == 200) {
          $("#tb_mail").DataTable().clear().draw();
          swal({
            title: "Data Saved : Success!",
            type: "success"
          });
        }
      }
    });
  } else {
    swal({
      title: "Minimal 1 data untuk bisa melakukan pengiriman email!",
      type: "error"
    });
  }
});

$(document).on("click", "#remove_mail", function(e) {
  var table = $('#tb_mail').DataTable();
  table.row($(this).parents('tr')).remove().draw();
});
