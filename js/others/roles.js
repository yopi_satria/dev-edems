var oTable;

$(document).ready(function() {
  reset_form();
  tb_roles();
  $("#tb_forms").dataTable({
    "ordering": false,
    aLengthMenu: [
      [10, 25, 50, 100, 200, -1],
      [10, 25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1
  });
});

function setActiveTab(ul_id = null, content_id = null) {
  $(".nav-link").removeClass("active show");
  $('#' + ul_id).addClass('active show');
  $(".tab-pane").removeClass("active show");
  $('#' + content_id).addClass('active show');
}

function tb_roles() {
  oTable = $('#tb_roles').DataTable({
    destroy: true,
    processing: true,
    serverSide: true,
    // scrollY: "300px",
    // scrollX: true,
    // scrollCollapse: true,
    // paging: false,
    // fixedColumns: {
    //   leftColumns: 2
    // },
    ajax: {
      url: 'others/roles/data_list',
      type: "POST"
    },
    columns: [{
        "data": "NAME"
      },
      {
        "data": "DESCRIPTION"
      },
      {
        "data": "SPECIAL",
        "width": 120
      },
      {
        "data": "CREATE_BY",
        "width": 70
      },
      {
        "data": "CREATE_AT",
        "width": 75
      },
      {
        "data": "ID",
        "width": 40,
        "mRender": function(row, data, index) {
          sdis = '';
          if (page_state.indexOf(page_short + '-delete=1') == -1) {
            sdis = 'disabled';
          }
          return '<center><div style="float: none;" class="btn-group"><button class="btnEdit btn btn-xs btn-primary" data-toggle="tooltip" title="Update" ><i class="fa fa-pencil"></i></button><button class="btnDel btn btn-xs btn-danger ' + page_short + '-delete ' + sdis + '" data-toggle="tooltip" title="Delete" ' + sdis + '><i class="fa fa-trash"></i></button></div></center>';
        }
      },
    ],
    "order": [
      [4, "desc"]
    ]
  });
}

function reset_form(reset = null) {
  $('#ID').val('');
  $('#NAME').val('');
  $('#DESCRIPTION').val('');
  $('.special').prop('checked', false);
  // $('[name="cekval[]"]').removeAttr('checked');
  $('[name="cekval[]"]').prop('checked', false);

  $("#btn-save").removeClass("disabled");
  $("#btn-save").html(' <i class="fa fa-check"></i> Save');
  $('.selectpicker').selectpicker('refresh');
  $('#ul-new').text('Create Roles');

  if (reset) {
    setActiveTab('ul-list', 'list-form');
  }

}

$("#btn-cancel").on("click", function() {
  reset_form();
});

// export
// $(document).on('click', ".btnExport", function() {
//   var data = oTable.row($(this).parents('tr')).data();
//
//   window.location.assign('others/roles/export_pdf/' + data['ID']);
// });

// view
$(document).on('click', ".btnEdit", function() {
  var data = oTable.row($(this).parents('tr')).data();
  reset_form();

  $('#ID').val(data['ID']);
  $('#NAME').val(data['NAME']);
  $('#DESCRIPTION').val(data['DESCRIPTION']);
  if (data['SPECIAL'] == 'true') {
    $('.special').prop('checked', true);
  } else {
    $('.special').prop('checked', false);
  }
  $.each(data['DTL'], function(index, value) {
    var dTable = $("#tb_forms").dataTable();
    $("input:checkbox", dTable.fnGetNodes()).each(function() {
      // var cRow = $(this).is(":checked");
      var row = $(this).closest('tr');
      console.log(row.find('[name="cekval[]"]').attr("value") + ' : ' + value['FORM_ID'] + '-' + value['PERMISSION_ID']);
      var sval = value['FORM_ID'] + '-' + value['PERMISSION_ID'];
      if (row.find('[name="cekval[]"]').attr("value") == sval) {
        // row.find('[name="cekval[]"]').attr('checked', 'checked');
        row.find('[name="cekval[]"]').prop('checked', true);;
      }
    });
  });

  $('#btn-save').show();
  setActiveTab('ul-new', 'new-form');
  $('#btn-save').html(' <i class="fa fa-check"></i> Edit');
  $('#ul-new').text('Edit Roles');
  if (page_state.indexOf(page_short+'-modify=1') == -1) {
    $('.'+page_short+'-modify').hide();
  }
});

// save
$("#fm-new").submit(function(e) {
  e.preventDefault();
  $("#btn-save").addClass("disabled");

  id = $('#ID').val();
  name = $('#NAME').val();
  description = $('#DESCRIPTION').val();
  if ($('.special').is(':checked')) {
    special = '1';
  } else {
    special = '0';
  }

  var arDtl = [];
  var dTable = $("#tb_forms").dataTable();
  $("input:checkbox", dTable.fnGetNodes()).each(function() {
    var cRow = $(this).is(":checked");
    var row = $(this).closest('tr');
    if (cRow) {
      id_param = row.find('[name="cekval[]"]').attr("value");
      arDtl.push(id_param);
    }
  });

  var formURL = "";

  var form_data = new FormData();
  form_data.append('ID', id);
  form_data.append('NAME', name);
  form_data.append('DESCRIPTION', description);
  form_data.append('SPECIAL', special);
  form_data.append('DTL', JSON.stringify(arDtl));

  var uri = 'create';
  if (id) {
    uri = 'update';
  }

  $.ajax({
    url: 'others/roles/' + uri, // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    // data: {
    //   "ID":id,
    //   "NAME":name,
    //   "DESCRIPTION":description,
    //   "SPECIAL":special,
    //   "DTL":arDtl
    // },
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Data Save : NOT Success! " + error,
        type: "error"
      });
      $("#btn-save").removeClass("disabled");
    },
    success: function(json) {
      if (json['status'] == 200) {
        tb_roles();
        swal({
          title: "Data Saved!\n" + name,
          type: "success"
        });
        reset_form('yes');
      }
      $("#btn-save").removeClass("disabled");
    }
  });
  return false;
});

// edit

// delete
$(document).on('click', ".btnDel", function() {
  var data = oTable.row($(this).parents('tr')).data();
  swal({
      title: "Are you sure?",
      text: "Data (" + data['DESCRIPTION'] + ") will be deleted!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Delete",
      confirmButtonClass: "btn-danger",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        url = 'others/roles/delete/' + data['ID'];
        $.ajax({
          url: url, // point to server-side PHP script
          dataType: 'json', // what to expect back from the PHP script, if anything
          cache: false,
          contentType: false,
          processData: false,
          type: 'get',
          error: function(xhr, status, error) {
            swal("Error", "Your data failed to delete!", "error");
          },
          success: function(json) {
            if (json['status'] == 200) {
              tb_roles();
              swal("Deleted!", "Your data has been deleted.", "success");
            }
          }
        });
      } else {
        swal("Cancelled", "Deletion of data canceled", "error");
      }
    });
});
