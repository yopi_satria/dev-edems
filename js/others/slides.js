var oTable;

$(document).ready(function() {
  reset_form();
  tb_slides();
  $("#tb_forms").dataTable({
    "ordering": false,
    aLengthMenu: [
        [10, 25, 50, 100, 200, -1],
        [10, 25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1
  });
  $('.select2').select2();
});

function setActiveTab(ul_id = null, content_id = null) {
  $(".nav-link").removeClass("active show");
  $('#' + ul_id).addClass('active show');
  $(".tab-pane").removeClass("active show");
  $('#' + content_id).addClass('active show');
}

function tb_slides() {
  oTable = $('#tb_slides').DataTable({
    destroy: true,
    processing: true,
    serverSide: true,
    // scrollY: "300px",
    // scrollX: true,
    // scrollCollapse: true,
    // paging: false,
    // fixedColumns: {
    //   leftColumns: 2
    // },
    ajax: {
      url: 'others/slides/data_list',
      type: "POST"
    },
    columns: [{
        "data": "NO"
      },
      {
        "data": "SHORT_DESC"
      },
      {
        "data": "DESCRIPTION",
      },
      {
        "data": "CREATE_BY",
      },
      {
        "data": "CREATE_AT",
      },
      {
        "data": "id",
        "mRender": function(row, data, index) {
          sdis = '';
          if (page_state.indexOf(page_short+'-delete=1') == -1) {
            sdis = 'disabled';
          }
          return '<center><div style="float: none;" class="btn-group"><button class="btnEdit btn btn-xs btn-primary" data-toggle="tooltip" title="Update" ><i class="fa fa-pencil"></i></button><button class="btnDel btn btn-xs btn-danger '+page_short+'-delete '+sdis+'" data-toggle="tooltip" title="Delete" '+sdis+'><i class="fa fa-trash"></i></button></div></center>';
        }
      },
    ],
    "order": [
      [4, "asc"]
    ]
  });
}

function reset_form(reset = null) {
  // $('#ID').val('');
  // $('#NAME').val('');
  // $('#DESCRIPTION').val('');
  // $('.special').prop('checked', false);
  // $('[name="cekval[]"]').removeAttr('checked');
  // $('[name="cekval[]"]').prop('checked', false);

  $("#fm-new")[0].reset();
  $('#file_uri').attr('href', '');
  $('#file_name').text('');
  $('#file_name').removeClass('fa fa-file-pdf-o');
  // $(".multifile_container").html('');
  $("#btn-save").removeClass("disabled");
  $("#btn-save").html(' <i class="fa fa-check"></i> Save');
  $('.selectpicker').selectpicker('refresh');
  $('#ul-new').text('Create Slide');

  if (page_state.indexOf(page_short+'-write=1') == -1) {
    $('.'+page_short+'-write').hide();
  }
  // $("#btn-save").hide();

  if (reset) {
    setActiveTab('ul-list', 'list-form');
  }

}

$("#btn-cancel").on("click", function() {
  reset_form();
});

// export
// $(document).on('click', ".btnExport", function() {
//   var data = oTable.row($(this).parents('tr')).data();
//
//   window.location.assign('others/slides/export_pdf/' + data['ID']);
// });

// view
$(document).on('click', ".btnEdit", function() {
  var data = oTable.row($(this).parents('tr')).data();
  reset_form();

  $('#ID').val(data['ID']);
  $('#TIPE').val(data['TIPE']);
  $('#SHORT_DESC').val(data['SHORT_DESC']);
  $('#DESCRIPTION').val(data['DESCRIPTION']);
  if (data['PATHS']) {
    var aFiles = data['PATHS'].split('/');

    $('#file_uri').attr('href', data['PATHS']);
    $('#file_name').text(' ' + aFiles[(aFiles.length - 1)]);
    $('#file_name').addClass('fa fa-file-pdf-o');
  }

  $('.select2').select2();
  $('#btn-save').show();
  setActiveTab('ul-new', 'new-form');
  $('#btn-save').html(' <i class="fa fa-check"></i> Edit');
  $('#ul-new').text('Edit User');
  if (page_state.indexOf(page_short+'-modify=1') == -1) {
    $('.'+page_short+'-modify').hide();
  }
});

// save
$("#fm-new").submit(function(e) {
  e.preventDefault();
  $("#btn-save").addClass("disabled");

  id = $('#ID').val();
  tipe = $('#TIPE').val();
  short_desc = $('#SHORT_DESC').val();
  description = $('#DESCRIPTION').val();
  strpath = document.getElementById('FILES').files[0];
  strfile = $('#FILES').attr('data-url');

  var formURL = "";

  var form_data = new FormData();
  form_data.append('ID', id);
  form_data.append('TIPE', tipe);
  form_data.append('SHORT_DESC', short_desc);
  form_data.append('DESCRIPTION', description);
  if (strpath) {
    form_data.append('STRPATH', strpath);
  } else {
    if (strfile) {
      form_data.append('PATHS', strfile);
    }
  }

  var uri = 'create';
  if (id) {
    uri = 'update';
  }

  $.ajax({
    url: 'others/slides/' + uri, // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    // data: {
    //   "ID":id,
    //   "NAME":name,
    //   "DESCRIPTION":description,
    //   "SPECIAL":special,
    //   "DTL":arDtl
    // },
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Data Save : NOT Success! " + error,
        type: "error"
      });
      $("#btn-save").removeClass("disabled");
    },
    success: function(json) {
      if (json['status'] == 200) {
        tb_slides();
        swal({
          title: "Data Saved!\n" + name,
          type: "success"
        });
        reset_form('yes');
      }
      $("#btn-save").removeClass("disabled");
    }
  });
  return false;
});

// edit

// delete
$(document).on('click', ".btnDel", function() {
  var data = oTable.row($(this).parents('tr')).data();
  swal({
      title: "Are you sure?",
      text: "Data (" + data['name'] + ") will be deleted!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Delete",
      confirmButtonClass: "btn-danger",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        url = 'others/slides/delete/' + data['ID'];
        console.log(url);
        $.ajax({
          url: url, // point to server-side PHP script
          dataType: 'json', // what to expect back from the PHP script, if anything
          cache: false,
          contentType: false,
          processData: false,
          type: 'get',
          error: function(xhr, status, error) {
            swal("Error", "Your data failed to delete!", "error");
          },
          success: function(json) {
            if (json['status'] == 200) {
              tb_slides();
              swal("Deleted!", "Your data has been deleted.", "success");
            }
          }
        });
      } else {
        swal("Cancelled", "Deletion of data canceled", "error");
      }
    });
});
