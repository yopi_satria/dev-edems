var oTable;

$(document).ready(function () {
    tb_list();
    if (page_state.indexOf(page_short + '-total=1') == -1) {
        $('.' + page_short + '-total').hide();
    } else
        getChart();
});

function tb_list(start = null, end = null) {
    // var form_data = new FormData();
    // if (start) {
    //   form_data.append('START_DATE', start);
    // }
    // if (end) {
    //   form_data.append('END_DATE', end);
    // }
    var ino = 1;
    oTable = $('#tb_list').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        // scrollY: "500px",
        // scrollX: true,
        // scrollCollapse: true,
        // paging: false,
        // fixedColumns: {
        //   leftColumns: 3
        // },
        ajax: {
            url: 'report/monitor/data_list',
            data: {
                'start_date': start,
                'end_date': end
            },
            type: "POST"
        },
        columns: [{
                "data": "RNO",
                "sClass": "right",
                "mRender": function (row, data, index) {
                    return ino++;
                }
            }, {
                "data": "NAMA_PEKERJAAN"
            }, {
                "data": "NO_PENGAJUAN"
            }, {
                "data": "NOTIFIKASI"
            }, {
                "data": "R_DATE"
            }, {
                "data": "DESCPSECTION"
            }, {
                "data": "DESCPGROUP"
            }, {
                "data": "TIPE"
            }, {
                "data": "WBS_CODE"
            }, {
                "data": "R_APPROVE",
                "className": "text-center"
            }, {
                "data": "NO_PENUGASAN"
            }, {
                "data": "A_DATE"
            }, {
                "data": "A_APPROVE",
                "className": "text-center"
            }, {
                "data": "A_PACKET"
            }, {
                "data": "NO_DOK_ENG"
            }, {
                "data": "D_DATE"
            }, {
                "data": "D_APPROVE",
                "className": "text-center"
            }, {
                "data": "TRANSMITAL"
            },{
                "data": "TOT_PROGRESS",
                "mRender": function (row, data, index) {
                    state = '0 %';
                    if (index['TOT_PROGRESS']) {
                        state = index['TOT_PROGRESS'] + ' %';
                    }
                    return state;
                }
            }, {
                "data": "D_STATUS"
            }, {
                "data": "D_STATUS"
            }, {
                "data": "NO_DOK_TEND"
            }, {
                "data": "T_DATE"
            }, {
                "data": "T_APPROVE",
                "className": "text-center"
            }
        ],
        "order": [
            [2, "desc"],
            [2, "asc"]
        ]
    });
}

function getChart() {
    $.ajax({
        type: 'POST',
        url: 'report/monitor/count_report',
        success: function (json) {
            json = $.parseJSON(json);
            foreigns = '';
            if (json) {
                if (json['status'] == 200) {
                    graph = json['data'][0];
                    // foreigns += ('<option value >Please select...</option>');
                    // $.each(json['data'], function(i, data) {
                    //   foreigns += ('<option value="' + data['ID_PENGAJUAN'] + '" title="' + data['NO_PENGAJUAN'] + ' - ' + data['NOTIF_NO'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-no="' + data['NO_PENGAJUAN'] + '" data-notif="' + data['NOTIF_NO'] + '" data-flcode="' + data['FUNCT_LOC'] + '" data-fltext="' + data['FL_TEXT'] + '" data-shorttext="' + data['SHORT_TEXT'] + '" data-uktext="' + data['UK_TEXT'] + '" >' + data['SHORT_TEXT'] + '</option>');
                    // });

                    // Morris bar chart
                    Morris.Bar({
                        element: 'morris-bar-chart',
                        data: [{
                                y: 'ERF',
                                a: graph['J_ERF']
                            }, {
                                y: 'EAT',
                                a: graph['J_EAT']
                            }, {
                                y: 'Packet',
                                a: graph['J_PROG']
                            }, {
                                y: 'Packet Uploaded',
                                a: graph['J_DOK']
                            }, {
                                y: 'Transmital',
                                a: graph['J_BAST']
                            }],
                        xkey: 'y',
                        ykeys: ['a'],
                        labels: ['Jml '],
                        barColors: ['#26DAD2'],
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true
                    });

                }
            }
        },
        error: function (xhr, status, error) {
            alert('Server Error ...');
        }
    });
}

// event date
function changeDate() {
    var start = null;
    var end = null;
    if ($('#START_DATE').val()) {
        start = $('#START_DATE').val();
    }
    if ($('#END_DATE').val()) {
        end = $('#END_DATE').val();
    }
    tb_list(start, end);
}

$('#START_DATE').change(function () {
    changeDate();
});

$('#END_DATE').change(function () {
    changeDate();
});

// export
$("#toExcel").on("click", function () {
    var start = '';
    var end = '';
    if ($('#START_DATE').val()) {
        start = $('#START_DATE').val();
    }
    if ($('#END_DATE').val()) {
        end = $('#END_DATE').val();
    }
    $.redirect('report/monitor/excel', {
        start: start,
        end: end
    }, 'POST', '_blank');
    // window.location.assign('report/monitor/excel?&start=' + (new Date()).getFullYear() + start + end);
    // window.location.assign('report/monitor/excel?&start=' + (new Date()).getFullYear());
});

// export pdf
$("#toPdf").on("click", function () {
    var start = '';
    var end = '';
    if ($('#START_DATE').val()) {
        start = $('#START_DATE').val();
    }
    if ($('#END_DATE').val()) {
        end = $('#END_DATE').val();
    }
    $.redirect('report/monitor/pdf', {
        start: start,
        end: end
    }, 'POST', '_blank');
    // window.location.assign('report/monitor/excel?&start=' + (new Date()).getFullYear() + start + end);
    // window.location.assign('report/monitor/excel?&start=' + (new Date()).getFullYear());
});
