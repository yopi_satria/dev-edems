var dtTable;
var base_url_api = "<?= base_url(); ?>";
var editUserNopeg, editUserName;
var tmpSwerk, tmpIwerk;
var tmpSplant, tmpPgrp, tmpSpgrp;

$(document).ready(function() {
  // tb_request();
  tb_erf2();
  // reloadtabel();
  // refreshFunloc();
  // refreshKDLoc();
  // refreshKDArea();
  // generateDoc();
  // locOnChange();
  // areaOnChange();
  tmpSplant = $('#PLANPLANT').val();
  tmpPgrp = $('#PLANGROUP').val();
  tmpSpgrp = $('#PLANGROUP option:selected').text();
  $('.select2').select2();
  select_content_change('#FUNCT_LOC', "notif/create/view_funcloc", 'Select Functional Location ...');
});

function setActiveTab(ul_id = null, content_id = null) {
  $(".nav-link").removeClass("active show");
  $('#' + ul_id).addClass('active show');
  $(".tab-pane").removeClass("active show");
  $('#' + content_id).addClass('active show');
}

function openModal(id = null) {
  $('#' + id).modal('show');
  $('#' + id).show();
  $('.modal-backdrop').show();
  // console.log('open');
}

function closeModal(id = null) {
  $('#' + id).hide();
  $('.modal-backdrop').hide();
  // console.log('closed ');
}

$(function() {
  $('[data-toggle="tooltip"]').tooltip()
})

function tb_erf2() {
  var oTable = $('#tb_erf2').DataTable({
    destroy: true,
    processing: true,
    serverSide: true,
    ajax: {
      url: 'notif/create/data_list',
      type: "POST"
    },
    columns: [{
        "className": 'details-control',
        "orderable": false,
        "data": null,
        "defaultContent": ''
      },
      {
        "data": "NOTIFIKASI"
      },
      {
        "data": "NAMA_PEKERJAAN"
      },
      {
        "data": "DESCPGROUP"
      },
      {
        "data": "DESCPSECTION"
      },
      {
        "data": "UK_TEXT"
      },
      // {
      //   "data": "DEPT_TEXT"
      // },
      {
        "data": "CREATE_BY"
      },
      {
        "data": "CREATE_AT",
        "width": 75
      },
      {
        "data": "ID_MPE",
        "width": 100,
        "mRender": function(row, data, index) {
          // var stat;
          // var sresend;
          // var srevisi;
          sdis = '';
          if (page_state.indexOf(page_short + '-delete=1') == -1 || jQuery.type(index['NO_PENGAJUAN']) !== "null") {
            sdis = 'disabled';
          }
          console.log(index['NO_PENGAJUAN']);
          sexp = '';
          if (page_state.indexOf(page_short + '-export=1') == -1) {
            sexp = ' style="display:none;"';
          }
          // if (index['STATUS'] == 'Approved') {
          //   // stat='disabled';
          //   // sresend='';
          //   // srevisi='<button class="btnRevisi btn btn-xs btn-warning" data-toggle="tooltip" title="Revisi"><i class="fa fa-magic"></i></button>';
          //   return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Export"'+sexp+' '+sexp+' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-info" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button><button class="btnDel btn btn-xs btn-danger '+page_short+'-delete'+' disabled" data-toggle="tooltip" title="Delete" disabled><i class="fa fa-trash"></i></button></div></center>';
          //   //<button class="btnRevisi btn btn-xs btn-warning" data-toggle="tooltip" title="Revisi">
          // } else if (index['STATUS'] == 'Open') {
          //   // stat='';
          //   // srevisi='';
          //   // sresend='<button class="btnResend btn btn-xs btn-success" data-toggle="tooltip" title="Resend Email"><i class="fa fa-envelope-square"></i></button>';
          //   return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Export"'+sexp+' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-info" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button><button class="btnResend btn btn-xs btn-success" data-toggle="tooltip" title="Resend Email"><i class="fa fa-envelope-square"></i></button><button class="btnDel btn btn-xs btn-danger '+page_short+'-delete '+sdis+'" data-toggle="tooltip" title="Delete" '+sdis+'><i class="fa fa-trash"></i></button></div></center>';
          // } else {
          //   return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Export"'+sexp+' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-info" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button><button class="btnDel btn btn-xs btn-danger '+page_short+'-delete '+sdis+'" data-toggle="tooltip" title="Delete" '+sdis+'><i class="fa fa-trash"></i></button></div></center>';
          //   // return '';
          // }

          return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Export"' + sexp + ' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-info" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button><button class="btnDel btn btn-xs btn-danger ' + page_short + '-delete ' + sdis + '" data-toggle="tooltip" title="Delete" ' + sdis + '><i class="fa fa-trash"></i></button></div></center>';
        }
      },
    ],
    "order": [
      [0, "desc"]
    ]
  });

  //<button class="btnDel btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></button>

  dtTable = oTable;

  // Add event listener for opening and closing details
  $('#tb_erf2 tbody').on('click', 'td.details-control', function() {
    var tr = $(this).closest('tr');
    var row = oTable.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    } else {
      // Open this row
      var html = '';
      var d = row.data();
      var data;
      var approveAt = '';
      var status = '';
      html += '<div class="container">' +
        '<ul class="progressbar">' +
        '<li data-toggle="tooltip" data-placement="top" title="' + status + '">Request Form</li>' +
        '<li>Assign Task</li>' +
        '<li>Documents</li>' +
        '<li>Finish</li>' +
        '</ul>';

      var dokeng;

      $.ajax({
        url: 'notif/create/getApproval',
        data: {
          id: d.ID_MPE
        },
        dataType: 'json',
        type: 'POST',
        success: function(result) {
          data = result;
          statuspenugasan = '';
          statusdok = '';
          statusbast = '';
          $.map(data, function(item, index) {

            pengajuan = item.STATUS1;
            penugasan = item.STATUS2;
            if (item.STATUS1 === 'Open') {
              status = 'Pending';
            } else if (item.STATUS1 === 'Approved') {
              status = 'active';
            } else {
              status = 'reject';
            }
            if (item.STATUS2 === 'In Approval') {
              statuspenugasan = 'pending';
            } else if (item.STATUS2 === 'Approved Biro.' || item.STATUS2 === 'SM Approved') {
              statuspenugasan = 'pending';
            } else if (item.STATUS2 === 'Approved Dept.' || item.STATUS2 === 'GM Approved') {
              statuspenugasan = 'active';
            } else if (item.STATUS2 === 'Rejected') {
              statuspenugasan = 'reject';
            } else {
              statuspenugasan = '';
            }
            if (item.DOKSTATUS === 'ada') {
              statusdok = 'active';
            } else {
              statusdok = '';
            }
            if (item.STATUSBAST === 'Closed') {
              statusbast = 'active';
            } else if (item.STATUSBAST === 'Rejected') {
              statuspenugasan = 'reject';
            } else {
              statusbast = '';
            }
          });

          html = '<div class="container">' +
            '<ul class="progressbar">';

          if (status === 'Pending') {
            html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" title="Pending">Request Form</li>';
          } else if (status === 'reject') {
            html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" title="Pending">Request Form</li>';
          } else {
            html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" >Request Form</li>';
          }
          html += '<li class="' + statuspenugasan + '">Assign Task</li>';
          html += '<li class="' + statusdok + '">Engineering Documents</li>';
          html += '<li class="' + statusbast + '">Finish</li>' +

            '</ul>' +
            '</div>' +
            '<div class="container" style="height:400px;width:1000px;overflow-y:scroll">' +
            '<table class="table display table-bordered table-striped" id="tableprogress">' +
            '<thead>' +
            '<tr>' +
            '<th>No</th>' +
            '<th>Type</th>' +
            '<th>Status</th>' +
            '<th>Date</th>' +
            '<th>By</th>' +
            '<th style="width:100px">Note</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>';



          $.ajax({
            url: 'notif/create/getLogPengajuan',
            data: {
              id: d.ID_MPE
            },
            dataType: 'json',
            type: 'POST',
            beforeSend: function() {},
            success: function(result) {
              data = result;
              $.map(data, function(item, index) {
                dokeng = item.ID;
                html += '<tr>' +
                  '<td>' + (index + 1) + '</td>' +
                  '<td>ERF Create</td>' +

                  '<td>Created</td>' +
                  '<td>' + item.CREATE_AT + '</td>' +
                  '<td>' + item.CREATE_BY + '</td>' +

                  '<td style="width:280px">-</td>' +
                  '</tr>';
                if (item.APPROVE_AT != null && item.APPROVE_BY != null) {
                  html += '<tr>' +
                    '<td>' + (index + 2) + '</td>' +
                    '<td>ERF Approval</td>' +

                    '<td>Approved</td>' +
                    '<td>' + item.APPROVE_AT + '</td>' +
                    '<td>' + item.APPROVE_BY + '</td>' +
                    '<td style="width:280px">' + item.NOTE + '</td>' +
                    '</tr>';
                } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                  html += '<tr>' +
                    '<td>' + (index + 2) + '</td>' +
                    '<td>ERF Approval</td>' +
                    '<td>Rejected</td>' +
                    '<td>' + item.REJECT_DATE + '</td>' +
                    '<td>' + item.REJECT_BY + '</td>' +
                    '<td style="width:280px">' + item.NOTE + '</td>' +
                    '</tr>';
                } else {
                  html += '<tr>' +
                    '<td>' + (index + 2) + '</td>' +
                    '<td>ERF Approval</td>' +
                    '<td>-</td>' +
                    '<td>-</td>' +
                    '<td>-</td>' +
                    '<td style="width:280px">-</td>' +
                    '</tr>';
                }
                if (item.CREATEAT2 != null) {
                  html += '<tr>' +
                    '<td>' + (index + 3) + '</td>' +
                    '<td>EAT Create</td>' +
                    '<td>Created</td>' +
                    '<td>' + item.CREATEAT2 + '</td>' +
                    '<td>' + item.CREATEBY2 + '</td>' +
                    '<td style="width:280px">-</td>' +
                    '</tr>';
                } else {
                  html += '<tr>' +
                    '<td>' + (index + 3) + '</td>' +
                    '<td>EAT Create</td>' +
                    '<td>-</td>' +
                    '<td>-</td>' +
                    '<td>-</td>' +

                    '<td style="width:280px">-</td>' +
                    '</tr>';
                }
                if (item.APPROVE0_BY != null && item.APPROVE0_AT != null) {
                  html += '<tr>' +
                    '<td>' + (index + 4) + '</td>' +
                    '<td>EAT Approval MGR</td>' +
                    '<td>Approved</td>' +
                    '<td>' + item.APPROVE0_AT + '</td>' +
                    '<td>' + item.APPROVE0_BY + '</td>' +
                    '<td style="width:280px">' + item.NOTE0 + '</td>' +
                    '</tr>';
                } else if (item.REJECTAT2 != null && item.REJECTBY2 != null && item.APPROVE1_BY == item.REJECTBY2) {
                  html += '<tr>' +
                    '<td>' + (index + 4) + '</td>' +
                    '<td>EAT Approval MGR</td>' +
                    '<td>Approved</td>' +
                    '<td>' + item.REJECTAT2 + '</td>' +
                    '<td>' + item.REJECTBY2 + '</td>' +
                    '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                    '</tr>';
                } else {
                  html += '<tr>' +
                    '<td>' + (index + 4) + '</td>' +
                    '<td>EAT Approval MGR</td>' +
                    '<td>-</td>' +
                    '<td>-</td>' +
                    '<td>-</td>' +

                    '<td style="width:280px">-</td>' +
                    '</tr>';
                }
                if (item.APPROVE1_BY != null && item.APPROVE1_AT != null) {
                  html += '<tr>' +
                    '<td>' + (index + 5) + '</td>' +
                    '<td>EAT Approval SM</td>' +
                    '<td>Approved</td>' +
                    '<td>' + item.APPROVE1_AT + '</td>' +
                    '<td>' + item.APPROVE1_BY + '</td>' +
                    '<td style="width:280px">' + item.NOTE1 + '</td>' +
                    '</tr>';
                } else if (item.REJECTAT2 != null && item.REJECTBY2 != null && item.APPROVE1_BY == item.REJECTBY2) {
                  html += '<tr>' +
                    '<td>' + (index + 5) + '</td>' +
                    '<td>EAT Approval SM</td>' +
                    '<td>Approved</td>' +
                    '<td>' + item.REJECTAT2 + '</td>' +
                    '<td>' + item.REJECTBY2 + '</td>' +
                    '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                    '</tr>';
                } else {
                  html += '<tr>' +
                    '<td>' + (index + 5) + '</td>' +
                    '<td>EAT Approval SM</td>' +
                    '<td>-</td>' +
                    '<td>-</td>' +
                    '<td>-</td>' +

                    '<td style="width:280px">-</td>' +
                    '</tr>';
                }
                if (item.APPROVE2_AT != null && item.APPROVE2_BY != null) {
                  html += '<tr>' +
                    '<td>' + (index + 6) + '</td>' +
                    '<td>EAT APPROVE GM</td>' +
                    '<td>Approved</td>' +
                    '<td>' + item.APPROVE2_AT + '</td>' +
                    '<td>' + item.APPROVE2_BY + '</td>' +

                    '<td style="width:280px"> ' + item.NOTE2 + '</td>' +
                    '</tr>';
                } else if (item.REJECTAT2 != null && item.REJECTBY2 != null && item.APPROVE2_BY == item.REJECTBY2) {
                  html += '<tr>' +
                    '<td>' + (index + 6) + '</td>' +
                    '<td>EAT Approval SM</td>' +
                    '<td>Approved</td>' +
                    '<td>' + item.REJECTAT2 + '</td>' +
                    '<td>' + item.REJECTBY2 + '</td>' +
                    '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                    '</tr>';
                } else {
                  html += '<tr>' +
                    '<td>' + (index + 6) + '</td>' +
                    '<td>EAT APPROVE GM</td>' +
                    '<td>-</td>' +
                    '<td>-</td>' +
                    '<td>-</td>' +

                    '<td style="width:280px">-</td>' +
                    '</tr>';
                }
              });
              return html;
            },
            error: function() {

            },
            complete: function() {
              var i = 7;
              //paraf
              $.ajax({
                url: 'dok_eng/documents/get_prog_dok',
                data: {
                  id: d.ID_MPE
                },
                dataType: 'json',
                type: 'POST',
                beforeSend: function() {},
                success: function(result) {
                  data = result;
                  $.map(data, function(item, index) {

                    html += '<tr>' +
                      '<td>' + (index + i) + '</td>' +
                      '<td>Document Engineering</td>' +
                      '<td>Created</td>' +
                      '<td>' + item.CREATE_AT + '</td>' +
                      '<td>' + item.CREATE_BY + '</td>' +
                      '<td style="width:280px"> ' + item.PACKET_TEXT + '</td>' +
                      '</tr>';

                    i++;

                    if (item.APPROVE1_AT != null) {
                      html += '<tr>' +
                        '<td>' + (index + i) + '</td>' +
                        '<td>TTD Manager</td>' +
                        '<td>Approved</td>' +
                        '<td>' + item.APPROVE1_AT + '</td>' +
                        '<td>' + item.APPROVE1_BY + '</td>' +

                        '<td style="width:280px"> ' + item.NOTE1 + '</td>' +
                        '</tr>';
                    } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                      html += '<tr>' +
                        '<td>' + (index + i) + '</td>' +
                        '<td>TTD Manager</td>' +
                        '<td>Rejected</td>' +
                        '<td>' + item.REJECT_DATE + '</td>' +
                        '<td>' + item.REJECT_BY + '</td>' +
                        '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                        '</tr>';
                    } else {
                      html += '<tr>' +
                        '<td>' + (index + i) + '</td>' +
                        '<td>TTD Manager</td>' +
                        '<td>-</td>' +
                        '<td>-</td>' +
                        '<td>' + item.APPROVE1_BY + '</td>' +
                        '<td style="width:280px">-</td>' +
                        '</tr>';
                    }
                    i++;

                    if (item.APPROVE2_AT != null) {
                      html += '<tr>' +
                        '<td>' + (index + i) + '</td>' +
                        '<td>TTD SM</td>' +
                        '<td>Approved</td>' +
                        '<td>' + item.APPROVE2_AT + '</td>' +
                        '<td>' + item.APPROVE2_BY + '</td>' +

                        '<td style="width:280px"> ' + item.NOTE2 + '</td>' +
                        '</tr>';
                    } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                      html += '<tr>' +
                        '<td>' + (index + i) + '</td>' +
                        '<td>TTD SM</td>' +
                        '<td>Rejected</td>' +
                        '<td>' + item.REJECT_DATE + '</td>' +
                        '<td>' + item.REJECT_BY + '</td>' +
                        '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                        '</tr>';
                    } else {
                      html += '<tr>' +
                        '<td>' + (index + i) + '</td>' +
                        '<td>TTD SM</td>' +
                        '<td>-</td>' +
                        '<td>-</td>' +
                        '<td>' + item.APPROVE2_BY + '</td>' +
                        '<td style="width:280px">-</td>' +
                        '</tr>';
                    }

                    i++

                    if (item.APPROVE3_AT != null) {
                      html += '<tr>' +
                        '<td>' + (index + i) + '</td>' +
                        '<td>TTD GM</td>' +
                        '<td>Approved</td>' +
                        '<td>' + item.APPROVE3_AT + '</td>' +
                        '<td>' + item.APPROVE3_BY + '</td>' +

                        '<td style="width:280px"> ' + item.NOTE3 + '</td>' +
                        '</tr>';
                    } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                      html += '<tr>' +
                        '<td>' + (index + i) + '</td>' +
                        '<td>TTD GM</td>' +
                        '<td>Rejected</td>' +
                        '<td>' + item.REJECT_DATE + '</td>' +
                        '<td>' + item.REJECT_BY + '</td>' +
                        '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                        '</tr>';
                    } else {
                      html += '<tr>' +
                        '<td>' + (index + i) + '</td>' +
                        '<td>TTD GM</td>' +
                        '<td>-</td>' +
                        '<td>-</td>' +
                        '<td>' + item.APPROVE3_BY + '</td>' +
                        '<td style="width:280px">-</td>' +
                        '</tr>';
                    }
                    i++;

                    if (item.BASTAT != null && item.BASTBY != null) {
                      html += '<tr>' +
                        '<td>' + (index + i) + '</td>' +
                        '<td>BAST</td>' +
                        '<td>Created</td>' +
                        '<td>' + item.BASTAT + '</td>' +
                        '<td>' + item.BASTBY + '</td>' +

                        '<td style="width:280px"> BAST Created ' + item.NO_BAST + '</td>' +
                        '</tr>';
                      i++;

                      if (item.APPROVE1_0_AT != null) {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD SM Cust.</td>' +
                          '<td>Approved</td>' +
                          '<td>' + item.APPROVE1_0_AT + '</td>' +
                          '<td>' + item.APPROVE1_0_BY + '</td>' +
                          '<td style="width:280px"> ' + item.NOTE1_0 + '</td>' +
                          '</tr>';
                      } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.APPROVE1_0_BY) {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD SM Cust.</td>' +
                          '<td>Rejected</td>' +
                          '<td>' + item.BASTREJECTAT + '</td>' +
                          '<td>' + item.BASTREJECTBY + '</td>' +
                          '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
                          '</tr>';
                      } else {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD SM Cust.</td>' +
                          '<td>-</td>' +
                          '<td>-</td>' +
                          '<td>' + item.APPROVE1_0_BY + '</td>' +
                          '<td style="width:280px">-</td>' +
                          '</tr>';
                      }
                      i++;

                      if (item.APPROVE2_0_AT != null) {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD GM Cust.</td>' +
                          '<td>Approved</td>' +
                          '<td>' + item.APPROVE2_0_AT + '</td>' +
                          '<td>' + item.APPROVE2_0_BY + '</td>' +
                          '<td style="width:280px">  ' + item.NOTE2_0 + '</td>' +
                          '</tr>';
                      } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.APPROVE2_0_BY) {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD GM Cust.</td>' +
                          '<td>Rejected</td>' +
                          '<td>' + item.BASTREJECTAT + '</td>' +
                          '<td>' + item.BASTREJECTBY + '</td>' +
                          '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
                          '</tr>';
                      } else {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD GM Cust.</td>' +
                          '<td>-</td>' +
                          '<td>-</td>' +
                          '<td>' + item.APPROVE2_0_BY + '</td>' +
                          '<td style="width:280px">-</td>' +
                          '</tr>';
                      }
                      i++;

                      if (item.BAST1AT != null) {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD SM</td>' +
                          '<td>Approved</td>' +
                          '<td>' + item.BAST1AT + '</td>' +
                          '<td>' + item.BAST1BY + '</td>' +
                          '<td style="width:280px">  ' + item.BASTNOTE1 + '</td>' +
                          '</tr>';
                      } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.BAST1BY) {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD SM</td>' +
                          '<td>Rejected</td>' +
                          '<td>' + item.BASTREJECTAT + '</td>' +
                          '<td>' + item.BASTREJECTBY + '</td>' +
                          '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
                          '</tr>';
                      } else {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD SM</td>' +
                          '<td>-</td>' +
                          '<td>-</td>' +
                          '<td>' + item.BAST1BY + '</td>' +
                          '<td style="width:280px">-</td>' +
                          '</tr>';
                      }
                      i++;

                      if (item.bast2AT != null) {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD GM</td>' +
                          '<td>Approved</td>' +
                          '<td>' + item.bast2AT + '</td>' +
                          '<td>' + item.bast2BY + '</td>' +
                          '<td style="width:280px">  ' + item.BASTNOTE2 + '</td>' +
                          '</tr>';
                      } else if (item.BASTREJECTAT != null && item.BASTREJECTBY == item.bast2BY) {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD GM</td>' +
                          '<td>Rejected</td>' +
                          '<td>' + item.BASTREJECTAT + '</td>' +
                          '<td>' + item.BASTREJECTBY + '</td>' +
                          '<td style="width:280px"> ' + item.BASTREJECTNOTE + '</td>' +
                          '</tr>';
                      } else {
                        html += '<tr>' +
                          '<td>' + (index + i) + '</td>' +
                          '<td>TTD GM</td>' +
                          '<td>-</td>' +
                          '<td>-</td>' +
                          '<td>' + item.bast2BY + '</td>' +
                          '<td style="width:280px">-</td>' +
                          '</tr>';
                      }

                    } else {

                    }

                  });
                  return html;
                },
                error: function() {

                },
                complete: function() {
                  html +=
                    '</tbody>' +
                    '</table>' +
                    '</div>';
                  // console.log(html);
                  row.child(html).show();
                  tr.addClass('shown');
                  return html;
                }
              });





            }
          });

          return html;

        },
        error: function() {
          alert("Error");
        },
        complete: function() {
          return html;
        }
      });


    }
  });

}

function scroll() {
  $('#tableprogress').DataTable({
    "scrollY": 100,
    "paging": false
  });
}

function generateDoc() {
  var tahun;
  var lokasi;
  var Nomor;
  $.ajax({
    type: 'POST',
    url: 'notif/create/generateNoDoc',
    //data:{'id':id},
    success: function(data) {
      // console.log("test " + data);
      try {
        data = JSON.parse(data);
        Nomor = data.nomor;
        tahun = data.tahun;

        $("#NOPENGAJUAN").val(tahun + "/" + "/" + "/" + "EM/" + Nomor);
        if ($('#KDLOC').val()) {
          locOnChange();
        }
        if ($('#KDAREA').val()) {
          areaOnChange();
        }

      } catch (e) {
        alert(e);
      } finally {

      }

    },
    error: function() {
      console.log("error");
    }
  });

}

function locOnChange() {
  var no_pengajuan = $("#NOPENGAJUAN").val();
  var arr = no_pengajuan.split("/");
  kdlc = $("#KDLOC").val();
  console.log(kdlc);
  $("#NOPENGAJUAN").val(arr[0] + "/" + kdlc + "/" + arr[2] + "/" + arr[3] + "/" + arr[4]);
}

$('#KDLOC').change(function() {
  locOnChange();
});

function areaOnChange() {
  var no_pengajuan = $("#NOPENGAJUAN").val();
  var arr = no_pengajuan.split("/");
  kdarea = $("#KDAREA").val();
  $("#NOPENGAJUAN").val(arr[0] + "/" + arr[1] + "/" + kdarea + "/" + arr[3] + "/" + arr[4]);
}

$('#KDAREA').change(function() {
  areaOnChange();
});

function refreshFunloc(str = null) {
  var qq;
  if (str) {
    qq = str;
  } else {
    qq = "{{{q}}}"
  }
  // console.log(qq);
  $(".selectpicker").selectpicker().filter('#FUNCT_LOC').ajaxSelectPicker({
    "ajax": {
      "url": 'notif/create/view_funcloc',
      "type": "GET",
      "dataType": "json",
      "data": {
        token: localStorage.token,
        q: qq
      }
    },
    "log": false,
    "preprocessData": function(json) {
      return_data = [];
      if (json) {
        $.each(json, function(i, item) {
          return_data.push({
            "value": item['TPLNR'],
            "text": item['DESC'],
            "data": {
              "subtext": item['TPLNR'],
              "tplnr": item['TPLNR'],
              // "strno": item['STRNO'],
              "objnr": item['OBJNR'],
              "pltxt": item['DESC'],
              "swerk": item['MPLANT'],
              "descmplant": item['AS_DESCMPLANT'],
              "iwerk": item['IWERK']
            }
          });
        });
      }
      // console.log(qq);

      return return_data;
      $('.selectpicker').selectpicker('refresh');
    }
  });
}

function select_content_change(selecttag, urlselect, placeholder) {
  $('' + selecttag).select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: placeholder,
    theme: 'bootstrap',
    ajax: {
      dataType: 'json',
      url: urlselect,
      // data: data,
      delay: 50,
      type: 'POST',
      data: function(params) {
        return {
          search: params.term
          // search: params.term,
          // additional_data: data
        }
      },
      processResults: function(data, page) {
        console.log(data);
        return {
          results: $.map(data, function(obj) {
            console.log(obj);
            if (obj.OBJNR) {
              // var objectcode = '000'+obj.ID_DATA+'';
              // var stdcode = objectcode.slice(-3);

              return {
                id: obj.OBJNR + ' - ' + obj.TPLNR + ' - ' + obj.MPLANT + ' - ' + obj.AS_DESCMPLANT + ' - ' + obj.IWERK,
                text: '[ ' + obj.TPLNR + ' ] ' + obj.DESC,
                // photo: obj.photo
              };
            } else {
              return {
                id: obj.OBJNR + ' - ' + obj.TPLNR + ' - ' + obj.MPLANT + ' - ' + obj.AS_DESCMPLANT + ' - ' + obj.IWERK,
                text: '[ ' + obj.NOBADGE + ' ] ' + obj.DESC,
                // photo: obj.photo
              };
            }

          })
        };
      },
    }
  });
  $('' + selecttag).trigger('change');
}

function load_selected_option(key, id, text) {
  $(key).empty().append('<option selected value="' + id + '">' + text + '</option>');
  $(key).select2('data', {
    id: id,
    label: text
  });
  $(key).trigger('change'); // Notify any JS components that the value changed
}

$('#FUNCT_LOC').on('change', function() {
  if ($('#FUNCT_LOC').val()) {
    var arFuncloc = $('#FUNCT_LOC').val().split(' - ');
    $('#SWERK').val(arFuncloc[2] + ' - ' + arFuncloc[3]);
    // $('#SWERK').val($('option:selected', this).attr('data-swerk') + ' - ' + $('option:selected', this).attr('data-descmplant'));
    if (!$('#IDMPE').val()) {
      if (($('#SWERK').val() && tmpSwerk != $('#SWERK').val()) || !$('#SWERK').val()) {
        $('#BEBER').html('');
        getPSection(arFuncloc[2]);
        // getPSection($('option:selected', this).attr('data-swerk'));
        tmpSwerk = $('#SWERK').val();
      }
    }
    // $('#PLANPLANT').val(arFuncloc[4]);
    // $('#PLANPLANT').val($('option:selected', this).attr('data-iwerk'));
    // if (($('#PLANPLANT').val() && tmpIwerk != $('#PLANPLANT').val()) || !$('#PLANPLANT').val()) {
    //   $('#PLANGROUP').html('');
    //   getPGroup(arFuncloc[4]);
    //   getPGroup($('option:selected', this).attr('data-iwerk'));
    //   tmpIwerk = $('#PLANPLANT').val();
    // }
  }
  // alert( this.value + " - " + $('option:selected', this).attr('data-swerk'));
})

function getPSection(mplant, psection = null) {
  $.ajax({
    type: 'POST',
    url: 'notif/create/view_psection',
    data: {
      "MPLANT": mplant
    },
    success: function(json) {
      // json = JSON.parse(json);
      json = $.parseJSON(json);
      $('#BEBER').html('');
      psections = '';
      // console.log(json);
      if (json) {
        if (json['status'] == 200) {
          $.each(json['data'], function(i, item) {
            if (psection) {
              psections += ('<option selected value="' + item['PSECTION'] + '">' + item['PSECTION'] + ' - ' + item['DESCPSECTION'] + '</option>');
            } else {
              psections += ('<option value="' + item['PSECTION'] + '">' + item['PSECTION'] + ' - ' + item['DESCPSECTION'] + '</option>');
            }
          });
        }
        $('#BEBER').html(psections);
      }
      // $('#BEBER').selectpicker('refresh');
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });
}

function getPGroup(pplant, pgroup = null) {
  $.ajax({
    type: 'POST',
    url: 'notif/create/view_pgroup',
    data: {
      "PPLANT": pplant
    },
    success: function(json) {
      // json = JSON.parse(json);
      json = $.parseJSON(json);
      $('#PLANGROUP').html('');
      pgroups = '';
      // console.log(json);
      if (json) {
        if (json['status'] == 200) {
          $.each(json['data'], function(i, item) {
            if (pgroup) {
              pgroups += ('<option selected value="' + item['PGROUP'] + '">' + item['PGROUP'] + ' - ' + item['DESCPGROUP'] + '</option>');
            } else {
              pgroups += ('<option value="' + item['PGROUP'] + '">' + item['PGROUP'] + ' - ' + item['DESCPGROUP'] + '</option>');
            }
          });
        }
        $('#PLANGROUP').html(pgroups);
      }
      // $('#PLANGROUP').selectpicker('refresh');
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });
}

function reset_form(reset = null) {
  $('#IDMPE').val('');
  $('#REV_NO').val('0');
  // $('#NOPENGAJUAN').val('');
  // generateDoc();
  // $('#KDLOC').val('ALL0').trigger('change');
  // $('#KDAREA').val('00').trigger('change');
  $('#NOTIFIKASI').val('');
  $('#SHORT_TEXT').val('');
  // $('#FUNCT_LOC').html('');
  $('#FUNCT_LOC').val('').trigger('change');
  $('#SWERK').val('');
  $('#BEBER').html('');
  $('#PLANPLANT').val(tmpSplant);
  $('#PLANGROUP').html('<option selected value="' + tmpPgrp + '">' + tmpSpgrp + '</option>');
  // $('#LOKASI').val('');
  $('#PRIORITY').val('3');
  // $('#WBS_CODE').val('');
  // $('#WBS_TEXT').val('');
  $('#DESSTDATE').val('');
  $('#DESENDDATE').val('');
  // $('#LATAR_BELAKANG').val('');
  // $('#CUST_REQ').val('');
  // $('#TECH_INFO').val('');
  // $('#RES_MIT').val('');

  $('#FUNCT_LOC').attr('required');
  $('#FUNCT_LOC').prop('required', true);
  // $('#FILES').val('');
  $("#btn-save").removeClass("disabled");
  $("#btn-save").prop("disabled", false);
  $("#btn-save").val("Save");
  $('.selectpicker').selectpicker('refresh');
  $('.select2').select();

  $('#ul-new').text('Create Notification');
  $("#btn-save").show();
  if (reset) {
    setActiveTab('ul-list', 'list-form');
  }
}

// Cancel
$("#btn-cancel").on("click", function() {
  reset_form();
});

// export
$(document).on('click', ".btnExport", function() {
  var data = dtTable.row($(this).parents('tr')).data();
  window.location.assign('notif/create/export_pdf/' + data['ID_MPE']);
});

$("#btn-cancel").on("click", function() {
  reset_form();
});

// event view
$(document).on('click', ".btnResend", function() {
  $('.title_ukur').text('Resend Email');
  var data = dtTable.row($(this).parents('tr')).data();


  var form_data = new FormData();
  form_data.append('ID_MPE', data['ID_MPE']);

  $.ajax({
    url: 'notif/create/resend', // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Resend Email : NOT Success! " + error,
        type: "error"
      });
      // $("#btn-save").removeClass("disabled");
      // $("#btn-save").val("Save");
    },
    success: function(json) {
      if (json['status'] == 200) {
        // tb_request();
        // tb_erf2();
        swal({
          title: "Email was sent.",
          type: "success"
        });
        reset_form('yes');
      }
      $("#btn-save").removeClass("disabled");
      // $("#btn-save").val("Save");
    }
  });
});

// event view
$(document).on('click', ".btnEdit", function() {
  $('.title_ukur').text('Data Request');
  var data = dtTable.row($(this).parents('tr')).data();

  $('#IDMPE').val(data['ID_MPE']);
  // $('#KDLOC').val(data['KDLOC']).trigger('change');
  // $('#KDAREA').val(data['KDAREA']).trigger('change');
  // $('#NOPENGAJUAN').val(data['NO_PENGAJUAN']);
  $('#NOTIFIKASI').val(data['NOTIFIKASI']);
  $('#SHORT_TEXT').val(data['NAMA_PEKERJAAN']);

  if (data['FUNCT_LOC']) {
    // $('#FUNCT_LOC').html('<option value="' + data['FUNCT_LOC'] + '" title="' + data['FL_TEXT'] + '" data-subtext="' + data['FUNCT_LOC'] + '" data-tplnr="' + data['FUNCT_LOC'] + '" data-pltxt="' + data['FL_TEXT'] + '" data-swerk="' + data['MPLANT'] + '" data-descmplant="' + data['DESCMPLANT'] + '" data-iwerk="' + data['PLANT_SECTION'] + ' >' + data['FL_TEXT'] + '</option>')
    //   .selectpicker('refresh');
    load_selected_option('#FUNCT_LOC', data['OBJNR'] + " - " + data['FUNCT_LOC'] + " - " + data['MPLANT'] + " - " + data['DESCMPLANT'] + " - " + data['PPLANT'], data['FL_TEXT']);
    // var newOption = $('<option selected value="' + data['FUNCT_LOC'] + '" title="' + data['FL_TEXT'] + '" data-subtext="' + data['FUNCT_LOC'] + '" data-tplnr="' + data['FUNCT_LOC'] + '" data-pltxt="' + data['FL_TEXT'] + '" data-swerk="' + data['MPLANT'] + '" data-descmplant="' + data['DESCMPLANT'] + '" data-iwerk="' + data['PLANT_SECTION'] + ' >' + data['FL_TEXT'] + '</option>');
    // refreshFunloc(data['FUNCT_LOC']);
    // console.log(data['FUNCT_LOC']);
    //     $('#FUNCT_LOC').empty(); //remove all child nodes
    //     $('#FUNCT_LOC').append(newOption);
    //     $('#FUNCT_LOC').trigger("chosen:updated");
    //     $('.selectpicker').selectpicker('refresh');
  }
  if (data['MPLANT']) {
    $('#SWERK').val(data['MPLANT'] + ' - ' + data['DESCMPLANT']);
  }
  if (data['PLANT_SECTION']) {
    $('#BEBER').html('<option selected value="' + data['PLANT_SECTION'] + '">' + data['PLANT_SECTION'] + ' - ' + data['DESCPSECTION'] + '</option>');
  }
  $('#PLANPLANT').val(data['PPLANT'] + ' - ' + data['DESCPLANT']);
  if (data['PLANNER_GROUP']) {
    $('#PLANGROUP').html('<option selected value="' + data['PLANNER_GROUP'] + '">' + data['PLANNER_GROUP'] + ' - ' + data['DESCPGROUP'] + '</option>');
  }
  // $('#LOKASI').val(data['LOKASI']);
  $('#PRIORITY').val(data['PRIORITY']);
  // $('#WBS_CODE').val(data['WBS_CODE']);
  // $('#WBS_TEXT').val(data['WBS_TEXT']);
  $('#DESSTDATE').val(data['DESSTDATE']);
  $('#DESENDDATE').val(data['DESENDDATE']);
  // $('#LATAR_BELAKANG').val(data['LATAR_BELAKANG']);
  // $('#CUST_REQ').val(data['CUST_REQ']);
  // $('#TECH_INFO').val(data['TECH_INFO']);
  // $('#RES_MIT').val(data['RES_MIT']);
  // if (data['FILES']) {
  //   var aFiles = data['FILES'].split('/');
  //
  //   $('#file_uri').attr('href', data['FILES']);
  //   $('#file_name').text(' ' + aFiles[(aFiles.length - 1)]);
  //   $('#file_name').addClass('fa fa-file-pdf-o');
  // }

  $('#btn-save').hide();
  $('#ul-new').text('View Engineering Request');
  $('.selectpicker').selectpicker('refresh');
  setActiveTab('ul-new', 'new-form');
  if (page_state.indexOf(page_short + '-modify=1') == -1) {
    $('.' + page_short + '-modify').hide();
  }
});

// event revision
$(document).on('click', ".btnRevisi", function() {
  $('.title_ukur').text('Data Request');
  var data = dtTable.row($(this).parents('tr')).data();

  $('#IDMPE').val(data['ID_MPE']);
  $('#REV_NO').val(parseInt(data['REV_NO']) + 1);
  $('#KDLOC').val(data['KDLOC']);
  $('#KDAREA').val(data['KDAREA']);
  $('#NOPENGAJUAN').val(data['NO_PENGAJUAN']);
  $('#NOTIFIKASI').val(data['NOTIFIKASI']);
  $('#SHORT_TEXT').val(data['NAMA_PEKERJAAN']);
  if (data['FUNCT_LOC']) {
    $('#FL_CODE').val(data['FUNCT_LOC']);
    load_selected_option('#FUNCT_LOC', data['OBJNR'] + " - " + data['FUNCT_LOC'] + " - " + data['MPLANT'] + " - " + data['DESCMPLANT'] + " - " + data['PPLANT'], data['FL_TEXT']);
    $('#FUNCT_LOC').html('<option selected value="' + data['FUNCT_LOC'] + '" title="' + data['FL_TEXT'] + '" data-subtext="' + data['FUNCT_LOC'] + '" data-tplnr="' + data['FUNCT_LOC'] + '" data-pltxt="' + data['FL_TEXT'] + '" data-swerk="' + data['MPLANT'] + '" data-descmplant="' + data['DESCMPLANT'] + '" data-iwerk="' + data['PLANT_SECTION'] + ' >' + data['FL_TEXT'] + '</option>');
  }
  if (data['MPLANT']) {
    $('#SWERK').val(data['MPLANT'] + ' - ' + data['DESCMPLANT']);
  }
  if (data['PLANT_SECTION']) {
    $('#BEBER').html('<option selected value="' + data['PLANT_SECTION'] + '">' + data['PLANT_SECTION'] + ' - ' + data['DESCPSECTION'] + '</option>');
  }
  $('#PLANPLANT').val(data['PPLANT']);
  if (data['PLANNER_GROUP']) {
    $('#PLANGROUP').html('<option selected value="' + data['PLANNER_GROUP'] + '">' + data['PLANNER_GROUP'] + ' - ' + data['DESCPGROUP'] + '</option>');
  }
  $('#LOKASI').val(data['LOKASI']);
  $('#PRIORITY').val(data['PRIORITY']);
  $('#WBS_CODE').val(data['WBS_CODE']);
  $('#WBS_TEXT').val(data['WBS_TEXT']);
  $('#DESSTDATE').val(data['DESSTDATE']);
  $('#DESENDDATE').val(data['DESENDDATE']);
  $('#LATAR_BELAKANG').val(data['LATAR_BELAKANG']);
  $('#CUST_REQ').val(data['CUST_REQ']);
  $('#TECH_INFO').val(data['TECH_INFO']);
  $('#RES_MIT').val(data['RES_MIT']);
  if (data['FILES']) {
    var aFiles = data['FILES'].split('/');

    $('#file_uri').attr('href', data['FILES']);
    $('#file_name').text(' ' + aFiles[(aFiles.length - 1)]);
    $('#file_name').addClass('fa fa-file-pdf-o');
  }

  // $('#btn-save').hide();
  $('#FUNCT_LOC').removeAttr('required');
  $('#ul-new').text('Revision Engineering Request');
  $('.selectpicker').selectpicker('refresh');
  setActiveTab('ul-new', 'new-form');
});

// event save
$("#fm-new").submit(function(e) {
  e.preventDefault();
  $("#btn-save").addClass("disabled");
  // $("#btn-save").val("waiting...");
  if (parseInt($('#REV_NO').val()) > 0) {
    create_request($('#IDMPE').val(), $('#NOTIFIKASI').val(), 'revision');
  } else {
    create_notif_sap();
  }

  return false;
});

//
function create_notif_sap() {
  $("#btn-save").prop("disabled", true);

  var arFuncloc = $('#FUNCT_LOC').val().split(' - ');
  code_funloc = arFuncloc[0];
  // code_funloc = $('#FUNCT_LOC :selected').attr('data-objnr');
  var objnr = code_funloc.split('IF');
  idmpe = $('#IDMPE').val();
  // nopengajuan = $('#NOPENGAJUAN').val();
  short_text = $('#SHORT_TEXT').val();
  swerk = $('#SWERK').val();
  var mplant = swerk.split(' - ');
  beber = $('#BEBER').val();
  planplant = $('#PLANPLANT').val().split(' - ');
  plangroup = $('#PLANGROUP').val();
  funcloc = '';
  funcloc = arFuncloc[1];
  // funcloc = $('#FUNCT_LOC').val();
  // lokasi = $('#LOKASI').val();
  priority = $('#PRIORITY').val();
  // wbs_code = $('#WBS_CODE').val();
  // wbs_text = $('#WBS_TEXT').val();
  desstdate = $('#DESSTDATE').val();
  desenddate = $('#DESENDDATE').val();
  // latar_belakang = $('#LATAR_BELAKANG').val();
  // cust_req = $('#CUST_REQ').val();
  // tech_info = $('#TECH_INFO').val();
  // res_mit = $('#RES_MIT').val();

  // alert(param['tglmulaiuser']);
  // alert(tglmulai);
  // var tglselesai = param['tglselesaiiuser'].split('-');
  var form_data = new FormData();
  // form_data.append('token', localStorage.token_user);
  form_data.append('LOGINSAP', 'BNURFAHRI');
  form_data.append('NOTIFTYPE', '01');
  form_data.append('FUNCT_LOC', objnr[1]);
  form_data.append('SHORT_TEXT', short_text);
  // form_data.append('reporter', localStorage.nama);
  form_data.append('PRIORITY', priority);
  tglmulai = desstdate.split("-");
  // form_data.append('START_DATE', tglmulai[0] + '.' + tglmulai[1] + '.' + tglmulai[2]);
  form_data.append('START_DATE', tglmulai[0] + tglmulai[1] + tglmulai[2]);
  // form_data.append('timestart', param['jammulaiuser']);
  tglselesai = desenddate.split("-");
  // form_data.append('END_DATE', tglselesai[0] + '.' + tglselesai[1] + '.' + tglselesai[2]);
  form_data.append('END_DATE', tglselesai[0] + tglselesai[1] + tglselesai[2]);
  // form_data.append('timeend', param['jamselesaiuser']);
  form_data.append('PLANPLANT', planplant[0]);
  form_data.append('PLANGROUP', plangroup);
  form_data.append('SWERK', mplant[0]);
  form_data.append('BEBER', beber);
  // form_data.append('LATAR_BELAKANG', latar_belakang);
  // form_data.append('CUST_REQ', cust_req);
  // form_data.append('TECH_INFO', tech_info);
  // form_data.append('RES_MIT', res_mit);
  // alert(tglmulai);

  $.ajax({
    url: 'notifikasi/create_notif', // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Data Save : NOT Success! " + error,
        type: "error"
      });
      $("#btn-save").removeClass("disabled");
      // $("#btn-save").val("Save");
    },
    success: function(cekdata) {
      // console.log(cekdata);

      if (cekdata['status'] == 200) {
        if (cekdata['data']['NOTIF_NO'] || cekdata['data']['NOTIF_NO'] != "") {
          create_request(cekdata['data']['ID_SAP'], cekdata['data']['NOTIF_NO']);
          // console.log('success');
        } else {
          swal("Cancelled", "Your Request Cannot process by system", "error");
          // console.log('error');
        }
      } else {
        swal("Cancelled", "Your Request Cannot process by system", "error");
      }

    }
  });
}
//
function create_request(id_mpe, no_notif, stat = 'create_data') {

  // var formData = new FormData(this);
  // idmpe = $('#IDMPE').val();
  var arFuncloc = $('#FUNCT_LOC').val().split(' - ');
  // rev_no = $('#REV_NO').val();
  // kdloc = $('#KDLOC').val();
  // kdarea = $('#KDAREA').val();
  // nopengajuan = $('#NOPENGAJUAN').val();
  notifikasi = $('#NOTIFIKASI').val();
  short_text = $('#SHORT_TEXT').val();
  swerk = $('#SWERK').val();
  var mplant = swerk.split(' - ');
  beber = $('#BEBER').val();
  planplant = $('#PLANPLANT').val();
  plangroup = $('#PLANGROUP').val();
  if (!$('#FL_CODE').val()) {
    funcloc = $('#FUNCT_LOC').val();
  } else {
    funcloc = $('#FL_CODE').val();
  }
  funcloc = arFuncloc[1];
  // lokasi = $('#LOKASI').val();
  priority = $('#PRIORITY').val();
  // wbs_code = $('#WBS_CODE').val();
  // wbs_text = $('#WBS_TEXT').val();
  desstdate = $('#DESSTDATE').val();
  desenddate = $('#DESENDDATE').val();
  // latar_belakang = $('#LATAR_BELAKANG').val();
  // cust_req = $('#CUST_REQ').val();
  // tech_info = $('#TECH_INFO').val();
  // res_mit = $('#RES_MIT').val();
  // jenis = $('#JENIS').val();
  // strpath = document.getElementById('FILES').files[0];
  // strfile = $('#FILES').attr('data-url');

  var formURL = "";

  // var formData = new FormData(this);
  var form_data = new FormData();
  form_data.append('ID_MPE', id_mpe);
  if (stat != 'create') {
    // form_data.append('REV_NO', rev_no);
    form_data.append('NOTIFIKASI', notifikasi);
  }
  // form_data.append('KDLOC', kdloc);
  // form_data.append('KDAREA', kdarea);
  // form_data.append('NO_PENGAJUAN', nopengajuan);
  form_data.append('NOTIFIKASI', no_notif);
  form_data.append('NAMA_PEKERJAAN', short_text);
  form_data.append('MPLANT', mplant[0]);
  form_data.append('PLANT_SECTION', beber);
  form_data.append('PPLANT', planplant);
  form_data.append('PLANNER_GROUP', plangroup);
  form_data.append('FUNCT_LOC', funcloc);
  // form_data.append('LOKASI', lokasi);
  form_data.append('PRIORITY', priority);
  // form_data.append('WBS_CODE', wbs_code);
  // form_data.append('WBS_TEXT', wbs_text);
  form_data.append('DESSTDATE', desstdate);
  form_data.append('DESENDDATE', desenddate);
  // form_data.append('LATAR_BELAKANG', latar_belakang);
  // form_data.append('CUST_REQ', cust_req);
  // form_data.append('TECH_INFO', tech_info);
  // form_data.append('RES_MIT', res_mit);
  // form_data.append('TIPE', jenis);
  // if (strpath) {
  //   form_data.append('STRPATH', strpath);
  // } else {
  //   if (strfile) {
  //     form_data.append('FILES', strfile);
  //   }
  // }
  // console.log(form_data);

  $.ajax({
    url: 'notif/create/' + stat, // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Data Save : NOT Success! " + error,
        type: "error"
      });
      $("#btn-save").removeClass("disabled");
      // $("#btn-save").val("Save");
    },
    success: function(json) {
      console.log(json);
      if (json['status'] == 200) {
        // tb_request();
        tb_erf2();
        swal({
          title: "Data Saved!\n" + no_notif,
          type: "success"
        });
        reset_form('yes');
      }
      $("#btn-save").removeClass("disabled");
      // $("#btn-save").val("Save");
    }
  });
}

// event Update

// event delete
$(document).on('click', ".btnDel", function() {
  var data = dtTable.row($(this).parents('tr')).data();
  swal({
      title: "Are you sure?",
      text: "Data (" + data['NOTIFIKASI'] + ") will be deleted!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Delete",
      confirmButtonClass: "btn-danger",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        url = 'notif/create/delete/' + data['ID_MPE'];
        $.ajax({
          url: url, // point to server-side PHP script
          dataType: 'json', // what to expect back from the PHP script, if anything
          cache: false,
          contentType: false,
          processData: false,
          type: 'post',
          error: function(xhr, status, error) {
            swal("Error", "Your data failed to delete!", "error");
          },
          success: function(json) {
            if (json['status'] == 200) {
              tb_erf2();
              swal("Deleted!", "Your data has been deleted.", "success");
            }
          }
        });
      } else {
        swal("Cancelled", "Deletion of data canceled", "error");
      }
    });
});

function req_getdel(id_mpe, no_form, notif, desc) {
  swal({
      title: "Are you sure?",
      text: "Data (" + no_form + ") will be deleted!",
      type: "warning",
      showCancelButton: true,
      // confirmButtonColor: "#DD6B55",
      confirmButtonText: "Delete",
      confirmButtonClass: "btn-danger",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        url = 'notif/create/delete/' + id_mpe;
        $.ajax({
          url: url, // point to server-side PHP script
          dataType: 'json', // what to expect back from the PHP script, if anything
          cache: false,
          contentType: false,
          processData: false,
          type: 'get',
          error: function(xhr, status, error) {
            swal("Error", "Your data failed to delete!", "error");
          },
          success: function(json) {
            if (json['status'] == 200) {
              // tb_request();
              tb_erf2();
              swal("Deleted!", "Your data has been deleted.", "success");
            }
          }
        });
      } else {
        swal("Cancelled", "Deletion of data canceled", "error");
      }
    });
}

$('#KDLOC').on('change', function() {
  if ($('#FUNCT_LOC').val()) {
    $('#NOPENGAJUAN').val();
  }

  // alert( this.value + " - " + $('option:selected', this).attr('data-swerk'));
})
