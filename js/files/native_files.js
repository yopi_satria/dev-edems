Dropzone.autoDiscover = false;
$(function () {  
    var fileupload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('index.php/upload/proses_upload') ?>",
        maxFilesize: 100,
        method: "post",
//        acceptedFiles: "image/*, application/pdf, application/ppt, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/vnd.openxmlformats-officedocument.presentationml.presentation",
        acceptedFiles: "image/*, application/pdf, .ppt, .pptx, .doc, .docx, .xls, .xlsx",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
    });
});