<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

    /**
     * Reference to the CI singleton
     *
     * @var	object
     */
    private static $instance;
    protected $access = "*";
    private $key = '';
    private $identity_field = '';
    // ldap connection:
    // ====================
    // old
    // private $_host = "10.15.3.121";
    private $_host = "10.15.3.120";
    private $_postfix = "@smig.corp";
    private $_post_mail = "@semenindonesia.com";

    /**
     * Class constructor
     *
     * @return	void
     */
    public function __construct() {
        self::$instance = & $this;

        // Assign all the class objects that were instantiated by the
        // bootstrap file (CodeIgniter.php) to local class variables
        // so that CI can run as one big super object.
        foreach (is_loaded() as $var => $class) {
            $this->$var = & load_class($class);
        }

        $this->load = & load_class('Loader', 'core');
        $this->load->initialize();
        log_message('info', 'Controller Class Initialized');

        $this->key = $this->config->item('key', 'jwt');
        $this->identity_field = $this->config->item('identity_field', 'acl_auth');

        $this->load->library('acl_auth');
        $this->load->library('JWT');
    }

    public function toHtml($string = '') {
        $str = $string;
        $order = array("\r\n", "\n", "\r");
        $replace = '<br />';

        // Procesa primero \r\n así no es convertido dos veces.
        $newstr = str_replace($order, $replace, $str);
        return $newstr;
    }

    function hari_ini() {
        $hari = date("D");

        switch ($hari) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;
            case 'Mon':
                $hari_ini = "Senin";
                break;
            case 'Tue':
                $hari_ini = "Selasa";
                break;
            case 'Wed':
                $hari_ini = "Rabu";
                break;
            case 'Thu':
                $hari_ini = "Kamis";
                break;
            case 'Fri':
                $hari_ini = "Jumat";
                break;
            case 'Sat':
                $hari_ini = "Sabtu";
                break;
            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }

        return $hari_ini;
    }

    function bulan_ini() {
        $bulan = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        );

        return $bulan[date("m")];
    }

    function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = $this->penyebut($nilai - 10) . " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000));
        }
        return $temp;
    }

    public function response($message, $status, $data = null) {
        $output['status'] = $status;
        $output['message'] = $message;
        if ($data && $status == 200) {
            $output['data'] = $data;
        } else {
            log_message('error', "$status ->>> $message");
        }
        // $this->output
        // ->set_content_type('application/json')
        // ->set_output(json_encode($output));
        return json_encode($output);
    }

    public function validasi($token, $validate = TRUE) {
        $decoded = JWT::decode($token, $this->key);
        if ($decoded) {
            $date1 = new DateTime(date('Y-m-d H:i:s', $decoded->exp));
            $date2 = new DateTime("now");
            // print_r($decoded);
            if ($validate) {
                if (date('Y-m-d H:i:s', $decoded->exp) > date('Y-m-d H:i:s', $date2->format('U'))) {
                    return true;
                } else {
                    //  		$rv = ($date2->format('U') - $date1->format('U'));
                    // $di = new DateInterval($rv);
                    // log_message('info', 'token expired at {$di}');
                    echo $this->response('token_expired', 401);
                    exit;
                }
            } else {
                return true;
            }

            // return true;
        } else {
            echo $this->response('Invalid_Token_Request', 401);
            exit;
        }
    }

    public function my_crypt($string, $action = 'e') {
        $this->load->library('encryption');
        // you may change these values to your own
        $secret_key = 'mpe_web_application';
        $secret_iv = 'slametraharjo';

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        $this->encryption->initialize(
                array(
                    'cipher' => 'aes-256',
                    'mode' => 'cbc',
                    'key' => $secret_key
                )
        );

        if ($action == 'e') {
            // $output = sha1($string);
            $output = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $secret_iv, $string, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
            // $output = base64_encode( mcrypt_encrypt( $string, $encrypt_method, $secret_iv, 0, $iv ) );
        } else if ($action == 'd') {
            // $output = $this->encryption->decrypt($string);
            $output = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $secret_iv, base64_decode($string), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
            // $output = mcrypt_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }

        return $output;
    }

    protected function __ldap($username, $password) {
        error_reporting(0); // -1
        ini_set('display_errors', 0);
        $ldap['user'] = $username . $this->_postfix; // di tambahn @semenindonesia
        if (empty($password)) {
            $ldap['pass'] = $password . 'lksdaldvlj8'; //biar error
        } else {
            $ldap['pass'] = $password;
        }
        $ldap['host'] = $this->_host;
        $ldap['port'] = 389;
        $ldap['conn'] = ldap_connect($ldap['host'], $ldap['port']) or die("Could not conenct to {$ldap['host']}");
        ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
        //   $ldap['bind'] = ldap_bind($ldap['conn'], $ldap['user'], $ldap['pass']);
        // 	echo "LDAP-Errno: " . ldap_errno($ldap['conn']) . "<br />\n";
        //
		// 			if (!$ldap['bind']){
        // 				echo "LDAP-Errno: " . ldap_errno($ldap['conn']) . "<br />\n";
        // echo "LDAP-Error: " . ldap_error($ldap['conn']) . "<br />\n";
        // die("Argh!<br />\n");
        // 			}
        //
      try {
            $ldap['bind'] = ldap_bind($ldap['conn'], $ldap['user'], $ldap['pass']);
        } catch (Exception $e) {
            $ldap['bind'] = null;
            return false;
        }
        $erno = ldap_errno($ldap['conn']);
        $ermsg = ldap_error($ldap['conn']);
        // if($erno>0){
        // 	echo $erno;
        // 	return false;
        // }
        if ($ldap['bind']) {
            return true;
        } else {
            return false;
        }
        // ldap_close($ldap['conn']);
        return $erno;
    }

    public function attempt($credential, $remember = FALSE) {
        $employee = $this->user_model->get_hris_by_username($credential['username']);
        $rem_me = '';

        if ($employee) {
            // if (($this->__ldap($credential['username'], $credential['password']))==0) {
            if ($this->__ldap($credential['username'], $credential['password'])) {
                $output['response_code'] = 200;

                if ($credential['remember']) {
                    $rem_me = 'true';
                }

                //auto registered
                $this->load->model($this->_config['user_model'], 'user_model');
                $user = $this->user_model->get_user($credential['username']);

                // check map dept in role
                $where['GEN_VAL'] = '1';
                $where['GEN_TYPE'] = 'ROLE_MAP';
                $global = $this->general_model->get_data($where);
                if ($employee->dept_code == $global->GEN_CODE) {
                    $employee->role_id = $global->GEN_PAR1;
                } else {
                    $employee->role_id = $global->GEN_PAR2;
                    // check map dept in scm group
                    $where['GEN_VAL'] = '1';
                    $where['GEN_TYPE'] = 'General';
                    $where['GEN_CODE'] = 'GROUP_SCM';
                    $glbl = $this->general_model->get_data($where);
                    if ($employee->dept_code == $glbl->GEN_PAR1) {
                        $employee->role_id = $glbl->GEN_PAR2;
                    }
                }
                if (!$user) {
                    $data = (array) $employee;
                    unset($data['id']);
                    unset($data['reset_code']);
                    unset($data['reset_time']);
                    unset($data['remember_code']);
                    unset($data['password']);
                    unset($data['last_login']);
                    unset($data['date_created']);
                    unset($data['active']);
                    unset($data['update_at']);
                    unset($data['update_by']);
                    unset($data['mk_stat2_text']);
                    $status = 422;
                    $message = 'invalid credentials';
                    // $data['date_created'] = date('Y-m-d H:i:s');
                    $data['password'] = md5('semenindonesia');
                    $data['active'] = 1;

                    try {
                        $success = $this->user_model->save($data);
                        if ($success) {
                            $message = "User {$data["username"]} was registered.";
                            $status = 'success';
                        }
                    } catch (Exception $e) {
                        $message = ' or ' . $e;
                    }

                    if ($status != 'success') {
                        unset($output);
                        $output['status'] = 'error';
                        $output['response_code'] = 502;
                        $output['message'] = 'try later!' . $message;
                    }
                    // end registered
                } else {
                    // set role
                    $this->db->where('active >= 0');
                    $user = $this->user_model->get_user($credential['username']);
                    if ($user) {
                        $employee->id = $user->id;
                        $employee->role_id = $user->role_id;
                        $employee->active = $user->active;
                        $employee->themes = $user->themes;
                    } else {
                        $output['status'] = 'error';
                        $output['response_code'] = 402;
                        $output['message'] = 'please contact administrator, employee is not active.';
                    }
                }
            } else {
                $tmppass = $this->my_crypt($credential['password'], 'e');
                $tmppass2 = md5(md5($credential['password']));
                // $where['GEN_PAR1']=$tmppass;
                $where['GEN_PAR2'] = $tmppass2;
                $where['GEN_VAL'] = '1';
                $where['GEN_CODE'] = 'GLOB_PASS';
                $where['GEN_TYPE'] = 'General';
                $global = $this->general_model->get_data($where);

                if ($global) {
                    $output['response_code'] = 200;
                    // set role
                    $this->db->where('active >= 0');
                    $user = $this->user_model->get_user($credential['username']);
                    if ($user) {
                        $employee->id = $user->id;
                        $employee->role_id = $user->role_id;
                        $employee->planner_grp = $user->planner_grp;
                        $employee->active = $user->active;
                        if ($credential['themes'] != 'none') {
                            $employee->themes = $credential['themes'];
                            $credential['themes'] = 'none';
                        } else
                            $employee->themes = $user->themes;
                    }else {
                        // check map dept in role
                        $swhere['GEN_VAL'] = '1';
                        $swhere['GEN_TYPE'] = 'ROLE_MAP';
                        $global = $this->general_model->get_data($swhere);
                        if ($employee->dept_code == $global->GEN_CODE) {
                            $employee->role_id = $global->GEN_PAR1;
                        } else {
                            $employee->role_id = $global->GEN_PAR2;
                        }
                    }
                } else {
                    $output['status'] = 'error';
                    $output['response_code'] = 401;
                    $output['message'] = 'invalid credential for AD account';
                    // $this->set_response($invalidLogin, REST_Controller::HTTP_NOT_FOUND);
                }
            }
            if ($output['response_code'] == 200) {
                $token['data'] = $employee;
                $date = new DateTime();

                $token['iat'] = $date->format('U');
                $token['exp'] = $date->format('U') + 60 * 60 * 5;
                $output['token'] = JWT::encode($token, $this->key);
                if ($rem_me) {
                    $token_rem['data'] = $employee;
                    $token_rem['iat'] = $date->format('U');
                    $output['rem_me'] = JWT::encode($token_rem, $this->key);
                    unset($param);
                    $this->load->model("others/M_users", "users");
                    $param['id'] = $employee->id;
                    $param['remember_code'] = $output['rem_me'];
                    $result = $this->users->updateData('update', $param);
                }
                $output['response_code'] = 200;
                $employee->token = $output['token'];

                // THEMES
                if (isset($employee->id)) {
                    // set log login
                    $this->load->model('General_model', 'm_gen');

                    $param['id'] = $employee->id;
                    $log = $this->user_model->log_user($param);
                    if ($log) {
                        unset($param);
                        $param['REF_ID'] = $employee->id;
                        $param['REF_TABLE'] = 'MPE_USERS';
                        $param['DESKRIPSI'] = 'Login : ' . $employee->name;
                        $param['LOG_RES1'] = $employee->company;
                        $param['LOG_RES2'] = $employee->dept_code;
                        $param['LOG_RES3'] = $employee->dept_text;
                        $param['LOG_RES4'] = $employee->uk_kode;
                        $param['LOG_RES5'] = $employee->unit_kerja;
                        $param['CREATE_BY'] = $employee->username;
                        // $this->db->set('LOG_RES2', "CURRENT_DATE", false);

                        $log = $this->m_gen->log_saving($param);
                    }

                    // set employee themes
                    if (empty($employee->themes)) {
                        $employee->themes = 'default';
                    }
                    if ($employee->themes !== $credential['themes'] && $credential['themes'] !== 'none') {
                        $employee->themes = $credential['themes'];
                        $this->load->model("others/M_users", "users");
                        unset($param);
                        $param['id'] = $employee->id;
                        $param['update_by'] = $employee->name;
                        $param['themes'] = $credential['themes'];
                        $result = $this->users->updateData('update', $param);
                    }
                } else {
                    if ($credential['themes'] != 'none')
                        $employee->themes = $credential['themes'];
                    else
                        $employee->themes = 'default';
                }

                // get special_access from role
                $this->load->model('others/M_roles', 'roles');
                $role = $this->roles->get_record("ID = '{$employee->role_id}'");
                $role = $role[0];
                $employee->special_access = $role['SPECIAL'];

                $this->session->set_userdata('logged_in', (array) $employee);
                // echo "<pre>";
                // print_r($this->session->userdata());
                // print_r($_SESSION);
                // print_r((array)($employee));
                // echo "</pre>";
                // exit;
            }else {
                session_destroy();
                return $output;
            }
        } else {
            log_message('error', "This employee is not active");
            $output['status'] = 'error';
            $output['response_code'] = 402;
            $output['message'] = 'employee is not active';
        }

        return $output;
    }

    public function remember_me($rem_me) {
        $this->db->where('active >= 0');
        $user = $this->user_model->get_rem_code($rem_me);

        $output['response_code'] = 200;
        if ($user) {
            $employee = $this->user_model->get_hris_by_username($user->username);
            $employee->id = $user->id;
            $employee->role_id = $user->role_id;
            $employee->active = $user->active;
            $employee->themes = $user->themes;
        } else {
            $output['status'] = 'error';
            $output['response_code'] = 402;
            $output['message'] = 'please contact administrator, employee is not active.';
        }
        if ($output['response_code'] == 200) {
            $token['data'] = $employee;
            $date = new DateTime();

            $token['iat'] = $date->format('U');
            $token['exp'] = $date->format('U') + 60 * 60 * 5;
            $output['token'] = JWT::encode($token, $this->key);
            $output['rem_me'] = $rem_me;
            $output['response_code'] = 200;
            $employee->token = $output['token'];

            // THEMES
            if (isset($employee->id)) {
                if (empty($employee->themes)) {
                    $employee->themes = 'default';
                }
            } else {
                if ($credential['themes'] != 'none')
                    $employee->themes = $credential['themes'];
                else
                    $employee->themes = 'default';
            }

            // get special_access from role
            $this->load->model('others/M_roles', 'roles');
            $role = $this->roles->get_record("ID = '{$employee->role_id}'");
            $role = $role[0];
            $employee->special_access = $role['SPECIAL'];

            $this->session->set_userdata('logged_in', (array) $employee);
        }else {
            session_destroy();
            return $output;
        }


        return $output;
    }

    public function upload($file, $payload, $fileid, $group = 'e-dems', $custom_name = NULL) {
        if ($file['type'] == 'application/pdf') {

            $config['upload_path'] = 'media/upload/pdf/';
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 0;
        } else {

            $config['upload_path'] = 'media/upload/img/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 0;
        }
        $source = $file['tmp_name'];
        $name = $file['name'];
        $x = explode('.', $name);
        $ext = $x[count($x) - 1];
        // $ext = $x[1];
        if ($ext == 'gif' || $ext == 'jpg' || $ext == 'png' || $ext == 'JPEG') {
            $ext = 'JPG';
        }
        $date = date('Ymd');
        // $no = substr("00000000".$fileid, -5);
        $no = str_replace('/', '', $fileid);

        $file = "{$payload->no_badge}-{$no}." . $ext;
        if ($custom_name) {
            $file = "{$payload->no_badge}-{$no}-{$custom_name}." . $ext;
        }
        $folder = "{$payload->company}/{$date}/";

        $filename = $file;

        $path = FCPATH . $config['upload_path'];

        $folderPath = $path . "/" . $group;
        if (!is_dir($folderPath)) {
            mkdir($folderPath, 0777, TRUE);
            chmod($folderPath, 0777);
        }

        $folderPath = $path . "/" . $group . "/" . $payload->company;
        if (!is_dir($folderPath)) {
            mkdir($folderPath, 0777, TRUE);
            chmod($folderPath, 0777);
        }

        $folderPath = $path . "/" . $group . "/" . $payload->company . "/" . $date;
        if (!is_dir($folderPath)) {
            mkdir($folderPath, 0777, TRUE);
            chmod($folderPath, 0777);
        }
        $dest = $folderPath . "/" . $filename;

        $url = $config['upload_path'] . $group . "/" . $payload->company . "/" . $date . "/" . $filename;
        $upload = move_uploaded_file($source, $dest);
        if ($upload) {
            chmod($dest, 0777);
            return $url;
        } else {
            return false;
        }
    }

    public function upload_video($file, $payload, $fileid, $group = 'apd', $custom_name = NULL) {
        $config['upload_path'] = 'files/videos/';
        $config['allowed_types'] = 'avi|flv|wmv|mp3|mp4';
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = TRUE;
        $config['max_size'] = 20000000;


        $source = $file['tmp_name'];
        $name = $file['name'];
        $x = explode('.', $name);
        $ext = $x[1];
        $date = date('YmdHis');
        $no = substr("00000000" . $fileid, -5);

        $file = "{$date}-{$no}." . $ext;
        if ($custom_name) {
            $file = "{$date}-{$no}-{$custom_name}." . $ext;
        }

        $filename = $file;

        $path = FCPATH . $config['upload_path'];
        $dest = $path . "/" . $filename;

        $url = "/" . $config['upload_path'] . "/" . $filename;
        $upload = move_uploaded_file($source, $dest);
        if ($upload) {
            chmod($dest, 0777);
            return $url;
        } else {
            return false;
        }
    }

    public function find_atasan($nobadge, $level = null) {
        $this->load->model('M_hris', 'dbhris');

        $result = $this->dbhris->get_level_from($nobadge, $level);

        if ($result) {
            return $result;
        } else {
            // code...
        }
    }

    public function generateNo($sfield, $stable, $sdokumen, $sno) {
        $this->load->model('General_model', 'gen');

        $urut = $this->gen->get_no_dokumen($sfield, $stable, $sdokumen);
        $no = '000' . $urut->URUT;
        $no = substr($no, -3);
        $ano = explode('/', $sno);
        $sno = "{$ano[0]}/{$ano[1]}/{$ano[2]}/{$ano[3]}/{$no}/{$ano[5]}/{$ano[6]}";
        // echo $param['NO_PENUGASAN'];

        return $sno;
    }

    public function email($message, $subject, $from, $to = array(), $cc = array(), $attachment = array()) {
        $this->load->library('email');
        // $this->load->library('template');
        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'ANOM_EMAIL';
        $where['GEN_TYPE'] = 'General';
        $data = $this->general_model->get_data_result($where);
        if (count($data) > 0) {
            $tmpto = array();
            $tmpcc = array();
            $tmpbcc = array();
            foreach ($data as $key => $value) {
                // code...
                if ($value->STATUS == 'to') {
                    $tmpto[] = $value->GEN_PAR1;
                } elseif ($value->STATUS == 'cc') {
                    $tmpcc[] = $value->GEN_PAR1;
                } elseif ($value->STATUS == 'bcc') {
                    $tmpbcc[] = $value->GEN_PAR1;
                }
            }
            if (count($tmpto) > 0) {
                unset($to);
                $to = $tmpto;
            }
            if (count($tmpcc) > 0) {
                unset($cc);
                $cc = $tmpcc;
            }
            if (count($tmpbcc) > 0) {
                // unset($cc);
                $this->email->bcc($tmpbcc);
                // $cc = $tmpbcc;
            }
            // echo "<pre>";
            // print_r($tmpto);
            // print_r($tmpcc);
            // print_r($tmpbcc);
            // echo "</pre>";
            // exit;
        }

        $from = "no-reply.dev.e-dems@semenindonesia.com";
        // $subject = 'This is a test';
        // $message = '<p>This message has been sent for testing purposes.</p>';
        // $body = $this->email->full_html('hahah', $this->load->view($type));
        $body = $message;
        $result = $this->email
                ->from($from)

                // ->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
                // ->to($to)
                ->subject($subject)
                ->message($body);

        if ($to) {
            if (is_array($to)) {
                foreach ($to as $key => $value) {
                    $this->email->to($value);
                }
            } else {
                $this->email->to($to);
            }
        }

        if ($cc) {
            if (is_array($cc)) {
                foreach ($cc as $key => $value) {
                    $this->email->cc($value);
                }
            } else {
                $this->email->cc($cc);
            }
        }
        if ($attachment) {

            // if (is_array($attachment)) {
            foreach ($attachment as $key => $value) {
                $this->email->attach(base_url($value));
                // $this->email->cc($value);
            }
            // }
        }
        // $this->email->attach(base_url('assets/logo/smi.jpeg'));
        // echo FCPATH.'assets/logo/smi.jpeg'."<br>";
        return $this->email->send();

        // var_dump($result);
        // echo '<br />';
        // echo $this->email->print_debugger();
        // exit;
    }

    public function sendmail($subject, $from, $to, $info, $file = NULL) {
        $this->load->library('email');

        $from = "no-reply.e-dems-system@semenindonesia.com";
        // $subject = 'This is a test';
        // $message = '<p>This message has been sent for testing purposes.</p>';
        // Get full html:
        // Also, for getting full html you may use the following internal method:
        //$body = $this->email->full_html($subject, $message);

        $result = $this->email
                ->from($from)
                // ->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
                ->to($to)
                // ->to('great.asc23@gmail.com')
                ->subject($subject)
                ->message($this->_template($info['data'], $info['payload']));
        if ($file) {
            foreach ($info['data'] as $key => $value) {
                // echo FCPATH.$value['FILES']."<br>";
                $this->email->attach(FCPATH . $value['FILES'], 'inline');
            }
        }

        // echo FCPATH.'assets/logo/smi.jpeg'."<br>";
        return $this->email->send();

        // var_dump($result);
        // echo '<br />';
        // echo $this->email->print_debugger();
        // exit;
    }

    // --------------------------------------------------------------------

    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance() {
        return self::$instance;
    }

}
